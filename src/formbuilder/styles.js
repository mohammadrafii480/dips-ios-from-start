export const color = {
  WHITE: '#FFFFFF',
  BLACK: '#000000',
  GREY: '#DADADA',
  YELLOW: '#FFDE2F',
};

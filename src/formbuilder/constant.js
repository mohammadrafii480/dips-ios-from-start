export const componentName = {
  CHECKBOX: 'Checkbox',
  DATE_PICKER: 'Datepicker',
  DROPDOWN: 'Dropdown',
  DATALIST: 'Datalist',
  DROPDOWNSUMBERDANA: 'DropdownSumberDana',
  PROFILRESIKO: 'ProfilResiko',
  TEXT_AREA: 'TextArea',
  TEXT_INPUT: 'AutoComplete',
  CURRENCY: 'Currency',
  RADIO: 'RadioButton',
  RATING: 'rating',
  READ_ONLY_TEXT: 'read-only-text',
  IMAGE_WITH_LINK: 'image-with-link',
  IMAGE: 'image',
};

export const radioButton = {
  selected: require('./assets/selectedRadio.png'),
  unselected: require('./assets/unselectedRadio.png'),
};

// TODO: Need to remove isMandatory for below fields from template json.
export const skipValidationForFields = [
  'image',
  'image-with-link',
  'read-only-text',
];

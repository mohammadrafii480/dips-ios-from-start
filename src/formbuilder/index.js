import React, {useEffect, useState} from 'react';
import {View, StyleSheet, Text, Alert} from 'react-native';
import PropTypes from 'prop-types';
import {Button} from 'react-native-paper';
import {componentName, skipValidationForFields} from './constant';
import {getComponent, getValidator} from './componentMap';
import {RadioButton} from 'react-native-paper';
import {set} from 'react-native-reanimated';
import {Server} from '../api/server';
import {ApiService} from '../api/ApiService';

export default function DynamicForm({
  formTemplate,
  onSubmit,
  btnStyle,
  btnLabel,
  accessToken,
  exchangeToken,
  onPressed,
  onProfilResikoPressed,
  statusProfilResiko,
  defaultValue,
  bahasa,
  arrayRedaksi,
  redaksiTextStyle,
}) {
  const [idProvinsi, setIdProvinsi] = React.useState('');
  const [idKabupaten, setIdKabupaten] = React.useState('');
  const [idKecamatan, setIdKecamatan] = React.useState('');
  const [Provinsi, setProvinsi] = React.useState('');
  const [Kabupaten, setKabupaten] = React.useState('');
  const [Kecamatan, setKecamatan] = React.useState('');
  const [KodePos, setKodePos] = React.useState('');
  const [formFields, setFormFields] = useState({});
  const [listKabupatenKota, setlistKabupatenKota] = useState({
    listdd: {
      placeholder:
        bahasa === 'id' ? '== Silakan Pilih ==' : '== Please Choose ==',
      list: [],
    },
    label: bahasa === 'id' ? 'Kabupaten/Kota' : 'District',
  });
  const [listKecamatan, setlistKecamatan] = useState({
    listdd: {
      placeholder:
        bahasa === 'id' ? '== Silakan Pilih ==' : '== Please Choose ==',
      list: [],
    },
    label: bahasa === 'id' ? 'Kecamatan' : 'Sub-district',
  });
  const [listKelurahan, setlistKelurahan] = useState({
    listdd: {
      placeholder:
        bahasa === 'id' ? '== Silakan Pilih ==' : '== Please Choose ==',
      list: [],
    },
    label: bahasa === 'id' ? 'Kelurahan/Desa' : 'Village',
  });
  const [checked, setChecked] = React.useState('');
  const [checkedGender, setCheckedGender] = React.useState('');
  const [checkedReligion, setCheckedReligion] = React.useState('');
  const [checkedKwrgn, setCheckedKwrgn] = React.useState('');
  const [checkedJenisidentitas, setCheckedJenisIdentitas] = React.useState('');
  const [checkedstsMenikah, setCheckedstsMenikah] = React.useState('');
  const [checkedjmltanggungan, setCheckedjmltanggungan] = React.useState('');
  const [checkedstsrumah, setCheckedstsrumah] = React.useState('');
  console.log('bahasa', bahasa);
  useEffect(() => {
    Object.keys(defaultValue).length !== 0
      ? Object.keys(formFields).length === 0
        ? setFormFields(defaultValue)
        : formFields.hasOwnProperty('jenisform')
        ? setFormFields(defaultValue)
        : null
      : null;
  }, [defaultValue]);

  useEffect(() => {
    if (defaultValue.hasOwnProperty('jenisidentitas')) {
      setCheckedJenisIdentitas(defaultValue.jenisidentitas);
    }
    if (defaultValue.hasOwnProperty('jeniskelamin')) {
      setCheckedGender(defaultValue.jeniskelamin.trim());
    }
    if (defaultValue.hasOwnProperty('statusmenikah')) {
      setCheckedstsMenikah(defaultValue.statusmenikah.trim());
    }
    if (defaultValue.hasOwnProperty('agama')) {
      setCheckedReligion(defaultValue.agama.trim());
    }
    if (defaultValue.hasOwnProperty('kewarganegaraan')) {
      setCheckedKwrgn(defaultValue.kewarganegaraan.trim());
    }
    if (defaultValue.hasOwnProperty('jenispenduduk')) {
      setChecked(defaultValue.jenispenduduk.trim());
    }
    if (defaultValue.hasOwnProperty('jenisdeposito')) {
      setChecked(defaultValue.jenisdeposito.trim());
    }
  }, [defaultValue]);

  const onChangeInputValue = fieldName => value => {
    setFormFields({
      ...formFields,
      [fieldName]: value,
    });
  };

  const onChangeInputValue2 = (fieldName, value) => {
    setFormFields({
      ...formFields,
      [fieldName]: value,
    });
  };
  const onRadioChangeInputValue = (fieldName, value) => {
    setFormFields({
      ...formFields,
      [fieldName]: value,
    });
  };

  const getValue = element => formFields[element.field_name]?.value;

  const onSubmitButtonPress = () => {
    onSubmit(formFields);
  };
  const getDropdownKab = url => {
    var dataItemList = [];
    fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
    })
      .then(response => response.json())
      .then(responseJson => {
        responseJson.data.map(element => {
          if (responseJson.code < 300) {
            placeholder = responseJson.data[0];
            dataItemList.push({
              id: element.id !== undefined ? element.id : '',
              label: bahasa === 'id' ? element.labelIdn : element.labelEng,
              value: bahasa === 'id' ? element.labelIdn : element.labelEng,
            });
          }
        });
        setlistKabupatenKota({
          listdd: {
            placeholder:
              bahasa === 'id' ? '== Silakan Pilih ==' : '== Please Choose ==',
            list: dataItemList,
          },
          label: bahasa === 'id' ? 'Kabupaten/Kota' : 'District',
        });
      })
      .catch(error => {
        Alert.alert('Gagal Terhubung ke server', error);
      });
  };
  const getDropdownKec = url => {
    var dataItemList = [];
    fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
    })
      .then(response => response.json())
      .then(responseJson => {
        responseJson.data.map(element => {
          if (responseJson.code < 300) {
            placeholder = responseJson.data[0];
            dataItemList.push({
              id: element.id !== undefined ? element.id : '',
              label: bahasa === 'id' ? element.labelIdn : element.labelEng,
              value: bahasa === 'id' ? element.labelIdn : element.labelEng,
            });
          }
        });
        setlistKecamatan({
          listdd: {
            placeholder:
              bahasa === 'id' ? '== Silakan Pilih ==' : '== Please Choose ==',
            list: dataItemList,
          },
          label: bahasa === 'id' ? 'Kecamatan' : 'Sub-district',
        });
      })
      .catch(error => {
        Alert.alert('Gagal Terhubung ke server', error);
      });
  };
  const getDropdownKel = url => {
    var dataItemList = [];
    fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
    })
      .then(response => response.json())
      .then(responseJson => {
        responseJson.data.map(element => {
          if (responseJson.code < 300) {
            placeholder = responseJson.data[0];
            dataItemList.push({
              id: element.id !== undefined ? element.id : '',
              label: bahasa === 'id' ? element.labelIdn : element.labelEng,
              value: bahasa === 'id' ? element.labelIdn : element.labelEng,
            });
          }
        });
        setlistKelurahan({
          listdd: {
            placeholder:
              bahasa === 'id' ? '== Silakan Pilih ==' : '== Please Choose ==',
            list: dataItemList,
          },
          label: bahasa === 'id' ? 'Kelurahan/Desa' : 'Village',
        });
      })
      .catch(error => {
        Alert.alert('Gagal Terhubung ke server', error);
      });
  };
  const getDropdownKodePos = Kel => {
    var payload = {
      kelurahan: Kel,
      kecamatan: Kecamatan,
      kabupaten: Kabupaten,
      provinsi: Provinsi,
    };
    fetch(Server.BASE_URL_API + ApiService.kodePos, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: JSON.stringify(payload),
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.code < 300) {
          console.log('Kode Pos', responseJson.data.kodepos);
          setKodePos(responseJson.data.kodepos);
        }
      })
      .catch(error => {
        Alert.alert('Gagal Terhubung ke server', error);
      });
  };

  return (
    <View style={styles.container}>
      {formTemplate &&
        formTemplate.data.map((element, key) => {
          const Component = getComponent(element?.component);
          var point =
            element?.meta?.pointer !== undefined
              ? element?.meta?.pointer.toLowerCase().trim()
              : '';
          var point2 = point.includes('(wajib diisi)')
            ? point
                .replace('(wajib diisi)', '')
                .trim()
                .replace(/\s/g, '')
                .replace('.', '')
            : point.replace(/\s/g, '').replace('.', '');

          if (element?.component !== 'RadioButton') {
            if (element?.component !== 'Checkbox') {
              if (element?.meta?.pointer === 'Provinsi') {
                return (
                  Component && (
                    <Component
                      key={key}
                      index={key}
                      name={element.field_name}
                      meta={element.meta}
                      type={element.type}
                      style={element.style}
                      pointer={element.meta.pointer}
                      onChangeInputValue={value => {
                        setlistKabupatenKota({
                          listdd: {
                            placeholder:
                              bahasa === 'id'
                                ? '== Silakan Pilih =='
                                : '== Please Choose ==',
                            list: [],
                          },
                          label:
                            bahasa === 'id' ? 'Kabupaten/Kota' : 'District',
                        });
                        setlistKecamatan({
                          listdd: {
                            placeholder:
                              bahasa === 'id'
                                ? '== Silakan Pilih =='
                                : '== Please Choose ==',
                            list: [],
                          },
                          label: bahasa === 'id' ? 'Kecamatan' : 'Sub-district',
                        });
                        setlistKelurahan({
                          listdd: {
                            placeholder:
                              bahasa === 'id'
                                ? '== Silakan Pilih =='
                                : '== Please Choose ==',
                            list: [],
                          },
                          label: bahasa === 'id' ? 'Kelurahan/Desa' : 'Village',
                        });
                        onChangeInputValue2(point2, value);
                        if (value !== '') {
                          setIdProvinsi(value.id);
                          setProvinsi(value.value);
                          getDropdownKab(
                            `https://diops.victoriabank.co.id/gwnode/api/wilayah/${value.id}`,
                          );
                        }
                      }}
                      isMandatory={element.is_mandatory === 'true'}
                    />
                  )
                );
              } else if (element?.meta?.pointer === 'Kabupaten/Kota') {
                return (
                  Component && (
                    <Component
                      key={key}
                      index={key}
                      name={element.field_name}
                      meta={listKabupatenKota}
                      type={element.type}
                      pointer={element.meta.pointer}
                      style={element.style}
                      onChangeInputValue={value => {
                        setlistKecamatan({
                          listdd: {
                            placeholder:
                              bahasa === 'id'
                                ? '== Silakan Pilih =='
                                : '== Please Choose ==',
                            list: [],
                          },
                          label: bahasa === 'id' ? 'Kecamatan' : 'Sub-district',
                        });
                        setlistKelurahan({
                          listdd: {
                            placeholder:
                              bahasa === 'id'
                                ? '== Silakan Pilih =='
                                : '== Please Choose ==',
                            list: [],
                          },
                          label: bahasa === 'id' ? 'Kelurahan/Desa' : 'Village',
                        });
                        onChangeInputValue2(point2, value);
                        if (value !== '') {
                          setIdKabupaten(value.id);
                          setKabupaten(value.value);
                          getDropdownKec(
                            `https://diops.victoriabank.co.id/gwnode/api/wilayah/${idProvinsi}/${value.id}`,
                          );
                        }
                      }}
                      isMandatory={element.is_mandatory === 'true'}
                    />
                  )
                );
              } else if (element?.meta?.pointer === 'Kecamatan') {
                return (
                  Component && (
                    <Component
                      key={key}
                      index={key}
                      name={element.field_name}
                      meta={listKecamatan}
                      type={element.type}
                      style={element.style}
                      pointer={element.meta.pointer}
                      onChangeInputValue={value => {
                        setlistKelurahan({
                          listdd: {
                            placeholder:
                              bahasa === 'id'
                                ? '== Silakan Pilih =='
                                : '== Please Choose ==',
                            list: [],
                          },
                          label: bahasa === 'id' ? 'Kelurahan/Desa' : 'Village',
                        });
                        onChangeInputValue2(point2, value);
                        if (value !== '') {
                          setIdKecamatan(value.id);
                          setKecamatan(value.value);
                          getDropdownKel(
                            `https://diops.victoriabank.co.id/gwnode/api/wilayah/${idProvinsi}/${idKabupaten}/${value.id}`,
                          );
                        }
                      }}
                      isMandatory={element.is_mandatory === 'true'}
                    />
                  )
                );
              } else if (element?.meta?.pointer === 'Kelurahan/Desa') {
                return (
                  Component && (
                    <Component
                      key={key}
                      index={key}
                      name={element.field_name}
                      meta={listKelurahan}
                      type={element.type}
                      style={element.style}
                      pointer={element.meta.pointer}
                      onChangeInputValue={value => {
                        onChangeInputValue2(point2, value);
                        if (value !== '') {
                          getDropdownKodePos(value.value);
                        }
                      }}
                      isMandatory={element.is_mandatory === 'true'}
                    />
                  )
                );
              } else if (element?.meta?.pointer === 'Kode Pos') {
                return (
                  Component && (
                    <Component
                      key={key}
                      index={key}
                      name={element.field_name}
                      meta={element.meta}
                      type={element.type}
                      editable={false}
                      style={element.style}
                      pointer={element.meta.pointer}
                      defaultValue={{
                        kodepos: defaultValue.hasOwnProperty('kodepos')
                          ? defaultValue.kodepos
                          : KodePos,
                      }}
                      // onChangeInputValue={onChangeInputValue(
                      //   element.field_name
                      //     .toLowerCase()
                      //     .trim()
                      //     .replace(/\s/g, '_'),
                      //   element.component,
                      // )}
                      onChangeInputValue={onChangeInputValue(point2)}
                      isMandatory={element.is_mandatory === 'true'}
                    />
                  )
                );
              } else {
                return (
                  Component && (
                    <Component
                      key={key}
                      index={key}
                      name={element.field_name}
                      meta={element.meta}
                      type={element.type}
                      style={element.style}
                      editable={point2 === 'nomorcif' ? false : true}
                      defaultValue={defaultValue}
                      pointer={element.meta.pointer}
                      value={getValue(element)}
                      bahasa={bahasa}
                      redaksiTextStyle={redaksiTextStyle}
                      onProfilResikoPressed={onProfilResikoPressed}
                      arrayRedaksi={arrayRedaksi}
                      statusProfilResiko={statusProfilResiko}
                      onChangeInputValue={onChangeInputValue(point2)}
                      isMandatory={element.is_mandatory === 'true'}
                    />
                  )
                );
              }
            } else {
              return (
                Component && (
                  <Component
                    key={key}
                    index={key}
                    name={element.field_name}
                    meta={element.meta}
                    type={element.type}
                    style={element.style}
                    bahasa={bahasa}
                    onProfilResikoPressed={onProfilResikoPressed}
                    statusProfilResiko={statusProfilResiko}
                    onChangeInputValue={onChangeInputValue(point2)}
                    isMandatory={element?.is_mandatory === 'true'}
                  />
                )
              );
            }
          } else {
            if (element.meta.pointer === 'Jenis Kelamin (Wajib diisi)') {
              return (
                <RadioButton.Group
                  key={key}
                  onValueChange={value => {
                    onRadioChangeInputValue(point2, value);
                    setCheckedGender(value);
                  }}>
                  <View>
                    <Text
                      style={{
                        fontFamily: 'Helvetica',
                        fontSize: 16,
                        marginLeft: 10,
                        marginTop: 10,
                        color: '#54575C',
                        display: element.meta.label === '' ? 'none' : 'flex',
                      }}>{`${element.meta.label}`}</Text>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                      <RadioButton.Android
                        value={element.meta.placeholder}
                        color="#E34243"
                        uncheckedColor="black"
                        status={
                          checkedGender ===
                          element.meta.placeholder
                            .toUpperCase()
                            .trim()
                            .replace(/\s/g, '')
                            ? 'checked'
                            : 'unchecked'
                        }
                      />
                      <Text style={{fontFamily: 'Helvetica', fontSize: 16}}>
                        {element.meta.placeholder}
                      </Text>
                    </View>
                  </View>
                </RadioButton.Group>
              );
            } else if (element.meta.pointer === 'Agama (Wajib diisi)') {
              return (
                <RadioButton.Group
                  key={key}
                  onValueChange={value => {
                    onRadioChangeInputValue(point2, value);
                    setCheckedReligion(value);
                  }}>
                  <View>
                    <Text
                      style={{
                        fontFamily: 'Helvetica',
                        fontSize: 16,
                        marginLeft: 10,
                        color: '#54575C',
                        marginTop: 10,
                        display: element.meta.label === '' ? 'none' : 'flex',
                      }}>{`${element.meta.label}`}</Text>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                      <RadioButton.Android
                        value={element.meta.placeholder}
                        color="#E34243"
                        uncheckedColor="black"
                        status={
                          checkedReligion ===
                          element.meta.placeholder
                            .toUpperCase()
                            .trim()
                            .replace(/\s/g, '')
                            ? 'checked'
                            : 'unchecked'
                        }
                      />
                      <Text>{element.meta.placeholder}</Text>
                    </View>
                  </View>
                </RadioButton.Group>
              );
            } else if (
              element.meta.pointer === 'Kewarganegaraan (Wajib diisi)'
            ) {
              return (
                <RadioButton.Group
                  key={key}
                  onValueChange={value => {
                    onRadioChangeInputValue(point2, value);
                    setCheckedKwrgn(value);
                  }}>
                  <View>
                    <Text
                      style={{
                        fontFamily: 'Helvetica',
                        fontSize: 16,
                        marginLeft: 10,
                        marginTop: 10,
                        color: '#54575C',
                        display: element.meta.label === '' ? 'none' : 'flex',
                      }}>{`${element.meta.label}`}</Text>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                      <RadioButton.Android
                        value={element.meta.placeholder}
                        color="#E34243"
                        uncheckedColor="black"
                        status={
                          checkedKwrgn ===
                          element.meta.placeholder
                            .toUpperCase()
                            .trim()
                            .replace(/\s/g, '')
                            ? 'checked'
                            : 'unchecked'
                        }
                      />
                      <Text>{element.meta.placeholder}</Text>
                    </View>
                  </View>
                </RadioButton.Group>
              );
            } else if (
              element.meta.pointer === 'Jenis Identitas (Wajib diisi)'
            ) {
              return (
                <RadioButton.Group
                  key={key}
                  onValueChange={value => {
                    onRadioChangeInputValue(point2, value);
                    setCheckedJenisIdentitas(value);
                  }}>
                  <View>
                    <Text
                      style={{
                        fontFamily: 'Helvetica',
                        fontSize: 16,
                        marginLeft: 10,
                        marginTop: 10,
                        color: '#54575C',
                        display: element.meta.label === '' ? 'none' : 'flex',
                      }}>{`${element.meta.label}`}</Text>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                      <RadioButton.Android
                        value={element.meta.placeholder}
                        color="#E34243"
                        uncheckedColor="black"
                        status={
                          checkedJenisidentitas ===
                          element.meta.placeholder
                            .toUpperCase()
                            .trim()
                            .replace(/\s/g, '')
                            ? 'checked'
                            : 'unchecked'
                        }
                      />
                      <Text>{element.meta.placeholder}</Text>
                    </View>
                  </View>
                </RadioButton.Group>
              );
            } else if (
              element.meta.pointer === 'Status Menikah (Wajib diisi)'
            ) {
              return (
                <RadioButton.Group
                  key={key}
                  onValueChange={value => {
                    onRadioChangeInputValue(point2, value);
                    setCheckedstsMenikah(value);
                  }}>
                  <View>
                    <Text
                      style={{
                        fontFamily: 'Helvetica',
                        fontSize: 16,
                        marginLeft: 10,
                        marginTop: 10,
                        color: '#54575C',
                        display: element.meta.label === '' ? 'none' : 'flex',
                      }}>{`${element.meta.label}`}</Text>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                      <RadioButton.Android
                        value={element.meta.placeholder}
                        color="#E34243"
                        uncheckedColor="black"
                        status={
                          checkedstsMenikah ===
                          element.meta.placeholder.toUpperCase().trim()
                            ? 'checked'
                            : 'unchecked'
                        }
                      />
                      <Text>{element.meta.placeholder}</Text>
                    </View>
                  </View>
                </RadioButton.Group>
              );
            } else if (
              element.meta.pointer === 'Jumlah Tanggungan (Wajib diisi)'
            ) {
              return (
                <RadioButton.Group
                  key={key}
                  onValueChange={value => {
                    onRadioChangeInputValue(point2, value);
                    setCheckedjmltanggungan(value);
                  }}>
                  <View>
                    <Text
                      style={{
                        fontFamily: 'Helvetica',
                        fontSize: 16,
                        marginLeft: 10,
                        marginTop: 10,
                        color: '#54575C',
                        display: element.meta.label === '' ? 'none' : 'flex',
                      }}>{`${element.meta.label}`}</Text>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                      <RadioButton.Android
                        value={element.meta.placeholder}
                        color="#E34243"
                        uncheckedColor="black"
                        status={
                          checkedjmltanggungan === element.meta.placeholder
                            ? 'checked'
                            : 'unchecked'
                        }
                      />
                      <Text>{element.meta.placeholder}</Text>
                    </View>
                  </View>
                </RadioButton.Group>
              );
            } else if (element.meta.pointer === 'Status Rumah (Wajib diisi)') {
              return (
                <RadioButton.Group
                  key={key}
                  onValueChange={value => {
                    onRadioChangeInputValue(point2, value);
                    setCheckedstsrumah(value);
                  }}>
                  <View>
                    <Text
                      style={{
                        fontFamily: 'Helvetica',
                        fontSize: 16,
                        marginLeft: 10,
                        color: '#54575C',
                        marginTop: 10,
                        display: element.meta.label === '' ? 'none' : 'flex',
                      }}>{`${element.meta.label}`}</Text>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                      <RadioButton.Android
                        value={element.meta.placeholder}
                        color="#E34243"
                        uncheckedColor="black"
                        status={
                          checkedstsrumah === element.meta.placeholder
                            ? 'checked'
                            : 'unchecked'
                        }
                      />
                      <Text>{element.meta.placeholder}</Text>
                    </View>
                  </View>
                </RadioButton.Group>
              );
            } else {
              return (
                <RadioButton.Group
                  key={key}
                  onValueChange={value => {
                    onRadioChangeInputValue(point2, value);
                    setChecked(value);
                  }}>
                  <View>
                    <Text
                      style={{
                        fontFamily: 'Helvetica',
                        fontSize: 16,
                        marginLeft: 10,
                        marginTop: 10,
                        color: '#54575C',
                        display: element.meta.label === '' ? 'none' : 'flex',
                      }}>{`${element.meta.label}`}</Text>

                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                      <RadioButton.Android
                        value={element.meta.placeholder}
                        color="#E34243"
                        uncheckedColor="black"
                        status={
                          checked === element.meta.placeholder
                            ? 'checked'
                            : 'unchecked'
                        }
                      />
                      <Text style={{fontFamily: 'Helvetica', fontSize: 16}}>
                        {element.meta.placeholder}
                      </Text>
                    </View>
                  </View>
                </RadioButton.Group>
              );
            }
          }
        })}
      <Button
        accessibilityLabel="submit-button"
        style={[btnStyle]}
        labelStyle={{
          fontFamily: 'Helvetica',
          fontSize: 17,
        }}
        mode="contained"
        onPress={onSubmitButtonPress}>
        {btnLabel}
      </Button>
      <Button
        accessibilityLabel="submit-button"
        style={{display: 'none'}}
        labelStyle={{
          fontFamily: 'Helvetica',
          fontSize: 17,
        }}
        mode="contained"
        onPress={onPressed(formFields)}>
        {btnLabel}
      </Button>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    marginBottom: 10,
  },
});

DynamicForm.propTypes = {
  formTemplate: PropTypes.object.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onPressed: PropTypes.func.isRequired,
  onProfilResikoPressed: PropTypes.func,
  onProfilResikoPressed: PropTypes.func,
};

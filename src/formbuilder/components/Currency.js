import React, {useEffect} from 'react';
import PropTypes, {number} from 'prop-types';
import {View, StyleSheet, TextInput, Text} from 'react-native';
import {color} from '../styles';
import {TextInputMask} from 'react-native-masked-text';

export default function Currency(props) {
  const [Curr, SetCurr] = React.useState('');
  const {
    name,
    value,
    meta,
    style,
    onChangeInputValue,
    isMandatory,
    type,
    pointer,
    defaultValue,
    redaksiTextStyle,
    arrayRedaksi,
  } = props;
  var point = pointer !== undefined ? pointer.toLowerCase().trim() : '';
  var point2 = point.includes('(wajib diisi)')
    ? point
        .replace('(wajib diisi)', '')
        .trim()
        .replace(/\s/g, '')
        .replace('.', '')
    : point.replace(/\s/g, '').replace('.', '').trim();
  var objectValue = defaultValue !== undefined ? defaultValue : {};

  useEffect(() => {
    if (objectValue.hasOwnProperty('nominaltransaksi')) {
      SetCurr(objectValue.nominaltransaksi);
    } else if (objectValue.hasOwnProperty('nominaldeposito')) {
      SetCurr(objectValue.nominaldeposito);
    } else if (objectValue.hasOwnProperty('nominal')) {
      SetCurr(objectValue.nominal);
    } else {
      SetCurr('');
    }
  }, [objectValue]);

  return (
    <View key={name}>
      <Text style={styles.text}>
        {`${meta.label} ${isMandatory ? '*' : ''}`}
      </Text>

      <View style={{flexDirection: 'row'}}>
        <View
          style={{
            borderTopStartRadius: 6,
            borderBottomStartRadius: 6,
            marginStart: 10,
            marginTop: 10,
            marginBottom: 10,
            height: 50,
            padding: 10,
            justifyContent: 'center',
            backgroundColor: '#50A0FF',
          }}>
          <Text
            style={{
              fontWeight: 'bold',
              fontSize: 16,
              color: 'white',
            }}>
            IDR
          </Text>
        </View>
        <TextInputMask
          style={styles.textBox}
          type={'money'}
          value={Curr}
          options={{
            precision: 0,
            separator: '.',
            unit: '',
            delimiter: '',
            suffixUnit: '',
          }}
          onChangeText={text => {
            SetCurr(text);
            onChangeInputValue(text);
          }}
        />
      </View>
      {arrayRedaksi &&
        arrayRedaksi.map((element, key) => {
          return (
            <Text
              key={'redaksi_cur' + key}
              style={[
                redaksiTextStyle[key],
                {
                  display:
                    element.pointerRedaksi === meta.pointer ? 'flex' : 'none',
                },
              ]}>
              {element.redaksiText}
            </Text>
          );
        })}
    </View>
  );
}

const styles = StyleSheet.create({
  text: {
    fontFamily: 'Helvetica',
    fontSize: 16,
    marginLeft: 10,
    marginTop: 10,
    fontWeight: 'bold',
    color: '#54575C',
  },
  textBox: {
    width: '82%',
    height: 50,
    borderColor: color.GREY,
    borderWidth: 1.5,
    borderBottomEndRadius: 6,
    borderTopEndRadius: 6,
    backgroundColor: 'white',
    marginEnd: 10,
    marginTop: 10,
    marginBottom: 10,
    paddingLeft: 10,
    fontSize: 16,
  },
});

Currency.propTypes = {
  name: PropTypes.string.isRequired,
  meta: PropTypes.object.isRequired,
  value: PropTypes.string,
  style: PropTypes.object,
  onChangeInputValue: PropTypes.func,
  isMandatory: PropTypes.bool,
};

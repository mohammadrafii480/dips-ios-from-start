import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import FastImage from 'react-native-fast-image';
const upArrow = require('../../../asset/up-arrow.png');
import {COLORS} from '../../color/color';
const downArrow = require('../../../asset/down-arrow.png');
import {Text, View, StyleSheet, Platform, Pressable, Image} from 'react-native';
export default function DropdownSumberDana(props) {
  const [isExpanded, setExpanded] = React.useState(false);
  const [pickedPorto, setPorto] = React.useState({});
  const {
    name,
    meta,
    isMandatory,
    onChangeInputValue,
    index,
    defaultValue,
    pointer,
  } = props;

  var point = pointer !== undefined ? pointer.toLowerCase().trim() : '';
  var point2 = point.includes('(wajib diisi)')
    ? point
        .replace('(wajib diisi)', '')
        .trim()
        .replace(/\s/g, '')
        .replace('.', '')
    : point.replace(/\s/g, '').replace('.', '').trim();
  var objectValue = defaultValue !== undefined ? defaultValue : {};

  useEffect(() => {
    if (
      objectValue.hasOwnProperty('rekeningsumberdana') ||
      objectValue.hasOwnProperty('pilihrekening')
    ) {
      setPorto(
        objectValue.hasOwnProperty('rekeningsumberdana')
          ? objectValue.rekeningsumberdana
          : objectValue.pilihrekening,
      );
    } else {
      setPorto({});
    }
  }, [objectValue]);

  const dataList = meta.listdd.list;
  var portotabungan = dataList.portotabungan;
  return (
    <View
      key={name}
      style={[
        styles.container,
        Platform.OS === 'ios' && styles.overrideOtherComponent(index),
      ]}>
      <Text style={styles.text}>{`${meta.label} ${
        isMandatory ? '*' : ''
      }`}</Text>
      <View style={{flexDirection: 'column'}}>
        <Pressable
          onPress={() => {
            if (isExpanded) {
              setExpanded(false);
            } else {
              setExpanded(true);
            }
          }}>
          <View
            style={{
              borderRadius: 6,
              borderColor: COLORS.border_textInput,
              borderWidth: 1.5,
              marginTop: 10,
              marginEnd: 10,
              marginStart: 10,
              height: Object.keys(pickedPorto).length === 0 ? 50 : 100,
              padding: 10,
              backgroundColor: 'white',
              justifyContent: 'space-between',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            {Object.keys(pickedPorto).length === 0 ? (
              <Text
                style={[
                  ,
                  {
                    textAlign: 'left',
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                  },
                ]}>
                {meta.listdd.placeholder}
              </Text>
            ) : (
              <View
                style={{
                  justifyContent: 'center',
                  flexDirection: 'column',
                  height: '100%',
                }}>
                <Text
                  style={{
                    fontWeight: 'bold',
                    marginBottom: 10,
                    alignSelf: 'flex-start',
                    textAlign: 'left',
                    fontFamily: 'Helvetica',
                    fontSize: 14,
                  }}>
                  {pickedPorto.prodName}
                </Text>
                <Text
                  numberOfLines={1}
                  ellipsizeMode="tail"
                  style={{
                    width: 270,
                    marginBottom: 5,
                    alignSelf: 'flex-start',
                    textAlign: 'left',
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                  }}>
                  {pickedPorto.accountNo} - {pickedPorto.accountName}
                </Text>
                <Text
                  style={{
                    alignSelf: 'flex-start',
                    textAlign: 'left',
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                  }}>
                  Rp{' '}
                  {(parseInt(pickedPorto.availBalance) / 100)
                    .toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, '.')}
                </Text>
              </View>
            )}
            <Image
              tintColor="black"
              resizeMode="contain"
              style={{width: 10, height: 10}}
              source={isExpanded ? upArrow : downArrow}
            />
          </View>
        </Pressable>
        <View
          style={{
            display: isExpanded ? 'flex' : 'none',
            padding: 10,
            flexDirection: 'column',
            borderRadius: 6,
            backgroundColor: 'white',
            borderColor: COLORS.border_textInput,
            borderWidth: 1.5,
            marginTop: -1,
            marginEnd: 10,
            marginStart: 10,
            height: 'auto',
            padding: 10,
          }}>
          {portotabungan?.map((element, index) => {
            return (
              <Pressable
                key={index}
                onPress={() => {
                  setExpanded(false);
                  setPorto({
                    prodName: element.prodName,
                    accountNo: element.accountNo,
                    accountName: element.accountName,
                    availBalance: element.availBalance,
                    accountType: element.accountType,
                  });
                  onChangeInputValue({
                    prodName: element.prodName,
                    accountNo: element.accountNo,
                    accountName: element.accountName,
                    availBalance: element.availBalance,
                    accountType: element.accountType,
                  });
                }}>
                <View
                  style={{
                    borderBottomColor: 'black',
                    borderBottomWidth: 1,
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    flexDirection: 'row',
                    height: 90,
                  }}>
                  <View
                    style={{
                      justifyContent: 'center',
                      flexDirection: 'column',
                    }}>
                    <Text
                      textAlign="start"
                      style={{
                        fontWeight: 'bold',
                        marginBottom: 10,
                        alignSelf: 'flex-start',
                        textAlign: 'left',
                        fontFamily: 'Helvetica',
                        fontSize: 14,
                      }}>
                      {element.prodName}
                    </Text>

                    <Text
                      numberOfLines={1}
                      ellipsizeMode="tail"
                      style={{
                        width: 270,
                        marginBottom: 5,
                        alignSelf: 'flex-start',
                        textAlign: 'left',
                        fontFamily: 'Helvetica',
                        fontSize: 16,
                      }}>
                      {element.accountNo} - {element.accountName}
                    </Text>

                    <Text
                      style={{
                        alignSelf: 'flex-start',
                        textAlign: 'left',
                        fontFamily: 'Helvetica',
                        fontSize: 16,
                      }}>
                      Rp{' '}
                      {(parseInt(element.availBalance) / 100)
                        .toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, '.')}
                    </Text>
                  </View>
                </View>
              </Pressable>
            );
          })}
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginBottom: 10,
  },
  overrideOtherComponent: index => ({
    zIndex: 99 - index,
  }),
  text: {
    fontFamily: 'Helvetica',
    fontSize: 16,
    marginLeft: 10,
    marginTop: 10,
    color: '#54575C',
    fontWeight: 'bold',
  },
  itemStyle: {
    justifyContent: 'flex-start',
  },
});

DropdownSumberDana.propTypes = {
  name: PropTypes.string.isRequired,
  meta: PropTypes.object.isRequired,
  index: PropTypes.number,
  onChangeInputValue: PropTypes.func,
  isMandatory: PropTypes.bool,
};

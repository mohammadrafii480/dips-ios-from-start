import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import FastImage from 'react-native-fast-image';
const logoSuccess = require('../../../asset/v_dialog_success.png');
import {COLORS} from '../../color/color';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import {id, en} from '../../string/text';
export default function ProfilResiko(props) {
  const {onProfilResikoPressed, bahasa, statusProfilResiko} = props;

  return (
    <TouchableOpacity
      onPress={onProfilResikoPressed}
      style={styles.container(statusProfilResiko)}>
      <View style={styles.container2}>
        <Text style={styles.title_container}>
          {bahasa === 'id' ? id.profilResiko : en.profilResiko}
        </Text>
        <Text style={styles.sub_title_container(statusProfilResiko)}>
          {bahasa === 'id' ? id.redaksiProfilResiko : en.redaksiProfilResiko}
        </Text>
      </View>
      {statusProfilResiko ? (
        <FastImage
          source={logoSuccess}
          resizeMode="contain"
          style={styles.logosukses}
        />
      ) : (
        <View style={styles.circlered} />
      )}
    </TouchableOpacity>
  );
}
const styles = StyleSheet.create({
  circlered: {
    alignSelf: 'center',
    width: 24,
    height: 24,
    marginEnd: 10,
    borderRadius: 12,
    borderWidth: 1.5,
    borderColor: COLORS.red,
  },
  logosukses: {
    width: 28,
    height: 28,
    alignSelf: 'center',
    marginEnd: 10,
  },
  sub_title_container: statusProfilResiko_ => ({
    display: statusProfilResiko_ ? 'none' : 'flex',
    fontSize: 14,
    fontFamily: 'Helvetica',
    color: '#54575C',
  }),
  title_container: {
    fontSize: 16,
    fontFamily: 'Helvetica',
    fontWeight: 'bold',
  },
  container2: {
    flexDirection: 'column',
    alignSelf: 'center',
    marginStart: 10,
  },
  container: statusProfilResiko_ => ({
    borderRadius: 6,
    borderColor: statusProfilResiko_ ? COLORS.succesH5 : COLORS.red,
    backgroundColor: 'white',
    borderWidth: 1.5,
    marginStart: 10,
    marginEnd: 10,
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
  }),
});
ProfilResiko.propTypes = {
  bahasa: PropTypes.string,
  onProfilResikoPressed: PropTypes.func,
  statusProfilResiko: PropTypes.bool,
};

import React from 'react';
import PropTypes from 'prop-types';
import {View, Text} from 'react-native';
import CheckBox from '../../checkbox';

export default function Checkbox(props) {
  const {name, meta, style, isMandatory, onChangeInputValue} = props;
  const [ischecked, setChecked] = React.useState(false);

  return (
    <View key={name}>
      <Text
        style={{
          fontFamily: 'Helvetica',
          fontSize: 16,
          marginLeft: 10,
          marginTop: 10,
          color: '#54575C',
        }}>{`${meta.label} ${isMandatory ? '*' : ''}`}</Text>
      <CheckBox
        label={meta.placeholder}
        status={ischecked ? 'checked' : 'unchecked'}
        onPress={() => {
          if (ischecked) {
            setChecked(false);
            onChangeInputValue(false);
          } else {
            setChecked(true);
            onChangeInputValue(true);
          }
        }}
        labelStyle={{fontSize: 14, fontFamily: 'Helvetica', fontSize: 16}}
        boxColor={'#E34243'}
      />
    </View>
  );
}

Checkbox.propTypes = {
  name: PropTypes.string.isRequired,
  meta: PropTypes.object.isRequired,
  value: PropTypes.string,
  style: PropTypes.object,
  onChangeInputValue: PropTypes.func,
};

import React, {useState} from 'react';
import PropTypes from 'prop-types';
const invisible = require('../../../asset/invisible.png');
const show = require('../../../asset/show.png');
import {View, StyleSheet, TextInput, Text, Image} from 'react-native';
import {color} from '../styles';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {COLORS} from '../../color/color';

export default function InputText(props) {
  const {
    name,
    meta,
    style,
    onChangeInputValue,
    isMandatory,
    defaultValue,
    pointer,
    type,
    editable,
    redaksiTextStyle,
    arrayRedaksi,
  } = props;
  var length = 0;

  if (meta.label === 'NIK' || meta.label === 'ID Card Number') {
    length = 16;
  } else {
    length = 1000;
  }
  var point = pointer !== undefined ? pointer.toLowerCase().trim() : '';
  var point2 = point.includes('(wajib diisi)')
    ? point
        .replace('(wajib diisi)', '')
        .trim()
        .replace(/\s/g, '')
        .replace('.', '')
    : point.replace(/\s/g, '').replace('.', '');
  var objectValue = defaultValue !== undefined ? defaultValue : {};
  const [showPassword, setShowPassword] = useState(false);

  if (type == 'password') {
    return (
      <View key={name}>
        <Text style={styles.text}>
          {`${meta.label} ${isMandatory ? '*' : ''}`}
        </Text>
        <View
          style={{
            flexDirection: 'row',
            height: 50,
            borderColor: color.GREY,
            borderWidth: 1.5,
            borderRadius: 6,
            backgroundColor: 'white',
            margin: 10,
            paddingLeft: 10,
            fontSize: 16,
          }}>
          <TextInput
            defaultValue={
              objectValue.hasOwnProperty(point2) ? objectValue[point2] : ''
            }
            style={{
              width: '90%',
              height: 50,
              fontSize: 16,
              paddingEnd: 10,
            }}
            secureTextEntry={!showPassword}
            maxLength={length}
            underlineColorAndroid="transparent"
            onChangeText={onChangeInputValue}
            accessibilityLabel={`input-${meta.label}`}
            editable={editable}
            keyboardType={type == 'number' ? 'numeric' : 'default'}
            placeholder={meta.placeholder}
            multiline={meta.multiline}
            numberOfLines={meta.numberOfLines}
          />
          <TouchableOpacity
            onPress={() => {
              setShowPassword(showPassword ? false : true);
            }}
            style={{
              width: '100%',
              height: '100%',
              justifyContent: 'center',
            }}>
            <Image
              style={{
                tintColor: '#B3B3B3',
                width: 28,
                height: 28,
              }}
              source={showPassword ? show : invisible}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  } else {
    return (
      <View key={name}>
        <Text style={styles.text}>
          {`${meta.label} ${isMandatory ? '*' : ''}`}
        </Text>
        <TextInput
          style={{
            ...styles.textBox(meta.multiline, meta.numberOfLines),
            ...style,
          }}
          defaultValue={
            objectValue.hasOwnProperty(point2) ? objectValue[point2] : ''
          }
          maxLength={length}
          underlineColorAndroid="transparent"
          onChangeText={onChangeInputValue}
          accessibilityLabel={`input-${meta.label}`}
          editable={editable}
          keyboardType={type == 'number' ? 'numeric' : 'default'}
          placeholder={meta.placeholder}
          multiline={meta.multiline}
          numberOfLines={meta.numberOfLines}
        />
        {arrayRedaksi &&
          arrayRedaksi.map((element, key) => {
            return (
              <Text
                key={'redaksi_input_text' + key}
                style={[
                  redaksiTextStyle[0],
                  {
                    display:
                      element.pointerRedaksi === meta.pointer ? 'flex' : 'none',
                  },
                ]}>
                {element.redaksiText}
              </Text>
            );
          })}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  text: {
    fontFamily: 'Helvetica',
    fontSize: 16,
    marginLeft: 10,
    marginTop: 10,
    fontWeight: 'bold',
    color: '#54575C',
  },
  textBox: (multiline, numberOfLines) => ({
    height: !multiline ? 50 : 45 * numberOfLines,
    borderColor: color.GREY,
    borderWidth: 1.5,
    borderRadius: 6,
    backgroundColor: 'white',
    margin: 10,
    paddingLeft: 10,
    fontSize: 16,
  }),
});

InputText.propTypes = {
  name: PropTypes.string.isRequired,
  meta: PropTypes.object.isRequired,
  value: PropTypes.string,
  style: PropTypes.object,
  onChangeInputValue: PropTypes.func,
  isMandatory: PropTypes.bool,
};

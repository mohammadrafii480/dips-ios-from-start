import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import DropDownPicker from 'react-native-dropdown-picker';
import {Text, View, StyleSheet, Platform} from 'react-native';
export default function DataList(props) {
  const {
    name,
    meta,
    isMandatory,
    onChangeInputValue,
    index,
    fromCode,
    pointer,
    defaultValue,
  } = props;
  var point = pointer !== undefined ? pointer.toLowerCase().trim() : '';
  var point2 = point.includes('(wajib diisi)')
    ? point
        .replace('(wajib diisi)', '')
        .trim()
        .replace(/\s/g, '')
        .replace('.', '')
    : point.replace(/\s/g, '').replace('.', '');
  var objectValue = defaultValue !== undefined ? defaultValue : {};
  const [open, setOpen] = React.useState(false);
  const [value, setValue] = React.useState(null);
  const onSelectItem = item => {
    if (item.id === '') {
      onChangeInputValue({});
    } else {
      onChangeInputValue(item);
    }
  };

  const data = meta.listdd.list;
  const label = meta.listdd.placeholder;
  return (
    <View
      key={name}
      style={[
        styles.container,
        Platform.OS === 'ios' && styles.overrideOtherComponent(index),
      ]}>
      <Text style={styles.text}>{`${meta.label} ${
        isMandatory ? '*' : ''
      }`}</Text>
      <DropDownPicker
        searchable={true}
        items={data}
        setItems={data}
        setOpen={setOpen}
        open={open}
        dropDownDirection="BOTTOM"
        itemStyle={styles.itemStyle}
        zIndex={99}
        borderRadius={0}
        disableBorderRadius={false}
        style={{
          borderColor: '#DADADA',
          borderWidth: 1.5,
          marginTop: 10,
          marginBottom: 10,
        }}
        value={value}
        setValue={setValue}
        labelStyle={{
          fontFamily: 'Helvetica',
          fontSize: 16,
        }}
        placeholderStyle={{
          fontFamily: 'Helvetica',
          fontSize: 16,
        }}
        dropDownContainerStyle={{
          marginTop: 10,
        }}
        listItemContainerStyle={{
          borderColor: '#000',
          borderBottomWidth: 1,
        }}
        listItemLabelStyle={{
          fontFamily: 'Helvetica',
          fontSize: 16,
        }}
        placeholder={label}
        onSelectItem={onSelectItem}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {marginStart: 10, marginEnd: 10},
  overrideOtherComponent: index => ({
    zIndex: 99 - index,
  }),
  text: {
    fontFamily: 'Helvetica',
    fontSize: 16,
    marginTop: 10,
    color: '#54575C',
    fontWeight: 'bold',
  },
  itemStyle: {
    justifyContent: 'flex-start',
  },
});

DataList.propTypes = {
  name: PropTypes.string.isRequired,
  meta: PropTypes.object.isRequired,
  index: PropTypes.number,
  onChangeInputValue: PropTypes.func,
  isMandatory: PropTypes.bool,
};

import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import DropDownPicker from 'react-native-dropdown-picker';
import {Text, View, StyleSheet, Platform} from 'react-native';
import {on} from 'events';
export default function DropDown(props) {
  const {
    name,
    meta,
    isMandatory,
    onChangeInputValue,
    index,
    pointer,
    defaultValue,
  } = props;

  var point = pointer !== undefined ? pointer.toLowerCase().trim() : '';
  var point2 = point.includes('(wajib diisi)')
    ? point
        .replace('(wajib diisi)', '')
        .trim()
        .replace(/\s/g, '')
        .replace('.', '')
    : point.replace(/\s/g, '').replace('.', '');
  var objectValue = defaultValue !== undefined ? defaultValue : {};
  const [open, setOpen] = React.useState(false);
  const [value, setValue] = React.useState('');
  const onSelectItem = item => {
    if (item.id === '') {
      onChangeInputValue({});
    } else {
      onChangeInputValue(item);
    }
  };

  const dataList = meta.listdd.list;

  const label = meta.listdd.placeholder;
  useEffect(() => {
    if (objectValue.hasOwnProperty(point2)) {
      setValue(
        objectValue[point2].label !== undefined
          ? objectValue[point2].label
          : objectValue[point2],
      );
    } else {
      setValue(label);
    }
  }, [objectValue]);
  return (
    <View
      key={name}
      style={[
        styles.container,
        Platform.OS === 'ios' && styles.overrideOtherComponent(index),
      ]}>
      <Text style={styles.text}>{`${meta.label} ${
        isMandatory ? '*' : ''
      }`}</Text>
      <DropDownPicker
        items={dataList}
        setItems={dataList}
        setOpen={setOpen}
        open={open}
        itemStyle={styles.itemStyle}
        dropDownDirection="BOTTOM"
        borderRadius={0}
        listMode="FLATLIST"
        zIndex={-100}
        disableBorderRadius={false}
        disabled={dataList.length === 0 ? true : false}
        style={{
          borderColor: '#DADADA',
          borderWidth: 1,
          marginTop: 10,
          marginBottom: 10,
          zIndex: -100000000000,
        }}
        value={value}
        setValue={setValue}
        labelStyle={{
          fontFamily: 'Helvetica',
          fontSize: 16,
        }}
        placeholderStyle={{
          fontFamily: 'Helvetica',
          fontSize: 16,
        }}
        dropDownContainerStyle={{
          marginTop: 10,
          zIndex: 100000,
        }}
        listItemContainerStyle={{
          zIndex: 100000,
          borderColor: '#54575C',
          borderBottomWidth: 0.8,
        }}
        listItemLabelStyle={{
          fontFamily: 'Helvetica',
          fontSize: 16,
        }}
        placeholder={value === '' ? label : value}
        onSelectItem={onSelectItem}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginStart: 10,
    marginEnd: 10,
  },
  overrideOtherComponent: index => ({
    zIndex: 99 - index,
  }),
  text: {
    fontFamily: 'Helvetica',
    fontSize: 16,
    marginTop: 10,
    fontWeight: 'bold',
    zIndex: -10,
    color: '#54575C',
  },
  itemStyle: {
    justifyContent: 'flex-start',
  },
});

DropDown.propTypes = {
  name: PropTypes.string.isRequired,
  meta: PropTypes.object.isRequired,
  index: PropTypes.number,
  onChangeInputValue: PropTypes.func,
  isMandatory: PropTypes.bool,
};

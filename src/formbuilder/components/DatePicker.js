import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import {color} from '../styles';
import {COLORS} from '../../color/color';
import {id, en} from '../../string/text';
import moment from 'moment';
import {Image} from 'react-native';
const calendar = require('../../../asset/calendar.png');

export default function Datepicker(props) {
  const {
    name,
    value,
    meta,
    style,
    onChangeInputValue,
    isMandatory,
    bahasa,
    defaultValue,
    pointer,
  } = props;
  const [isDatePickerVisible, setDatePickerVisible] = React.useState(false);
  const [Tanggal, setTanggal] = React.useState('');

  const handleConfirm = date => {
    const NewDate = moment(date).format('DD-MM-YYYY');
    setTanggal(NewDate);
    onChangeInputValue(NewDate);
    setDatePickerVisible(false);
  };
  var point = pointer !== undefined ? pointer.toLowerCase().trim() : '';
  var point2 = point.includes('(wajib diisi)')
    ? point
        .replace('(wajib diisi)', '')
        .trim()
        .replace(/\s/g, '')
        .replace('.', '')
    : point.replace(/\s/g, '').replace('.', '');
  var objectValue = defaultValue !== undefined ? defaultValue : {};

  useEffect(() => {
    if (objectValue.hasOwnProperty(point2)) {
      setTanggal(objectValue[point2]);
    }
  }, [objectValue[point2]]);
  console.log('placeholder datepicker', meta.placeholder);
  return (
    <View key={name}>
      <Text style={styles.text}>
        {' '}
        {`${meta.label} ${isMandatory ? '*' : ''}`}
      </Text>
      <TouchableOpacity
        onPress={() => {
          if (point2 !== 'tanggalterbit') {
            setDatePickerVisible(true);
          }
        }}
        style={{
          justifyContent: 'center',
          borderRadius: 6,
          borderColor: COLORS.border_textInput,
          borderWidth: 1.5,
          marginTop: 10,
          marginEnd: 10,
          marginStart: 10,
          height: 50,
          padding: 10,
          backgroundColor: 'white',
        }}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text
            style={{
              alignSelf: 'center',
              textAlign: 'left',
              fontFamily: 'Helvetica',
              fontSize: 16,
              color: Tanggal === '' ? COLORS.border_textInput : 'black',
            }}>
            {Tanggal === '' ? meta.placeholder : Tanggal}
          </Text>
          <Image
            source={calendar}
            style={{
              width: 25,
              height: 25,
              alignSelf: 'center',
              tintColor: '#929292',
            }}
          />
        </View>
      </TouchableOpacity>
      <DateTimePickerModal
        display="inline"
        date={new Date()}
        locale="id_ID"
        accentColor={COLORS.red}
        buttonTextColorIOS={COLORS.red}
        cancelTextIOS={bahasa === 'id' ? id.batal : en.batal}
        confirmTextIOS={bahasa === 'id' ? id.pilih : en.pilih}
        isVisible={isDatePickerVisible}
        mode="date"
        onConfirm={handleConfirm}
        onCancel={() => {
          setDatePickerVisible(false);
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  text: {
    fontFamily: 'Helvetica',
    fontSize: 16,
    marginLeft: 10,
    marginTop: 10,
    fontWeight: 'bold',
    color: '#54575C',
  },
  date: {
    width: '90%',
    height: 45,
    borderColor: color.GREY,
    borderWidth: 1.5,
    borderRadius: 6,
    backgroundColor: 'white',
    margin: 10,
    paddingLeft: 10,
    fontSize: 16,
  },
});

Datepicker.propTypes = {
  name: PropTypes.string.isRequired,
  meta: PropTypes.object.isRequired,
  value: PropTypes.string,
  style: PropTypes.object,
  onChangeInputValue: PropTypes.func,
  isMandatory: PropTypes.bool,
};

import React, {useEffect} from 'react';
import PropTypes, {element} from 'prop-types';
import {View, StyleSheet, Text} from 'react-native';
import {RadioButton} from 'react-native-paper';

export default function Radio(props) {
  const {name, value, meta, onChangeInputValue, isMandatory} = props;
  onPress = value => () => {
    onChangeInputValue(value);
  };
  return (
    <View key={name}>
      <Text
        style={{
          fontFamily: 'Helvetica',
          fontSize: 16,
          marginLeft: 10,
          marginTop: 10,
          display: meta.label === '' ? 'none' : 'flex',
        }}>{`${meta.label} ${isMandatory ? '*' : ''}`}</Text>
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        <RadioButton.Android
          value={value}
          color="#E34243"
          uncheckedColor="black"
          onPress={onPress(meta.placeholder)}
          status={value === meta.placeholder ? 'checked' : 'unchecked'}
        />
        <Text>{meta.placeholder}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    borderRadius: 2,
  },
  buttonContainer: {
    flexDirection: 'row',
  },
  radioButtonImage: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  text: {
    paddingLeft: 10,
  },
  slop: {
    top: 10,
    bottom: 10,
    left: 10,
    right: 10,
  },
  radioContainer: {
    paddingVertical: 10,
    width: '100%',
    height: 40,
    paddingLeft: 10,
  },
});

Radio.propTypes = {
  name: PropTypes.string.isRequired,
  meta: PropTypes.object.isRequired,
  value: PropTypes.string,
  onChangeInputValue: PropTypes.func,
  isMandatory: PropTypes.bool,
};

import InputText from './components/InputText';
import Checkbox from './components/Checkbox';
import Radio from './components/Radio';
import Dropdown from './components/Dropdown';
import Datalist from './components/Datalist';
import Datepicker from './components/DatePicker';
import Text from './components/Text';
import {componentName} from './constant';
import Currency from './components/Currency';
import DropdownSumberDana from './components/DropdownSumberDana';
import ProfilResiko from './components/ProfilResiko';

const componentMap = {
  [componentName.TEXT_INPUT]: {
    component: InputText,
    validator: inputTextValidator,
  },
  [componentName.CURRENCY]: {
    component: Currency,
  },
  [componentName.DATE_PICKER]: {
    component: Datepicker,
  },
  [componentName.DATALIST]: {
    component: Datalist,
  },
  [componentName.CHECKBOX]: {
    component: Checkbox,
  },
  [componentName.RADIO]: {
    component: Radio,
  },
  [componentName.DROPDOWN]: {
    component: Dropdown,
  },
  [componentName.DROPDOWNSUMBERDANA]: {
    component: DropdownSumberDana,
  },
  [componentName.TEXT_AREA]: {
    component: InputText,
  },
  [componentName.READ_ONLY_TEXT]: {
    component: Text,
  },
  [componentName.PROFILRESIKO]: {
    component: ProfilResiko,
  },
};

export const getComponent = id => componentMap[id]?.component || null;

export const getValidator = id => componentMap[id]?.validator || null;

function inputTextValidator(text, inputType) {
  const reg = /^([A-Za-z0-9_\-.])+@([A-Za-z0-9_\-\.])+.([A-Za-z]{2,4})$/;

  if (inputType === 'email') {
    return text && reg.test(text);
  }

  return true;
}

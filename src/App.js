import {
  Alert,
  Modal,
  StyleSheet,
  Animated as AnimatedReact,
  Easing as EasingReact,
  SafeAreaView,
  Text,
  Pressable,
  View,
  StatusBar,
  Image,
  RefreshControl,
  TouchableOpacity,
  TouchableHighlight,
  ImageBackground,
  ScrollView,
  Linking,
  Dimensions,
  TextInput,
  FlatList,
  LogBox,
  ActionSheetIOS,
  AppState,
  Platform,
  NativeModules,
} from 'react-native';
import {
  BottomSheetModal,
  BottomSheetModalProvider,
  BottomSheetScrollView,
} from '@gorhom/bottom-sheet';
import {RadioButton} from 'react-native-paper';
import {TextInputMask} from 'react-native-masked-text';
import Countdown from './CountDown';
import {CameraRoll} from '@react-native-camera-roll/camera-roll';
import Video from 'react-native-video';
import RNFetchBlob from 'rn-fetch-blob';
import {PieChart} from 'react-native-gifted-charts';
import {Rating} from 'react-native-ratings';
import OTPTextView from './OTP';
import CheckBox from './checkbox';
import RenderHtml from 'react-native-render-html';
import React, {Component} from 'react';
import moment from 'moment';
import 'moment/locale/id';
import FastImage from 'react-native-fast-image';
import {launchImageLibrary} from 'react-native-image-picker';
import RNFS from 'react-native-fs';
import ImageEditor from '@react-native-community/image-editor';
import DropDownPicker from 'react-native-dropdown-picker';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import RNExitApp from 'react-native-exit-app';
import DynamicForm from './formbuilder/index';
import {RNCamera} from 'react-native-camera';
import {Button, Card} from 'react-native-paper';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import AAIIOSLivenessSDK from 'react-native-aaiios-liveness-sdk';
import {COLORS} from './color/color';
import {id, en} from './string/text';
import {Server} from './api/server';
import {ApiService} from './api/ApiService';
import {
  EventType,
  useZoom,
  ZoomVideoSdkProvider,
  ZoomVideoSdkUser,
  ZoomVideoSdkUserType,
  ZoomVideoSdkChatMessage,
  ZoomVideoSdkChatMessageType,
  ZoomVideoSdkLiveTranscriptionMessageInfo,
  ZoomVideoSdkLiveTranscriptionMessageInfoType,
  ChatMessageDeleteType,
  ShareStatus,
  LiveStreamStatus,
  RecordingStatus,
  Errors,
  PhoneFailedReason,
  PhoneStatus,
  VideoPreferenceMode,
  LiveTranscriptionStatus,
  MultiCameraStreamStatus,
  SystemPermissionType,
  NetworkStatus,
  ConsentType,
} from '@zoom/react-native-videosdk';
import {Easing, log, withTiming} from 'react-native-reanimated';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {VideoView2} from './components/video-view';
import {useIsMounted} from './utils/hooks';
import stylesglobal from './styles/stylesglobal';
import backgroundServer from 'react-native-background-actions';
import BackgroundFetch from 'react-native-background-fetch';
import DeviceInfo from 'react-native-device-info';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import RNQRGenerator from 'rn-qr-generator';
import {title} from 'process';

LogBox.ignoreAllLogs();
const unggahabu = require('../asset/unggahabu.png');
const unggahbiru = require('../asset/unggahbiru.png');
const qrcode = require('../asset/qrcode.png');
const btnKirimChat = require('../asset/send.png');
const upArrow = require('../asset/up-arrow.png');
const downArrow = require('../asset/down-arrow.png');
const ic_tabungan = require('../asset/ic_porto_tabungan.png');
const user = require('../asset/user.png');
const eyeOpen = require('../asset/eye.png');
const eyeClose = require('../asset/hidden.png');
const delete_icon = require('../asset/delete.png');
const wrong_icon = require('../asset/v_dialog_wrong.png');
const dalampengerjaan = require('../asset/dalampengerjaan.png');
const img_opening_account = require('../asset/img_opening_account.png');
const img_dormant = require('../asset/img_aktivasi_dormant.png');
const img_aktivasi_ibmb = require('../asset/img_aktivasi_ibmb.png');
const img_blokir_rekening = require('../asset/img_blokir_rekening.png');
const img_formulir_kredit = require('../asset/img_formulir_kredit.png');
const img_formulir_transfer = require('../asset/img_formulir_transfer.png');
const img_tanpa_kartu = require('../asset/img_tanpa_kartu.png');
const img_kartu_atm = require('../asset/img_kartu_atm.png');
const img_keluhan_nasabah = require('../asset/img_keluhan_nasabah.png');
const img_penutupan_rekening = require('../asset/img_penutupan_rekening.png');
const img_perbarui_data = require('../asset/img_perbarui_data.png');
const img_pinjaman = require('../asset/img_pinjaman.png');
const img_surat_kuasa = require('../asset/img_surat_kuasa.png');
const img_transaksi_antar_bank = require('../asset/img_transaksi_antar_bank.png');
const img_transaksi_ibmb = require('../asset/img_transaksi_ibmb.png');
const img_transaksi_inter_bank = require('../asset/img_transaksi_inter_bank.png');
const img_transaksi_kredit = require('../asset/img_transaksi_kredit.png');
const img_transaksi_titipan = require('../asset/img_transaksi_titipan.png');
const img_transaksi_valas = require('../asset/img_transaksi_valas.png');
const img_transaksi_wealth = require('../asset/img_transaksi_wealth.png');
const ic_download = require('../asset/download.png');
const ic_open_acc = require('../asset/ic_prod_open_account.png');
const ic_list_saving = require('../asset/ic_prod_saving.png');
const mic = require('../asset/v_mic.png');
const chat = require('../asset/v_chat.png');
const unmute = require('../asset/v_unmute.png');
const ktpDummy = require('../asset/ktpdummy.png');
const noAvatar = require('../asset/no_avatar.png');
const v_people = require('../asset/v_people.png');
const v_people2 = require('../asset/v_people2.png');
const v_people3 = require('../asset/v_people3.png');
const v_people4 = require('../asset/v_people4.png');
const v_people5 = require('../asset/v_people5.png');
const v_building = require('../asset/v_building.png');
const bgPickImage = require('../asset/bg.png');
const pickCamera = require('../asset/kamera.png');
const pickGallery = require('../asset/galeri.png');
const logoInfo = require('../asset/v_dialog_info.png');
const logoSuccess = require('../asset/v_dialog_success.png');
const logoWarning = require('../asset/v_dialog_warning.png');
const batikbvic = require('../asset/batikbvic.png');
const logo = require('../asset/logcom.png');
const loadingvictoria = require('../asset/anim_bvic.gif');
const logo_merah = require('../asset/logo_bank_victoria.png');
const background = require('../asset/background_splash.png');
const check = require('../asset/greencheck.png');
const version = require('../package.json');
const dot = require('../asset/icon_timer.png');
const changeImage = require('../asset/v_camera.png');
const people_three = require('../asset/ic_people3.png');
const icon_next_play = require('../asset/form_transact2.png');
const form_transact = require('../asset/form_transact.png');
const back = require('../asset/back.png');
const agent = require('../asset/agent.png');
const terimapanggilan = require('../asset/terimapanggilan.png');
const rejectpanggilan = require('../asset/rejectpanggilan.png');
const bvicIcon = require('../asset/bvictopicon.png');
const zoomIcon = require('../asset/zoomtopicon.png');
const iconTimer = require('../asset/icon_timer.png');
const dataPie = [{value: 100, color: '#FF9658', text: '100%'}];

const styles = StyleSheet.create({
  image: {
    flex: 1,
    justifyContent: 'center',
  },
  container: {
    flex: 1,
  },
  containerZoom: {
    width: '100%',
    height: '100%',
    backgroundColor: '#232323',
  },
  radiobtn: {
    paddingTop: 10,
    paddingBottom: 20,
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  text: {
    fontFamily: 'Helvetica',
    color: 'white',
    alignSelf: 'center',
    paddingTop: 450,
    fontSize: 22,
    fontWeight: 'bold',
  },
  text_H5: {
    top: 30,
    fontFamily: 'Helvetica',
    color: 'black',
    alignSelf: 'center',
    fontSize: 22,
  },
  textRadiobtn: {
    fontFamily: 'Helvetica',
    color: 'black',
    fontSize: 17,
  },
  modalView: {
    backgroundColor: 'white',
    borderRadius: 20,
    paddingStart: 35,
    paddingEnd: 35,
    paddingTop: 35,
    paddingBottom: 20,
    width: '75%',
    height: 'auto',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 0,
  },
  button: {
    width: '110%',
    height: 'auto',
    borderRadius: 10,
    padding: 10,
    elevation: 2,
    backgroundColor: COLORS.red,
  },
  textbtn: {
    alignSelf: 'center',
    textAlign: 'center',
    fontWeight: 'bold',
    fontFamily: 'Helvetica',
    fontSize: 17,
    color: 'white',
  },
  strip: {
    marginTop: 10,
    marginBottom: 10,
    backgroundColor: 'rgba(0, 0, 0, 0.14)',
    width: '110%',
    height: '1.5%',
  },
  modalText: {
    fontFamily: 'Helvetica',
    fontSize: 20,
    fontWeight: 'bold',
  },
  bgmodal: {
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
    height: '100%',
    width: '100%',
  },
  bgloading: {
    backgroundColor: 'white',
    height: '100%',
    width: '100%',
  },
  containerLoading: {
    width: 200,
    height: 200,
    alignSelf: 'center',
    borderRadius: 188,
  },
  containerH5_1: {
    width: 310,
    height: 310,
    marginBottom: 300,
  },
  containerH5: {
    width: 316,
    height: 316,
    marginBottom: 300,
  },
  imageH5Success: {
    width: '100%',
    height: '100%',
    borderColor: COLORS.succesH5,
    borderWidth: 8,
    borderRadius: 310,
  },
  greenCheck: {
    width: '15%',
    height: '15%',
    alignSelf: 'center',
    alignItems: 'center',
    bottom: 330,
    position: 'relative',
  },
  imageH5: {
    borderRadius: 310,
    width: '100%',
    height: '100%',
  },
  imageH5_2: {
    borderRadius: 316,
    position: 'absolute',
    width: '95%',
    height: '95%',
    left: 8,
    top: 8,
  },
  circular: {
    width: '100%',
    height: '100%',
    borderBottomColor: 'white',
    borderBottomWidth: 10,
    borderColor: COLORS.succesH5,
    borderWidth: 20,
    borderRadius: 316,
  },
  imgSize: {
    width: '100%',
    height: '100%',
  },
  imageBatik: {
    width: '100%',
    height: 290,
    position: 'absolute',
    bottom: 0,
    resizeMode: 'contain',
  },
  containerBatik: {
    justifyContent: 'flex-end',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    backgroundColor: 'white',
  },
  bottomView: {
    width: '100%',
    height: '10%',
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
  },
  upperView: {
    width: '90%',
    borderRadius: 30,
    height: '25%',
    top: 20,
    position: 'relative',
    backgroundColor: COLORS.background_waiting_room,
  },
  upperViewWaiting: {
    width: '90%',
    borderRadius: 30,
    height: '25%',
    top: 20,
    backgroundColor: COLORS.background_waiting_room,
  },
  btn: {
    borderRadius: 10,
    borderColor: COLORS.red,
    borderWidth: 2,
  },
  btnForm: {
    borderRadius: 12,
    backgroundColor: COLORS.red,
    width: '100%',
    top: 20,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnLabel: {
    fontFamily: 'Helvetica',
    fontSize: 15,
  },
  connectingWrapper: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  connectingText: {
    fontSize: 24,
    fontWeight: 'bold',
    color: 'red',
  },
  safeArea: {
    flex: 1,
  },
  bottomWrapper: {
    paddingHorizontal: 8,
  },
  keyboardArea: {
    height: 0,
    width: 0,
    zIndex: -100,
  },
  userList: {
    width: '100%',
  },
  userListContentContainer: {
    flexGrow: 1,
    justifyContent: 'center',
  },
});

spinValue = new AnimatedReact.Value(0);
AnimatedReact.loop(
  AnimatedReact.timing(this.spinValue, {
    toValue: 1,
    duration: 3000,
    easing: EasingReact.linear,
    useNativeDriver: true,
  }),
).start();
const spin = this.spinValue.interpolate({
  inputRange: [0, 1],
  outputRange: ['0deg', '360deg'],
});
var noCIF = '51691813', //no Cif rafii = 51691813, ade = 24200417
  custName = 'MOHAMMAD RAFII BURHANUDDIN',
  custEmail = '',
  custNoHp = '081215702727',
  custAlamat = '',
  custBranchCode = '020',
  custGelar = '',
  noRekening = '',
  detailPengaduan = '',
  noPengaduan = '',
  dataresp = [],
  idDips = 'sz76KGMmz3N0lghG', ///Test IdDips = n5TS2ZUiaS5J0Dse, idDipsku = sz76KGMmz3N0lghG
  accessToken = '',
  exchangeToken = '',
  bahasa = 'id',
  flagBlacklist = false,
  boolPernyataan = false,
  fileNamePendukung = '',
  fileNamePendukung2 = '',
  fileNamePendukung3 = '',
  fileUriPendukung = '',
  fileUriPendukung2 = '',
  fileUriPendukung3 = '',
  base64Pendukung = '',
  base64Pendukung2 = '',
  base64Pendukung3 = '',
  base64Resi = '',
  linkPDFResi = '',
  fileNameKTP = '',
  fileUriKTP = '',
  base64KTP = '',
  fileUriSwafoto = '',
  base64Swafoto = '',
  fileUriNPWP = '',
  base64NPWP = '',
  fileUriTTD = '',
  base64TTD = '',
  linkURI = '',
  isCust = false,
  nik_ = '3374150804000005',
  idAgent = '',
  nama_lengkap_ = '',
  tempat_lahir_ = '',
  tgl_lahir_ = '',
  jenis_kelamin_ = '',
  golongan_darah_ = '',
  alamat_ = '',
  rt_rw_ = '',
  provinsi_ = '',
  kabupaten_kota_ = '',
  kecamatan_ = '',
  kelurahan_desa_ = '',
  agama_ = '',
  status_perkawinan_ = '',
  kewarganegaraan_ = '',
  pekerjaan_ = '',
  uri_1 = '',
  uri_2 = '',
  nama_ibu_kandung_ = '',
  npwp_ = '',
  ws_session_name = '',
  ws_password = '',
  isMuted = false,
  csId = '',
  signatureZoom = '',
  valueWaitingTime = {},
  arrayBarcode = [],
  arraynoForm = [],
  arrayInquiry = [],
  arrayUploadBarcode = [],
  arrayJawaban = [],
  arrayJawaban2 = [],
  count_popUpWaiting = 0,
  arrayIdForm = [],
  arrayBerhasilPengajuan = [],
  correctTransaction = true,
  mandatoryIsOke = true,
  wrongInquiry = false;
arrayTransaksiMultiForm = [];

class SplashScreen extends Component {
  state = {
    modalVisible: false,
    radiobtnindo: false,
    radiobtneng: false,
    loadingvisible: false,
    popupblack: false,
  };
  constructor(props) {
    super(props);
  }
  async sdkDemo() {
    AAIIOSLivenessSDK.initSDKByLicense('AAILivenessMarketIndonesia', false);
    AAIIOSLivenessSDK.setDetectOcclusion(true);
    AAIIOSLivenessSDK.setActionTimeoutSeconds(20);
    AAIIOSLivenessSDK.setActionSequence(true, ['BLINK']);
    AAIIOSLivenessSDK.setResultPictureSize(300);
    var payload = {
      applicationId: 'co.id.dips361.ios',
    };
    fetch(Server.BASE_URL_API + ApiService.licenseAAI, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      timeout: 1500,
      body: JSON.stringify(payload),
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.code < 300) {
          setTimeout(() => {
            this.setState({loadingvisible: false});
          }, 2000);
          AAIIOSLivenessSDK.setLicenseAndCheck(
            responseJson.data.license,
            result => {
              if (result === 'SUCCESS') {
                this.showSDKPage();
              } else {
                this.setState({message: result});
              }
            },
          );
        } else {
          this.setState({loadingvisible: false});
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        console.error(error);
        Alert.alert('Gagal Terhubung ke server', JSON.stringify(error));
      });
  }
  showSDKPage() {
    var bahasa;
    if (this.state.radiobtnindo) {
      bahasa = 'id';
    } else {
      bahasa = 'en';
    }
    var config = {
      showHUD: true,
      animated: false,
      prepareTimeoutInterval: 20,
      roundBorderColor: '#00000000',
      ellipseLineColor: '#FFFFFF',
      language: bahasa,
    };
    var oneLoop = 0;
    var callbackMap = {
      onCameraPermissionDenied: (errorKey, errorMessage) => {
        Alert.alert(
          'Silahkan izinkan kamera untuk melanjutkan proses selanjutnya.',
        );
        this.setState({message: errorMessage});
      },

      livenessViewBeginRequest: () => {},

      // Optional
      livenessViewEndRequest: () => {},

      // Required
      onDetectionComplete: (livenessId, base64Img) => {
        this.setState({message: livenessId});
        this.props.navigation.navigate('H5Liveness', {
          img: base64Img,
          bahasa: bahasa,
        });
      },
      onDetectionFailed: (errorCode, errorMessage) => {
        if (oneLoop == 0) {
          this.setState({message: errorMessage});
          this.setState({modalVisible: true});
          Alert.alert(errorMessage);
        }
        oneLoop++;
      },

      onLivenessViewRequestFailed: (errorCode, errorMessage, transactionId) => {
        console.log(
          '>>>>> onLivenessViewRequestFailed:',
          errorCode,
          errorMessage,
          transactionId,
        );
        this.setState({message: errorMessage});
        this.setState({modalVisible: true});
      },
      onGiveUp: () => {
        this.setState({message: 'onGiveUp'});
        this.setState({modalVisible: true});
      },
    };

    AAIIOSLivenessSDK.startLiveness(config, callbackMap);
  }
  componentDidMount() {
    const systemVersion = DeviceInfo.getSystemVersion();
    const majorVersion = parseInt(systemVersion, 10);
    console.log('Versi IOS', majorVersion);
    if (majorVersion > 11) {
      if (!flagBlacklist) {
        this.timeoutHandle = setTimeout(() => {
          this.setState({modalVisible: true});
        }, 3000);
      }
    } else {
      Alert.alert('Maaf perangkat anda tidak kompatibel');
    }
  }
  componentWillUnmount() {
    clearTimeout(this.timeoutHandle);
  }
  form_ind() {
    if (this.state.radiobtnindo == false) {
      return (
        <View>
          <TouchableOpacity
            onPress={() => {
              this.setState({radiobtnindo: true});
              this.setState({radiobtneng: false});
            }}>
            <Text style={[styles.textRadiobtn, {alignSelf: 'flex-end'}]}>
              ⚪ Indonesia
            </Text>
          </TouchableOpacity>
        </View>
      );
    }
    if (this.state.radiobtnindo == true) {
      return (
        <View>
          <TouchableOpacity
            onPress={() => {
              this.setState({radiobtnindo: false});
            }}>
            <Text style={[styles.textRadiobtn, {alignSelf: 'flex-end'}]}>
              🔘 Indonesia
            </Text>
          </TouchableOpacity>
        </View>
      );
    }
  }
  form_english() {
    if (this.state.radiobtneng == false) {
      return (
        <View>
          <TouchableOpacity
            onPress={() => {
              this.setState({radiobtneng: true});
              this.setState({radiobtnindo: false});
            }}>
            <Text style={[styles.textRadiobtn, {alignSelf: 'flex-start'}]}>
              ⚪ English
            </Text>
          </TouchableOpacity>
        </View>
      );
    }
    if (this.state.radiobtneng == true) {
      return (
        <View>
          <TouchableOpacity
            onPress={() => {
              this.setState({radiobtneng: false});
            }}>
            <Text style={[styles.textRadiobtn, {alignSelf: 'flex-start'}]}>
              🔘 English
            </Text>
          </TouchableOpacity>
        </View>
      );
    }
  }
  popUpBlacklist() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={flagBlacklist}
        statusBarTranslucent>
        <TouchableOpacity
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View
              style={{
                margin: 100,
                backgroundColor: 'white',
                borderRadius: 20,
                justifyContent: 'space-between',
                padding: 35,
                width: '80%',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
                alignItems: 'center',
              }}>
              <FastImage
                transition={false}
                source={logoWarning}
                style={{width: 55, height: 55, marginBottom: 15}}
              />
              <Text
                style={{
                  marginBottom: 20,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  textAlign: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {
                  'Gagal\n\nMaaf Anda tidak dapat menggunakan layanan Digital Operations Bank Victoria\n\nUntuk info lebih lanjut silakan menghubungi Call Center Bank Victoria'
                }
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Button
                  textColor={COLORS.red}
                  labelStyle={styles.btnLabel}
                  style={[styles.btn, {marginEnd: 10}]}
                  mode="outlined">
                  Call Center
                </Button>
                <Button
                  style={styles.btn}
                  labelStyle={styles.btnLabel}
                  buttonColor={COLORS.red}
                  mode="contained"
                  onPress={() => this.processSignature()}>
                  Keluar
                </Button>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      </Modal>
    );
  }
  render() {
    console.log('RENDER AppState currentState', AppState.currentState);
    const {navigation} = this.props;
    flagBlacklist = navigation.getParam('blacklist', false);
    const {modalVisible} = this.state;
    if (this.state.loadingvisible) {
      return (
        <View style={styles.container}>
          <StatusBar hidden="true" />
          <View style={styles.bgloading}>
            <View style={styles.centeredView}>
              <View style={styles.containerLoading}>
                <FastImage
                  transition={false}
                  source={loadingvictoria}
                  style={styles.imgSize}
                />
              </View>
            </View>
          </View>
        </View>
      );
    } else {
      return (
        <View style={styles.container}>
          <StatusBar hidden="true" />
          {this.popUpBlacklist()}
          <Modal
            animationType="fade"
            transparent={true}
            visible={modalVisible}
            statusBarTranslucent
            onRequestClose={() => {
              Alert.alert('Modal has been closed.');
              this.setState({modalVisible: !modalVisible});
            }}>
            <View style={styles.bgmodal}>
              <View style={styles.centeredView}>
                <View style={styles.modalView}>
                  <Text style={styles.modalText}>Bahasa / Language</Text>
                  <View style={styles.strip}></View>
                  <View style={styles.radiobtn}>
                    <View style={{width: '45%'}}>{this.form_ind()}</View>
                    <View style={{width: '10%'}}></View>
                    <View style={{width: '45%'}}>{this.form_english()}</View>
                  </View>
                  <Pressable
                    style={[styles.button]}
                    onPress={() => {
                      if (!this.state.radiobtnindo && !this.state.radiobtneng) {
                        Alert.alert('Silahkan pilih bahasa');
                      } else {
                        this.setState({modalVisible: !modalVisible});
                        this.setState({loadingvisible: true});
                        this.sdkDemo();
                      }
                    }}>
                    <Text style={styles.textbtn}>Lanjut / Next</Text>
                  </Pressable>
                </View>
              </View>
            </View>
          </Modal>
          <FastImage
            transition={false}
            source={background}
            resizeMode="cover"
            style={styles.image}>
            <Image
              transition={false}
              source={logo}
              style={{alignSelf: 'center', position: 'absolute'}}
            />
            <Text style={styles.text}>V.{version.version}</Text>
          </FastImage>
        </View>
      );
    }
  }
}
class H5Liveness extends Component {
  state = {
    isSuccess: false,
  };
  constructor(props) {
    super(props);
  }
  async customerAuth() {
    var payload = {
      image: base64H5,
    };
    fetch(Server.BASE_URL_API + ApiService.customerAuth, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      timeout: 1500,
      body: JSON.stringify(payload),
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.code < 300) {
          console.log('Response Cust Auth', responseJson);
          this.setState({isSuccess: true});
          if (
            (responseJson.data.customer.noCif === null &&
              responseJson.data.customer.isSwafoto === false) ||
            (responseJson.data.customer.noCif === null &&
              responseJson.data.customer.isSwafoto === true)
          ) {
            isCust = false;
          } else {
            isCust = true;
            noCIF = responseJson.data.customer.noCif;
            custEmail = responseJson.data.customer.email;
            custNoHp = responseJson.data.customer.noHp;
          }
          var idDipsNew = '';
          nik_ = responseJson.data.customer.nik;
          custName = responseJson.data.customer.namaLengkap;
          custBranchCode = responseJson.data.customer.branchCode;
          custAlamat = responseJson.data.customer.alamat;
          idDipsNew = responseJson.data.customer.idDips;
          var blacklist = responseJson.data.customer.blacklist;
          accessToken = responseJson.data.token.token;
          exchangeToken = responseJson.data.token.exchange;
          console.log('isCust', isCust);
          if (blacklist) {
            this.props.navigation.navigate('SplashScreen', {
              blacklist: true,
            });
          } else {
            this.getParameterize();
            idDips = idDipsNew;
            console.log('idDips = ', idDips);
            setTimeout(() => {
              if (responseJson.data.customer.isSwafoto === true) {
                this.props.navigation.navigate('ConnectionForm');
              } else {
                this.props.navigation.navigate('DipsSwafoto');
              }
            }, 1500);
          }
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        console.error(error);
        Alert.alert('Gagal Terhubung ke server', JSON.stringify(error));
      });
  }
  async getParameterize() {
    fetch(Server.BASE_URL_API + ApiService.listParameterize, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.code < 300) {
          responseJson.data.map(element => {
            if (element.name === 'waitingTime') {
              valueWaitingTime = JSON.parse(element.value);
            }
          });
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        console.error(error);
        Alert.alert('Gagal Terhubung ke server');
      });
  }
  componentDidMount() {
    this.customerAuth();
  }
  render() {
    const {navigation} = this.props;
    bahasa = navigation.getParam('bahasa', 'id');
    base64H5 = navigation.getParam('img', '');
    linkURI = `data:image/png;base64,${base64H5}`;
    let encoded = encodeURI(linkURI);
    if (this.state.isSuccess) {
      return (
        <View style={styles.container}>
          <StatusBar hidden="true" />
          <View style={styles.bgloading}>
            <View style={styles.centeredView}>
              <View style={styles.containerH5_1}>
                <Image
                  transition={false}
                  style={styles.imageH5Success}
                  source={{
                    uri: encoded,
                  }}
                  resizeMode={'cover'}
                />
                <Image
                  transition={false}
                  style={styles.greenCheck}
                  source={check}
                  resizeMode={'cover'}
                />
              </View>
            </View>
          </View>
        </View>
      );
    } else {
      return (
        <View style={styles.container}>
          <StatusBar hidden="true" />
          <View style={styles.bgloading}>
            <View style={styles.centeredView}>
              <View style={styles.containerH5}>
                <View>
                  <AnimatedReact.View
                    style={[styles.circular, {transform: [{rotate: spin}]}]}
                  />
                  <Image
                    transition={false}
                    style={styles.imageH5_2}
                    source={{
                      uri: encoded,
                    }}
                    resizeMode={'cover'}
                  />
                </View>
                <Text style={styles.text_H5}>
                  {bahasa == 'id' ? id.harap_menunggu : en.harap_menunggu}
                </Text>
              </View>
            </View>
          </View>
        </View>
      );
    }
  }
}
class ConnectionForm extends Component {
  state = {
    loadingvisible: false,
    setRefreshing: false,
    isPilihButton: true,
    itemLayanan: [
      {
        title:
          bahasa === 'id'
            ? id.transaksi_rekening_pribadi
            : en.transaksi_rekening_pribadi,
        id: 3,
        img: '',
      },
      {
        title:
          bahasa === 'id'
            ? id.transaksi_antar_rekening
            : en.transaksi_antar_rekening,
        id: 2,
        img: img_transaksi_inter_bank,
      },
      {
        title:
          bahasa === 'id' ? id.transaksi_antar_bank : en.transaksi_antar_bank,
        id: 1,
        img: img_transaksi_antar_bank,
      },
      {
        title:
          bahasa === 'id' ? id.transaksi_tanpa_kartu : en.transaksi_tanpa_kartu,
        id: 4,
        img: img_tanpa_kartu,
      },
    ],
    modalscheduler: false,
    numColumns: 3,
    base64QR: '',
    dateShcedule: '',
    idSchedule: null,
    open: false,
    openCamera: false,
    value: null,
    items: [],
    isDatePickerVisible: false,
    //Session 0 : List Form Transaksi
    //Session 1 : Form Builder
    //Session 2 : QrCode
    //Session 3 : ConfirmCardless
    sessionTransaksi: 0,
    // 1 : Transaksi Antar Bank
    // 2 : Transaksi Antar Rekening
    // 3 : Transaksi Rekning Pribadi
    // 4 : Transaksi Tanpa Kartu
    jenisTransaksiId: 1,
    formCode: 0,
    ListNoRek: [],
    dataPortofolio: {},
    isChecked: false,
    isCheckedTNC: false,
    uriKTP: '',
    noHpKuasa: null,
    jenisCardless: bahasa == 'id' ? id.setor_tunai : en.setor_tunai,
    modalScheduleDone: false,
  };
  componentDidMount() {
    this.countdown.startCountdown();
    this.scheduleDropdown();
    this.getNoRekening();
  }
  async scheduleDropdown() {
    var dataSchedule = [];
    fetch(`${Server.BASE_URL_API}${ApiService.timePeriodSchedule}`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log('responseJson schedule dd', responseJson);
        if (responseJson.code < 300) {
          responseJson.data.map(element => {
            dataSchedule.push({
              label: element.periode,
              value: element.id,
            });
          });
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        Alert.alert('Gagal Terhubung ke server', error);
      });
    this.setState({items: dataSchedule});
  }
  onSubmitScedule() {
    if (this.state.dateShcedule === '') {
      Alert.alert(
        bahasa === 'id'
          ? id.pilih_tanggal_jadwalkan
          : en.pilih_tanggal_jadwalkan,
      );
    } else if (this.state.idSchedule === null) {
      Alert.alert(
        bahasa === 'id' ? id.pilih_jam_jadwalkan : en.pilih_jam_jadwalkan,
      );
    } else {
      this.setState({dateShcedule: ''});
      this.setState({idSchedule: null});

      var formatLama = moment(this.state.dateShcedule, 'DD-MM-YYYY');
      var formatBaru = moment(formatLama).format('YYYY-MM-DD');

      this.saveSchedule(formatBaru);
    }
  }
  popUpScheduler() {
    const showDatePicker = () => {
      this.setState({isDatePickerVisible: true});
    };
    const hideDatePicker = () => {
      this.setState({isDatePickerVisible: false});
    };
    const handleConfirm = date => {
      const NewDate = moment(date).format('DD/MM/YYYY');
      this.setState({dateShcedule: NewDate});
      hideDatePicker();
    };
    const getValueDropdown = valu => {
      this.setState({idSchedule: valu.value, value: valu.value});
    };
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.modalscheduler}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({modalscheduler: false});
        }}>
        <TouchableOpacity
          onPress={() => {
            this.setState({modalscheduler: false});
          }}
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View
              style={{
                margin: 100,
                backgroundColor: 'white',
                borderRadius: 20,
                justifyContent: 'space-between',
                padding: 35,
                width: '80%',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
              }}>
              <Text
                style={{
                  marginBottom: 35,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  textAlign: 'left',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {bahasa === 'id'
                  ? id.konten_popup_jadwalkan
                  : en.konten_popup_jadwalkan}
              </Text>
              <Text
                style={{
                  marginBottom: 20,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  alignItems: 'flex-start',
                  textAlign: 'left',
                  fontWeight: 'bold',
                }}>
                {bahasa === 'id' ? id.hari_dan_tanggal : en.hari_dan_tanggal}
              </Text>
              <TouchableOpacity
                onPress={showDatePicker}
                style={{
                  justifyContent: 'center',
                  borderRadius: 5,
                  borderColor: COLORS.border_textInput,
                  borderWidth: 1.5,
                  marginBottom: 10,
                  width: '100%',
                  height: 50,
                  padding: 10,
                }}>
                <View style={{flexDirection: 'row'}}>
                  <Text
                    style={[
                      ,
                      {
                        alignSelf: 'flex-start',
                        textAlign: 'left',
                        fontFamily: 'Helvetica',
                        fontSize: 17,
                      },
                    ]}>
                    {this.state.dateShcedule}
                  </Text>
                </View>
              </TouchableOpacity>
              <DateTimePickerModal
                display="inline"
                date={new Date()}
                locale="id_ID"
                minimumDate={new Date()}
                accentColor={COLORS.red}
                buttonTextColorIOS={COLORS.red}
                cancelTextIOS={bahasa === 'id' ? id.batal : en.batal}
                confirmTextIOS={bahasa === 'id' ? id.pilih : en.pilih}
                isVisible={this.state.isDatePickerVisible}
                mode="date"
                onConfirm={handleConfirm}
                onCancel={hideDatePicker}
              />
              <Text
                style={{
                  marginBottom: 10,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  textAlign: 'left',
                  fontWeight: 'bold',
                }}>
                {bahasa === 'id' ? id.waktu : en.waktu}
              </Text>
              <DropDownPicker
                dropDownMaxHeight={250}
                dropDownDirection="BOTTOM"
                placeholder={
                  this.state.items.length == 0 ? '' : this.state.items[0].label
                }
                onSelectItem={getValueDropdown}
                style={{
                  borderColor: COLORS.border_textInput,
                  borderWidth: 1.5,
                  marginBottom: 20,
                }}
                labelStyle={{
                  fontFamily: 'Helvetica',
                  fontSize: 16,
                }}
                placeholderStyle={{
                  fontFamily: 'Helvetica',
                  fontSize: 16,
                }}
                listItemContainerStyle={{
                  borderColor: '#000',
                  borderBottomWidth: 1,
                }}
                listItemLabelStyle={{
                  fontFamily: 'Helvetica',
                  fontSize: 16,
                }}
                open={this.state.open}
                value={this.state.value}
                items={this.state.items}
                setOpen={set => {
                  this.setState({open: set});
                }}
              />
              <Pressable
                style={{
                  width: '100%',
                  height: 43,
                  borderRadius: 10,
                  padding: 10,
                  justifyContent: 'center',
                  alignItems: 'center',
                  elevation: 2,
                  backgroundColor: COLORS.red,
                }}
                onPress={() => {
                  this.onSubmitScedule();
                }}>
                <Text style={styles.textbtn}>
                  {bahasa === 'id'
                    ? id.jadwalkan_panggilan
                    : en.jadwalkan_panggilan}
                </Text>
              </Pressable>
            </View>
          </View>
        </TouchableOpacity>
      </Modal>
    );
  }
  async saveSchedule(tgl) {
    var payload = {
      idDips: idDips,
      tanggal: tgl,
      periodeId: this.state.idSchedule,
    };
    fetch(Server.BASE_URL_API + ApiService.saveSchedule, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: JSON.stringify(payload),
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({modalscheduler: false, modalscheduler2: false});

        if (responseJson.code < 300) {
          setTimeout(() => {
            this.setState({modalScheduleDone: true});
          }, 1000);
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        console.error(error);
        Alert.alert('Gagal Terhubung ke server', JSON.stringify(error));
      });
  }
  formatData = (data, numColumns) => {
    if (data !== undefined) {
      const numberOfFullRows = Math.floor(data.length / numColumns);
      let numberOfElementsLastRow = data.length - numberOfFullRows * numColumns;
      while (
        numberOfElementsLastRow !== numColumns &&
        numberOfElementsLastRow !== 0
      ) {
        data.push({key: `blank-${numberOfElementsLastRow}`, empty: true});
        numberOfElementsLastRow++;
      }
      return data;
    }
  };
  getDropdown(url) {
    var dataItemList = [];
    var placeholder = '';
    var result = {};
    fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
    })
      .then(response => response.json())
      .then(responseJson => {
        responseJson.data.map(element => {
          if (responseJson.code < 300) {
            placeholder = responseJson.data[0];
            dataItemList.push({
              id: element.id !== undefined ? element.id : '',
              label: bahasa === 'id' ? element.labelIdn : element.labelEng,
              value: bahasa === 'id' ? element.labelIdn : element.labelEng,
              beneficiaryCode: element.hasOwnProperty('beneficiaryCode')
                ? element.beneficiaryCode
                : '',
            });
          }
        });
      })
      .catch(error => {
        Alert.alert('Gagal Terhubung ke server', error);
      });
    result = {
      placeholder:
        bahasa === 'id' ? '== Silakan Pilih ==' : '== Please Choose ==',
      list: dataItemList,
    };
    return result;
  }
  async getNoRekening() {
    var List = [];
    var payload = {
      noCif: noCIF,
      bahasa: bahasa,
    };
    console.log('payload', payload);
    fetch(Server.BASE_URL_API + ApiService.listAccount, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: JSON.stringify(payload),
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log('response get no rek', responseJson);
        if (responseJson.code < 300) {
          this.setState({dataPortofolio: responseJson.data});
          responseJson.data.portotabungan.map(element => {
            List.push({
              label: element.accountNo,
              value: element.accountNo,
            });
          });
          this.setState({ListNoRek: List});
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        Alert.alert('Gagal Terhubung ke server', error);
      });
  }
  onSubmited = formFields => {
    if (this.state.jenisTransaksiId === 1) {
      if (
        formFields.bankpenerima === undefined ||
        !formFields.bankpenerima.hasOwnProperty('value')
      ) {
        Alert.alert(
          bahasa === 'id'
            ? id.silahkan_masukkan_bank_penerima
            : en.silahkan_masukkan_bank_penerima,
        );
      } else if (
        formFields.rekeningpenerima === undefined ||
        formFields.rekeningpenerima === ''
      ) {
        Alert.alert(
          bahasa === 'id'
            ? id.silahkan_masukkan_rekening_penerima
            : en.silahkan_masukkan_rekening_penerima,
        );
      } else if (
        formFields.nominaltransaksi === undefined ||
        formFields.nominaltransaksi === ''
      ) {
        Alert.alert(
          bahasa === 'id'
            ? id.silahkan_masukkan_nominal_transaksi
            : en.silahkan_masukkan_nominal_transaksi,
        );
      } else if (
        formFields.jenislayanan === undefined ||
        !formFields.jenislayanan.hasOwnProperty('value')
      ) {
        Alert.alert(
          bahasa === 'id'
            ? id.silahkan_pilih_jenis_layanan
            : en.silahkan_pilih_jenis_layanan,
        );
      } else if (
        formFields.tujuantransaksi === undefined ||
        !formFields.tujuantransaksi.hasOwnProperty('value')
      ) {
        Alert.alert(
          bahasa === 'id'
            ? id.silahkan_pilih_tujuan_transaksi
            : en.silahkan_pilih_tujuan_transaksi,
        );
      } else if (formFields.berita === undefined || formFields.berita === '') {
        Alert.alert(bahasa === 'id' ? id.berita : en.berita);
      } else {
        console.log('Hasil Form', formFields);
        this.createQRCode(formFields);
      }
    } else if (this.state.jenisTransaksiId === 2) {
      if (
        formFields.rekeningpenerima === undefined ||
        formFields.rekeningpenerima === ''
      ) {
        Alert.alert(
          bahasa === 'id'
            ? id.silahkan_masukkan_rekening_penerima
            : en.silahkan_masukkan_rekening_penerima,
        );
      } else if (
        formFields.nominaltransaksi === undefined ||
        formFields.nominaltransaksi === ''
      ) {
        Alert.alert(
          bahasa === 'id'
            ? id.silahkan_masukkan_nominal_transaksi
            : en.silahkan_masukkan_nominal_transaksi,
        );
      } else if (
        formFields.tujuantransaksi === undefined ||
        !formFields.tujuantransaksi.hasOwnProperty('value')
      ) {
        Alert.alert(
          bahasa === 'id'
            ? id.silahkan_pilih_tujuan_transaksi
            : en.silahkan_pilih_tujuan_transaksi,
        );
      } else if (formFields.berita === undefined || formFields.berita === '') {
        Alert.alert(bahasa === 'id' ? id.berita : en.berita);
      } else {
        console.log('Hasil Form', formFields);
        this.createQRCode(formFields);
      }
    } else if (this.state.jenisTransaksiId === 3) {
      if (
        formFields.rekeningpenerima === undefined ||
        formFields.rekeningpenerima === ''
      ) {
        Alert.alert(
          bahasa === 'id'
            ? id.silahkan_masukkan_rekening_penerima
            : en.silahkan_masukkan_rekening_penerima,
        );
      } else if (
        formFields.nominaltransaksi === undefined ||
        formFields.nominaltransaksi === ''
      ) {
        Alert.alert(
          bahasa === 'id'
            ? id.silahkan_masukkan_nominal_transaksi
            : en.silahkan_masukkan_nominal_transaksi,
        );
      } else if (formFields.berita === undefined || formFields.berita === '') {
        Alert.alert(bahasa === 'id' ? id.berita : en.berita);
      } else {
        console.log('Hasil Form', formFields);
        this.createQRCode(formFields);
      }
    }
  };
  onSubmitTransaction = formFields => {
    if (this.state.jenisTransaksiId === 4) {
      console.log('formFields', formFields);
      arrayBarcode[0] = formFields;
    }
  };
  onRefresh = () => {
    this.setState({setRefreshing: true});
    setTimeout(() => {
      this.formBuilder(this.state.formCode);
    }, 100);
  };
  async formBuilder(formCode) {
    this.setState({loadingvisible: true});
    dataresp = [];
    console.log(`${Server.BASE_URL_API}${ApiService.formBuilder}${formCode}`);
    fetch(`${Server.BASE_URL_API}${ApiService.formBuilder}${formCode}`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({loadingvisible: false});
        if (responseJson.code < 300) {
          const response = JSON.parse(responseJson.data.data);
          response.map((element, index) => {
            element.components.map(componentx => {
              dataresp.push({
                component: componentx.name,
                field_name: componentx.placeholder.placeholderIdn,
                is_mandatory: 'false',
                type:
                  componentx.label.labelIdn === 'NIK'
                    ? 'number'
                    : componentx.type,
                meta: {
                  multiline: componentx.name === 'TextArea' ? true : false,
                  numberOfLines: componentx.name === 'TextArea' ? 2 : 1,
                  listdd:
                    componentx.name === 'Dropdown' ||
                    componentx.name === 'Datalist'
                      ? this.getDropdown(componentx.url)
                      : componentx.name === 'DropdownSumberDana'
                      ? {
                          placeholder:
                            bahasa === 'id'
                              ? '== Silakan Pilih =='
                              : '== Please Choose ==',
                          list: this.state.dataPortofolio,
                        }
                      : '',
                  url: componentx.url,
                  label:
                    bahasa === 'id'
                      ? componentx.label.labelIdn
                      : componentx.label.labelEng,
                  pointer:
                    componentx.label.labelIdn === ''
                      ? element.components[index - 1].label.labelIdn
                      : componentx.label.labelIdn,
                  placeholder:
                    bahasa === 'id'
                      ? componentx.placeholder.placeholderIdn
                      : componentx.placeholder.placeholderEng,
                },
              });
            });
          });
          this.setState({sessionTransaksi: 1, setRefreshing: false});
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        console.log('error', error);
        Alert.alert('Gagal Terhubung ke server', error);
      });
  }
  async createQRCode(formFields) {
    if (this.state.jenisTransaksiId === 1) {
      var payload = {
        jenisTransaksi: formFields.jenislayanan.label,
        data: {
          bankpenerima: formFields.bankpenerima,
          jenislayanan: formFields.jenislayanan.label,
          nominaltransaksi: formFields.nominaltransaksi,
          rekeningpenerima: formFields.rekeningpenerima,
          tujuantransaksi: formFields.tujuantransaksi.label,
          berita: formFields.berita,
        },
        tanggal: moment().format('YYYY-MM-DD HH:mm:ss'),
      };
    } else if (this.state.jenisTransaksiId === 2) {
      var payload = {
        jenisTransaksi: 'ANTAR REKENING',
        data: {
          nominaltransaksi: formFields.nominaltransaksi,
          rekeningpenerima: formFields.rekeningpenerima,
          tujuantransaksi: formFields.tujuantransaksi.label,
          berita: formFields.berita,
        },
        tanggal: moment().format('YYYY-MM-DD HH:mm:ss'),
      };
    } else if (this.state.jenisTransaksiId === 3) {
      var payload = {
        jenisTransaksi: 'REKENING PRIBADI',
        data: {
          nominaltransaksi: formFields.nominaltransaksi,
          rekeningpenerima: formFields.rekeningpenerima,
          berita: formFields.berita,
        },
        tanggal: moment().format('YYYY-MM-DD HH:mm:ss'),
      };
    }
    fetch(Server.BASE_URL_API + ApiService.createQRForm, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: JSON.stringify(payload),
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.code < 300) {
          this.downloadQR(responseJson.data);
          this.setState({base64QR: responseJson.data, sessionTransaksi: 2});
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        console.error(error);
        Alert.alert('Gagal Terhubung ke server');
      });
  }
  downloadQR = base64_QR => {
    CameraRoll.save(base64_QR, {type: 'photo'})
      .then(result => {
        Alert.alert(
          bahasa == 'id' ? id.save_barcode_redaksi : en.save_barcode_redaksi,
        );
      })
      .catch(err => {});
  };
  form_transaksi() {
    if (this.state.sessionTransaksi === 0) {
      return (
        <SafeAreaView
          style={{
            alignSelf: 'center',
            flexDirection: 'column',
          }}>
          <View
            style={{
              backgroundColor: 'white',
              flexDirection: 'row',
            }}>
            <Pressable
              onPress={() => {
                this.countdown.resetCountdown();
                this.setState({isPilihButton: true});
              }}
              style={{marginEnd: 10, alignSelf: 'center'}}>
              <FastImage
                style={{
                  width: 30,
                  height: 30,
                }}
                source={back}
              />
            </Pressable>
            <Text
              style={{
                marginTop: 15,
                marginBottom: 15,
                fontFamily: 'Helvetica',
                fontSize: 18,
                fontWeight: 'bold',
                borderBottomWidth: 1,
                borderBottomColor: 'black',
              }}>
              {bahasa === 'id' ? id.transaksi : en.transaksi}
            </Text>
          </View>
          <FlatList
            data={this.formatData(
              this.state.itemLayanan,
              this.state.numColumns,
            )}
            renderItem={({item, index}) => (
              <TouchableOpacity
                key={index}
                onPress={() => {
                  if (item.id == 1) {
                    //Transaksi Antar Bank
                    this.setState({
                      sessionTransaksi: 1,
                      jenisTransaksiId: 1,
                      formCode: 44,
                    });
                    this.formBuilder(44);
                  } else if (item.id == 2) {
                    //Transaksi Antar Rekening
                    this.setState({
                      sessionTransaksi: 1,
                      jenisTransaksiId: 2,
                      formCode: 47,
                    });
                    this.formBuilder(47);
                  } else if (item.id == 3) {
                    //Transaksi Rekening Pribadi
                    this.setState({
                      sessionTransaksi: 1,
                      jenisTransaksiId: 3,
                      formCode: 57,
                    });
                    this.formBuilder(57);
                  } else if (item.id == 4) {
                    //Transaksi Tanpa Kartu
                    this.setState({
                      sessionTransaksi: 1,
                      jenisTransaksiId: 4,
                      formCode: 53,
                    });
                    this.formBuilder(53);
                  }
                }}
                style={{
                  alignSelf: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                  alignContent: 'center',
                  width: 121,
                  marginTop: 10,
                  justifyContent: 'flex-start',
                  flexDirection: 'column',
                  height: 165,
                }}>
                <FastImage
                  style={{
                    width: 100,
                    height: 100,
                    alignContent: 'center',
                    alignSelf: 'center',
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginBottom: 10,
                    borderRadius: 13,
                  }}
                  source={item.img}
                />
                <Text
                  ellipsizeMode="tail"
                  textAlign="center"
                  numberOfLines={2}
                  style={{
                    width: 121,
                    color: 'black',
                    fontSize: 15,
                    fontWeight: 'bold',
                    textAlign: 'center',
                    fontFamily: 'Helvetica',
                  }}>
                  {item.title}
                </Text>
              </TouchableOpacity>
            )}
            numColumns={this.state.numColumns}
            keyExtractor={item => item.id}
          />
        </SafeAreaView>
      );
    } else if (this.state.sessionTransaksi === 1) {
      return (
        <SafeAreaView
          style={{
            alignSelf: 'center',
            flexDirection: 'column',
          }}>
          <View
            style={{
              marginStart: 20,
              width: 370,
              backgroundColor: 'white',
              flexDirection: 'row',
            }}>
            <Pressable
              onPress={() => {
                arrayBarcode = [];
                this.setState({
                  sessionTransaksi: 0,
                  isChecked: false,
                  isCheckedTNC: false,
                  uriKTP: '',
                });
              }}
              style={{marginEnd: 10, alignSelf: 'center'}}>
              <FastImage
                style={{
                  width: 30,
                  height: 30,
                }}
                source={back}
              />
            </Pressable>
            <Text
              style={{
                marginTop: 15,
                marginBottom: 15,
                fontFamily: 'Helvetica',
                fontSize: 18,
                fontWeight: 'bold',
                borderBottomWidth: 1,
                borderBottomColor: 'black',
              }}>
              {this.state.jenisTransaksiId == 1
                ? bahasa === 'id'
                  ? id.transaksi_antar_bank
                  : en.transaksi_antar_bank
                : this.state.jenisTransaksiId == 2
                ? bahasa === 'id'
                  ? id.transaksi_antar_rekening
                  : en.transaksi_antar_rekening
                : this.state.jenisTransaksiId == 3
                ? bahasa === 'id'
                  ? id.transaksi_rekening_pribadi
                  : en.transaksi_rekening_pribadi
                : this.state.jenisTransaksiId == 4
                ? bahasa === 'id'
                  ? id.transaksi_tanpa_kartu
                  : en.transaksi_tanpa_kartu
                : ''}
            </Text>
          </View>
          <ScrollView
            contentContainerStyle={{paddingStart: 20, paddingEnd: 20}}
            keyboardDismissMode="none"
            automaticallyAdjustKeyboardInsets="true"
            automaticallyAdjustContentInsets="true"
            refreshControl={
              <RefreshControl
                refreshing={this.state.setRefreshing}
                onRefresh={this.onRefresh}
              />
            }>
            <Text
              style={{
                display:
                  this.state.jenisTransaksiId === 4 && dataresp.length > 0
                    ? 'flex'
                    : 'none',
                fontSize: 16,
                marginStart: 10,
                marginEnd: 10,
                marginBottom: 10,
              }}>
              {bahasa == 'id' ? id.jenis_layanan : en.jenis_layanan}
            </Text>
            <View
              style={{
                display: this.state.jenisTransaksiId === 4 ? 'flex' : 'none',
                marginStart: 10,
                marginBottom: 20,
                marginEnd: 10,
                justifyContent: 'space-between',
                flexDirection: 'row',
              }}>
              <Button
                onPress={() => {
                  this.setState({
                    sessionTransaksi: 1,
                    jenisTransaksiId: 4,
                    formCode: 67,
                    isChecked: false,
                    isCheckedTNC: false,
                    uriKTP: '',
                    jenisCardless:
                      bahasa == 'id' ? id.tarik_tunai : en.tarik_tunai,
                  });
                  this.formBuilder(67);
                }}
                style={[
                  {
                    display: dataresp.length === 0 ? 'none' : 'flex',
                    borderRadius: 12,
                    width: '48%',
                    borderColor: COLORS.red,
                    borderWidth: 2,
                    alignSelf: 'flex-end',
                  },
                ]}
                labelStyle={styles.btnLabel}
                textColor={this.state.formCode === 53 ? COLORS.red : 'white'}
                buttonColor={this.state.formCode === 53 ? 'white' : COLORS.red}
                mode="contained">
                {bahasa == 'id' ? id.tarik_tunai : en.tarik_tunai}
              </Button>
              <Button
                onPress={() => {
                  this.setState({
                    sessionTransaksi: 1,
                    jenisTransaksiId: 4,
                    formCode: 53,
                    isChecked: false,
                    isCheckedTNC: false,
                    uriKTP: '',
                    jenisCardless:
                      bahasa == 'id' ? id.setor_tunai : en.setor_tunai,
                  });
                  this.formBuilder(53);
                }}
                style={[
                  {
                    display: dataresp.length === 0 ? 'none' : 'flex',
                    width: '48%',
                    borderRadius: 12,
                    borderColor: COLORS.red,
                    borderWidth: 2,
                    alignSelf: 'flex-end',
                  },
                ]}
                labelStyle={styles.btnLabel}
                textColor={this.state.formCode === 53 ? 'white' : COLORS.red}
                buttonColor={this.state.formCode === 53 ? COLORS.red : 'white'}
                mode="contained">
                {bahasa == 'id' ? id.setor_tunai : en.setor_tunai}
              </Button>
            </View>
            <DynamicForm
              bahasa={bahasa}
              accessToken={accessToken}
              exchangeToken={exchangeToken}
              formCode={this.state.formCode}
              formTemplate={{data: dataresp}}
              onSubmit={this.onSubmited}
              defaultValue={arrayBarcode.length > 0 ? arrayBarcode[0] : {}}
              onPressed={this.onSubmitTransaction}
              btnStyle={{
                borderRadius: 12,
                backgroundColor: COLORS.red,
                top: 20,
                width: 310,
                display: this.state.jenisTransaksiId === 4 ? 'none' : 'flex',
                alignSelf: 'center',
                alignItems: 'center',
                justifyContent: 'center',
              }}
              btnLabel={bahasa === 'id' ? id.lanjut : en.lanjut}
            />
            <CheckBox
              label={bahasa == 'id' ? id.kuasa : en.kuasa}
              status={this.state.isChecked ? 'checked' : 'unchecked'}
              onPress={() => {
                if (this.state.isChecked) {
                  this.setState({
                    isChecked: false,
                    isCheckedTNC: false,
                    uriKTP: '',
                  });
                } else {
                  this.setState({isChecked: true});
                }
              }}
              containerStyle={{
                marginBottom: 10,
                display:
                  this.state.jenisTransaksiId === 4 && dataresp.length > 0
                    ? 'flex'
                    : 'none',
              }}
              labelStyle={{fontSize: 16}}
              boxColor={COLORS.red}
            />
            <View
              style={{
                display: this.state.isChecked ? 'flex' : 'none',
              }}>
              <Text
                style={{
                  fontFamily: 'Helvetica',
                  fontSize: 16,
                  marginLeft: 10,
                  marginBottom: 10,
                  color: '#54575C',
                }}>
                {bahasa === 'id' ? id.unggah_ktp_kuasa : en.unggah_ktp_kuasa}
              </Text>
              {this.state.uriKTP === '' ? (
                <View
                  style={{
                    flexDirection: 'column',
                    alignItems: 'center',
                    marginBottom: 10,
                    marginStart: 10,
                    marginEnd: 10,
                    display: this.state.uriKTP === '' ? 'flex' : 'none',
                    justifyContent: 'center',
                    height: 250,
                  }}>
                  <FastImage
                    transition={false}
                    resizeMode="stretch"
                    style={{
                      width: '100%',
                      height: '100%',
                      justifyContent: 'center',
                    }}
                    source={bgPickImage}>
                    <View
                      style={{
                        padding: 50,
                        justifyContent: 'center',
                        flexDirection: 'column',
                        alignItems: 'center',
                      }}>
                      <Pressable
                        onPress={() => {
                          this.setState({openCamera: true});
                        }}>
                        <FastImage
                          source={pickCamera}
                          style={{
                            marginBottom: 20,
                            width: 60,
                            height: 60,
                          }}
                        />
                      </Pressable>
                      <View
                        style={{
                          alignItems: 'center',
                          justifyContent: 'center',
                          flexDirection: 'row',
                          width: '33%',
                          height: '20%',
                        }}>
                        <View
                          style={{
                            marginStart: 10,
                            marginEnd: 10,
                            backgroundColor: COLORS.textColorGray,
                            width: '100%',
                            height: '2.5%',
                          }}></View>
                        <Text
                          style={{
                            fontFamily: 'Helvetica',
                            color: COLORS.textColorGray,
                            fontSize: 17,
                          }}>
                          {bahasa === 'id' ? id.atau : en.atau}
                        </Text>
                        <View
                          style={{
                            marginEnd: 10,
                            marginStart: 10,
                            backgroundColor: COLORS.textColorGray,
                            width: '100%',
                            height: '2.5%',
                          }}></View>
                      </View>
                      <Pressable
                        style={{
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}
                        onPress={() => {
                          this.launchImageLibraryfunc();
                        }}>
                        <View
                          style={{
                            marginTop: 20,
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                          }}>
                          <FastImage
                            source={pickGallery}
                            style={{
                              marginEnd: 8,
                              width: 26,
                              height: 26,
                            }}
                          />
                          <Text
                            style={{
                              fontFamily: 'Helvetica',
                              color: COLORS.textColorGray,
                              fontSize: 15,
                            }}>
                            {bahasa === 'id'
                              ? id.pilih_dari_galeri
                              : en.pilih_dari_galeri}
                          </Text>
                        </View>
                      </Pressable>
                    </View>
                  </FastImage>
                </View>
              ) : (
                <View>
                  <View
                    style={{
                      display: this.state.uriKTP === '' ? 'none' : 'flex',
                      flexDirection: 'column',
                      alignItems: 'center',
                      marginBottom: 10,
                      marginStart: 10,
                      marginEnd: 10,
                      justifyContent: 'center',
                      height: 250,
                    }}>
                    <FastImage
                      transition={false}
                      resizeMode="stretch"
                      style={{
                        justifyContent: 'center',
                        width: '100%',
                        height: '100%',
                      }}
                      source={bgPickImage}>
                      <View
                        style={{
                          padding: 20,
                          width: '100%',
                          height: '100%',
                        }}>
                        <FastImage
                          resizeMode="contain"
                          style={{width: '100%', height: '100%'}}
                          source={{
                            uri: this.state.uriKTP,
                            priority: FastImage.priority.normal,
                          }}
                        />
                      </View>
                    </FastImage>
                  </View>
                  <Pressable
                    onPress={() => {
                      this.setState({uriKTP: ''});
                      base64KTP = '';
                    }}>
                    <FastImage
                      transition={false}
                      style={{
                        marginStart: 10,
                        marginBottom: 10,
                        width: 45,
                        height: 45,
                      }}
                      source={changeImage}
                    />
                  </Pressable>
                </View>
              )}
              <Text
                style={{
                  fontFamily: 'Helvetica',
                  fontSize: 16,
                  marginLeft: 10,
                  marginBottom: 10,
                  color: '#54575C',
                }}>
                {bahasa === 'id' ? id.no_hp_kuasa : en.no_hp_kuasa}
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  width: '100%',
                  marginBottom: 10,
                  marginStart: 10,
                  marginEnd: 10,
                }}>
                <View
                  style={{
                    borderTopStartRadius: 6,
                    borderBottomStartRadius: 6,

                    height: 50,
                    padding: 10,
                    justifyContent: 'center',
                    backgroundColor: '#50A0FF',
                  }}>
                  <Text
                    style={{
                      fontWeight: 'bold',
                      fontSize: 16,
                      color: 'white',
                    }}>
                    +62
                  </Text>
                </View>
                <TextInputMask
                  style={{
                    width: '82%',
                    height: 50,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 1.5,
                    borderTopEndRadius: 6,
                    borderBottomEndRadius: 6,
                    backgroundColor: 'white',
                    paddingLeft: 10,
                    fontSize: 16,
                  }}
                  type="only-numbers"
                  value={this.state.noHpKuasa}
                  onChangeText={text => {
                    if (text.startsWith('0')) {
                      this.setState({noHpKuasa: text.substring(1)});
                    } else {
                      this.setState({noHpKuasa: text});
                    }
                  }}
                />
              </View>
              <CheckBox
                label={bahasa == 'id' ? id.tnc_cardless : en.tnc_cardless}
                status={this.state.isCheckedTNC ? 'checked' : 'unchecked'}
                onPress={() => {
                  if (this.state.isCheckedTNC) {
                    this.setState({isCheckedTNC: false});
                  } else {
                    this.setState({isCheckedTNC: true});
                  }
                }}
                containerStyle={{
                  marginBottom: 10,
                  marginEnd: 20,
                  display:
                    this.state.jenisTransaksiId === 4 && dataresp.length > 0
                      ? 'flex'
                      : 'none',
                }}
                labelStyle={{fontSize: 16, textAlign: 'justify', marginEnd: 20}}
                boxColor={COLORS.red}
              />
            </View>
            <Button
              onPress={() => {
                if (
                  parseInt(arrayBarcode[0]?.nominal?.replace(/\./g, ''), 10) >=
                  parseInt(arrayBarcode[0]?.pilihrekening?.availBalance) / 100
                ) {
                  Alert.alert(
                    bahasa === 'id'
                      ? id.saldo_tidak_mecukupi
                      : en.saldo_tidak_mecukupi,
                  );
                } else {
                  if (!this.state.isChecked) {
                    this.setState({
                      sessionTransaksi: 3,
                    });
                  } else {
                    if (this.state.isChecked && this.state.isCheckedTNC) {
                      this.setState({
                        sessionTransaksi: 3,
                      });
                    } else {
                      Alert.alert(
                        bahasa === 'id'
                          ? 'Silahkan setujui syarat & ketentuan'
                          : 'Please Agree Terms & Condition',
                      );
                    }
                  }
                }
              }}
              style={[
                styles.btn,
                {
                  width: '100%',
                  display:
                    this.state.jenisTransaksiId === 4 && dataresp.length > 0
                      ? 'flex'
                      : 'none',
                },
              ]}
              labelStyle={styles.btnLabel}
              buttonColor={COLORS.red}
              mode="contained">
              {bahasa == 'id' ? id.selanjutnya : en.selanjutnya}
            </Button>
          </ScrollView>
        </SafeAreaView>
      );
    } else if (this.state.sessionTransaksi == 2) {
      return (
        <SafeAreaView
          style={{
            alignSelf: 'center',
            flexDirection: 'column',
          }}>
          <Text
            style={{
              marginTop: 15,
              marginBottom: 15,
              fontFamily: 'Helvetica',
              fontSize: 18,
              textAlign: 'center',
              fontWeight: 'bold',
              borderBottomWidth: 1,
              borderBottomColor: 'black',
            }}>
            {this.state.jenisTransaksiId == 1
              ? bahasa === 'id'
                ? id.transaksi_antar_bank
                : en.transaksi_antar_bank
              : this.state.jenisTransaksiId == 2
              ? bahasa === 'id'
                ? id.transaksi_antar_rekening
                : en.transaksi_antar_rekening
              : ''}
          </Text>
          <ScrollView
            contentContainerStyle={{paddingStart: 20, paddingEnd: 20}}
            keyboardDismissMode="none"
            automaticallyAdjustKeyboardInsets="true"
            automaticallyAdjustContentInsets="true">
            <View
              style={{
                height: '100%',
                padding: 30,
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 10,
                  marginBottom: 20,
                  backgroundColor: 'white',
                  height: 60,
                  width: '100%',
                }}>
                <FastImage
                  source={logoSuccess}
                  resizeMode="contain"
                  style={{width: '100%', height: '100%'}}
                />
              </View>
              <Text
                textAlign="center"
                numberOfLines={5}
                style={{
                  marginBottom: 20,
                  fontFamily: 'Helvetica',
                  fontSize: 16,
                  textAlign: 'center',
                  alignItems: 'center',
                  textAlign: 'center',
                  justifyContent: 'center',
                }}>
                {bahasa === 'id' ? id.headline_qrcode : en.headline_qrcode}
              </Text>
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginBottom: 20,
                  borderRadius: 10,
                  backgroundColor: 'white',
                  height: 250,
                  width: '100%',
                }}>
                <FastImage
                  source={{
                    uri: encodeURI(this.state.base64QR),

                    priority: FastImage.priority.high,
                  }}
                  // source={qrcode}
                  resizeMode="contain"
                  style={{width: '100%', height: '100%'}}
                />
              </View>
              <Text
                textAlign="center"
                numberOfLines={5}
                style={{
                  marginBottom: 20,
                  fontFamily: 'Helvetica',
                  fontSize: 16,
                  textAlign: 'center',
                  alignItems: 'center',
                  textAlign: 'center',
                  justifyContent: 'center',
                }}>
                {bahasa === 'id' ? id.redaksi_qr : en.redaksi_qr}
              </Text>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <Button
                  onPress={() => {
                    this.setState({sessionTransaksi: 0});
                  }}
                  style={[
                    styles.btn,
                    {
                      width: 'auto',
                      alignSelf: 'center',
                    },
                  ]}
                  textColor={COLORS.red}
                  labelStyle={styles.btnLabel}
                  buttonColor={COLORS.white}
                  mode="contained">
                  {bahasa === 'id' ? id.menu_transaksi : en.menu_transaksi}
                </Button>
                <Button
                  onPress={() => {
                    this.setState({isPilihButton: true});
                    this.props.navigation.navigate('DipsWaitingRoom');
                  }}
                  style={[
                    styles.btn,
                    {
                      width: 'auto',
                      alignSelf: 'center',
                    },
                  ]}
                  labelStyle={styles.btnLabel}
                  buttonColor={COLORS.red}
                  mode="contained">
                  {bahasa === 'id' ? id.hubungkan : en.hubungkan}
                </Button>
              </View>
            </View>
          </ScrollView>
        </SafeAreaView>
      );
    } else if (this.state.sessionTransaksi == 3) {
      return (
        <SafeAreaView
          style={{
            width: '100%',
            height: '100%',
            alignSelf: 'center',
            flexDirection: 'column',
          }}>
          <View
            style={{
              marginStart: 10,
              width: 370,
              backgroundColor: 'white',
              flexDirection: 'row',
            }}>
            <Pressable
              onPress={() => {
                this.setState({
                  sessionTransaksi: 1,
                });
              }}
              style={{marginEnd: 10, alignSelf: 'center'}}>
              <FastImage
                style={{
                  width: 30,
                  height: 30,
                }}
                source={back}
              />
            </Pressable>
            <Text
              style={{
                marginTop: 15,
                marginBottom: 15,
                fontFamily: 'Helvetica',
                fontSize: 18,
                fontWeight: 'bold',
                borderBottomWidth: 1,
                borderBottomColor: 'black',
              }}>
              {bahasa === 'id'
                ? id.konfirmasi + ' ' + this.state.jenisCardless
                : en.konfirmasi + ' ' + this.state.jenisCardless}
            </Text>
          </View>
          <ScrollView
            contentContainerStyle={{paddingStart: 20, paddingEnd: 20}}
            keyboardDismissMode="none"
            automaticallyAdjustKeyboardInsets="true"
            automaticallyAdjustContentInsets="true">
            <View
              style={{
                marginTop: 15,
                marginEnd: 10,
                flexDirection: 'column',
              }}>
              <Text
                style={{
                  fontSize: 15,
                  fontWeight: 'bold',
                  marginEnd: 10,
                  marginBottom: 10,
                }}>
                {bahasa == 'id' ? id.nomor_referensi : en.nomor_referensi}
              </Text>
              <Text
                style={{
                  fontSize: 15,
                  marginEnd: 10,
                  marginBottom: 10,
                }}>
                {'-'}
              </Text>

              <Text
                style={{
                  fontSize: 15,
                  fontWeight: 'bold',
                  marginEnd: 10,
                  marginBottom: 10,
                }}>
                {bahasa == 'id' ? id.jenis_layanan : en.jenis_layanan}
              </Text>
              <Text
                style={{
                  fontSize: 15,
                  marginEnd: 10,
                  marginBottom: 10,
                }}>
                {this.state.jenisCardless}
              </Text>

              <Text
                style={{
                  fontSize: 15,
                  fontWeight: 'bold',
                  marginEnd: 10,
                  marginBottom: 10,
                }}>
                {bahasa == 'id'
                  ? id.rekening_sumber_dana
                  : en.rekening_sumber_dana}
              </Text>
              <Text
                style={{
                  fontSize: 15,
                  marginEnd: 10,
                  marginBottom: 10,
                }}>
                {arrayBarcode[0]?.pilihrekening !== undefined
                  ? arrayBarcode[0].pilihrekening.accountNo
                  : '-'}
              </Text>

              <Text
                style={{
                  fontSize: 15,
                  fontWeight: 'bold',
                  marginEnd: 10,
                  marginBottom: 10,
                }}>
                {bahasa == 'id'
                  ? id.nama_pemilik_rekening
                  : en.nama_pemilik_rekening}
              </Text>
              <Text
                style={{
                  fontSize: 15,
                  marginEnd: 10,
                  marginBottom: 10,
                }}>
                {arrayBarcode[0]?.pilihrekening !== undefined
                  ? arrayBarcode[0].pilihrekening.accountName
                  : '-'}
              </Text>

              <Text
                style={{
                  fontSize: 15,
                  fontWeight: 'bold',
                  marginEnd: 10,
                  marginBottom: 10,
                }}>
                {bahasa == 'id' ? id.nominal : en.nominal}
              </Text>
              <Text
                style={{
                  fontSize: 15,
                  marginEnd: 10,
                  marginBottom: 10,
                }}>
                {arrayBarcode[0]?.nominal !== undefined
                  ? 'IDR ' + arrayBarcode[0].nominal
                  : 'IDR -'}
              </Text>

              <Text
                style={{
                  fontSize: 15,
                  fontWeight: 'bold',
                  marginEnd: 10,
                  marginBottom: 10,
                }}>
                {bahasa == 'id' ? id.berita : en.berita}
              </Text>
              <Text
                style={{
                  fontSize: 15,
                  marginEnd: 10,
                  marginBottom: 20,
                }}>
                {arrayBarcode[0]?.berita !== undefined &&
                arrayBarcode[0]?.berita.length !== 0
                  ? arrayBarcode[0].berita
                  : '-'}
              </Text>
              <View
                style={{
                  borderColor: COLORS.border_textInput,
                  borderTopWidth: 1.5,
                  marginBottom: 20,
                  display: this.state.isChecked ? 'flex' : 'none',
                }}>
                <Text
                  style={{
                    fontSize: 20,
                    fontWeight: 'bold',
                    marginTop: 20,
                    marginEnd: 10,
                    marginBottom: 20,
                  }}>
                  {bahasa == 'id' ? id.kuasa : en.kuasa}
                </Text>
                <Text
                  style={{
                    fontSize: 15,
                    fontWeight: 'bold',
                    marginEnd: 10,
                    marginBottom: 10,
                  }}>
                  {bahasa == 'id' ? id.nama_kuasa : en.nama_kuasa}
                </Text>
                <Text
                  style={{
                    fontSize: 15,
                    marginEnd: 10,
                    marginBottom: 10,
                  }}>
                  -
                </Text>
                <Text
                  style={{
                    fontSize: 15,
                    fontWeight: 'bold',
                    marginEnd: 10,
                    marginBottom: 10,
                  }}>
                  {bahasa == 'id' ? id.nik_kuasa : en.nik_kuasa}
                </Text>
                <Text
                  style={{
                    fontSize: 15,
                    marginEnd: 10,
                    marginBottom: 10,
                  }}>
                  -
                </Text>
                <Text
                  style={{
                    fontSize: 15,
                    fontWeight: 'bold',
                    marginEnd: 10,
                    marginBottom: 10,
                  }}>
                  {bahasa == 'id' ? id.no_hp_kuasa : en.no_hp_kuasa}
                </Text>
                <Text
                  style={{
                    fontSize: 15,
                    marginEnd: 10,
                    marginBottom: 10,
                  }}>
                  {'0' + this.state.noHpKuasa}
                </Text>
                <Text
                  style={{
                    fontSize: 20,
                    fontWeight: 'bold',
                    marginEnd: 10,
                  }}>
                  {bahasa == 'id' ? id.lampiran : en.lampiran}
                </Text>
              </View>
            </View>
          </ScrollView>
          <Button
            onPress={() => {}}
            style={[
              styles.btn,
              {
                margin: 20,
              },
            ]}
            labelStyle={styles.btnLabel}
            buttonColor={COLORS.red}
            mode="contained">
            {bahasa == 'id' ? id.selanjutnya : en.selanjutnya}
          </Button>
        </SafeAreaView>
      );
    }
  }
  takePicture = async () => {
    if (this.camera) {
      const options = {quality: 0.5, base64: true};
      const data = await this.camera.takePictureAsync(options);
      var cropData = {
        offset: {x: 132, y: 830},
        size: {width: 1620, height: 2300},
        displaySize: {width: 700, height: 440},
        resizeMode: 'cover',
      };
      //Crop Image by border
      ImageEditor.cropImage(data.uri, cropData).then(url => {
        //Convert Uri to Base64
        RNFS.readFile(url, 'base64').then(res => {
          //KTP
          base64KTP = res;
          this.setState({
            openCamera: false,
            uriKTP: url,
          });
          this.countdown.pauseCountdown();
        });
      });
    }
  };
  popUpDoneSchedule() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.modalScheduleDone}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({modalScheduleDone: false});
        }}>
        <View
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View
              style={{
                margin: 100,
                backgroundColor: 'white',
                borderRadius: 20,
                justifyContent: 'space-between',
                padding: 35,
                width: '80%',
                alignItems: 'center',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
              }}>
              <FastImage
                source={logoSuccess}
                style={{width: 55, height: 55, marginBottom: 15}}
              />
              <Text
                style={{
                  marginBottom: 20,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  textAlign: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {bahasa === 'id'
                  ? id.konten_sukses_jadwalkan
                  : en.konten_sukses_jadwalkan}
              </Text>
              <Pressable
                style={{
                  width: '40%',
                  height: 43,
                  borderRadius: 10,
                  padding: 10,
                  elevation: 2,
                  backgroundColor: COLORS.red,
                }}
                onPress={() => {
                  this.setState({modalScheduleDone: false});
                  // RNExitApp.exitApp();
                }}>
                <Text style={styles.textbtn}>
                  {bahasa === 'id' ? id.selesai : en.selesai}
                </Text>
              </Pressable>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
  loadingBar() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.loadingvisible}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({loadingvisible: false});
        }}>
        <View
          style={{
            backgroundColor: 'rgba(255,255,255, 0.8)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View style={styles.centeredView}>
              <View style={styles.containerLoading}>
                <FastImage
                  transition={false}
                  source={loadingvictoria}
                  style={styles.imgSize}
                />
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
  launchImageLibraryfunc = () => {
    var options = {
      mediaType: 'photo',
      includeBase64: true,
      maxHeight: 200,
      maxWidth: 200,
    };
    launchImageLibrary(options, response => {
      if (response.didCancel) {
      } else if (response.error) {
      } else if (response.customButton) {
        alert(response.customButton);
      } else {
        base64KTP = response.assets[0].base64;
        this.setState({uriKTP: response.assets[0].uri});
      }
    });
  };
  render() {
    if (this.state.openCamera) {
      return (
        <View
          style={[
            styles.container,
            {
              backgroundColor: 'white',
              alignItems: 'center',
              position: 'relative',
            },
          ]}>
          <StatusBar hidden="true" />
          <View
            style={{
              width: '100%',
              height: '100%',
            }}>
            <RNCamera
              captureAudio={false}
              ref={ref => {
                this.camera = ref;
              }}
              style={{
                justifyContent: 'space-between',
                alignSelf: 'center',
                alignItems: 'center',
                height: '100%',
                flexDirection: 'column',
                width: '100%',
              }}
              type={this.state.session == 5 ? 'front' : 'back'}>
              <View
                style={{
                  padding: 20,
                  justifyContent: 'flex-end',
                  width: '100%',
                  height: '10%',
                  backgroundColor: COLORS.red,
                }}>
                <Pressable
                  onPress={() => {
                    this.setState({openCamera: false});
                  }}>
                  <Text
                    style={{
                      textAlign: 'left',
                      color: 'white',
                      fontFamily: 'Helvetica',
                      fontSize: 20,
                    }}>
                    {bahasa == 'id' ? id.kembali : en.kembali}
                  </Text>
                </Pressable>
              </View>
              <View
                style={{
                  marginTop: this.state.session == 5 ? 350 : 0,
                  alignItems: 'center',
                  width: '85%',
                  height: '25%',
                  borderRadius: 12,
                  borderColor: COLORS.red,
                  borderWidth: 4,
                  borderStyle: 'dashed',
                }}
              />
              <View
                style={{
                  borderTopStartRadius: 15,
                  borderTopEndRadius: 15,
                  alignItems: 'center',
                  justifyContent: 'center',
                  width: '100%',
                  height: '14%',
                  backgroundColor: 'rgba(0, 0, 0, 0.3)',
                }}>
                <Pressable onPress={this.takePicture}>
                  <View
                    style={{
                      width: 70,
                      height: 70,
                      borderRadius: 70 / 2,
                      borderWidth: 10,
                      borderColor: COLORS.red,
                      backgroundColor: 'white',
                    }}
                  />
                </Pressable>
              </View>
            </RNCamera>
          </View>
        </View>
      );
    } else {
      return (
        <View
          style={{
            flex: 1,
            backgroundColor: 'white',
          }}>
          <StatusBar hidden="true" />
          {this.popUpScheduler()}
          {this.popUpDoneSchedule()}
          {this.loadingBar()}
          <View
            style={{
              flexDirection: 'column',
              justifyContent: 'flex-end',
            }}>
            <ImageBackground
              transition={false}
              style={{
                width: '100%',
                height: 210,
                bottom: 0,
                position: 'absolute',
              }}
              imageStyle={{
                width: '100%',
                height: '100%',
                resizeMode: 'contain',
                alignItems: 'flex-end',
              }}
              source={batikbvic}
            />
            <View
              style={{
                height: '100%',
                flexDirection: 'column',
              }}>
              <View
                style={{
                  height: this.state.isPilihButton ? '100%' : '90%',
                }}>
                <View
                  style={{
                    display: this.state.isPilihButton ? 'flex' : 'none',
                    alignSelf: 'center',
                    justifyContent: 'center',
                    flexDirection: 'column',
                    height: '100%',
                  }}>
                  <Text
                    style={{
                      fontFamily: 'Helvetica',
                      fontSize: 17,
                      textAlign: 'center',
                      marginBottom: 15,
                    }}>
                    {bahasa === 'id'
                      ? id.hubungkan_dengan_agent_sekarang
                      : en.hubungkan_dengan_agent_sekarang}
                  </Text>
                  <Button
                    style={[
                      styles.btn,
                      {
                        width: '100%',
                        alignSelf: 'center',
                      },
                    ]}
                    labelStyle={styles.btnLabel}
                    buttonColor={COLORS.red}
                    mode="contained"
                    onPress={() => {
                      this.props.navigation.navigate('DipsWaitingRoom');
                    }}>
                    {bahasa == 'id' ? id.hubungkan : en.hubungkan}
                  </Button>
                  <Text
                    style={{
                      textAlign: 'center',
                      fontFamily: 'Helvetica',
                      fontSize: 17,
                      marginTop: 15,
                      marginBottom: 15,
                    }}>
                    {bahasa === 'id' ? id.atau : en.atau}
                  </Text>
                  <Button
                    style={[
                      styles.btn,
                      {
                        width: '100%',
                        alignSelf: 'center',
                      },
                    ]}
                    labelStyle={styles.btnLabel}
                    buttonColor={COLORS.red}
                    mode="contained"
                    onPress={() => {
                      this.countdown.pauseCountdown();
                      this.getNoRekening();
                      this.setState({isPilihButton: false});
                    }}>
                    {bahasa == 'id' ? id.form_transaksi : en.form_transaksi}
                  </Button>
                  <Countdown
                    ref={ref => (this.countdown = ref)}
                    durationInSeconds={30}
                    bahasa={bahasa}
                    onlyMinutes={true}
                    styles={{
                      marginTop: 25,
                      textAlign: 'center',
                      fontFamily: 'Helvetica',
                      fontSize: 17,
                    }}
                    onFinish={() => {
                      console.log('Countdown Selesai');
                      this.props.navigation.navigate('DipsWaitingRoom');
                    }}
                  />
                  <Text
                    style={{
                      textAlign: 'center',
                      fontFamily: 'Helvetica',
                      fontSize: 17,
                    }}>
                    {bahasa === 'id'
                      ? id.otomatis_menghubungkan_dengan_agent
                      : en.otomatis_menghubungkan_dengan_agent}
                  </Text>
                </View>
                <View style={{height: '100%'}}>{this.form_transaksi()}</View>
              </View>
              <View
                style={{
                  display: this.state.isPilihButton ? 'none' : 'flex',
                  width: '100%',
                  height: '10%',
                  backgroundColor: 'white',
                  justifyContent: 'center',
                  alignItems: 'center',
                  position: 'absolute',
                  bottom: 0,
                }}>
                <View style={{flexDirection: 'row', bottom: 0}}>
                  <Button
                    textColor={COLORS.red}
                    labelStyle={styles.btnLabel}
                    style={[styles.btn, {marginEnd: 10}]}
                    mode="outlined"
                    onPress={() => {
                      this.setState({modalscheduler: true});
                      backgroundServer.stop();
                    }}>
                    {bahasa == 'id'
                      ? id.jadwalkan_panggilan
                      : en.jadwalkan_panggilan}
                  </Button>
                  <Button
                    style={styles.btn}
                    labelStyle={styles.btnLabel}
                    buttonColor={COLORS.red}
                    mode="contained"
                    onPress={() => {
                      RNExitApp.exitApp();
                    }}>
                    {bahasa == 'id' ? id.akhiri_panggilan : en.akhiri_panggilan}
                  </Button>
                </View>
              </View>
            </View>
          </View>
        </View>
      );
    }
  }
}
class DipsSwafoto extends Component {
  state = {
    ///Session 3 untuk mengambil foto diri dengan KTP
    ///Session 4 untuk menampilkan gambar Foto Diri dengan KTP
    session: 3,
    modalScheduleDone: false,
    modalscheduler: false,
    modalscheduler2: false,
    modalVisible: false,
    dateShcedule: '',
    isDatePickerVisible: false,
    open: false,
    value: null,
    items: [],
    isChecked: false,
    isWrongMothersName: false,
    fileData: '',
    fileUri: '',
    openCamera: false,
    idSchedule: null,
    loadingvisible: false,
    modalOCR: false,
    agentImage: '',
    reachMax: 0,
    modalFailedSwafoto: false,
    modalFailedEverything: false,
    modalInfo: false,
    modalLimitIbuKandung: false,
    panggilanmasuk_outbond: false,
  };
  componentDidMount() {
    this.setState({modalInfo: true});
    this.scheduleDropdown();
  }
  async scheduleDropdown() {
    var dataSchedule = [];
    fetch(`${Server.BASE_URL_API}${ApiService.timePeriodSchedule}`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.code < 300) {
          responseJson.data.map(element => {
            dataSchedule.push({
              label: element.periode,
              value: element.id,
            });
          });
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        Alert.alert('Gagal Terhubung ke server', error);
      });
    this.setState({items: dataSchedule});
  }
  async saveSchedule(tgl) {
    var payload = {
      idDips: idDips,
      tanggal: tgl,
      periodeId: this.state.idSchedule,
    };
    fetch(Server.BASE_URL_API + ApiService.saveSchedule, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: JSON.stringify(payload),
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({modalscheduler: false, modalscheduler2: false});

        if (responseJson.code < 300) {
          this.setState({modalScheduleDone: true});
          this.start_proses_socket();
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        console.error(error);
        Alert.alert('Gagal Terhubung ke server', JSON.stringify(error));
      });
  }
  async start_proses_socket() {
    const sleep = time =>
      new Promise(resolve => setTimeout(() => resolve(), time));
    const veryIntensiveTask = async taskDataArguments => {
      const {delay} = taskDataArguments;

      await new Promise(async resolve => {
        for (let i = 0; backgroundServer.isRunning(); i++) {
          console.log(
            '==================start bs====================',
            i + '| ' + AppState.currentState,
          );
          this.rabbitListenCall();
          await sleep(delay);
        }
      });
    };
    const options = {
      taskName: 'Example',
      taskTitle: 'ExampleTask title',
      taskDesc: 'ExampleTask description',
      taskIcon: {
        name: 'ic_launcher',
        type: 'mipmap',
      },
      color: '#ff00ff',
      linkingURI: 'yourSchemeHere://chat/jane', // See Deep Linking for more info
      parameters: {
        delay: 8000,
      },
    };
    await backgroundServer.start(veryIntensiveTask, options);
    await backgroundServer.updateNotification({
      taskDesc: 'New ExampleTask description',
    }); // Only Android, iOS will ignore this call
  }
  rabbitListenCall() {
    var payload = {
      custId: idDips,
    };

    fetch(`${Server.BASE_URL_API_RABBIT}${ApiService.rabbitListenCall}`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(payload),
    })
      .then(response => {
        const statusCode = response.status;
        const data = response.json();
        return Promise.all([statusCode, data]);
      })
      .then(([statusCode, responseJson]) => {
        this.setState({loadingvisible: false});
        var actionCall = '';
        if (responseJson.action != undefined) {
          actionCall = responseJson.action;
        }
        if (actionCall == 'info') {
          this.setState({csId: responseJson.csId});
          this.rabbitListenCall();
        } else {
          if (this.state.data_ticket.ticket == responseJson.ticket) {
            var sessionId = idDips;
            if (responseJson.sessionId != undefined) {
              sessionId = responseJson.sessionId;
            }
            AsyncStorage.setItem('ws_session_name', sessionId);
            AsyncStorage.setItem('ws_password', responseJson.password);
            AsyncStorage.setItem('csId', responseJson.csId);
            AsyncStorage.setItem('custName', custName);
            csId = responseJson.csId;
            this.setState({
              modalApproveCalling: true,
              ws_session_name: sessionId, ///id dips
              ws_password: responseJson.password, //////// dari respon si password
              isMuted: true,
              csId: responseJson.csId,
            });
          } else if (responseJson.agentImage != undefined) {
            AsyncStorage.setItem('ws_session_name', sessionId);
            AsyncStorage.setItem('ws_password', responseJson.password);
            AsyncStorage.setItem('csId', responseJson.csId);
            AsyncStorage.setItem('custName', custName);
            if (AppState.currentState == 'active') {
              this.setState({
                panggilanmasuk_outbond: true,
                ws_session_name: sessionId, ///id dips
                ws_password: responseJson.password, //////// dari respon si password
                sessionKey: responseJson.password, ////////// dari respon si password
                isMuted: false,
                csId: responseJson.csId,
                agentImage: responseJson.agentImage,
              });
            } else if (AppState.currentState == 'background') {
              this.setState({
                panggilanmasuk_outbond: true,
                ws_session_name: sessionId, ///id dips
                ws_password: responseJson.password, //////// dari respon si password
                sessionKey: responseJson.password, ////////// dari respon si password
                isMuted: false,
                csId: responseJson.csId,
                agentImage: responseJson.agentImage,
              });
            }
          }
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        this.rabbitListenCall();
      });
  }
  popupOutbound() {
    return (
      <View
        style={{
          backgroundColor: 'white',
          height: '100%',
          weight: '100%',
          display: this.state.panggilanmasuk_outbond ? 'flex' : 'none',
        }}>
        <Modal
          transparent
          animationType="fade"
          visible={this.state.panggilanmasuk_outbond}
          onRequestClose={() => this.setState({panggilanmasuk_outbond: false})}>
          <RNCamera
            ref={ref => {
              this.camera = ref;
            }}
            style={{
              justifyContent: 'space-between',
              height: '100%',
            }}
            type={this.state.cameraType}
            captureAudio={false}
            zoom={this.state.zoomcamera}
            ratio={this.state.ratio}>
            <View
              style={{
                backgroundColor: 'white',
                height: '35%',
                weight: '100%',
                justifyContent: 'center',
                borderBottomLeftRadius: 25,
                borderBottomRightRadius: 25,
                overflow: 'hidden',
              }}>
              <View>
                <AnimatedReact.View
                  style={{
                    transform: [
                      {
                        rotate: this.state.rotateValueHolder.interpolate({
                          inputRange: [0, 0.5, 1, 1.5],
                          outputRange: ['0deg', '8deg', '0deg', '8deg'],
                        }),
                      },
                    ],
                  }}>
                  <View
                    style={{
                      justifyContent: 'center',
                      width: 150,
                      height: 150,
                      backgroundColor: '#FAE5D6',
                      alignSelf: 'center',
                      paddingBottom: 5,
                      paddingTop: 5,
                      borderRadius: 80,
                      marginTop: '5%',
                      overflow: 'hidden',
                    }}>
                    <Image
                      style={{
                        width: 128,
                        height: 138,
                        alignSelf: 'center',
                        marginTop: 10,
                      }}
                      source={{
                        uri: this.state.agentImage,
                        headers: {
                          Authorization: 'Bearer ' + accessToken,
                          exchangeToken: exchangeToken,
                        },
                      }}
                    />
                  </View>
                </AnimatedReact.View>
                <View>
                  <Text
                    style={[
                      stylesglobal.textsedang_hitam_tebal,
                      {alignSelf: 'center', marginTop: 9},
                    ]}>
                    {bahasa === 'id' ? id.panggilan_masuk : en.panggilan_masuk}
                  </Text>
                  <Text
                    style={[
                      stylesglobal.textsedang_hitam_tebal,
                      {alignSelf: 'center', marginTop: 2},
                    ]}>
                    {bahasa === 'id' ? 'dari DIPS Agent' : 'from Agent DIPS'}
                  </Text>
                </View>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignSelf: 'center',
                width: '100%',
                justifyContent: 'space-between',
                bottom: 70,
              }}>
              <View style={{width: '42%'}}>
                <TouchableHighlight
                  style={{
                    justifyContent: 'center',
                    width: 80,
                    height: 80,
                    backgroundColor: '#FF002E',
                    alignSelf: 'flex-end',
                    marginTop: '25%',
                    paddingBottom: 5,
                    paddingTop: 5,
                    borderRadius: 50,
                  }}
                  onPress={() => {
                    //this.proses_tolak_panggilan_outbound()
                  }}>
                  <View
                    style={{
                      width: 28,
                      height: 28,
                      justifyContent: 'center',
                      alignSelf: 'center',
                    }}>
                    <Image
                      style={{
                        alignSelf: 'center',
                      }}
                      source={rejectpanggilan}
                    />
                  </View>
                </TouchableHighlight>
              </View>

              <View style={{width: '42%'}}>
                <TouchableHighlight
                  style={{
                    justifyContent: 'center',
                    width: 80,
                    height: 80,
                    backgroundColor: '#22D882',
                    alignSelf: 'flex-start',
                    marginTop: '25%',
                    paddingBottom: 5,
                    paddingTop: 5,
                    borderRadius: 50,
                  }}
                  onPress={() => {
                    setTimeout(() => {
                      this.processSignature();
                      backgroundServer.stop();
                    }, 1000);
                  }}>
                  <View
                    style={{
                      width: 28,
                      height: 28,
                      justifyContent: 'center',
                      alignSelf: 'center',
                    }}>
                    <Image
                      style={{
                        alignSelf: 'center',
                      }}
                      source={terimapanggilan}
                    />
                  </View>
                </TouchableHighlight>
              </View>
            </View>
          </RNCamera>
        </Modal>
      </View>
    );
  }
  popUpApproveCalling() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.modalApproveCalling}
        statusBarTranslucent
        RequestClose={() => {
          this.setState({modalApproveCalling: false});
        }}>
        <TouchableOpacity
          // onPress={() => {
          //   this.setState({modalApproveCalling: false});
          // }}
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View
              style={{
                margin: 100,
                backgroundColor: 'white',
                borderRadius: 20,
                justifyContent: 'space-between',
                padding: 35,
                width: '80%',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
                alignItems: 'center',
              }}>
              <FastImage
                transition={false}
                source={logoSuccess}
                style={{width: 55, height: 55, marginBottom: 15}}
              />
              <Text
                style={{
                  marginBottom: 20,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  textAlign: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {bahasa === 'id' ? id.headline_success : en.headline_success}
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Button
                  textColor={COLORS.red}
                  labelStyle={styles.btnLabel}
                  style={[styles.btn, {marginEnd: 10}]}
                  mode="outlined">
                  {bahasa == 'id' ? id.reject : en.reject}
                </Button>
                <Button
                  style={styles.btn}
                  labelStyle={styles.btnLabel}
                  buttonColor={COLORS.red}
                  mode="contained"
                  onPress={() => this.processSignature()}>
                  {bahasa == 'id' ? id.setuju : en.setuju}
                </Button>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      </Modal>
    );
  }
  processSignature() {
    this.setState({modalApproveCalling: false});
    //this.setState({loadingvisible: true});
    var body_signature = {
      sessionName: this.state.ws_session_name,
      role: 0,
      sessionKey: this.state.ws_password,
      userIdentity: custName,
    };

    fetch(`${Server.BASE_URL_API}${ApiService.signatureZoomRN}`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body_signature),
    })
      .then(response => {
        this.setState({loadingvisible: false});
        const statusCode = response.status;
        const data = response.json();
        return Promise.all([statusCode, data]);
      })
      .then(([statusCode, responseJson]) => {
        var getSignature = responseJson.data.signature;
        this.setState({
          loadingvisible: false,
          zoomView: true,
        });
        AsyncStorage.setItem('signatureZoom', getSignature);
      })
      .catch(error => {
        Alert.alert('Gagal ');
        this.setState({loadingvisible: false});
      });
  }
  async processOCR() {
    this.setState({loadingvisible: true});
    var payload = {
      image: this.state.fileData,
    };
    fetch(Server.BASE_URL_API + ApiService.ocrKTP, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: JSON.stringify(payload),
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.code < 300) {
          console.log('responsejson', responseJson.data);
          //SetValue
          nik_ = responseJson.data.nik;
          nama_lengkap_ = responseJson.data.nama;
          tempat_lahir_ =
            responseJson.data.ttl !== null
              ? responseJson.data.ttl.split(',')[0].trim()
              : '';
          tgl_lahir_ =
            responseJson.data.ttl !== null
              ? responseJson.data.ttl.split(',')[1].trim()
              : '';
          jenis_kelamin_ = responseJson.data.jeniskelamin;
          golongan_darah_ = responseJson.data.golongan_darah;
          alamat_ = responseJson.data.alamat;
          rt_rw_ = responseJson.data.rtrw;
          provinsi_ = responseJson.data.provinsi;
          kabupaten_kota_ = responseJson.data.kota_kabupaten;
          kecamatan_ = responseJson.data.kecamatan;
          kelurahan_desa_ = responseJson.data.desa_kelurahan;
          agama_ = responseJson.data.agama;
          status_perkawinan_ = responseJson.data.status_perkawinan;
          kewarganegaraan_ = responseJson.data.kewarganegaraan;
          pekerjaan_ = responseJson.data.pekerjaan;
          accessToken = responseJson.token;
          exchangeToken = responseJson.exchange;
          this.processSwafoto();
        } else {
          accessToken = responseJson.token;
          exchangeToken = responseJson.exchange;
          this.setState({loadingvisible: false});
          setTimeout(() => {
            this.setState({modalVisible: true});
          }, 2000);
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        console.error(error);
        Alert.alert('Gagal Terhubung ke server');
      });
  }
  async processIbuKandung() {
    var payload = {
      idDips: idDips,
      propinsi: provinsi_.trim(),
      kabupaten: kabupaten_kota_.trim(),
      nik: nik_.trim(),
      namaLengkap: nama_lengkap_.trim(),
      tempatlahir: tempat_lahir_.trim(),
      tglLahir: tgl_lahir_.trim(),
      jenisKelamin: jenis_kelamin_.trim(),
      namaLengkapIbu: nama_ibu_kandung_.trim(),
      alamat: alamat_.trim(),
      rt: rt_rw_.split('/')[0].trim(),
      rw: rt_rw_.split('/')[1].trim(),
      kelurahan: kelurahan_desa_.trim(),
      kecamatan: kecamatan_.trim(),
      statusKawin: status_perkawinan_.trim(),
      jenisPekerjaan: pekerjaan_.trim(),
    };
    fetch(Server.BASE_URL_API + ApiService.namaIbuKandung, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: JSON.stringify(payload),
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.status === 'oke') {
          accessToken = responseJson.token;
          exchangeToken = responseJson.exchange;
          this.setState({isWrongMothersName: false, isChecked: true});
        } else {
          accessToken = responseJson.token;
          exchangeToken = responseJson.exchange;
          if (this.state.reachMax > 2) {
            this.setState({
              isWrongMothersName: true,
              isChecked: true,
              modalOCR: false,
            });
            setTimeout(() => {
              this.setState({modalLimitIbuKandung: true});
            }, 1500);

            setTimeout(() => {
              this.setState({
                isWrongMothersName: false,
                isChecked: false,
                reachMax: 0,
              });
            }, 120000);
          } else {
            this.setState({isWrongMothersName: true, isChecked: false});
          }
        }
      })
      .catch(error => {
        console.error(error);
        Alert.alert('Gagal Terhubung ke server');
      });
  }
  async processDukcapil() {
    console.log('masuk dukcapil');
    this.setState({modalOCR: false});
    this.setState({loadingvisible: true});
    var payload = {
      idDips: idDips,
      propinsi: provinsi_.trim(),
      kabupaten: kabupaten_kota_.trim(),
      nik: nik_.trim(),
      agama: agama_.trim(),
      kewarganegaraan: kewarganegaraan_.trim(),
      namaLengkap: nama_lengkap_.trim(),
      tempatlahir: tempat_lahir_.trim(),
      tglLahir: tgl_lahir_.trim(),
      jenisKelamin: jenis_kelamin_.trim(),
      namaLengkapIbu: nama_ibu_kandung_.trim(),
      alamat: alamat_.trim(),
      rt: rt_rw_.split('/')[0].trim(),
      rw: rt_rw_.split('/')[1].trim(),
      kelurahan: kelurahan_desa_.trim(),
      kecamatan: kecamatan_.trim(),
      statusKawin: status_perkawinan_.trim(),
      jenisPekerjaan: pekerjaan_.trim(),
    };
    fetch(Server.BASE_URL_API + ApiService.validasiDuckapil, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: JSON.stringify(payload),
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.status === 'oke') {
          //Berhasil Dukcapil
          accessToken = responseJson.token;
          exchangeToken = responseJson.exchange;
          this.validasiWajahDukcapil();
        } else {
          this.setState({loadingvisible: false});
          setTimeout(() => {
            this.setState({modalFailedEverything: true});
          }, 2000);
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        console.error(error);
        Alert.alert('Gagal Terhubung ke server');
      });
  }
  async processDTOTT() {
    console.log('masuk dtott');
    var payload = {
      idDips: idDips,
      propinsi: provinsi_.trim(),
      kabupaten: kabupaten_kota_.trim(),
      nik: nik_.trim(),
      namaLengkap: nama_lengkap_.trim(),
      tempatlahir: tempat_lahir_.trim(),
      tglLahir: tgl_lahir_.trim(),
      jenisKelamin: jenis_kelamin_.trim(),
      namaLengkapIbu: nama_ibu_kandung_.trim(),
      alamat: alamat_.trim(),
      rt: rt_rw_.split('/')[0].trim(),
      rw: rt_rw_.split('/')[1].trim(),
      kelurahan: kelurahan_desa_.trim(),
      kecamatan: kecamatan_.trim(),
      statusKawin: status_perkawinan_.trim(),
      jenisPekerjaan: pekerjaan_.trim(),
    };
    fetch(Server.BASE_URL_API + ApiService.validasiDTOTT, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: JSON.stringify(payload),
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.status === 'oke') {
          //Berhasil DTOTT
          this.cekNIK();
          accessToken = responseJson.token;
          exchangeToken = responseJson.exchange;
        } else {
          //Gagal DTOTT
          this.setState({loadingvisible: false});
          setTimeout(() => {
            this.setState({modalFailedEverything: true});
          }, 1000);
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        console.error(error);
        Alert.alert('Gagal Terhubung ke server');
      });
  }
  async cekNIK() {
    console.log('cek nik');
    var payload = {
      nama: nama_lengkap_,
      nik: nik_,
      idDips: idDips,
    };
    fetch(Server.BASE_URL_API + ApiService.cekNIK, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: JSON.stringify(payload),
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log('ceknik', responseJson);
        if (responseJson.code < 300) {
          if (responseJson.data.noCif === null) {
            this.processAttachment('ktp');
            this.processAttachment('foto');
            this.processAttachment('ttd');
            isCust = false;
            this.setState({loadingvisible: false});
            this.props.navigation.navigate('ConnectionForm');
          } else {
            custName = responseJson.data.namaLengkap;
            noCIF = responseJson.data.noCif;
            isCust = true;
            custEmail = responseJson.data.email;
            custNoHp = responseJson.data.noHp;
            custAlamat = responseJson.data.alamat;
            custBranchCode = responseJson.data.branchCode;
            custGelar =
              responseJson.data.gelar === null ? '' : responseJson.data.gelar;
            accessToken = responseJson.token;
            exchangeToken = responseJson.exchange;
            this.validasiCoreBank();
          }
        } else {
          isCust = false;
          accessToken = responseJson.token;
          exchangeToken = responseJson.exchange;
          this.setState({loadingvisible: false});
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        console.error(error);
        Alert.alert('Gagal Terhubung ke server');
      });
  }
  async validasiCoreBank() {
    console.log('validasi core bank');
    var payload = {
      nik: nik_,
      namaLengkap: nama_lengkap_,
      tempatlahir: tempat_lahir_,
      tglLahir: tgl_lahir_,
      namaLengkapIbu: nama_ibu_kandung_,
      idDips: idDips,
    };
    fetch(Server.BASE_URL_API + ApiService.validasiCoreBank, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: JSON.stringify(payload),
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({loadingvisible: false});
        console.log('response validasi core bank', responseJson);
        if (responseJson.status === 'oke') {
          accessToken = responseJson.token;
          exchangeToken = responseJson.exchange;
          this.processAttachment('ktp');
          this.processAttachment('foto');
          this.processAttachment('ttd');
          this.props.navigation.navigate('ConnectionForm');
        } else {
          setTimeout(() => {
            this.setState({modalFailedEverything: true});
          }, 2000);
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        console.error(error);
        Alert.alert('Gagal Terhubung ke server');
      });
  }
  async processSwafoto() {
    let formdata = new FormData();
    formdata.append('firstImage', {
      uri: uri_1,
      name: 'image.jpg',
      type: 'image/jpeg',
    });
    formdata.append('secondImage', {
      uri: uri_2,
      name: 'image.jpg',
      type: 'image/jpeg',
    });

    fetch(Server.BASE_URL_API + ApiService.swafotoCheck, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: formdata,
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.code < 300) {
          //Jika muka sesuai dengan KTP
          accessToken = responseJson.token;
          exchangeToken = responseJson.exchange;
          this.setState({loadingvisible: false});
          setTimeout(() => {
            this.setState({modalOCR: true});
          }, 2000);
        } else {
          accessToken = responseJson.token;
          exchangeToken = responseJson.exchange;
          //Jika muka tidak sesuai dengan KTP
          this.setState({loadingvisible: false});
          setTimeout(() => {
            this.setState({modalFailedSwafoto: true});
          }, 2000);
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        console.error(error);
        Alert.alert('Gagal Terhubung ke server');
      });
  }
  async validasiWajahDukcapil() {
    console.log('masuk validasi dukcapil');
    var payload = {
      idDips: idDips,
      nik: nik_,
      image: uri_1,
    };
    fetch(Server.BASE_URL_API + ApiService.validasiWajahDukcapil, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: JSON.stringify(payload),
    })
      .then(response => response.json())
      .then(responseJson => {
        accessToken = responseJson.token;
        exchangeToken = responseJson.exchange;
        //Alert.alert(responseJson.type);
        this.processDTOTT();

        // if (responseJson.code < 300) {
        //   //Berhasil Wajah Dukcapil
        //   accessToken = responseJson.token;
        //   exchangeToken = responseJson.exchange;
        //   this.processDTOTT();
        // } else {
        //   //Gagal Wajah Dukcapil
        //   this.setState({loadingvisible: false});
        //   setTimeout(() => {
        //     this.setState({modalFailedEverything: true});
        //   }, 1000);
        // }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        console.error(error);
        Alert.alert('Gagal Terhubung ke server');
      });
  }
  popUpDoneSchedule() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.modalScheduleDone}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({modalScheduleDone: false});
        }}>
        <View
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View
              style={{
                margin: 100,
                backgroundColor: 'white',
                borderRadius: 20,
                justifyContent: 'space-between',
                padding: 35,
                width: '80%',
                alignItems: 'center',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
              }}>
              <FastImage
                source={logoSuccess}
                style={{width: 55, height: 55, marginBottom: 15}}
              />
              <Text
                style={{
                  marginBottom: 20,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  textAlign: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {bahasa === 'id'
                  ? id.konten_sukses_jadwalkan
                  : en.konten_sukses_jadwalkan}
              </Text>
              <Pressable
                style={{
                  width: '40%',
                  height: 43,
                  borderRadius: 10,
                  padding: 10,
                  elevation: 2,
                  backgroundColor: COLORS.red,
                }}
                onPress={() => {
                  this.setState({modalScheduleDone: false});
                  // RNExitApp.exitApp();
                }}>
                <Text style={styles.textbtn}>
                  {bahasa === 'id' ? id.selesai : en.selesai}
                </Text>
              </Pressable>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
  loadingBar() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.loadingvisible}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({loadingvisible: false});
        }}>
        <View
          style={{
            backgroundColor: 'rgba(255,255,255, 0.8)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View style={styles.centeredView}>
              <View style={styles.containerLoading}>
                <FastImage
                  transition={false}
                  source={loadingvictoria}
                  style={styles.imgSize}
                />
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
  onSubmitScedule() {
    if (this.state.dateShcedule === '') {
      Alert.alert(
        bahasa === 'id'
          ? id.pilih_tanggal_jadwalkan
          : en.pilih_tanggal_jadwalkan,
      );
    } else if (this.state.idSchedule === null) {
      Alert.alert(
        bahasa === 'id' ? id.pilih_jam_jadwalkan : en.pilih_jam_jadwalkan,
      );
    } else {
      this.setState({dateShcedule: ''});
      this.setState({idSchedule: null});

      var formatLama = moment(this.state.dateShcedule, 'DD-MM-YYYY');
      var formatBaru = moment(formatLama).format('YYYY-MM-DD');

      this.saveSchedule(formatBaru);
    }
  }
  popUpScheduler() {
    const showDatePicker = () => {
      this.setState({isDatePickerVisible: true});
    };
    const hideDatePicker = () => {
      this.setState({isDatePickerVisible: false});
    };
    const handleConfirm = date => {
      const NewDate = moment(date).format('DD/MM/YYYY');
      this.setState({dateShcedule: NewDate});
      hideDatePicker();
    };
    const getValueDropdown = valu => {
      this.setState({idSchedule: valu.value});
    };
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.modalscheduler}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({modalscheduler: false});
        }}>
        <TouchableOpacity
          onPress={() => {
            this.setState({modalscheduler: false});
          }}
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View
              style={{
                margin: 100,
                backgroundColor: 'white',
                borderRadius: 20,
                justifyContent: 'space-between',
                padding: 35,
                width: '80%',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
              }}>
              <Text
                style={{
                  marginBottom: 35,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  textAlign: 'left',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {bahasa === 'id'
                  ? id.konten_popup_jadwalkan
                  : en.konten_popup_jadwalkan}
              </Text>
              <Text
                style={{
                  marginBottom: 20,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  alignItems: 'flex-start',
                  textAlign: 'left',
                  fontWeight: 'bold',
                }}>
                {bahasa === 'id' ? id.hari_dan_tanggal : en.hari_dan_tanggal}
              </Text>
              <TouchableOpacity
                onPress={showDatePicker}
                style={{
                  justifyContent: 'center',
                  borderRadius: 5,
                  borderColor: COLORS.border_textInput,
                  borderWidth: 1.5,
                  marginBottom: 10,
                  width: '100%',
                  height: 50,
                  padding: 10,
                }}>
                <View style={{flexDirection: 'row'}}>
                  <Text
                    style={[
                      ,
                      {
                        alignSelf: 'flex-start',
                        textAlign: 'left',
                        fontFamily: 'Helvetica',
                        fontSize: 17,
                      },
                    ]}>
                    {this.state.dateShcedule}
                  </Text>
                </View>
              </TouchableOpacity>
              <DateTimePickerModal
                display="inline"
                date={new Date()}
                locale="id_ID"
                minimumDate={new Date()}
                accentColor={COLORS.red}
                buttonTextColorIOS={COLORS.red}
                cancelTextIOS={bahasa === 'id' ? id.batal : en.batal}
                confirmTextIOS={bahasa === 'id' ? id.pilih : en.pilih}
                isVisible={this.state.isDatePickerVisible}
                mode="date"
                onConfirm={handleConfirm}
                onCancel={hideDatePicker}
              />
              <Text
                style={{
                  marginBottom: 10,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  textAlign: 'left',
                  fontWeight: 'bold',
                }}>
                {bahasa === 'id' ? id.waktu : en.waktu}
              </Text>
              <DropDownPicker
                dropDownMaxHeight={250}
                placeholder={
                  this.state.items.length == 0 ? '' : this.state.items[0].label
                }
                onChangeItem={getValueDropdown}
                containerStyle={{
                  width: '100%',
                  height: 50,
                  marginBottom: 20,
                }}
                style={{
                  borderColor: COLORS.border_textInput,
                  borderWidth: 1.5,
                  borderRadius: 30,
                }}
                labelStyle={{
                  fontFamily: 'Helvetica',
                  fontSize: 19,
                }}
                itemStyle={{
                  height: 50,
                  borderBottomColor: COLORS.border_textInput,
                  borderBottomWidth: 0.5,
                  marginBottom: 10,
                }}
                open={this.state.open}
                value={this.state.value}
                items={this.state.items}
                setOpen={() => {
                  this.setState({open: true});
                }}
              />
              <Pressable
                style={{
                  width: '100%',
                  height: 43,
                  borderRadius: 10,
                  padding: 10,
                  justifyContent: 'center',
                  alignItems: 'center',
                  elevation: 2,
                  backgroundColor: COLORS.red,
                }}
                onPress={() => {
                  this.onSubmitScedule();
                }}>
                <Text style={styles.textbtn}>
                  {bahasa === 'id'
                    ? id.jadwalkan_panggilan
                    : en.jadwalkan_panggilan}
                </Text>
              </Pressable>
            </View>
          </View>
        </TouchableOpacity>
      </Modal>
    );
  }
  popUpScheduler2() {
    const showDatePicker = () => {
      this.setState({isDatePickerVisible: true});
    };
    const hideDatePicker = () => {
      this.setState({isDatePickerVisible: false});
    };
    const handleConfirm = date => {
      const NewDate = moment(date).format('DD/MM/YYYY');
      this.setState({dateShcedule: NewDate});
      hideDatePicker();
    };
    const getValueDropdown = valu => {
      this.setState({idSchedule: valu.value});
    };
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.modalscheduler2}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({modalscheduler2: false});
        }}>
        <View
          style={{
            backgroundColor: 'rgba(255, 255, 255, 100)',
            height: '100%',
            width: '100%',
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            marginTop: 0,
          }}>
          <View
            style={{
              margin: 100,
              backgroundColor: 'white',
              borderRadius: 20,
              justifyContent: 'space-between',
              padding: 35,
              width: '80%',
              shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: 0.5,
              shadowRadius: 7,
              elevation: 10,
            }}>
            <Text
              style={{
                marginBottom: 35,
                fontFamily: 'Helvetica',
                fontSize: 18,
                textAlign: 'left',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              {bahasa === 'id'
                ? id.redaksi_jadwalkan_outbound
                : en.redaksi_jadwalkan_outbound}
            </Text>
            <Text
              style={{
                marginBottom: 20,
                fontFamily: 'Helvetica',
                fontSize: 18,
                alignItems: 'flex-start',
                textAlign: 'left',
                fontWeight: 'bold',
              }}>
              {bahasa === 'id' ? id.hari_dan_tanggal : en.hari_dan_tanggal}
            </Text>
            <TouchableOpacity
              onPress={showDatePicker}
              style={{
                justifyContent: 'center',
                borderRadius: 5,
                borderColor: COLORS.border_textInput,
                borderWidth: 1.5,
                marginBottom: 10,
                width: '100%',
                height: 50,
                padding: 10,
              }}>
              <View style={{flexDirection: 'row'}}>
                <Text
                  style={[
                    ,
                    {
                      alignSelf: 'flex-start',
                      textAlign: 'left',
                      fontFamily: 'Helvetica',
                      fontSize: 17,
                    },
                  ]}>
                  {this.state.dateShcedule}
                </Text>
              </View>
            </TouchableOpacity>
            <DateTimePickerModal
              display="inline"
              date={new Date()}
              locale="id_ID"
              minimumDate={new Date()}
              accentColor={COLORS.red}
              buttonTextColorIOS={COLORS.red}
              cancelTextIOS={bahasa === 'id' ? id.batal : en.batal}
              confirmTextIOS={bahasa === 'id' ? id.pilih : en.pilih}
              isVisible={this.state.isDatePickerVisible}
              mode="date"
              onConfirm={handleConfirm}
              onCancel={hideDatePicker}
            />
            <Text
              style={{
                marginBottom: 10,
                fontFamily: 'Helvetica',
                fontSize: 18,
                textAlign: 'left',
                fontWeight: 'bold',
              }}>
              {bahasa === 'id' ? id.waktu : en.waktu}
            </Text>
            <DropDownPicker
              dropDownMaxHeight={250}
              placeholder={
                this.state.items.length == 0 ? '' : this.state.items[0].label
              }
              onChangeItem={getValueDropdown}
              containerStyle={{
                width: '100%',
                height: 50,
                marginBottom: 20,
              }}
              style={{
                borderColor: COLORS.border_textInput,
                borderWidth: 1.5,
                borderRadius: 30,
              }}
              labelStyle={{
                fontFamily: 'Helvetica',
                fontSize: 19,
              }}
              itemStyle={{
                height: 50,
                borderBottomColor: COLORS.border_textInput,
                borderBottomWidth: 0.5,
                marginBottom: 10,
              }}
              open={this.state.open}
              value={this.state.value}
              items={this.state.items}
              setOpen={() => {
                this.setState({open: true});
              }}
            />
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                justifyContent: 'center',
              }}>
              <Pressable
                style={[
                  styles.btn,
                  {
                    height: 'auto',
                    marginEnd: 10,
                    padding: 15,
                    width: 'auto',
                    backgroundColor: COLORS.red,
                    alignContent: 'center',
                    alignSelf: 'center',
                  },
                ]}
                labelStyle={styles.btnLabel}
                mode="outlined"
                onPress={() => {
                  this.setState({modalscheduler2: false});
                  RNExitApp.exitApp();
                }}>
                <Text
                  ellipsizeMode="tail"
                  numberOfLines={2}
                  style={{
                    textAlign: 'center',
                    fontSize: 15,
                    color: 'white',
                    textAlign: 'center',
                  }}>
                  {bahasa === 'id' ? id.akhiri : en.akhiri}
                </Text>
              </Pressable>
              <Pressable
                style={[
                  styles.btn,
                  {
                    alignContent: 'center',
                    alignSelf: 'center',
                    height: 'auto',
                    padding: 15,
                    width: 'auto',
                  },
                ]}
                labelStyle={styles.btnLabel}
                mode="contained"
                onPress={() => {
                  this.onSubmitScedule();
                }}>
                <Text
                  numberOfLines={1}
                  style={{
                    textAlign: 'center',
                    fontSize: 15,
                    color: COLORS.red,
                    textAlign: 'center',
                  }}>
                  {bahasa === 'id'
                    ? id.jadwalkan_panggilan
                    : en.jadwalkan_panggilan}
                </Text>
              </Pressable>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
  popUpOCR() {
    const showDatePicker = () => {
      this.setState({isDatePickerVisible: true});
    };
    const hideDatePicker = () => {
      this.setState({isDatePickerVisible: false});
    };
    const handleConfirm = date => {
      const NewDate = moment(date).format('DD-MM-YYYY');
      tgl_lahir_ = NewDate;
      hideDatePicker();
    };
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.modalOCR}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({modalOCR: false});
        }}>
        <View
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              marginTop: 0,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                margin: 100,
                backgroundColor: 'white',
                borderRadius: 20,
                justifyContent: 'center',
                padding: 20,
                width: '80%',
                height: '70%',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                alignItems: 'center',
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
              }}>
              <ScrollView
                style={{
                  width: '95%',
                }}
                keyboardDismissMode="interactive"
                automaticallyAdjustKeyboardInsets={true}
                contentContainerStyle={{
                  alignItems: 'center',
                }}>
                <FastImage
                  source={logoInfo}
                  style={{width: 55, height: 55, marginBottom: 15}}
                />
                <Text
                  style={{
                    marginBottom: 15,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    textAlign: 'left',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  {bahasa === 'id'
                    ? id.apakah_data_anda_sesuai
                    : en.apakah_data_anda_sesuai}
                </Text>
                <Text
                  style={{
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    fontWeight: 'bold',
                  }}>
                  {bahasa === 'id' ? id.nik : en.nik}
                </Text>
                <TextInput
                  style={{
                    marginBottom: 10,
                    width: '100%',
                    height: 50,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 1.5,
                    borderRadius: 10,
                    backgroundColor: 'white',
                    paddingLeft: 10,
                    fontSize: 16,
                  }}
                  onChangeText={value => (nik_ = value)}
                  defaultValue={nik_}
                  maxLength={16}
                  underlineColorAndroid="transparent"
                  keyboardType="numeric"
                />
                <Text
                  style={{
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    fontWeight: 'bold',
                  }}>
                  {bahasa === 'id' ? id.nama_lengkap : en.nama_lengkap}
                </Text>
                <TextInput
                  style={{
                    marginBottom: 10,
                    width: '100%',
                    height: 50,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 1.5,
                    borderRadius: 10,
                    backgroundColor: 'white',
                    paddingLeft: 10,
                    fontSize: 16,
                  }}
                  onChangeText={value => (nama_lengkap_ = value)}
                  autoCapitalize="characters"
                  defaultValue={nama_lengkap_}
                  underlineColorAndroid="transparent"
                  keyboardType="default"
                />
                <Text
                  style={{
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    fontWeight: 'bold',
                  }}>
                  {bahasa === 'id' ? id.tempat_lahir : en.tempat_lahir}
                </Text>
                <TextInput
                  style={{
                    marginBottom: 10,
                    width: '100%',
                    height: 50,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 1.5,
                    borderRadius: 10,
                    backgroundColor: 'white',
                    paddingLeft: 10,
                    fontSize: 16,
                  }}
                  autoCapitalize="characters"
                  defaultValue={tempat_lahir_}
                  onChangeText={value => (tempat_lahir_ = value)}
                  underlineColorAndroid="transparent"
                  keyboardType="default"
                />
                <Text
                  style={{
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    fontWeight: 'bold',
                  }}>
                  {bahasa === 'id' ? id.tgl_lahir : en.tgl_lahir}
                </Text>
                <TouchableOpacity
                  onPress={showDatePicker}
                  style={{
                    justifyContent: 'center',
                    borderRadius: 10,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 1.5,
                    marginBottom: 10,
                    width: '100%',
                    height: 50,
                    padding: 10,
                  }}>
                  <View style={{flexDirection: 'row'}}>
                    <Text
                      style={[
                        ,
                        {
                          alignSelf: 'flex-start',
                          textAlign: 'left',
                          fontFamily: 'Helvetica',
                          fontSize: 17,
                        },
                      ]}>
                      {tgl_lahir_}
                    </Text>
                  </View>
                </TouchableOpacity>
                <DateTimePickerModal
                  display="inline"
                  date={new Date()}
                  locale="id_ID"
                  accentColor={COLORS.red}
                  buttonTextColorIOS={COLORS.red}
                  cancelTextIOS={bahasa === 'id' ? id.batal : en.batal}
                  confirmTextIOS={bahasa === 'id' ? id.pilih : en.pilih}
                  isVisible={this.state.isDatePickerVisible}
                  mode="date"
                  onConfirm={handleConfirm}
                  onCancel={hideDatePicker}
                />
                <Text
                  style={{
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    fontWeight: 'bold',
                  }}>
                  {bahasa === 'id' ? id.jenis_kelamin : en.jenis_kelamin}
                </Text>
                <TextInput
                  style={{
                    marginBottom: 10,
                    width: '100%',
                    height: 50,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 1.5,
                    borderRadius: 10,
                    backgroundColor: 'white',
                    paddingLeft: 10,
                    fontSize: 16,
                  }}
                  autoCapitalize="characters"
                  defaultValue={jenis_kelamin_}
                  onChangeText={value => (jenis_kelamin_ = value)}
                  underlineColorAndroid="transparent"
                  keyboardType="default"
                />
                <Text
                  style={{
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    fontWeight: 'bold',
                  }}>
                  {bahasa === 'id' ? id.golongan_darah : en.golongan_darah}
                </Text>
                <TextInput
                  style={{
                    marginBottom: 10,
                    width: '100%',
                    height: 50,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 1.5,
                    borderRadius: 10,
                    backgroundColor: 'white',
                    paddingLeft: 10,
                    fontSize: 16,
                  }}
                  autoCapitalize="characters"
                  defaultValue={golongan_darah_}
                  onChangeText={value => (golongan_darah_ = value)}
                  underlineColorAndroid="transparent"
                  keyboardType="default"
                />
                <Text
                  style={{
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    fontWeight: 'bold',
                  }}>
                  {bahasa === 'id' ? id.alamat : en.alamat}
                </Text>
                <TextInput
                  style={{
                    marginBottom: 10,
                    width: '100%',
                    height: 80,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 1.5,
                    borderRadius: 10,
                    backgroundColor: 'white',
                    paddingLeft: 10,
                    fontSize: 16,
                  }}
                  defaultValue={alamat_}
                  onChangeText={value => (alamat_ = value)}
                  multiline={true}
                  numberOfLines={3}
                  autoCapitalize="characters"
                  underlineColorAndroid="transparent"
                  keyboardType="default"
                />
                <Text
                  style={{
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    fontWeight: 'bold',
                  }}>
                  {bahasa === 'id' ? id.rt_rw : en.rt_rw}
                </Text>
                <TextInput
                  style={{
                    marginBottom: 10,
                    width: '100%',
                    height: 50,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 1.5,
                    borderRadius: 10,
                    backgroundColor: 'white',
                    paddingLeft: 10,
                    fontSize: 16,
                  }}
                  onChangeText={value => (rt_rw_ = value)}
                  defaultValue={rt_rw_}
                  underlineColorAndroid="transparent"
                  keyboardType="default"
                />
                <Text
                  style={{
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    fontWeight: 'bold',
                  }}>
                  {bahasa === 'id' ? id.provinsi : en.provinsi}
                </Text>
                <TextInput
                  style={{
                    marginBottom: 10,
                    width: '100%',
                    height: 50,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 1.5,
                    borderRadius: 10,
                    backgroundColor: 'white',
                    paddingLeft: 10,
                    fontSize: 16,
                  }}
                  onChangeText={value => (provinsi_ = value)}
                  autoCapitalize="characters"
                  defaultValue={provinsi_}
                  underlineColorAndroid="transparent"
                  keyboardType="default"
                />
                <Text
                  style={{
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    fontWeight: 'bold',
                  }}>
                  {bahasa === 'id' ? id.kabupaten_kota : en.kabupaten_kota}
                </Text>
                <TextInput
                  style={{
                    marginBottom: 10,
                    width: '100%',
                    height: 50,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 1.5,
                    borderRadius: 10,
                    backgroundColor: 'white',
                    paddingLeft: 10,
                    fontSize: 16,
                  }}
                  onChangeText={value => (kabupaten_kota_ = value)}
                  autoCapitalize="characters"
                  defaultValue={kabupaten_kota_}
                  underlineColorAndroid="transparent"
                  keyboardType="default"
                />
                <Text
                  style={{
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    fontWeight: 'bold',
                  }}>
                  {bahasa === 'id' ? id.kecamatan : en.kecamatan}
                </Text>
                <TextInput
                  style={{
                    marginBottom: 10,
                    width: '100%',
                    height: 50,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 1.5,
                    borderRadius: 10,
                    backgroundColor: 'white',
                    paddingLeft: 10,
                    fontSize: 16,
                  }}
                  onChangeText={value => (kecamatan_ = value)}
                  autoCapitalize="characters"
                  defaultValue={kecamatan_}
                  underlineColorAndroid="transparent"
                  keyboardType="default"
                />
                <Text
                  style={{
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    fontWeight: 'bold',
                  }}>
                  {bahasa === 'id' ? id.kelurahan_desa : en.kelurahan_desa}
                </Text>
                <TextInput
                  style={{
                    marginBottom: 10,
                    width: '100%',
                    height: 50,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 1.5,
                    borderRadius: 10,
                    backgroundColor: 'white',
                    paddingLeft: 10,
                    fontSize: 16,
                  }}
                  onChangeText={value => (kelurahan_desa_ = value)}
                  autoCapitalize="characters"
                  defaultValue={kelurahan_desa_}
                  underlineColorAndroid="transparent"
                  keyboardType="default"
                />
                <Text
                  style={{
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    fontWeight: 'bold',
                  }}>
                  {bahasa === 'id' ? id.agama : en.agama}
                </Text>
                <TextInput
                  style={{
                    marginBottom: 10,
                    width: '100%',
                    height: 50,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 1.5,
                    borderRadius: 10,
                    backgroundColor: 'white',
                    paddingLeft: 10,
                    fontSize: 16,
                  }}
                  onChangeText={value => (agama_ = value)}
                  autoCapitalize="characters"
                  defaultValue={agama_}
                  underlineColorAndroid="transparent"
                  keyboardType="default"
                />
                <Text
                  style={{
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    fontWeight: 'bold',
                  }}>
                  {bahasa === 'id'
                    ? id.status_perkawinan
                    : en.status_perkawinan}
                </Text>
                <TextInput
                  style={{
                    marginBottom: 10,
                    width: '100%',
                    height: 50,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 1.5,
                    borderRadius: 10,
                    backgroundColor: 'white',
                    paddingLeft: 10,
                    fontSize: 16,
                  }}
                  onChangeText={value => (status_perkawinan_ = value)}
                  autoCapitalize="characters"
                  defaultValue={status_perkawinan_}
                  underlineColorAndroid="transparent"
                  keyboardType="default"
                />
                <Text
                  style={{
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    fontWeight: 'bold',
                  }}>
                  {bahasa === 'id' ? id.kewarganegaraan : en.kewarganegaraan}
                </Text>
                <TextInput
                  style={{
                    marginBottom: 10,
                    width: '100%',
                    height: 50,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 1.5,
                    borderRadius: 10,
                    backgroundColor: 'white',
                    paddingLeft: 10,
                    fontSize: 16,
                  }}
                  onChangeText={value => (kewarganegaraan_ = value)}
                  autoCapitalize="characters"
                  defaultValue={kewarganegaraan_}
                  underlineColorAndroid="transparent"
                  keyboardType="default"
                />
                <Text
                  style={{
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    fontWeight: 'bold',
                  }}>
                  {bahasa === 'id' ? id.pekerjaan : en.pekerjaan}
                </Text>
                <TextInput
                  style={{
                    marginBottom: 10,
                    width: '100%',
                    height: 50,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 1.5,
                    borderRadius: 10,
                    backgroundColor: 'white',
                    paddingLeft: 10,
                    fontSize: 16,
                  }}
                  onChangeText={value => (pekerjaan_ = value)}
                  autoCapitalize="characters"
                  defaultValue={pekerjaan_}
                  underlineColorAndroid="transparent"
                  keyboardType="default"
                />
                <Text
                  style={{
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    fontWeight: 'bold',
                  }}>
                  {bahasa === 'id' ? id.nama_ibu_kandung : en.nama_ibu_kandung}
                </Text>
                <TextInput
                  style={{
                    marginBottom: 5,
                    width: '100%',
                    height: 50,
                    borderColor: this.state.isWrongMothersName
                      ? COLORS.red
                      : COLORS.border_textInput,
                    borderWidth: 1.5,
                    borderRadius: 10,
                    backgroundColor: 'white',
                    paddingLeft: 10,
                    fontSize: 16,
                  }}
                  defaultValue={nama_ibu_kandung_}
                  onChangeText={value => {
                    this.setState({
                      isWrongMothersName: false,
                      isChecked: false,
                    });
                    nama_ibu_kandung_ = value;
                  }}
                  editable={this.state.reachMax > 3 ? false : true}
                  autoCapitalize="characters"
                  underlineColorAndroid="transparent"
                  keyboardType="default"
                />
                <Text
                  style={{
                    display: this.state.isWrongMothersName ? 'flex' : 'none',
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 15,
                    fontWeight: 'bold',
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    color: COLORS.red,
                  }}>
                  {this.state.reachMax > 3
                    ? bahasa === 'id'
                      ? id.reach_max_hit
                      : en.reach_max_hit
                    : bahasa === 'id'
                    ? id.warning_nama_ibu
                    : en.warning_nama_ibu}
                </Text>
                <Text
                  style={{
                    display: !this.state.isWrongMothersName ? 'flex' : 'none',
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 15,
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    color: COLORS.red,
                  }}>
                  {bahasa === 'id' ? id.wajib_diisi : en.wajib_diisi}
                </Text>
                <View
                  style={{
                    width: '100%',
                  }}>
                  <CheckBox
                    label={
                      bahasa == 'id' ? id.data_sudah_benar : en.data_sudah_benar
                    }
                    status={this.state.isChecked ? 'checked' : 'unchecked'}
                    onPress={() => {
                      if (!this.state.isChecked) {
                        if (nama_ibu_kandung_ === '') {
                          Alert.alert(
                            bahasa === 'id'
                              ? id.silahkan_isi_kolom
                              : en.silahkan_isi_kolom,
                          );
                        } else {
                          this.setState({
                            isChecked: true,
                            reachMax: this.state.reachMax + 1,
                          });
                          this.processIbuKandung();
                        }
                      }
                    }}
                    containerStyle={{
                      width: '100%',
                      alignSelf: 'flex-start',
                      alignItems: 'flex-start',
                    }}
                    boxStyle={{
                      width: '100%',
                    }}
                    labelStyle={{fontSize: 14}}
                    boxColor={COLORS.red}
                  />
                </View>
                <View
                  style={{
                    marginTop: 15,
                    width: '100%',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <Button
                    textColor={COLORS.red}
                    labelStyle={styles.btnLabel}
                    style={{
                      width: 120,
                      borderRadius: 12,
                      borderColor: COLORS.red,
                      borderWidth: 2,
                    }}
                    mode="outlined"
                    onPress={() =>
                      this.setState({modalOCR: false, session: 3})
                    }>
                    {bahasa == 'id' ? id.batal : en.batal}
                  </Button>
                  <Button
                    style={{
                      width: 120,
                      borderRadius: 12,
                      borderColor: COLORS.red,
                      borderWidth: 2,
                    }}
                    labelStyle={styles.btnLabel}
                    buttonColor={COLORS.red}
                    mode="contained"
                    onPress={() => this.submitOCR()}>
                    {bahasa == 'id' ? id.lanjut : en.lanjut}
                  </Button>
                </View>
              </ScrollView>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
  popUpFailedEverything() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.modalFailedEverything}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({modalFailedEverything: false});
        }}>
        <View
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View
              style={{
                margin: 100,
                backgroundColor: 'white',
                borderRadius: 20,
                justifyContent: 'space-between',
                padding: 35,
                width: '80%',
                alignItems: 'center',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
              }}>
              <FastImage
                source={logoWarning}
                style={{width: 55, height: 55, marginBottom: 15}}
              />
              <Text
                style={{
                  marginBottom: 20,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  textAlign: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {bahasa === 'id'
                  ? id.konten_failed_dtott
                  : en.konten_failed_dtott}
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  width: '100%',
                  justifyContent: 'center',
                }}>
                <Button
                  textColor={COLORS.red}
                  labelStyle={styles.btnLabel}
                  style={[styles.btn, {marginEnd: 10}]}
                  mode="outlined"
                  onPress={() => Linking.openURL('tel:1500977')}>
                  Call Center
                </Button>
                <Button
                  style={styles.btn}
                  labelStyle={styles.btnLabel}
                  buttonColor={COLORS.red}
                  mode="contained"
                  onPress={() => {
                    RNExitApp.exitApp();
                    this.setState({modalFailedEverything: false});
                  }}>
                  OK
                </Button>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
  showContent() {
    if (this.state.session == 3) {
      return (
        <View
          style={{
            flexDirection: 'column',
            alignItems: 'center',
          }}>
          <Pressable
            onPress={() => {
              this.setState({openCamera: true});
            }}>
            <FastImage
              source={pickCamera}
              style={{
                marginBottom: 20,
                width: 90,
                height: 90,
              }}
            />
          </Pressable>
        </View>
      );
    } else if (this.state.session == 4) {
      return (
        <View
          style={{
            padding: 30,
            width: '100%',
            height: '100%',
          }}>
          <FastImage
            resizeMode="contain"
            style={{width: '100%', height: '100%'}}
            source={{
              uri: this.state.fileUri,
              priority: FastImage.priority.normal,
            }}
          />
        </View>
      );
    }
  }
  popUpLimitIbuKandung() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.modalLimitIbuKandung}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({modalLimitIbuKandung: false});
        }}>
        <View
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View
              style={{
                margin: 100,
                backgroundColor: 'white',
                borderRadius: 20,
                justifyContent: 'space-between',
                padding: 35,
                width: '80%',
                alignItems: 'center',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
              }}>
              <FastImage
                source={logoInfo}
                style={{width: 55, height: 55, marginBottom: 15}}
              />
              <Text
                style={{
                  marginBottom: 20,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  textAlign: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {bahasa === 'id'
                  ? id.headline_limit_ibu_kandung
                  : en.headline_limit_ibu_kandung}
              </Text>
              <Pressable
                style={{
                  width: '100%',
                  height: 43,
                  borderRadius: 10,
                  padding: 10,
                  elevation: 2,
                  backgroundColor: COLORS.red,
                }}
                onPress={() => {
                  this.setState({modalLimitIbuKandung: false});
                  setTimeout(() => {
                    this.setState({modalOCR: true});
                  }, 1500);
                }}>
                <Text style={styles.textbtn}>OK</Text>
              </Pressable>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
  popUpInfoKtp() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.modalInfo}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({modalInfo: false});
        }}>
        <View
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View
              style={{
                margin: 100,
                backgroundColor: 'white',
                borderRadius: 20,
                justifyContent: 'space-between',
                padding: 35,
                width: '80%',
                alignItems: 'center',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
              }}>
              <FastImage
                source={logoInfo}
                style={{width: 55, height: 55, marginBottom: 15}}
              />
              <Text
                style={{
                  marginBottom: 20,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  textAlign: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {bahasa === 'id'
                  ? id.harap_untuk_menyiapkan_ktp
                  : en.harap_untuk_menyiapkan_ktp}
              </Text>
              <Pressable
                style={{
                  width: '100%',
                  height: 43,
                  borderRadius: 10,
                  padding: 10,
                  elevation: 2,
                  backgroundColor: COLORS.red,
                }}
                onPress={() => {
                  this.setState({modalInfo: false});
                }}>
                <Text style={styles.textbtn}>OK</Text>
              </Pressable>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
  popUpFailedOCRKtp() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.modalVisible}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({modalVisible: false});
        }}>
        <View
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View
              style={{
                margin: 100,
                backgroundColor: 'white',
                borderRadius: 20,
                justifyContent: 'space-between',
                padding: 35,
                width: '80%',
                alignItems: 'center',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
              }}>
              <FastImage
                source={logoInfo}
                style={{width: 55, height: 55, marginBottom: 15}}
              />
              <Text
                style={{
                  marginBottom: 20,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  textAlign: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {bahasa === 'id' ? id.konten_failed_ocr : en.konten_failed_ocr}
              </Text>
              <Pressable
                style={{
                  width: '100%',
                  height: 43,
                  borderRadius: 10,
                  padding: 10,
                  elevation: 2,
                  backgroundColor: COLORS.red,
                }}
                onPress={() => {
                  this.setState({modalVisible: false, session: 3});
                }}>
                <Text style={styles.textbtn}>
                  {bahasa === 'id'
                    ? id.masukkan_ulang_ktp
                    : en.masukkan_ulang_ktp}
                </Text>
              </Pressable>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
  popUpFailedSwafoto() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.modalFailedSwafoto}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({modalFailedSwafoto: false});
        }}>
        <View
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View
              style={{
                margin: 100,
                backgroundColor: 'white',
                borderRadius: 20,
                justifyContent: 'space-between',
                padding: 35,
                width: '80%',
                alignItems: 'center',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
              }}>
              <FastImage
                source={logoInfo}
                style={{width: 55, height: 55, marginBottom: 15}}
              />
              <Text
                style={{
                  marginBottom: 20,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  textAlign: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {bahasa === 'id'
                  ? id.title_failed_swafoto
                  : en.konten_failed_dukcapil}
              </Text>
              <Pressable
                style={{
                  width: '100%',
                  height: 43,
                  borderRadius: 10,
                  padding: 10,
                  elevation: 2,
                  backgroundColor: COLORS.red,
                }}
                onPress={() => {
                  this.setState({modalFailedSwafoto: false, session: 3});
                }}>
                <Text style={styles.textbtn}>
                  {bahasa === 'id'
                    ? id.text_btn_failed_swafoto
                    : en.text_btn_failed_swafoto}
                </Text>
              </Pressable>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
  submitOCR() {
    if (
      nik_ === '' ||
      nama_lengkap_ === '' ||
      tempat_lahir_ === '' ||
      tgl_lahir_ === '' ||
      jenis_kelamin_ === '' ||
      golongan_darah_ === '' ||
      alamat_ === '' ||
      rt_rw_ === '' ||
      provinsi_ === '' ||
      kabupaten_kota_ === '' ||
      kecamatan_ === '' ||
      kelurahan_desa_ === '' ||
      agama_ === '' ||
      status_perkawinan_ === '' ||
      kewarganegaraan_ === '' ||
      pekerjaan_ === '' ||
      nama_ibu_kandung_ === '' ||
      !this.state.isChecked
    ) {
      Alert.alert(
        bahasa === 'id' ? id.silahkan_isi_kolom : en.silahkan_isi_kolom,
      );
    } else {
      this.setState({modalOCR: false});
      this.processDukcapil();
    }
  }
  async processAttachment(path) {
    let formdata = new FormData();
    formdata.append('idDips', idDips);
    switch (path) {
      case 'foto':
        formdata.append(path, {
          uri: this.state.fileUri,
          name: 'image.jpg',
          type: 'image/jpeg',
        });
        break;
      case 'ktp':
        formdata.append(path, {
          uri: fileUriKTP,
          name: 'image.jpg',
          type: 'image/jpeg',
        });
        break;
      case 'ttd':
        formdata.append(path, {
          uri: fileUriKTP,
          name: 'image.jpg',
          type: 'image/jpeg',
        });
        break;
    }

    fetch(Server.BASE_URL_API + ApiService.formAttachment, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: formdata,
    })
      .then(response => response.json())
      .then(responseJson => {})
      .catch(error => {
        console.error(error);
        Alert.alert('Gagal Terhubung ke server');
      });
  }
  facesDetectedselfie(faces) {
    if (faces.faces.length > 0) {
      const rightEye = faces.faces[0].rightEyeOpenProbability;
      const leftEye = faces.faces[0].leftEyeOpenProbability;
      const bothEyes = (rightEye + leftEye) / 2;
      if (bothEyes < 0.3) {
        this.setState({blinkDetected: true, canDetectFaces: false});

        setTimeout(() => {
          this.takePicture();
        }, 1500);
      }
      if (this.state.blinkDetected && bothEyes >= 0.9) {
        this.setState({blinkDetected: false});
      }
    }
  }
  takePicture = async () => {
    if (this.camera) {
      const options = {quality: 0.5, base64: true};
      const data = await this.camera.takePictureAsync(options);
      if (this.state.session == 3) {
        var cropDataPreview = {
          offset: {x: 132, y: 460},
          size: {width: 1200, height: 2400},
          displaySize: {width: 450, height: 900},
          resizeMode: 'contain',
        };
        var cropDataPeople = {
          offset: {x: 132, y: 250},
          size: {width: 1420, height: 2300},
          displaySize: {width: 700, height: 740},
          resizeMode: 'cover',
        };
        var cropDataKTP = {
          offset: {x: 162, y: 2000},
          size: {width: 1170, height: 800},
          displaySize: {width: 774, height: 530},
          resizeMode: 'contain',
        };
        var cropPeopleDataKTP = {
          offset: {x: 960, y: 2150},
          size: {width: 290, height: 440},
          displaySize: {width: 650, height: 627},
          resizeMode: 'cover',
        };
        //Crop Image PREVIEW
        ImageEditor.cropImage(data.uri, cropDataPreview).then(url => {
          this.setState({
            fileUri: url,
            openCamera: false,
            session: 4,
          });
        });
        //Crop Image KTP by border
        ImageEditor.cropImage(data.uri, cropDataKTP).then(url => {
          //Convert Uri to Base64
          fileUriKTP = url;
          RNFS.readFile(url, 'base64').then(res => {
            this.setState({
              fileData: res,
            });
          });
        });
        //Crop Image People KTP by border
        ImageEditor.cropImage(data.uri, cropPeopleDataKTP).then(url => {
          uri_2 = url;
        });
        //Crop Image PEOPLE
        ImageEditor.cropImage(data.uri, cropDataPeople).then(url => {
          uri_1 = url;
        });
      }
    }
  };
  render() {
    if (!this.state.openCamera) {
      return (
        <View
          style={[
            styles.container,
            {
              backgroundColor: 'white',
              alignItems: 'center',
              position: 'relative',
            },
          ]}>
          <StatusBar hidden="true" />
          {this.popUpScheduler()}
          {this.popUpScheduler2()}
          {this.popUpDoneSchedule()}
          {this.loadingBar()}
          {this.popUpOCR()}
          {this.popUpFailedOCRKtp()}
          {this.popUpFailedSwafoto()}
          {this.popUpFailedEverything()}
          {this.popUpInfoKtp()}
          {this.popUpLimitIbuKandung()}
          <View style={styles.containerBatik}>
            <FastImage
              transition={false}
              source={batikbvic}
              style={styles.imageBatik}
            />
            <View style={[styles.upperView, {flexDirection: 'row'}]}>
              <View
                style={{
                  padding: 20,
                  width: '50%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    alignSelf: 'center',
                    height: 145,
                    width: 145,
                    backgroundColor: '#E4E4E4',
                    borderRadius: 80,
                    overflow: 'hidden',
                  }}>
                  <RNCamera
                    captureAudio={false}
                    ref={ref => {
                      this.camera = ref;
                    }}
                    style={{
                      alignSelf: 'center',
                      height: '100%',
                      width: '100%',
                    }}
                    type={'front'}
                  />
                </View>
              </View>
              <View
                style={{
                  padding: 20,
                  width: '50%',
                  flexDirection: 'column',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <View
                  style={{
                    justifyContent: 'center',
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <FastImage
                    transition={false}
                    source={dot}
                    style={{
                      right: 5,
                      width: 14,
                      height: 14,
                    }}
                  />
                  <View
                    style={{
                      justifyContent: 'center',
                      width: '100%',
                      borderRadius: 9,
                      backgroundColor: 'white',
                      height: 32,
                    }}>
                    <Text
                      ellipsizeMode="tail"
                      numberOfLines={1}
                      style={{
                        marginLeft: 10,
                        marginRight: 10,
                        textAlign: 'left',
                        fontSize: 15,
                        fontFamily: 'Helvetica',
                        fontWeight: 'bold',
                      }}>
                      {'000'}
                      {bahasa == 'id'
                        ? id.antrian_sekarang
                        : en.antrian_sekarang}
                    </Text>
                  </View>
                </View>
                <View
                  style={{
                    justifyContent: 'center',
                    flexDirection: 'row',
                    alignItems: 'center',
                    top: 10,
                  }}>
                  <FastImage
                    transition={false}
                    source={people_three}
                    style={{
                      right: 5,
                      width: 14,
                      height: 14,
                    }}
                  />
                  <View
                    style={{
                      justifyContent: 'center',
                      width: '100%',
                      borderRadius: 9,
                      backgroundColor: COLORS.red,
                      height: 32,
                    }}>
                    <Text
                      ellipsizeMode="tail"
                      numberOfLines={1}
                      style={{
                        color: 'white',
                        marginLeft: 10,
                        marginRight: 10,
                        textAlign: 'left',
                        fontSize: 15,
                        fontFamily: 'Helvetica',
                        fontWeight: 'bold',
                      }}>
                      {'000'}
                      {bahasa == 'id' ? id.antrian_saya : en.antrian_saya}
                    </Text>
                  </View>
                </View>
              </View>
            </View>
            <ScrollView
              style={{
                marginTop: 20,
                marginBottom: 27,
                width: '100%',
                height: '60%',
              }}
              contentContainerStyle={{
                width: '100%',
                flexGrow: 1,
              }}>
              <View
                style={{
                  paddingTop: 10,
                  paddingBottom: 10,
                  borderBottomColor: 'rgba(0, 0, 0, 0.1)',
                  borderBottomWidth: 1.3,
                }}>
                <Text
                  style={{
                    padding: 10,
                    marginStart: 20,
                    fontFamily: 'Helvetica',
                    fontSize: 18,
                    fontWeight: 'bold',
                    borderBottomWidth: 1,
                    borderBottomColor: 'black',
                  }}>
                  {bahasa === 'id'
                    ? id.title_foto_diri_dengan_ktp
                    : en.title_foto_diri_dengan_ktp}
                </Text>
              </View>
              <Text
                style={{
                  padding: 15,
                  marginStart: 10,
                  marginEnd: 10,
                  fontFamily: 'Helvetica',
                  fontSize: 16,
                  textAlign: 'justify',
                  borderBottomWidth: 1,
                  borderBottomColor: 'black',
                }}>
                {bahasa === 'id'
                  ? id.headline_foto_diri_dengan_ktp
                  : en.headline_foto_diri_dengan_ktp}
              </Text>
              <View
                style={{
                  alignItems: 'center',
                  width: '100%',
                  height: '100%',
                }}>
                <FastImage
                  transition={false}
                  resizeMode="stretch"
                  style={{
                    justifyContent: 'center',
                    width: 360,
                    height: 270,
                  }}
                  source={bgPickImage}>
                  {this.showContent()}
                </FastImage>
                <View
                  style={{
                    display: this.state.session == 3 ? 'none' : 'flex',
                    justifyContent: 'space-between',
                    marginTop: 10,
                    width: 360,
                    flexDirection: 'row',
                  }}>
                  <Pressable
                    onPress={() => {
                      this.setState({session: 3, fileUri: '', fileData: ''});
                    }}>
                    <FastImage
                      transition={false}
                      style={{
                        width: 50,
                        height: 50,
                      }}
                      source={changeImage}
                    />
                  </Pressable>
                  <Button
                    style={{
                      justifyContent: 'center',
                      height: 50,
                      borderRadius: 7,
                      borderColor: COLORS.red,
                      borderWidth: 2,
                    }}
                    labelStyle={styles.btnLabel}
                    buttonColor={COLORS.red}
                    mode="contained"
                    onPress={() => {
                      this.processOCR();
                    }}>
                    {bahasa === 'id' ? id.lanjut : en.lanjut}
                  </Button>
                </View>
              </View>
            </ScrollView>
            <View style={styles.bottomView}>
              <View style={{flexDirection: 'row', bottom: 0}}>
                <Button
                  textColor={COLORS.red}
                  labelStyle={styles.btnLabel}
                  style={[styles.btn, {marginEnd: 10}]}
                  mode="outlined"
                  onPress={() => {
                    this.setState({modalscheduler: true});
                    backgroundServer.stop();
                  }}>
                  {bahasa == 'id'
                    ? id.jadwalkan_panggilan
                    : en.jadwalkan_panggilan}
                </Button>
                <Button
                  style={styles.btn}
                  labelStyle={styles.btnLabel}
                  buttonColor={COLORS.red}
                  mode="contained"
                  onPress={() => RNExitApp.exitApp()}>
                  {bahasa == 'id' ? id.tutup : en.tutup}
                </Button>
              </View>
            </View>
          </View>
        </View>
      );
    } else {
      return (
        <View
          style={[
            styles.container,
            {
              backgroundColor: 'white',
              alignItems: 'center',
              position: 'relative',
            },
          ]}>
          <StatusBar hidden="true" />
          <View
            style={{
              width: '100%',
              height: '100%',
            }}>
            <RNCamera
              captureAudio={false}
              ref={ref => {
                this.camera = ref;
              }}
              style={{
                justifyContent: 'space-between',
                alignSelf: 'center',
                alignItems: 'center',
                height: '100%',
                flexDirection: 'column',
                width: '100%',
              }}
              onCameraReady={() => {
                this.setState({canDetectFaces: true});
              }}
              faceDetectionMode={RNCamera.Constants.FaceDetection.Mode.accurate}
              faceDetectionLandmarks={
                RNCamera.Constants.FaceDetection.Landmarks.all
              }
              faceDetectionClassifications={
                RNCamera.Constants.FaceDetection.Classifications.all
              }
              onFacesDetected={faces => {
                this.state.canDetectFaces
                  ? this.facesDetectedselfie(faces)
                  : null;
              }}
              onFaceDetectionError={error => console.log('FDError', error)} // This is never triggered
              type={'front'}>
              <View
                style={{
                  flexDirection: 'column',
                  width: '100%',
                  height: '40%',
                }}>
                <View
                  style={{
                    height: 180,
                    padding: 20,
                    marginBottom: 10,
                    justifyContent: 'space-evenly',
                    flexDirection: 'column',
                    borderBottomEndRadius: 30,
                    borderBottomStartRadius: 30,
                    backgroundColor: 'white',
                  }}>
                  <Text
                    style={{
                      alignSelf: 'center',
                      textAlign: 'center',
                      color: 'black',
                      fontWeight: 'bold',
                      fontFamily: 'Helvetica',
                      fontSize: 17,
                    }}>
                    {bahasa === 'id'
                      ? id.title_blink_swafoto
                      : en.title_blink_swafoto}
                  </Text>
                  <Text
                    style={{
                      alignSelf: 'center',
                      textAlign: 'center',
                      color: 'black',
                      fontFamily: 'Helvetica',
                      fontSize: 17,
                    }}>
                    {bahasa === 'id'
                      ? id.content_blink_swafoto
                      : en.content_blink_swafoto}
                  </Text>
                </View>

                <View
                  style={{
                    padding: 20,
                    alignSelf: 'center',
                    justifyContent: 'space-evenly',
                    width: 320,
                    height: 'auto',
                    flexDirection: 'column',
                    borderRadius: 20,
                    backgroundColor: 'rgba(255, 255, 255, 0.5)',
                  }}>
                  <Text
                    style={{
                      alignSelf: 'center',
                      textAlign: 'center',
                      color: 'black',
                      fontFamily: 'Helvetica',
                      fontSize: 17,
                    }}>
                    {bahasa === 'id' ? id.kedipkan_mata : en.kedipkan_mata}
                  </Text>
                </View>
              </View>

              <View
                style={{
                  marginBottom: 80,
                  alignItems: 'center',
                  width: '85%',
                  height: '25%',
                  borderRadius: 12,
                  borderColor: COLORS.red,
                  borderWidth: 4,
                  borderStyle: 'dashed',
                }}
              />
            </RNCamera>
          </View>
        </View>
      );
    }
  }
}
function ZoomHook({children}) {
  const zoom = useZoom();
  const isMounted = useIsMounted();
  return children(zoom, isMounted);
}
class BaseMeetingNew extends Component {
  constructor() {
    super();
    this.state = {
      isInSession: true, //// default : true
      isKeyboardOpen: true,
      isLongTouch: false,
      contentHeight: '100%',
      users: [],
      sessionName: '',
      timer: 0,
      isVideoOn: false,
      isSpeakerOn: false,
      isOriginalAspectRatio: false,
      isReceiveSpokenLanguageContentEnabled: false,
      chatMessage: '',
      chatMessages: [],
      refreshFlatlist: false, //dari sini ke atas copy dari zoom call screen
      //Session 1 untuk menu setuju menggunakan layanan digital DiPS
      //Session 2 untuk menu daftar produk
      //Session 3 untuk menu buka rekening formulir transfer
      //Session 4 untuk menu upload berkas KTP pendukung pembukaan rekening (CIF NEW)
      //Session 5 untuk menu upload berkas Swafoto KTP pendukung pembukaan rekening (CIF NEW)
      //Session 6 untuk menu upload berkas NPWP pendukung pembukaan rekening (CIF NEW)
      //Session 7 untuk menu upload berkas TTD pendukung pembukaan rekening (CIF NEW)
      //Session 8 untuk menu pengisian berkas2 form pembukaan rekening (CIF NEW)
      //Session 9 untuk OTP
      //Session 10 RESI
      //Session 11 Portofolio
      //Session 12 Menu Layanan
      //Session 13 Pengaduan Nasabah
      //Session 14 Transaksi Antar Bank
      //Session 15 Layanan Pembukaan Akun
      //Session 16 Transaksi Antar Rekening
      //Session 17 Transaksi Rekening Sendiri
      //Session 18 Menu Deposito Online
      //Session 19 Transaksi Penempatan Deposito
      //Session 20 Transaksi Pencairan Depostio
      //Session 21 Transaksi Perubah Aro & Non Aro
      //Session 22 Menu Transaksi
      //Session 23 Resi Multi Form
      //Session 24 Perbarui Data ( Informasi Data Utama )
      //Session 25 Perbarui Data ( Data Pekerjaan )
      //Session 26 Perbarui Data ( Data Keuangan )
      //Session 27 Sub Menu Wealth Management
      //Session 28 Reksa Dana ( Wealth Management )
      //Session 29 Profil Resiko
      //Session 30 Pertanyaan Profil Resiko
      //Session 31 Konfirmasi Reksa Dana
      //Session 32 Menu WEALTH MANAGEMENT
      //Session 33 Menu Pembelian Reksa Dana
      //Session 34 Aktivasi IBMB TNC
      //Session 35 Aktivasi IBMB Form
      //Session 36 Registrasi Aplikasi 1
      //Session 37 Registrasi Aplikasi 2
      session: 1, ///// default : 1
      sessionBeforeOTP: 0, //default : 0
      isOpeningAccount: false,
      //Upload QRCode ke-....
      transaksiKe: 0,
      maxFilePendukung: 0,
      open: false,
      value: null,
      items: [],
      isResiPengaduan: false, /// default : false
      modalOCR: false,
      isLoading: true,
      isDecline: false,
      isDatePickerVisible: false,
      dateShcedule: '',
      itemProduct: [],
      setRefreshing: false,
      openCamera: false,
      modalTNC: false,
      isChecked: false,
      htmlTNC: '',
      pendukung: false,
      formCode: '', /// default : ''
      isMuted: false,
      isFormDataDiri: false,
      isFormAlamatBerbeda: false,
      isFormDataPekerjaan: false,
      isFormDataKeuangan: false,
      loadingvisible: true, //// default : true
      modalFailedDukcapil: false,
      modalFailedDTOTT: false,
      uri_1: '',
      uri_2: '',
      isExpanded: false,
      dataPortofolio: {},
      dataOcr: {},
      ListNoRek: [],
      isSecretBalnce: false,
      modalSuksesResi: false,
      pernyataanPengaduan: false,
      dateNow: moment().format('DD-MM-YYYY'),
      modalFailedSwafoto: false,
      isConfirmTransaksi: false, /// default : false
      itemDepositoOnline: [
        {
          title:
            bahasa === 'id' ? id.penempatan_deposito : en.penempatan_deposito,
          id: 1,
          img: null,
        },
        {
          title:
            bahasa === 'id' ? id.pencairan_deposito : en.pencairan_deposito,
          id: 2,
          img: null,
        },
        {
          title:
            bahasa === 'id'
              ? id.perubahan_ARO_NON_ARO
              : en.perubahan_ARO_NON_ARO,
          id: 3,
          img: null,
        },
        {
          title: bahasa === 'id' ? id.ebillyet_deposito : en.ebillyet_deposito,
          id: 4,
          img: null,
        },
      ],
      itemTransaksi: [
        {
          title:
            bahasa === 'id'
              ? id.transaksi_rekening_pribadi
              : en.transaksi_rekening_pribadi,
          id: 1,
          img: null,
        },
        {
          title:
            bahasa === 'id'
              ? id.transaksi_antar_rekening
              : en.transaksi_antar_rekening,
          id: 2,
          img: img_transaksi_inter_bank,
        },
        {
          title:
            bahasa === 'id' ? id.transaksi_antar_bank : en.transaksi_antar_bank,
          id: 3,
          img: img_transaksi_antar_bank,
        },
      ],
      itemLayanan: [
        {
          title: bahasa === 'id' ? id.aktivasi_dormant : en.aktivasi_dormant,
          id: 1,
          img: img_dormant,
        },
        {
          title: bahasa === 'id' ? id.aktivasi_ibmb : en.aktivasi_ibmb,
          id: 2,
          img: img_aktivasi_ibmb,
        },
        {
          title: bahasa === 'id' ? id.blokir_rekening : en.blokir_rekening,
          id: 3,
          img: img_blokir_rekening,
        },
        {
          title: bahasa === 'id' ? id.formulir_kredit : en.formulir_kredit,
          id: 4,
          img: img_formulir_kredit,
        },
        {
          title: bahasa === 'id' ? id.formulir_transfer : en.formulir_transfer,
          id: 5,
          img: img_formulir_transfer,
        },
        {
          title: bahasa === 'id' ? id.pengaduan_nasabah : en.pengaduan_nasabah,
          id: 6,
          img: img_keluhan_nasabah,
        },
        {
          title: bahasa === 'id' ? id.pembukaan_akun : en.pembukaan_akun,
          id: 7,
          img: img_opening_account,
        },
        {
          title: bahasa === 'id' ? id.penutupan_rekening : en.aktivasi_dormant,
          id: 8,
          img: img_penutupan_rekening,
        },
        {
          title: bahasa === 'id' ? id.perbarui_data : en.perbarui_data,
          id: 9,
          img: img_perbarui_data,
        },
        {
          title: bahasa === 'id' ? id.pinjaman : en.pinjaman,
          id: 10,
          img: img_pinjaman,
        },
        {
          title: bahasa === 'id' ? id.surat_kuasa : en.surat_kuasa,
          id: 11,
          img: img_surat_kuasa,
        },
        {
          title: bahasa === 'id' ? id.transaksi : en.transaksi,
          id: 12,
          img: img_transaksi_antar_bank,
        },
        {
          title: bahasa === 'id' ? id.transaksi_atm : en.transaksi_atm,
          id: 13,
          img: img_kartu_atm,
        },
        {
          title: bahasa === 'id' ? id.transaksi_ibmb : en.transaksi_ibmb,
          id: 14,
          img: img_transaksi_ibmb,
        },
        {
          title: bahasa === 'id' ? id.transaksi_kredit : en.transaksi_kredit,
          id: 16,
          img: img_transaksi_kredit,
        },
        {
          title: bahasa === 'id' ? id.transaksi_titipan : en.transaksi_titipan,
          id: 17,
          img: img_transaksi_titipan,
        },
        {
          title: bahasa === 'id' ? id.transaksi_valas : en.transaksi_valas,
          id: 18,
          img: img_transaksi_valas,
        },
        {
          title:
            bahasa === 'id'
              ? id.transaksi_wealth_manage
              : en.transaksi_wealth_manage,
          id: 19,
          img: img_transaksi_wealth,
        },
        {
          title: bahasa === 'id' ? id.deposito_online : en.deposito_online,
          id: 21,
          img: null,
        },
        {
          title: bahasa === 'id' ? id.Bancassurance : en.Bancassurance,
          id: 22,
          img: null,
        },
      ],
      itemTipeNasabah: [
        {
          title: bahasa === 'id' ? id.perorangan : en.perorangan,
          color: COLORS.red,
          id: 1,
          fontColor: 'white',
          img: v_people,
        },
        {
          title: bahasa === 'id' ? id.badan_usaha : en.badan_usaha,
          color: 'white',
          id: 2,
          fontColor: '#54575C',
          img: v_building,
        },
        {
          title: bahasa === 'id' ? id.rekening_gabungan : en.rekening_gabungan,
          color: 'white',
          id: 3,
          fontColor: '#54575C',
          img: v_people3,
        },
      ],
      itemJenisRekening: [
        {
          title: bahasa === 'id' ? id.rekening_tunggal : en.rekening_tunggal,
          color: COLORS.red,
          id: 1,
          fontColor: 'white',
          img: v_people2,
        },
        {
          title:
            bahasa === 'id'
              ? id.rekening_gabungan_and
              : en.rekening_gabungan_and,
          color: 'white',
          id: 2,
          fontColor: '#54575C',
          img: v_people4,
        },
        {
          title:
            bahasa === 'id' ? id.rekening_gabungan_or : en.rekening_gabungan_or,
          color: 'white',
          id: 3,
          fontColor: '#54575C',
          img: v_people5,
        },
      ],
      itemJenisPengaduan: [
        {
          label: 'Finansial',
          value: 'Financial',
        },
        {
          label: 'Non Finansial',
          value: 'Non Financial',
        },
      ],
      itemWealthManagement: [
        {
          title: bahasa === 'id' ? id.reksa_dana : en.reksa_dana,
          id: 1,
          img: null,
        },
      ],
      itemLayananReksaDana: [
        {
          title:
            bahasa == 'id' ? id.pembelian_reksa_dana : en.pembelian_reksa_dana,
          id: 1,
          img: null,
        },
        {
          title:
            bahasa == 'id' ? id.penjualan_reksa_dana : en.penjualan_reksa_dana,
          id: 2,
          img: null,
        },
        {
          title:
            bahasa == 'id'
              ? id.pengalihan_reksa_dana
              : en.pengalihan_reksa_dana,
          id: 3,
          img: null,
        },
        {
          title:
            bahasa == 'id'
              ? id.pembelian_reksa_dana_terproteksi
              : en.pembelian_reksa_dana_terproteksi,
          id: 4,
          img: null,
        },
        {
          title:
            bahasa == 'id'
              ? id.penjualan_reksa_dana_terproteksi
              : en.penjualan_reksa_dana_terproteksi,
          id: 5,
          img: null,
        },
      ],
      itemKategoriProduk: [],
      arrayPertanyaanProfilResiko: [],
      arrayJawabanProfilResiko: [],
      radioChoosen: {},
      currentQuestion: 0,
      percentProgress: '20%',
      portofolioDeposito: [],
      numColumns: 3,
      itemPerihalPengaduan: [],
      showFormBuilderQr: false,
      currentPage: 1,
      nextPage: 1,
      isOTPcorrect: true,
      isUnderDevelopment: false,
      modalWrongInquiry: false,
      transactionId: '',
      pin: '',
      intruksi: '',
      arrayInstruksi: [],
      choosenDeposito: 0,
      isResendOTP: false,
      previousPayload: null,
      interesetDeposito: 0,
      isUpdateSwafoto: false,
      showPopUpFailOcr: false,
      showPopUpFailSwafoto: false,
      depoCode_: '',
      isDoneProfilResiko: false,
      tipeProfilResikoNasabah: '',
      modalSuccesReksaDana: false,
      dataInquiryCIFbyNik: {},
      modalrequestSID: false,
      popUpAktivasiVimo: false,
      isRating: false,
      ratingStar: 0,
      comment: '',
      popUpFinishRating: false,
      loadingvisibleRating: false,
    };
    this.isLongTouchRef = React.createRef(this.state.isLongTouch);
    this.chatInputRef = React.createRef(TextInput);
    this.windowHeight = Dimensions.get('window').height;
    this.isLongTouchRef.current = this.state.isLongTouch;
    this.uiOpacity = 0;
    this.inputOpacity = 0;
    this.chatSendButtonScale = 0;
    this.touchTimer = null;
  }

  async componentDidMount() {
    this.interval = setInterval(() => {
      if (this.state.isInSession) {
        this.setState(prevState => ({
          timer: prevState.timer + 1,
        }));
      }
    }, 1000);
    this.zoomJoinSession();
    this.zoomListener();
    this.getTNC(1);
    this.getListProducts();
  }
  async componentWillUnmount() {
    clearInterval(this.interval);
  }
  getFormattedTime() {
    const minutes = Math.floor(this.state.timer / 60);
    const seconds = this.state.timer % 60;
    return `${minutes < 10 ? '0' : ''}${minutes}:${
      seconds < 10 ? '0' : ''
    }${seconds}`;
  }
  async zoomJoinSession() {
    var sessionNameStr = '';
    var sessionPasswordStr = '';
    var custNameStr = '';
    var signatureZoomStr = '';
    var sessionNames = await AsyncStorage.getItem('ws_session_name');
    if (sessionNames != null) {
      sessionNameStr = sessionNames.toString();
    }
    var sessionPassword = await AsyncStorage.getItem('ws_password');
    if (sessionPassword != null) {
      sessionPasswordStr = sessionPassword.toString();
    }
    var custName = await AsyncStorage.getItem('custName');
    if (custName != null) {
      custNameStr = custName.toString();
    }
    var signatureZoom = await AsyncStorage.getItem('signatureZoom');
    if (signatureZoom != null) {
      signatureZoomStr = signatureZoom.toString();
    }
    const token = signatureZoomStr;

    try {
      await this.props.zoom.joinSession({
        sessionName: sessionNameStr,
        sessionPassword: sessionPasswordStr,
        token: token,
        userName: custNameStr,
        audioOptions: {
          connect: true,
          mute: false,
        },
        videoOptions: {
          localVideoOn: true,
        },
        sessionIdleTimeoutMins: 40,
      });
    } catch (e) {
      Alert.alert('Failed to join the session');
    }
  }
  async sendOtp() {
    var payload = {
      msisdn: custNoHp,
      idDips: idDips,
    };
    console.log('payload sendOTP', payload);
    fetch(Server.BASE_URL_API + ApiService.sendOTP, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      timeout: 1500,
      body: JSON.stringify(payload),
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log('Response Send Otp', responseJson);
        if (responseJson.code < 300) {
          this.setState({transactionId: responseJson.data.transactionId});
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        console.error(error);
        Alert.alert('Gagal Terhubung ke server', JSON.stringify(error));
      });
  }
  async verifyOtp(sess) {
    var payload = {
      transactionId: this.state.transactionId,
      idDips: idDips,
      token: this.state.pin,
    };
    fetch(Server.BASE_URL_API + ApiService.verifyOTP, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      timeout: 1500,
      body: JSON.stringify(payload),
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log('Response verify Otp', responseJson);
        if (responseJson.code < 300) {
          this.setState({
            pin: '',
            transactionId: '',
            isOTPcorrect: true,
          });
          if (this.state.sessionBeforeOTP === 14) {
            this.loopPengajuan(sess);
          } else if (this.state.sessionBeforeOTP === 16) {
            this.loopPengajuan(sess);
          } else if (this.state.sessionBeforeOTP === 15) {
            //Pembukaan akun
            this.mirroringKey('pembukaanakun', {
              noponsel: '08' + custNoHp.slice(1),
              idForm: arrayIdForm[0],
            });
            this.loopPengajuanPembukaanAkun(sess);
          } else if (this.state.sessionBeforeOTP === 17) {
            //transaksi rekening sendiri
            this.loopPengajuan(sess);
          } else {
            this.setState({
              session: sess,
            });
          }
        } else {
          this.setState({isOTPcorrect: false});
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        console.error(error);
        Alert.alert('Gagal Terhubung ke server', JSON.stringify(error));
      });
  }
  mirroringSendIdDiPS() {
    var payload = {
      csId: csId,
      transaction: {
        idDips: idDips,
        bahasa: bahasa,
      },
    };
    fetch(Server.BASE_URL_API_RABBIT + ApiService.rabbitMirrorKey, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      timeout: 1500,
      body: JSON.stringify(payload),
    })
      .then(response => response.json())
      .then(responseJson => {})
      .catch(error => {
        this.setState({loadingvisible: false});
        console.error(error);
        Alert.alert('Gagal Terhubung ke server');
      });
  }
  zoomListener() {
    const sessionJoinListener = this.props.zoom.addListener(
      EventType.onSessionJoin,
      async session => {
        this.mirroringSendIdDiPS();
        this.setState({isInSession: true});
        this.toggleUI();
        var sessionNames = await this.props.zoom.session.getSessionName();
        this.setState({sessionName: sessionNames, loadingvisible: false});
        const mySelf = new ZoomVideoSdkUser(session.mySelf);
        const remoteUsers = await this.props.zoom.session.getRemoteUsers();
        const muted = await mySelf.audioStatus.isMuted();
        const videoOn = await mySelf.videoStatus.isOn();
        const speakerOn = await this.props.zoom.audioHelper.getSpeakerStatus();
        const originalAspectRatio =
          await this.props.zoom.videoHelper.isOriginalAspectRatioEnabled();
        const videoMirrored =
          await this.props.zoom.videoHelper.isMirrorMyVideoEnabled();
        const isReceiveSpokenLanguageContent =
          await this.props.zoom.liveTranscriptionHelper.isReceiveSpokenLanguageContentEnabled();

        this.setState({users: [mySelf, ...remoteUsers]});
        this.setState({
          isMuted: muted,
          isVideoOn: videoOn,
          isSpeakerOn: speakerOn,
          isOriginalAspectRatio: originalAspectRatio,
          isReceiveSpokenLanguageContentEnabled: isReceiveSpokenLanguageContent,
        });
      },
    );

    const sessionLeaveListener = this.props.zoom.addListener(
      EventType.onSessionLeave,
      () => {
        this.setState({isInSession: false, users: [], loadingvisible: false});
        Alert.alert('Session Timeout');
        this.props.navigation.navigate('SplashScreen');
      },
    );

    const sessionNeedPasswordListener = this.props.zoom.addListener(
      EventType.onSessionNeedPassword,
      () => {
        Alert.alert('SessionNeedPassword');
      },
    );

    const sessionPasswordWrongListener = this.props.zoom.addListener(
      EventType.onSessionPasswordWrong,
      () => {
        Alert.alert('SessionPasswordWrong');
      },
    );

    const userVideoStatusChangedListener = this.props.zoom.addListener(
      EventType.onUserVideoStatusChanged,
      async ({changedUsers}) => {
        const mySelf = new ZoomVideoSdkUser(
          await this.props.zoom.session.getMySelf(),
        );
        changedUsers.map(u => {
          if (mySelf.userId === u.userId) {
            mySelf.videoStatus
              .isOn()
              .then(on => this.setState({isVideoOn: on}));
          }
        });
      },
    );

    const userAudioStatusChangedListener = this.props.zoom.addListener(
      EventType.onUserAudioStatusChanged,
      async ({changedUsers}) => {
        const mySelf = new ZoomVideoSdkUser(
          await this.props.zoom.session.getMySelf(),
        );
        changedUsers.map(u => {
          if (mySelf.userId === u.userId) {
            mySelf.audioStatus
              .isMuted()
              .then(muted => this.setState({isMuted: muted}));
          }
        });
      },
    );

    const userJoinListener = this.props.zoom.addListener(
      EventType.onUserJoin,
      async ({remoteUsers}) => {
        if (!this.props.isMounted()) return;
        const mySelf = await this.props.zoom.session.getMySelf();
        const remote = remoteUsers.map(user => new ZoomVideoSdkUser(user));
        this.setState({users: [mySelf, ...remote]});
      },
    );

    const userLeaveListener = this.props.zoom.addListener(
      EventType.onUserLeave,
      async ({remoteUsers, leftUsers}) => {
        if (!this.props.isMounted()) return;
        const mySelf = await this.props.zoom.session.getMySelf();
        const remote = remoteUsers.map(user => new ZoomVideoSdkUser(user));
        this.setState({users: [mySelf, ...remote]});
      },
    );

    const chatNewMessageNotify = this.props.zoom.addListener(
      EventType.onChatNewMessageNotify,
      newMessage => {
        if (!this.props.isMounted()) return;
        this.setState({
          chatMessages: [
            new ZoomVideoSdkChatMessage(newMessage),
            ...this.state.chatMessages,
          ],
        });
      },
    );

    const chatDeleteMessageNotify = this.props.zoom.addListener(
      EventType.onChatDeleteMessageNotify,
      params => {},
    );

    const networkStatusChangeListener = this.props.zoom.addListener(
      EventType.onUserVideoNetworkStatusChanged,
      async ({user, status}) => {
        const networkUser = new ZoomVideoSdkUser(user);
        if (status == NetworkStatus.Bad) {
        }
      },
    );

    const requireSystemPermission = this.props.zoom.addListener(
      EventType.onRequireSystemPermission,
      ({permissionType}) => {
        switch (permissionType) {
          case SystemPermissionType.Camera:
            Alert.alert(
              "Can't Access Camera",
              'please turn on the toggle in system settings to grant permission',
            );
            break;
          case SystemPermissionType.Microphone:
            Alert.alert(
              "Can't Access Camera",
              'please turn on the toggle in system settings to grant permission',
            );
            break;
        }
      },
    );

    const eventErrorListener = this.props.zoom.addListener(
      EventType.onError,
      async error => {
        Alert.alert('Error: ' + error.error);
        switch (error.errorType) {
          case Errors.SessionJoinFailed:
            break;
          default:
        }
      },
    );

    return () => {
      sessionJoinListener.remove();
      sessionLeaveListener.remove();
      sessionPasswordWrongListener.remove();
      sessionNeedPasswordListener.remove();
      userVideoStatusChangedListener.remove();
      userAudioStatusChangedListener.remove();
      userJoinListener.remove();
      userLeaveListener.remove();
      chatNewMessageNotify.remove();
      eventErrorListener.remove();
      chatDeleteMessageNotify.remove();
      requireSystemPermission.remove();
      networkStatusChangeListener.remove();
    };
  }
  keyboardHeightChange = (isOpen, height) => {
    if (!isOpen) {
      this.scaleChatSend(false);
      this.chatInputRef.current?.clear();
    }

    this.setState({
      isKeyboardOpen: !isOpen,
      contentHeight: this.windowHeight - height,
    });
  };
  scaleChatSend = show => {
    const easeIn = Easing.in(Easing.exp);
    const easeOut = Easing.out(Easing.exp);
    this.chatSendButtonScale.value = withTiming(show ? 1 : 0, {
      duration: 500,
      easing: show ? easeIn : easeOut,
    });
  };
  toggleUI = () => {
    const easeIn = Easing.in(Easing.exp);
    const easeOut = Easing.out(Easing.exp);
    this.uiOpacity.value = withTiming(this.uiOpacity.value === 0 ? 100 : 0, {
      duration: 300,
      easing: this.uiOpacity.value === 0 ? easeIn : easeOut,
    });
    this.inputOpacity.value = withTiming(
      this.inputOpacity.value === 0 ? 100 : 0,
      {
        duration: 300,
        easing: this.inputOpacity.value === 0 ? easeIn : easeOut,
      },
    );
  };
  onListTouchStart = () => {
    this.touchTimer = setTimeout(() => {
      this.setState({isLongTouch: true});
    }, 200);
  };
  onListTouchEnd = event => {
    // Toggle UI behavior
    // - Toggle only when user list or chat list is tapped
    // - Block toggling when tapping on a list item
    // - Block toggling when keyboard is shown
    if (
      event._targetInst.elementType.includes('Scroll') &&
      this.state.isKeyboardOpen
    ) {
      !this.isLongTouchRef.current && this.toggleUI();
    }
    clearTimeout(this.touchTimer);
    this.setState({isLongTouch: false});
  };
  toggleUI = () => {
    const easeIn = Easing.in(Easing.exp);
    const easeOut = Easing.out(Easing.exp);
    this.uiOpacity.value = withTiming(this.uiOpacity.value === 0 ? 100 : 0, {
      duration: 300,
      easing: this.uiOpacity.value === 0 ? easeIn : easeOut,
    });
    this.inputOpacity.value = withTiming(
      this.inputOpacity.value === 0 ? 100 : 0,
      {
        duration: 300,
        easing: this.inputOpacity.value === 0 ? easeIn : easeOut,
      },
    );
  };
  sendChatMessage = async () => {
    this.chatInputRef.current?.clear();
    await this.props.zoom.chatHelper.sendChatToAll(this.state.chatMessage);
    this.setState({chatMessage: ''});
    // send the chat as a command
    this.props.zoom.cmdChannel.sendCommand(null, this.state.chatMessage);
  };
  deleteChatMessage = async (msgId, message) => {
    const canBeDelete =
      await this.props.zoom.chatHelper.canChatMessageBeDeleted(msgId);
    if (canBeDelete === true || msgId == null) {
      const error = await this.props.zoom.chatHelper.deleteChatMessage(msgId);
      if (error === Errors.Success) {
        const chatIndex = this.state.chatMessages.indexOf(message);
        this.state.chatMessages.splice(chatIndex, 1);
        this.setState({refreshFlatlist: !refreshFlatlist});
      } else {
        Alert.alert(error);
      }
    } else {
      Alert.alert('Message could not be deleted');
    }
  };
  leaveSession = endSession => {
    this.setState({isInSession: false, isRating: true});
    this.props.zoom.leaveSession(endSession);
  };
  onPressAudio = async () => {
    const mySelf = await this.props.zoom.session.getMySelf();
    const muted = await mySelf.audioStatus.isMuted();
    this.setState({isMuted: muted});
    muted
      ? await this.props.zoom.audioHelper.unmuteAudio(mySelf.userId)
      : await this.props.zoom.audioHelper.muteAudio(mySelf.userId);
  };
  onPressLeave = async () => {
    const mySelf = await this.props.zoom.session.getMySelf();
    const options = [
      {
        text: 'Leave Session',
        onPress: () => this.leaveSession(false),
      },
    ];

    if (mySelf.isHost) {
      options.unshift({
        text: 'End Session',
        onPress: () => this.leaveSession(true),
      });
    }

    if (Platform.OS === 'ios') {
      ActionSheetIOS.showActionSheetWithOptions(
        {
          options: ['Cancel', ...options.map(option => option.text)],
          cancelButtonIndex: 0,
        },
        buttonIndex => {
          if (buttonIndex !== 0) {
            options[buttonIndex - 1].onPress();
          }
        },
      );
    } else {
      Alert.alert('Do you want to leave this session?', '', options, {
        cancelable: true,
      });
    }
  };

  //function dari mas rafii
  formatData = (data, numColumns) => {
    if (data !== undefined) {
      const numberOfFullRows = Math.floor(data.length / numColumns);
      let numberOfElementsLastRow = data.length - numberOfFullRows * numColumns;
      while (
        numberOfElementsLastRow !== numColumns &&
        numberOfElementsLastRow !== 0
      ) {
        data.push({key: `blank-${numberOfElementsLastRow}`, empty: true});
        numberOfElementsLastRow++;
      }
      return data;
    }
  };
  async getListProducts() {
    dataresp = [
      {
        id: 'OpenRek',
        title: bahasa === 'id' ? id.buka_rekening : en.buka_rekening,
        img: ic_open_acc,
      },
    ];
    fetch(`${Server.BASE_URL_API}${ApiService.getListProduct}`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.code < 300) {
          responseJson.data.map((element, idx) => {
            dataresp.push({
              id: element.produkId.idProduk,
              title: element.produkId.namaProduk,
              img: ic_list_saving,
            });
          });
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        Alert.alert('Gagal Terhubung ke server', error);
      });

    this.setState({itemProduct: dataresp});
  }
  async getTNC(idTnc) {
    console.log('masuk tnc');
    fetch(`${Server.BASE_URL_API}${ApiService.getTNC}${idTnc}`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log('Response TNC', responseJson);
        if (responseJson.code < 300) {
          if (bahasa === 'id') {
            this.setState({
              htmlTNC: responseJson.data.data.labelIdn.replaceAll(
                '<p><br></p>',
                '',
              ),
            });
          } else {
            this.setState({
              htmlTNC: responseJson.data.data.labelEng.replaceAll(
                '<p><br></p>',
                '',
              ),
            });
          }
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        Alert.alert('Gagal Terhubung ke server', error);
      });
  }
  async processOCR() {
    this.setState({loadingvisible: true});

    var payload = {
      image: base64KTP,
    };
    fetch(Server.BASE_URL_API + ApiService.ocrKTP, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: JSON.stringify(payload),
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({loadingvisible: false});

        if (responseJson.code < 300) {
          //SetValue
          nik_ = responseJson.data.nik;
          nama_lengkap_ = responseJson.data.nama;
          tempat_lahir_ =
            responseJson.data.ttl !== null
              ? responseJson.data.ttl.split(',')[0].trim()
              : '';
          tgl_lahir_ =
            responseJson.data.ttl !== null
              ? responseJson.data.ttl.split(',')[1].trim()
              : '';
          jenis_kelamin_ = responseJson.data.jeniskelamin;
          golongan_darah_ = responseJson.data.golongan_darah;
          alamat_ = responseJson.data.alamat;
          rt_rw_ = responseJson.data.rtrw;
          provinsi_ = responseJson.data.provinsi;
          kabupaten_kota_ = responseJson.data.kota_kabupaten;
          kecamatan_ = responseJson.data.kecamatan;
          kelurahan_desa_ = responseJson.data.desa_kelurahan;
          agama_ = responseJson.data.agama;
          status_perkawinan_ = responseJson.data.status_perkawinan;
          kewarganegaraan_ = responseJson.data.kewarganegaraan;
          pekerjaan_ = responseJson.data.pekerjaan;
          this.setState({modalOCR: true});
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        console.error(error);
        Alert.alert('Gagal Terhubung ke server');
      });
  }
  async processOCR2() {
    this.setState({loadingvisible: true});
    var payload = {
      image: base64KTP,
    };
    fetch(Server.BASE_URL_API + ApiService.ocrKTP, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: JSON.stringify(payload),
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log('responseJson OCR2', responseJson);
        if (responseJson.code < 300) {
          //SetValue
          nik_ = responseJson.data.nik;
          nama_lengkap_ = responseJson.data.nama;
          tempat_lahir_ =
            responseJson.data.ttl !== null
              ? responseJson.data.ttl.split(',')[0].trim()
              : '';
          tgl_lahir_ =
            responseJson.data.ttl !== null
              ? responseJson.data.ttl.split(',')[1].trim()
              : '';
          jenis_kelamin_ = responseJson.data.jeniskelamin;
          golongan_darah_ = responseJson.data.golongan_darah;
          alamat_ = responseJson.data.alamat;
          rt_rw_ = responseJson.data.rtrw;
          provinsi_ = responseJson.data.provinsi;
          kabupaten_kota_ = responseJson.data.kota_kabupaten;
          kecamatan_ = responseJson.data.kecamatan;
          kelurahan_desa_ = responseJson.data.desa_kelurahan;
          agama_ = responseJson.data.agama;
          status_perkawinan_ = responseJson.data.status_perkawinan;
          kewarganegaraan_ = responseJson.data.kewarganegaraan;
          pekerjaan_ = responseJson.data.pekerjaan;
          accessToken = responseJson.token;
          exchangeToken = responseJson.exchange;
          this.processSwafoto2();
        } else {
          accessToken = responseJson.token;
          exchangeToken = responseJson.exchange;
          this.setState({loadingvisible: false});
          setTimeout(() => {
            this.setState({showPopUpFailOcr: true});
          }, 2000);
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        console.error(error);
        Alert.alert('Gagal Terhubung ke server');
      });
  }
  async processSwafoto2() {
    let formdata = new FormData();
    formdata.append('firstImage', {
      uri: uri_1,
      name: 'image.jpg',
      type: 'image/jpeg',
    });
    formdata.append('secondImage', {
      uri: uri_2,
      name: 'image.jpg',
      type: 'image/jpeg',
    });

    fetch(Server.BASE_URL_API + ApiService.swafotoCheck, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: formdata,
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.code < 300) {
          //Jika muka sesuai dengan KTP
          accessToken = responseJson.token;
          exchangeToken = responseJson.exchange;
          this.setState({loadingvisible: false});
          setTimeout(() => {
            this.setState({modalOCR: true});
          }, 2000);
        } else {
          accessToken = responseJson.token;
          exchangeToken = responseJson.exchange;
          //Jika muka tidak sesuai dengan KTP
          this.setState({loadingvisible: false});
          setTimeout(() => {
            this.setState({showPopUpFailSwafoto: true});
          }, 2000);
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        console.error(error);
        Alert.alert('Gagal Terhubung ke server');
      });
  }
  async processDukcapil() {
    this.setState({modalOCR: false});
    this.setState({loadingvisible: true});
    var payload = {
      idDips: idDips,
      propinsi: provinsi_.trim(),
      kabupaten: kabupaten_kota_.trim(),
      nik: nik_.trim(),
      namaLengkap: nama_lengkap_.trim(),
      tempatlahir: tempat_lahir_.trim(),
      tglLahir: tgl_lahir_.trim(),
      jenisKelamin: jenis_kelamin_.trim(),
      namaLengkapIbu: nama_ibu_kandung_.trim(),
      alamat: alamat_.trim(),
      rt: rt_rw_.split('/')[0].trim(),
      rw: rt_rw_.split('/')[1].trim(),
      kelurahan: kelurahan_desa_.trim(),
      kecamatan: kecamatan_.trim(),
      statusKawin: status_perkawinan_.trim(),
      jenisPekerjaan: pekerjaan_.trim(),
    };
    fetch(Server.BASE_URL_API + ApiService.validasiDuckapil, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: JSON.stringify(payload),
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.status === 'oke') {
          //Berhasil Dukcapil
          this.processDTOTT();
        } else {
          //Gagal Dukcapil
          this.setState({loadingvisible: false});
          nama_ibu_kandung_ = '';
          this.setState({modalFailedDukcapil: true});
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        console.error(error);
        Alert.alert('Gagal Terhubung ke server');
      });
  }
  async processDTOTT() {
    var payload = {
      idDips: idDips,
      propinsi: provinsi_.trim(),
      kabupaten: kabupaten_kota_.trim(),
      nik: nik_.trim(),
      namaLengkap: nama_lengkap_.trim(),
      tempatlahir: tempat_lahir_.trim(),
      tglLahir: tgl_lahir_.trim(),
      jenisKelamin: jenis_kelamin_.trim(),
      namaLengkapIbu: nama_ibu_kandung_.trim(),
      alamat: alamat_.trim(),
      rt: rt_rw_.split('/')[0].trim(),
      rw: rt_rw_.split('/')[1].trim(),
      kelurahan: kelurahan_desa_.trim(),
      kecamatan: kecamatan_.trim(),
      statusKawin: status_perkawinan_.trim(),
      jenisPekerjaan: pekerjaan_.trim(),
    };
    fetch(Server.BASE_URL_API + ApiService.validasiDTOTT, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: JSON.stringify(payload),
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.status === 'oke') {
          //Berhasil DTOTT
          this.processAttachment('ktp', fileUriKTP);
        } else {
          //Gagal DTOTT
          this.setState({loadingvisible: false});
          this.setState({modalFailedDTOTT: true});
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        console.error(error);
        Alert.alert('Gagal Terhubung ke server');
      });
  }
  async processAttachment(path, uri_photo) {
    let formdata = new FormData();
    formdata.append('idDips', idDips);
    formdata.append(path, {
      uri: uri_photo,
      name: 'image.jpg',
      type: 'image/jpeg',
    });
    fetch(Server.BASE_URL_API + ApiService.formAttachment, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: formdata,
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({loadingvisible: false});
        console.log('processAttachment', responseJson);
        if (responseJson.code < 300) {
          this.setState({modalOCR: false});

          if (this.state.session == 4) {
            this.setState({session: 5});
          } else if (this.state.session == 5) {
            this.setState({session: 6});
          } else if (this.state.session == 6) {
            this.setState({session: 7});
          } else if (this.state.session == 7) {
            this.setState({formCode: '10', session: 8});
            this.formBuilder('10');
          }
        } else {
          //Gagal Attachment
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        console.error(error);
        Alert.alert('Gagal Terhubung ke server');
      });
  }
  async processSwafoto() {
    this.setState({loadingvisible: true});
    let formdata = new FormData();
    formdata.append('firstImage', {
      uri: uri_1,
      name: 'image.jpg',
      type: 'image/jpeg',
    });
    formdata.append('secondImage', {
      uri: uri_2,
      name: 'image.jpg',
      type: 'image/jpeg',
    });

    fetch(Server.BASE_URL_API + ApiService.swafotoCheck, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: formdata,
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.code < 300) {
          this.processAttachment('foto', fileUriSwafoto);
        } else {
          this.setState({loadingvisible: false});

          this.setState({modalFailedSwafoto: true});
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        console.error(error);
        Alert.alert('Gagal Terhubung ke server');
      });
  }
  getDropdown(url) {
    var dataItemList = [];
    var placeholder = '';
    var result = {};
    fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
    })
      .then(response => response.json())
      .then(responseJson => {
        responseJson.data.map(element => {
          if (responseJson.code < 300) {
            placeholder = responseJson.data[0];
            dataItemList.push({
              id: element.id !== undefined ? element.id : null,
              label: element.hasOwnProperty('prefix')
                ? element.prefix + ' - ' + element.name
                : bahasa === 'id'
                ? element.labelIdn
                : element.labelEng,
              value: element.hasOwnProperty('code')
                ? element.prefix
                : bahasa === 'id'
                ? element.labelIdn
                : element.labelEng,
              beneficiaryCode: element.hasOwnProperty('beneficiaryCode')
                ? element.beneficiaryCode
                : null,
              swiftCode: element.hasOwnProperty('swiftCode')
                ? element.swiftCode
                : null,
              cityCode: element.hasOwnProperty('cityCode')
                ? element.cityCode
                : null,
              valueCode: element.hasOwnProperty('valueCode')
                ? element.valueCode
                : null,
            });
          }
        });
      })
      .catch(error => {
        Alert.alert('Gagal Terhubung ke server', error);
      });
    result = {
      placeholder:
        bahasa === 'id' ? '== Silakan Pilih ==' : '== Please Choose ==',
      list: dataItemList,
    };
    return result;
  }
  async formBuilder(Code) {
    this.setState({loadingvisible: true});
    dataresp = [];
    rdBtn = [];
    console.log(`${Server.BASE_URL_API}${ApiService.formBuilder}${Code}`);
    fetch(`${Server.BASE_URL_API}${ApiService.formBuilder}${Code}`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({loadingvisible: false});
        this.setState({modalVisible: true});
        if (responseJson.code < 300) {
          const response = JSON.parse(responseJson.data.data);
          response.map(element => {
            element.components.map(componentx => {
              dataresp.push({
                component: componentx.name,
                field_name: componentx.placeholder.placeholderIdn,
                is_mandatory: 'false',
                type:
                  componentx.label.labelIdn === 'NIK'
                    ? 'number'
                    : componentx.type,
                meta: {
                  listdd:
                    componentx.name === 'Dropdown'
                      ? this.getDropdown(componentx.url)
                      : '',
                  url: componentx.url,
                  label:
                    bahasa === 'id'
                      ? componentx.label.labelIdn
                      : componentx.label.labelEng,
                  pointer: componentx.label.labelIdn
                    ? componentx.label.labelIdn
                    : element.components[0].label.labelIdn,
                  placeholder:
                    bahasa === 'id'
                      ? componentx.placeholder.placeholderIdn
                      : componentx.placeholder.placeholderEng,
                },
              });
            });
          });
          this.setState({setRefreshing: false});
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        Alert.alert('Gagal Terhubung ke server', error);
      });
  }
  async formBuilder2(form) {
    this.setState({loadingvisible: true});
    dataresp = [];
    console.log(`${Server.BASE_URL_API}${ApiService.formBuilder}${form}`);
    fetch(`${Server.BASE_URL_API}${ApiService.formBuilder}${form}`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.code < 300) {
          const response = JSON.parse(responseJson.data.data);
          response.map(element => {
            element.components.map((componentx, index) => {
              dataresp.push({
                component: componentx.name,
                field_name: componentx.placeholder.placeholderIdn,
                is_mandatory: 'false',
                type:
                  componentx.label.labelIdn === 'NIK'
                    ? 'number'
                    : componentx.type,
                meta: {
                  multiline: componentx.name === 'TextArea' ? true : false,
                  numberOfLines: componentx.name === 'TextArea' ? 2 : 1,
                  listdd:
                    componentx.name === 'Dropdown' ||
                    componentx.name === 'Datalist'
                      ? this.getDropdown(componentx.url)
                      : componentx.name === 'DropdownSumberDana'
                      ? {
                          placeholder:
                            bahasa === 'id'
                              ? '== Silakan Pilih =='
                              : '== Please Choose ==',
                          list: this.state.dataPortofolio,
                        }
                      : '',
                  url: componentx.url,
                  label:
                    bahasa === 'id'
                      ? componentx.label.labelIdn
                      : componentx.label.labelEng,
                  pointer:
                    componentx.label.labelIdn === ''
                      ? element.components[index - 1].label.labelIdn
                      : componentx.label.labelIdn,
                  placeholder:
                    bahasa === 'id'
                      ? componentx.placeholder.placeholderIdn
                      : componentx.placeholder.placeholderEng,
                },
              });
              this.setState({loadingvisible: false});
            });
          });

          if (this.state.session == 28) {
            dataresp.splice(1, 0, {
              component: 'ProfilResiko',
              field_name: 'Profil Resiko',
              is_mandatory: false,
              type: 'Button',
              meta: {
                multiline: false,
                numberOfLines: 1,
                listdd: [],
                url: '',
                label: '',
                pointer: '',
                placeholder: '',
              },
            });
            console.log('dataResp', dataresp);
          }
          this.setState({sessionTransaksi: 1, setRefreshing: false});
        } else {
          this.setState({loadingvisible: false});
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        console.log('Error Form', '' + error);
        Alert.alert('Gagal Terhubung ke server', error);
      });
  }
  async getNoRekening() {
    var List = [];
    var payload = {
      noCif: noCIF,
      bahasa: bahasa,
    };

    fetch(Server.BASE_URL_API + ApiService.listAccount, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: JSON.stringify(payload),
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.code < 300) {
          console.log('response get no rek', responseJson);
          this.setState({dataPortofolio: responseJson.data});
          responseJson.data.portotabungan.map(element => {
            List.push({
              label: element.accountNo,
              value: element.accountNo,
            });
          });
          this.setState({ListNoRek: List});
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        Alert.alert('Gagal Terhubung ke server', error);
      });
  }
  async processOCRNPWP() {
    var payload = {
      image: base64NPWP,
    };
    fetch(Server.BASE_URL_API + ApiService.ocrNPWP, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: JSON.stringify(payload),
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({loadingvisible: false});

        if (responseJson.data !== null) {
          if (responseJson.data.npwpId !== undefined) {
            npwp_ = responseJson.data.npwpId;
          }
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        console.error(error);
        Alert.alert('Gagal Terhubung ke server');
      });
  }
  async hitComplain() {
    this.setState({loadingvisible: true});
    var payload = {
      idDips: idDips,
      data: {
        nama: custName,
        detailpengaduan: detailPengaduan,
        nomorrekening: noRekening,
        nomortelepon: custNoHp,
        alamatemail: custEmail,
      },
    };
    fetch(Server.BASE_URL_API + ApiService.komplain, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: JSON.stringify(payload),
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.code < 300) {
          noPengaduan = responseJson.data.noPengaduan;
          this.mirroringKeyKomplain();
          this.processAttachmentKomplain(responseJson.data.noPengaduan);
        } else {
          this.setState({loadingvisible: false});
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        Alert.alert('Gagal Terhubung ke server', error);
      });
  }
  async processAttachmentKomplain(noPengaduan) {
    let formdata = new FormData();
    formdata.append('noPengaduan', noPengaduan);
    if (this.state.maxFilePendukung == 1) {
      formdata.append('buktiPendukung1', {
        uri: fileUriPendukung,
        name: 'image.jpg',
        type: 'image/jpeg',
      });
    } else if (this.state.maxFilePendukung == 2) {
      formdata.append('buktiPendukung1', {
        uri: fileUriPendukung,
        name: 'image.jpg',
        type: 'image/jpeg',
      });
      formdata.append('buktiPendukung2', {
        uri: fileUriPendukung2,
        name: 'image.jpg',
        type: 'image/jpeg',
      });
    } else if (this.state.maxFilePendukung == 3) {
      formdata.append('buktiPendukung1', {
        uri: fileUriPendukung,
        name: 'image.jpg',
        type: 'image/jpeg',
      });
      formdata.append('buktiPendukung2', {
        uri: fileUriPendukung2,
        name: 'image.jpg',
        type: 'image/jpeg',
      });
      formdata.append('buktiPendukung3', {
        uri: fileUriPendukung3,
        name: 'image.jpg',
        type: 'image/jpeg',
      });
    }
    fetch(Server.BASE_URL_API + ApiService.formKomplainAddMedia, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: formdata,
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({loadingvisible: false});

        if (responseJson.code < 300) {
          //Berhasil Attachment
          this.getResiKomplain(noPengaduan);
        } else {
          //Gagal Attachment
          this.setState({loadingvisible: true});
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        console.error(error);
        Alert.alert('Gagal Terhubung ke server');
      });
  }
  async getResiKomplain(noPengaduan) {
    fetch(
      Server.BASE_URL_API +
        ApiService.resiFormPengaduan +
        noPengaduan +
        '?bahasa=' +
        bahasa,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + accessToken,
          exchangeToken: exchangeToken,
        },
        timeout: 1500,
      },
    )
      .then(response => response.json())
      .then(responseJson => {
        this.setState({loadingvisible: false});
        if (responseJson.code < 300) {
          //Berhasil Attachment
          base64Resi = `data:image/png;base64,${responseJson.data.image}`;
          linkPDFResi = responseJson.data.pdf;

          this.setState({isResiPengaduan: true});
          this.setState({session: 13});
          this.mirroringEndpoint(130);
        } else {
          //Gagal Attachment
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        console.error(error);
        Alert.alert('Gagal Terhubung ke server');
      });
  }
  async inqueryCIF() {
    var payload = {
      noKtp: nik_,
      idDips: idDips,
    };
    fetch(Server.BASE_URL_API + ApiService.inqueryCIF, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: JSON.stringify(payload),
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.code < 300) {
          accessToken = responseJson.token;
          exchangeToken = responseJson.exchange;
          tempat_lahir_ = responseJson.data.tempatLahir;
          tgl_lahir_ = moment(
            responseJson.data.tanggalLahir,
            'YYYYMMDD',
          ).format('DD-MM-YYYY');
          nama_ibu_kandung_ = responseJson.data.namaIbu;
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        Alert.alert('Gagal Terhubung ke server', error);
      });
  }
  launchImageLibraryfunc = () => {
    var options = {
      mediaType: 'photo',
      includeBase64: true,
      maxHeight: 200,
      maxWidth: 200,
    };
    launchImageLibrary(options, response => {
      if (response.didCancel) {
      } else if (response.error) {
      } else if (response.customButton) {
        alert(response.customButton);
      } else {
        if (this.state.session == 4) {
          fileUriKTP = response.assets[0].uri;
          base64KTP = response.assets[0].base64;
          this.setState({session: 4});
        } else if (this.state.session == 6) {
          fileUriNPWP = response.assets[0].uri;
          base64NPWP = response.assets[0].base64;
          this.setState({session: 6});
        } else if (this.state.session == 7) {
          fileUriTTD = response.assets[0].uri;
          base64TTD = response.assets[0].base64;
          this.setState({session: 7});
        } else if (this.state.session == 13) {
          if (this.state.pendukung) {
            if (this.state.maxFilePendukung == 1) {
              fileNamePendukung = response.assets[0].fileName;
              fileUriPendukung = response.assets[0].uri;
              base64Pendukung = response.assets[0].base64;
            } else if (this.state.maxFilePendukung == 2) {
              fileNamePendukung2 = response.assets[0].fileName;
              fileUriPendukung2 = response.assets[0].uri;
              base64Pendukung2 = response.assets[0].base64;
            } else if (this.state.maxFilePendukung == 3) {
              fileNamePendukung3 = response.assets[0].fileName;
              fileUriPendukung3 = response.assets[0].uri;
              base64Pendukung3 = response.assets[0].base64;
            }
          }
          this.mirroringKeyKomplain();
          this.setState({pendukung: false});
          this.setState({session: 13});
        } else if (
          this.state.session == 14 ||
          this.state.session == 16 ||
          this.state.session == 17
        ) {
          this.extractBarcode(
            response.assets[0].uri,
            response.assets[0].fileName,
          );
        }
      }
    });
  };
  extractBarcode = async (uri, file_name) => {
    this.setState({loadingvisible: true});
    RNQRGenerator.detect({
      uri: uri,
    })
      .then(response => {
        this.setState({loadingvisible: false});
        const {values} = response;
        if (values.length == 0) {
          this.setState({
            currentPage:
              this.state.currentPage > 1 ? this.state.currentPage - 1 : 1,
          });
          if (arrayBarcode.length === 0) {
            this.setState({
              showFormBuilderQr: false,
              currentPage: 1,
            });
          }
          Alert.alert(
            bahasa === 'id' ? id.qrcode_tidak_sesuai : en.qrcode_tidak_sesuai,
          );
        } else {
          arrayUploadBarcode[this.state.transaksiKe].nameBarcode = file_name;
          this.getObjectQR(values[0]);
          console.log('arrayUploadBarcode', arrayUploadBarcode);
          console.log('Hasil Decode QRCode', values[0]);
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        console.log('Cannot detect QR code in image', error);
      });
  };
  async getNoForm() {
    fetch(Server.BASE_URL_API + ApiService.generateNoForm, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      timeout: 1500,
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.code < 300) {
          arraynoForm.push(responseJson.data);
          console.log('ArrayNoForm', arraynoForm);
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        console.error(error);
        Alert.alert('Gagal Terhubung ke server');
      });
  }
  async getObjectQR(url) {
    fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.code < 300) {
          if (
            arraynoForm.some(item => item.noForm === responseJson.data.noForm)
          ) {
            delete arrayUploadBarcode[this.state.transaksiKe].nameBarcode;
            Alert.alert(bahasa === 'id' ? id.alert_same_qr : en.alert_same_qr);
          } else {
            responseJson.data.data.jenisform = 'qr';
            arrayBarcode.push(responseJson.data.data);
            arraynoForm.push(responseJson.data);
            this.formBuilder2(
              this.state.session === 14
                ? 48
                : this.state.session === 16
                ? 49
                : this.state.session === 17
                ? 56
                : 0,
            );
            console.log('arrayBarcode', arrayBarcode);
            console.log('arraynoForm', arraynoForm);
            this.setState({showFormBuilderQr: true});
          }
        } else {
          delete arrayUploadBarcode[this.state.transaksiKe].nameBarcode;
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        console.error(error);
        delete arrayUploadBarcode[this.state.transaksiKe].nameBarcode;
        Alert.alert('Gagal Terhubung ke server');
      });
  }
  async getPortoDeposito() {
    this.setState({loadingvisible: true});
    fetch(Server.BASE_URL_API + ApiService.getPortoDeposito + noCIF, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({loadingvisible: false});
        if (responseJson.code < 300) {
          console.log('responseJson Porto', responseJson);
          this.setState({portofolioDeposito: responseJson.data});
        } else if (responseJson.code === 404) {
          this.setState({portofolioDeposito: []});
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        console.error(error);
        delete arrayUploadBarcode[this.state.transaksiKe].nameBarcode;
        Alert.alert('Gagal Terhubung ke server');
      });
  }
  onRefresh = () => {
    console.log('masuk onRefresh');
    this.setState({setRefreshing: true});
    setTimeout(() => {
      this.formBuilder(this.state.formCode);
    }, 100);
  };
  takePicture = async () => {
    if (this.camera) {
      const options = {quality: 0.5, base64: true};
      const data = await this.camera.takePictureAsync(options);

      if (
        this.state.session == 4 ||
        this.state.session == 6 ||
        this.state.session == 7 ||
        this.state.session == 13
      ) {
        var cropData = {
          offset: {x: 132, y: 830},
          size: {width: 1620, height: 2300},
          displaySize: {width: 700, height: 440},
          resizeMode: 'cover',
        };
        //Crop Image by border
        ImageEditor.cropImage(data.uri, cropData).then(url => {
          //Convert Uri to Base64
          RNFS.readFile(url, 'base64').then(res => {
            if (this.state.session == 4) {
              //KTP
              fileUriKTP = url;
              base64KTP = res;
              this.setState({
                openCamera: false,
                session: 4,
              });
            } else if (this.state.session == 6) {
              //NPWP
              fileUriNPWP = url;
              base64NPWP = res;
              this.setState({
                openCamera: false,
                session: 6,
              });
            } else if (this.state.session == 7) {
              //TTD
              fileUriTTD = url;
              base64TTD = res;
              this.setState({
                openCamera: false,
                session: 7,
              });
            } else if (this.state.session == 13) {
              //KTP
              fileUriKTP = url;
              base64KTP = res;
              this.setState({
                openCamera: false,
                session: 13,
              });
              this.mirroringKeyKomplain();
            }
          });
        });
      } else if (this.state.session == 5) {
        fileUriSwafoto = data.uri;
        base64Swafoto = data.base64;
        var cropDataPeople = {
          offset: {x: 132, y: 250},
          size: {width: 1120, height: 2300},
          displaySize: {width: 700, height: 740},
          resizeMode: 'cover',
        };
        var cropDataKTP = {
          offset: {x: 960, y: 2150},
          size: {width: 290, height: 440},
          displaySize: {width: 650, height: 627},
          resizeMode: 'cover',
        };

        //Crop Image KTP by border
        ImageEditor.cropImage(data.uri, cropDataKTP).then(url => {
          uri_2 = url;
        });
        //Crop Image PEOPLE
        ImageEditor.cropImage(data.uri, cropDataPeople).then(url => {
          uri_1 = url;
        });
        this.setState({
          openCamera: false,
          session: this.state.session == 5 ? 5 : 24,
        });
      } else if (this.state.session == 24) {
        fileUriSwafoto = data.uri;
        var cropDataPeople = {
          offset: {x: 132, y: 250},
          size: {width: 1420, height: 2300},
          displaySize: {width: 700, height: 740},
          resizeMode: 'cover',
        };
        var cropDataKTP = {
          offset: {x: 162, y: 2000},
          size: {width: 1170, height: 800},
          displaySize: {width: 774, height: 530},
          resizeMode: 'contain',
        };
        var cropPeopleDataKTP = {
          offset: {x: 960, y: 2150},
          size: {width: 290, height: 440},
          displaySize: {width: 650, height: 627},
          resizeMode: 'cover',
        };

        //Crop Image KTP by border
        ImageEditor.cropImage(data.uri, cropDataKTP).then(url => {
          //Convert Uri to Base64
          fileUriKTP = url;
          RNFS.readFile(url, 'base64').then(res => {
            base64KTP = res;
          });
        });
        //Crop Image People KTP by border
        ImageEditor.cropImage(data.uri, cropPeopleDataKTP).then(url => {
          uri_2 = url;
        });
        //Crop Image PEOPLE
        ImageEditor.cropImage(data.uri, cropDataPeople).then(url => {
          uri_1 = url;
        });
        this.setState({
          openCamera: false,
          session: 24,
        });
      }
    }
  };
  getValueDropdown = valu => {
    this.setState({idSchedule: valu.value});
  };
  onSubmitPengaduan = () => {
    if (noRekening === '' || noRekening === '== Silakan Pilih ===') {
      Alert.alert('Silahkan Pilih No Rekening Anda');
    } else if (this.state.maxFilePendukung < 1) {
      Alert.alert('Silahkan Upload File Pendukung Anda Maksimal 3 File');
    } else if (!this.state.pernyataanPengaduan) {
      Alert.alert('Anda belum menyetujui pernyataan');
    } else {
      this.hitComplain();
    }
  };
  mirroringKeyKomplain() {
    payloadMirroringKomplain = {
      tanggalpengaduan: this.state.dateNow,
      nama: custName,
      nomorrekening: noRekening,
      nomorhandphone: custNoHp,
      alamatemail: custEmail,
      tempatlahir: tempat_lahir_,
      tanggallahir: tgl_lahir_,
      detailpengaduan: detailPengaduan,
      pernyataan: boolPernyataan,
      idDips: idDips,
      noPengaduan: noPengaduan,
      filependukung: [
        fileNamePendukung,
        fileNamePendukung2,
        fileNamePendukung3,
      ],
      namaibukandung: nama_ibu_kandung_,
    };
    var payload = {
      csId: csId,
      transaction: {
        komplain: payloadMirroringKomplain,
      },
    };

    fetch(Server.BASE_URL_API_RABBIT + ApiService.rabbitMirrorKey, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      timeout: 1500,
      body: JSON.stringify(payload),
    })
      .then(response => response.json())
      .then(responseJson => {})
      .catch(error => {
        this.setState({loadingvisible: false});
        console.error(error);
        Alert.alert('Gagal Terhubung ke server');
      });
  }
  mirroringKey(key_, payload_) {
    var payload = {
      csId: csId,
      transaction: {
        [key_]: payload_,
      },
    };
    console.log('payload mirroring key', payload_);
    fetch(Server.BASE_URL_API_RABBIT + ApiService.rabbitMirrorKey, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      timeout: 1500,
      body: JSON.stringify(payload),
    })
      .then(response => response.json())
      .then(responseJson => {})
      .catch(error => {
        console.error(error);
        Alert.alert('Gagal Mirroring');
      });
  }
  mirroringCIF() {
    var payload = {
      csId: csId,
      transaction: {
        noCif: noCIF,
      },
    };
    fetch(Server.BASE_URL_API_RABBIT + ApiService.rabbitMirrorKey, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      timeout: 1500,
      body: JSON.stringify(payload),
    })
      .then(response => response.json())
      .then(responseJson => {})
      .catch(error => {
        this.setState({loadingvisible: false});
        console.error(error);
        Alert.alert('Gagal Terhubung ke server');
      });
  }
  mirroringEndpoint(endpoint) {
    var payload = {
      csId: csId,
      endpoint: Number(endpoint),
    };
    console.log('payload mirroring endpoint', payload);
    fetch(Server.BASE_URL_API_RABBIT + ApiService.rabbitMirrorEnd, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      timeout: 1500,
      body: JSON.stringify(payload),
    })
      .then(response => response.json())
      .then(responseJson => {})
      .catch(error => {
        this.setState({loadingvisible: false});
        console.error(error);
        Alert.alert('Gagal Terhubung ke server');
      });
  }
  popUpBerhasilDownloadResi() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.modalSuksesResi}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({modalSuksesResi: false});
        }}>
        <View
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View
              style={{
                margin: 100,
                backgroundColor: 'white',
                borderRadius: 20,
                justifyContent: 'space-between',
                padding: 35,
                width: '80%',
                alignItems: 'center',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
              }}>
              <FastImage
                source={logoSuccess}
                style={{width: 55, height: 55, marginBottom: 15}}
              />
              <Text
                style={{
                  marginBottom: 20,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  fontWeight: 'bold',
                  textAlign: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                'Berhasil Menyimpan'
              </Text>
              <Pressable
                style={{
                  width: '100%',
                  height: 43,
                  borderRadius: 10,
                  padding: 10,
                  elevation: 2,
                  backgroundColor: COLORS.red,
                }}
                onPress={() => {
                  this.setState({session: 11});
                  this.setState({isResiPengaduan: false});
                  this.setState({modalSuksesResi: false});
                }}>
                <Text style={styles.textbtn}>OK</Text>
              </Pressable>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
  downloadResi = (link, name) => {
    const {dirs} = RNFetchBlob.fs;
    const dirToSave = dirs.DocumentDir;
    const configfb = {
      fileCache: true,
      useDownloadManager: true,
      notification: true,
      mediaScannable: true,
      title: name,
      path: `${dirToSave}/${name + '.pdf'}`,
    };
    const configOptions = Platform.select({
      ios: {
        fileCache: configfb.fileCache,
        title: configfb.title,
        path: configfb.path,
        appendExt: 'pdf',
      },
      android: configfb,
    });

    RNFetchBlob.config(configOptions)
      .fetch('GET', link, {
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      })
      .then(res => {
        if (Platform.OS === 'ios') {
          RNFetchBlob.fs.writeFile(configfb.path, res.data, 'base64');
          RNFetchBlob.ios.previewDocument(configfb.path);
        }
      })
      .catch(e => {});
  };
  async getDropdownInstruksi() {
    var dataItemList = [];
    fetch(Server.BASE_URL_API + ApiService.getDropdownARO, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
    })
      .then(response => response.json())
      .then(responseJson => {
        responseJson.data.map(element => {
          if (responseJson.code < 300) {
            console.log('response dropdown intruksi', responseJson);
            dataItemList.push({
              id: element.id !== undefined ? element.id : '',
              label: element.hasOwnProperty('prefix')
                ? element.prefix + ' - ' + element.name
                : bahasa === 'id'
                ? element.labelIdn
                : element.labelEng,
              value: element.hasOwnProperty('code')
                ? element.prefix
                : bahasa === 'id'
                ? element.labelIdn
                : element.labelEng,
              beneficiaryCode: element.hasOwnProperty('beneficiaryCode')
                ? element.beneficiaryCode
                : '',
              swiftCode: element.hasOwnProperty('swiftCode')
                ? element.swiftCode
                : '',
              cityCode: element.hasOwnProperty('cityCode')
                ? element.cityCode
                : '',
            });
            this.setState({arrayInstruksi: dataItemList});
          }
        });
      })
      .catch(error => {
        Alert.alert('Gagal Terhubung ke server', error);
      });
  }

  popUpFailedOCRKtp() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.showPopUpFailOcr}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({showPopUpFailOcr: false});
        }}>
        <View
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View
              style={{
                margin: 100,
                backgroundColor: 'white',
                borderRadius: 20,
                justifyContent: 'space-between',
                padding: 35,
                width: '80%',
                alignItems: 'center',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
              }}>
              <FastImage
                source={logoInfo}
                style={{width: 55, height: 55, marginBottom: 15}}
              />
              <Text
                style={{
                  marginBottom: 20,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  textAlign: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {bahasa === 'id' ? id.konten_failed_ocr : en.konten_failed_ocr}
              </Text>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <Pressable
                  style={{
                    width: '48%',
                    height: 43,
                    borderRadius: 10,
                    borderColor: COLORS.red,
                    borderWidth: 2,
                    padding: 10,
                    elevation: 2,
                    backgroundColor: 'white',
                  }}
                  onPress={() => {
                    this.setState({
                      showPopUpFailOcr: false,
                      isUpdateSwafoto: false,
                    });
                  }}>
                  <Text
                    style={{
                      alignSelf: 'center',
                      textAlign: 'center',
                      fontFamily: 'Helvetica',
                      fontSize: 15,
                      color: COLORS.red,
                    }}>
                    {bahasa === 'id' ? id.batal : en.batal}
                  </Text>
                </Pressable>
                <Pressable
                  style={{
                    width: '48%',
                    height: 43,
                    borderRadius: 10,
                    padding: 10,
                    elevation: 2,
                    backgroundColor: COLORS.red,
                  }}
                  onPress={() => {
                    this.setState({
                      showPopUpFailOcr: false,
                      openCamera: true,
                      isUpdateSwafoto: true,
                    });
                  }}>
                  <Text
                    style={{
                      alignSelf: 'center',
                      textAlign: 'center',
                      fontFamily: 'Helvetica',
                      fontSize: 15,
                      color: 'white',
                    }}>
                    {bahasa === 'id'
                      ? id.text_btn_failed_swafoto
                      : en.text_btn_failed_swafoto}
                  </Text>
                </Pressable>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
  popUpFailedSwafoto2() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.showPopUpFailSwafoto}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({showPopUpFailSwafoto: false});
        }}>
        <View
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View
              style={{
                margin: 100,
                backgroundColor: 'white',
                borderRadius: 20,
                justifyContent: 'space-between',
                padding: 35,
                width: '80%',
                alignItems: 'center',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
              }}>
              <FastImage
                source={logoInfo}
                style={{width: 55, height: 55, marginBottom: 15}}
              />
              <Text
                style={{
                  marginBottom: 20,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  textAlign: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {bahasa === 'id'
                  ? id.title_failed_swafoto
                  : en.title_failed_swafoto}
              </Text>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <Pressable
                  style={{
                    width: '48%',
                    height: 43,
                    borderRadius: 10,
                    borderColor: COLORS.red,
                    borderWidth: 2,
                    padding: 10,
                    elevation: 2,
                    backgroundColor: 'white',
                  }}
                  onPress={() => {
                    this.setState({
                      showPopUpFailSwafoto: false,
                      isUpdateSwafoto: false,
                    });
                  }}>
                  <Text
                    style={{
                      alignSelf: 'center',
                      textAlign: 'center',
                      fontFamily: 'Helvetica',
                      fontSize: 15,
                      color: COLORS.red,
                    }}>
                    {bahasa === 'id' ? id.batal : en.batal}
                  </Text>
                </Pressable>
                <Pressable
                  style={{
                    width: '48%',
                    height: 43,
                    borderRadius: 10,
                    padding: 10,
                    elevation: 2,
                    backgroundColor: COLORS.red,
                  }}
                  onPress={() => {
                    this.setState({
                      showPopUpFailSwafoto: false,
                      openCamera: true,
                      isUpdateSwafoto: true,
                    });
                  }}>
                  <Text
                    style={{
                      alignSelf: 'center',
                      textAlign: 'center',
                      fontFamily: 'Helvetica',
                      fontSize: 15,
                      color: 'white',
                    }}>
                    {bahasa === 'id'
                      ? id.text_btn_failed_swafoto
                      : en.text_btn_failed_swafoto}
                  </Text>
                </Pressable>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );
  }

  popUpSuccesReksaDana() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.modalSuccesReksaDana}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({modalSuccesReksaDana: false});
        }}>
        <View
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View
              style={{
                margin: 100,
                backgroundColor: 'white',
                borderRadius: 20,
                justifyContent: 'space-between',
                padding: 35,
                width: '80%',
                alignItems: 'center',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
              }}>
              <FastImage
                source={logoSuccess}
                style={{width: 55, height: 55, marginBottom: 15}}
              />
              <Text
                style={{
                  marginBottom: 10,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  textAlign: 'center',
                  fontWeight: 'bold',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {bahasa === 'id'
                  ? id.pendaftaran_berhasil
                  : en.pendaftaran_berhasil}
              </Text>
              <Text
                style={{
                  marginBottom: 20,
                  fontFamily: 'Helvetica',
                  fontSize: 16,
                  textAlign: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {bahasa === 'id' ? id.redaksi_popup_sid : en.redaksi_popup_sid}
              </Text>
              <Pressable
                style={{
                  width: '50%',
                  height: 43,
                  borderRadius: 10,
                  padding: 10,
                  elevation: 2,
                  backgroundColor: COLORS.red,
                }}
                onPress={() => {
                  this.setState({modalSuccesReksaDana: false});
                }}>
                <Text style={styles.textbtn}>OK</Text>
              </Pressable>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
  popUpRequestSID() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.modalrequestSID}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({modalrequestSID: false});
        }}>
        <View
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View
              style={{
                margin: 100,
                backgroundColor: 'white',
                borderRadius: 20,
                justifyContent: 'space-between',
                padding: 35,
                width: '80%',
                alignItems: 'center',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
              }}>
              <FastImage
                source={logoInfo}
                style={{width: 55, height: 55, marginBottom: 15}}
              />

              <Text
                style={{
                  marginBottom: 20,
                  fontFamily: 'Helvetica',
                  fontSize: 16,
                  textAlign: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {bahasa === 'id' ? id.redaksi_popup_sid : en.redaksi_popup_sid}
              </Text>
              <Pressable
                style={{
                  width: '50%',
                  height: 43,
                  borderRadius: 10,
                  padding: 10,
                  elevation: 2,
                  backgroundColor: COLORS.red,
                }}
                onPress={() => {
                  this.setState({modalrequestSID: false});
                }}>
                <Text style={styles.textbtn}>OK</Text>
              </Pressable>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
  view() {
    if (this.state.session == 1) {
      return (
        <View
          style={{
            width: '100%',
            height: '65%',
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
            flexDirection: 'column',
          }}>
          <Text
            style={{
              margin: 30,
              fontFamily: 'Helvetica',
              fontSize: 18,
              fontWeight: 'bold',
              textAlign: 'center',
            }}>
            {bahasa === 'id'
              ? id.konten_tnc_penggunaan_layanan
              : en.konten_tnc_penggunaan_layanan}
          </Text>
          <View
            style={{
              marginBottom: 50,
              flexDirection: 'row',
              width: '100%',
              justifyContent: 'center',
            }}>
            <Button
              textColor={COLORS.red}
              labelStyle={styles.btnLabel}
              style={[styles.btn, {marginEnd: 10}]}
              mode="outlined"
              onPress={() => {
                this.setState({isDecline: true});
              }}>
              {bahasa == 'id' ? id.tidak_setuju : en.tidak_setuju}
            </Button>
            <Button
              style={styles.btn}
              labelStyle={styles.btnLabel}
              buttonColor={COLORS.red}
              mode="contained"
              onPress={() => {
                if (isCust) {
                  //Masuk Ke Portofolio
                  this.getNoRekening();
                  this.mirroringCIF();
                  this.mirroringEndpoint(14);
                  this.setState({session: 11});
                } else {
                  //Masuk ke list produk
                  this.mirroringEndpoint(2);
                  this.getListProducts();
                  this.setState({session: 2});
                }
              }}>
              {bahasa == 'id' ? id.setuju : en.setuju}
            </Button>
          </View>
        </View>
      );
    } else if (this.state.session == 2) {
      return (
        <View
          style={{
            width: '100%',
            height: '65%',
            alignSelf: 'center',
            flexDirection: 'column',
          }}>
          <View
            style={{
              marginStart: 20,
              marginEnd: 20,
              borderBottomColor: 'rgba(0, 0, 0, 0.1)',
              borderBottomWidth: 1.3,
            }}>
            <Text
              style={{
                marginTop: 15,
                marginBottom: 15,
                fontFamily: 'Helvetica',
                fontSize: 18,
                fontWeight: 'bold',
                borderBottomWidth: 1,
                borderBottomColor: 'black',
              }}>
              {bahasa === 'id' ? id.daftar_produk : en.daftar_produk}
            </Text>
          </View>
          <FlatList
            data={this.state.itemProduct}
            renderItem={({item, index}) => (
              <TouchableOpacity
                onPress={() => {
                  if (item.id === 'OpenRek') {
                    this.mirroringEndpoint(201);
                    this.setState({session: 3});
                  } else {
                  }
                }}
                style={{
                  paddingTop: 10,
                  paddingBottom: 10,
                  height: 65,
                  marginStart: 20,
                  marginEnd: 20,
                  alignItems: 'center',
                  flexDirection: 'row',
                }}>
                <FastImage
                  transition={false}
                  style={{
                    width: 40,
                    height: 40,
                  }}
                  source={item.img}
                />
                <Text
                  key={index}
                  style={{
                    marginStart: 10,
                    fontFamily: 'Helvetica',
                    fontWeight: 'bold',
                    fontSize: 18,
                    textAlign: 'left',
                  }}>
                  {item.title}
                </Text>
              </TouchableOpacity>
            )}
            keyExtractor={item => item.id}
          />
        </View>
      );
    } else if (this.state.session == 3) {
      return (
        <View
          style={{
            width: '100%',
            height: '65%',
            alignSelf: 'center',
            flexDirection: 'column',
          }}>
          <View
            style={{
              marginStart: 20,
              marginEnd: 20,
              borderBottomColor: 'rgba(0, 0, 0, 0.1)',
              borderBottomWidth: 1.3,
              flexDirection: 'row',
            }}>
            <Pressable
              onPress={() => {
                if (this.state.isOpeningAccount) {
                  //Menuju Layanan
                  this.mirroringEndpoint(15);
                  this.setState({session: 12});
                } else {
                  this.mirroringEndpoint(2);
                  this.setState({session: 2});
                }
              }}
              style={{marginEnd: 10, alignSelf: 'center'}}>
              <FastImage
                style={{
                  width: 30,
                  height: 30,
                }}
                source={back}
              />
            </Pressable>
            <Text
              style={{
                marginTop: 15,
                marginBottom: 15,
                fontFamily: 'Helvetica',
                fontSize: 18,
                fontWeight: 'bold',
                borderBottomWidth: 1,
                borderBottomColor: 'black',
              }}>
              {bahasa === 'id' ? id.pembukaan_akun : en.pembukaan_akun}
            </Text>
          </View>
          <Text
            style={{
              marginTop: 15,
              marginBottom: 10,
              marginStart: 20,
              fontFamily: 'Helvetica',
              fontSize: 16,
              fontWeight: 'bold',
              borderBottomWidth: 1,
              borderBottomColor: 'black',
            }}>
            {bahasa === 'id' ? id.tipe_nasabah : en.tipe_nasabah}
          </Text>
          <FlatList
            style={{
              marginTop: 5,
              marginStart: 20,
              marginEnd: 15,
            }}
            data={this.formatData(
              this.state.itemTipeNasabah,
              this.state.numColumns,
            )}
            renderItem={({item, index}) => (
              <TouchableOpacity
                key={index}
                style={{
                  borderColor: COLORS.red,
                  borderWidth: 1.5,
                  backgroundColor: item.color,
                  borderRadius: 10,
                  alignItems: 'center',
                  justifyContent: 'center',
                  flex: 1,
                  marginEnd: 5,
                  marginBottom: 5,
                  height:
                    Dimensions.get('window').width / this.state.numColumns,
                }}>
                <View style={{flexDirection: 'column', alignItems: 'center'}}>
                  <FastImage
                    style={{width: 30, height: 30, marginBottom: 10}}
                    source={item.img}
                  />
                  <Text
                    style={{
                      color: item.fontColor,
                      fontSize: 15,
                      fontWeight: 'bold',
                      textAlign: 'center',
                      fontFamily: 'Helvetica',
                    }}>
                    {item.title}
                  </Text>
                </View>
              </TouchableOpacity>
            )}
            numColumns={this.state.numColumns}
            keyExtractor={item => item.id}
          />
          <Text
            style={{
              marginTop: 0,
              marginBottom: 10,
              marginStart: 20,
              fontFamily: 'Helvetica',
              fontSize: 16,
              fontWeight: 'bold',
              borderBottomWidth: 1,
              borderBottomColor: 'black',
            }}>
            {bahasa === 'id' ? id.jenis_rekening : en.jenis_rekening}
          </Text>
          <FlatList
            style={{
              marginStart: 20,
              marginEnd: 15,
            }}
            data={this.formatData(
              this.state.itemJenisRekening,
              this.state.numColumns,
            )}
            renderItem={({item, index}) => (
              <TouchableOpacity
                key={index}
                style={{
                  borderColor: COLORS.red,
                  borderWidth: 1.5,
                  backgroundColor: item.color,
                  borderRadius: 10,
                  alignItems: 'center',
                  justifyContent: 'center',
                  flex: 1,
                  marginEnd: 5,
                  height:
                    Dimensions.get('window').width / this.state.numColumns,
                }}>
                <View style={{flexDirection: 'column', alignItems: 'center'}}>
                  <FastImage
                    style={{width: 30, height: 30, marginBottom: 10}}
                    source={item.img}
                  />
                  <Text
                    style={{
                      color: item.fontColor,
                      fontSize: 15,
                      fontWeight: 'bold',
                      textAlign: 'center',
                      fontFamily: 'Helvetica',
                    }}>
                    {item.title}
                  </Text>
                </View>
              </TouchableOpacity>
            )}
            numColumns={this.state.numColumns}
            keyExtractor={item => item.id}
          />
          <Button
            style={[
              styles.btn,
              {
                marginBottom: 50,
                width: 'auto',
                alignSelf: 'center',
              },
            ]}
            labelStyle={styles.btnLabel}
            buttonColor={COLORS.red}
            mode="contained"
            onPress={() => {
              this.setState({modalTNC: true});
              if (this.state.isOpeningAccount) {
                this.getTNC(9);
                this.mirroringEndpoint(156);
              } else {
                this.getTNC(1);
                this.mirroringEndpoint(361);
              }
            }}>
            {bahasa == 'id' ? id.lanjut : en.lanjut}
          </Button>
        </View>
      );
    } else if (this.state.session >= 4 && this.state.session <= 8) {
      return (
        <View
          style={{
            width: '100%',
            height: '65%',
            flexDirection: 'column',
          }}>
          <View
            style={{
              padding: 10,
              flexDirection: 'row',
              backgroundColor: COLORS.bgHeadUpload,
              height: '12%',
              width: '100%',
              justifyContent: 'space-evenly',
              alignItems: 'center',
              alignSelf: 'center',
            }}>
            <View
              style={{
                display: 'none',
                width: '15%',
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: 12,
                  height: 12,
                  marginEnd: 5,
                  borderRadius: 12 / 2,
                  backgroundColor:
                    this.state.session == 4 ? COLORS.mainDot : COLORS.sucessDot,
                }}
              />
              <Text style={{fontSize: 12, width: 50}} numberOfLines={3}>
                {bahasa == 'id' ? id.foto_ktp : en.foto_ktp}
              </Text>
            </View>

            <View
              style={{
                width: '15%',
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: 12,
                  height: 12,
                  marginEnd: 5,
                  borderRadius: 12 / 2,
                  backgroundColor:
                    this.state.session > 4
                      ? this.state.session == 5
                        ? COLORS.mainDot
                        : COLORS.sucessDot
                      : COLORS.offDot,
                }}
              />
              <Text
                style={{fontSize: 12, width: 50}}
                ellipsizeMode="tail"
                numberOfLines={3}>
                {bahasa == 'id'
                  ? id.foto_diri_dengan_ktp
                  : en.foto_diri_dengan_ktp}
              </Text>
            </View>

            <View
              style={{
                width: '15%',
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <View
                style={{
                  marginStart: 5,
                  width: 12,
                  height: 12,
                  marginEnd: 5,
                  borderRadius: 12 / 2,
                  backgroundColor:
                    this.state.session > 5
                      ? this.state.session == 6
                        ? COLORS.mainDot
                        : COLORS.sucessDot
                      : COLORS.offDot,
                }}
              />
              <Text style={{fontSize: 12, width: 50}} numberOfLines={3}>
                {bahasa == 'id' ? id.foto_npwp : en.foto_npwp}
              </Text>
            </View>

            <View
              style={{
                width: '15%',
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: 12,
                  height: 12,
                  marginEnd: 5,
                  borderRadius: 12 / 2,
                  backgroundColor:
                    this.state.session > 6
                      ? this.state.session == 7
                        ? COLORS.mainDot
                        : COLORS.sucessDot
                      : COLORS.offDot,
                }}
              />
              <Text style={{fontSize: 12, width: 50}} numberOfLines={3}>
                {bahasa == 'id' ? id.foto_tanda_tangan : en.foto_tanda_tangan}
              </Text>
            </View>

            <View
              style={{
                width: '15%',
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: 12,
                  height: 12,
                  marginEnd: 5,
                  borderRadius: 12 / 2,
                  backgroundColor:
                    this.state.session > 7
                      ? this.state.session == 8
                        ? COLORS.mainDot
                        : COLORS.sucessDot
                      : COLORS.offDot,
                }}
              />
              <Text style={{fontSize: 12, width: 50}} numberOfLines={3}>
                {bahasa == 'id'
                  ? id.formulir_pembukaan_rekening
                  : en.formulir_pembukaan_rekening}
              </Text>
            </View>
          </View>
          <View
            style={{
              height: '10%',
              backgroundColor: 'white',
              justifyContent: 'center',
              alignItems: 'center',
              elevation: 10,
              borderBottomColor: 'rgba(0, 0, 0, 0.1)',
              borderBottomWidth: 1.3,
            }}>
            <Text
              style={{
                fontSize: 18,
                color: COLORS.textColorGray,
                fontWeight: 'bold',
              }}>
              {bahasa === 'id' ? id.pembukaan_akun : en.pembukaan_akun}
            </Text>
          </View>
          <ScrollView
            keyboardDismissMode="none"
            contentContainerStyle={{
              paddingBottom: '10%',
            }}
            automaticallyAdjustKeyboardInsets="true"
            automaticallyAdjustContentInsets="true"
            refreshControl={
              this.state.session == 5 || this.state.session >= 8 ? (
                <RefreshControl
                  refreshing={this.state.setRefreshing}
                  onRefresh={this.onRefresh}
                />
              ) : undefined
            }>
            {this.view_cif_new()}
          </ScrollView>
        </View>
      );
    } else if (this.state.session == 9) {
      return (
        <View
          style={{
            width: '100%',
            height: '65%',
            alignSelf: 'center',
            flexDirection: 'column',
          }}>
          <View
            style={{
              justifyContent: 'center',
              backgroundColor: 'white',
              flexDirection: 'row',
              marginBottom: 0,
            }}>
            <Text
              style={{
                marginTop: 15,
                marginBottom: 15,
                fontFamily: 'Helvetica',
                fontSize: 18,
                color: COLORS.textColorGray,
                textAlign: 'center',
                fontWeight: 'bold',
                borderBottomWidth: 1,
                borderBottomColor: 'black',
              }}>
              {bahasa === 'id' ? id.one_time_password : en.one_time_password}
            </Text>
          </View>
          <ScrollView
            automaticallyAdjustKeyboardInsets="true"
            keyboardDismissMode="interactive"
            style={{height: '100%', width: '100%'}}>
            <View
              style={{
                marginTop: 25,
                height: '100%',
                padding: 30,
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <FastImage
                source={this.state.isOTPcorrect ? logoInfo : wrong_icon}
                style={{width: 55, height: 55, marginBottom: 15, marginTop: 0}}
              />
              <Text
                style={{
                  marginBottom: 20,
                  fontFamily: 'Helvetica',
                  fontSize: 19,
                  textAlign: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {this.state.isOTPcorrect
                  ? bahasa === 'id'
                    ? id.title_otp
                    : en.title_otp
                  : bahasa === 'id'
                  ? id.title_otp_wrong
                  : en.title_otp_wrong}
              </Text>
              <OTPTextView
                defaultValue=""
                tintColor={COLORS.textBlue}
                containerStyle={{marginBottom: 10}}
                textInputStyle={{borderRadius: 10, borderWidth: 3}}
                inputCount={6}
                handleTextChange={pin => {
                  this.setState({pin: pin});
                  this.mirroringKey('otp', pin);
                }}
                keyboardType="numeric"
                inputCellLength={1}
              />
              <Button
                onPress={() => {
                  if (this.state.pin.trim().length === 6) {
                    //Jika page sebelumnya transaksi antar bank || antar rekening || rekening sendiri
                    if (
                      this.state.sessionBeforeOTP === 14 ||
                      this.state.sessionBeforeOTP === 16 ||
                      this.state.sessionBeforeOTP === 17
                    ) {
                      this.verifyOtp(23);
                    } else if (
                      this.state.sessionBeforeOTP === 19 ||
                      this.state.sessionBeforeOTP === 20 ||
                      this.state.sessionBeforeOTP === 21
                    ) {
                      this.setState({session: 10});
                    } else if (this.state.sessionBeforeOTP === 15) {
                      this.verifyOtp(10);
                    } else if (
                      this.state.sessionBeforeOTP === 37 ||
                      this.state.sessionBeforeOTP === 8
                    ) {
                      this.verifyOtp(10);
                    } else {
                      // this.verifyOtp(10);
                      this.loopPengajuan(23);
                    }
                  }
                }}
                style={{
                  marginBottom: 10,
                  width: '100%',
                  alignSelf: 'center',
                  borderRadius: 12,
                  borderColor:
                    this.state.pin.trim().length < 6 ? '#B5B5B5' : COLORS.red,
                  borderWidth: 2,
                }}
                labelStyle={styles.btnLabel}
                buttonColor={
                  this.state.pin.trim().length < 6 ? '#B5B5B5' : COLORS.red
                }
                mode="contained">
                {bahasa === 'id' ? id.verifikasi : en.verifikasi}
              </Button>
              <Countdown
                ref={ref => {
                  this.countdown = ref;
                }}
                durationInSeconds={120}
                bahasa={bahasa}
                onlyMinutes={false}
                styles={{
                  marginTop: 10,
                  marginBottom: 10,
                  fontFamily: 'Helvetica',
                  fontSize: 19,
                  color: COLORS.textColorGray,
                  textAlign: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
                onFinish={() => {
                  this.setState({isResendOTP: true});
                }}
              />
              <Text
                onPress={() => {
                  if (this.state.isResendOTP) {
                    this.setState({isResendOTP: false});
                    this.countdown.resetCountdown();
                    this.countdown.startCountdown();
                    this.sendOtp();
                  }
                }}
                style={{
                  marginBottom: 10,
                  fontFamily: 'Helvetica',
                  fontSize: 19,
                  color: this.state.isResendOTP ? COLORS.textBlue : '#B5B5B5',
                  textAlign: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {bahasa === 'id' ? id.kirim_ulang_otp : en.kirim_ulang_otp}
              </Text>
            </View>
          </ScrollView>
        </View>
      );
    } else if (this.state.session == 10) {
      return (
        <View
          style={{
            width: '100%',
            height: '65%',
            alignSelf: 'center',
            flexDirection: 'column',
          }}>
          <View
            style={{
              padding: 14,
              justifyContent: 'center',
              backgroundColor: 'white',
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                alignSelf: 'center',
                fontFamily: 'Helvetica',
                fontSize: 18,
                color: COLORS.textColorGray,
                textAlign: 'center',
                fontWeight: 'bold',
                borderBottomWidth: 1,
                borderBottomColor: 'black',
              }}>
              {this.state.isOpeningAccount
                ? bahasa === 'id'
                  ? id.title_resi_opening_akun
                  : en.title_resi
                : bahasa === 'id'
                ? id.title_resi
                : en.title_resi}
            </Text>
          </View>
          <ScrollView
            automaticallyAdjustKeyboardInsets="true"
            keyboardDismissMode="interactive"
            style={{height: '100%'}}>
            <View
              style={{
                height: '100%',
                padding: 30,
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginBottom: 20,
                  borderRadius: 10,
                  backgroundColor: 'white',
                  height: 250,
                  width: '100%',
                }}>
                <FastImage
                  source={{
                    uri: encodeURI(base64Resi),
                    priority: FastImage.priority.high,
                  }}
                  resizeMode="contain"
                  style={{width: '100%', height: '100%'}}
                />
              </View>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'column',
                }}>
                <Text
                  textAlign="center"
                  numberOfLines={5}
                  style={{
                    display:
                      this.state.sessionBeforeOTP === 15 ? 'flex' : 'none',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 15,
                    color: COLORS.red,
                    textAlign: 'left',
                  }}>
                  {bahasa === 'id' ? id.headline_resi2 : en.headline_resi2}
                </Text>
                <Text
                  textAlign="center"
                  numberOfLines={5}
                  style={{
                    marginBottom: 20,
                    fontFamily: 'Helvetica',
                    fontSize: 17,
                    textAlign: 'left',
                  }}>
                  {bahasa === 'id' ? id.headline_resi : en.headline_resi}
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <Button
                    icon={() => (
                      <Image
                        transition={false}
                        source={ic_download}
                        style={{
                          tintColor: COLORS.red,
                          justifyContent: 'center',
                          alignItems: 'center',
                          alignContent: 'center',
                          alignSelf: 'center',
                          width: 20,
                          height: 20,
                          resizeMode: 'contain',
                        }}
                      />
                    )}
                    style={[
                      styles.btn,
                      {
                        width: 'auto',
                        alignSelf: 'center',
                      },
                    ]}
                    textColor={COLORS.red}
                    labelStyle={styles.btnLabel}
                    buttonColor={COLORS.white}
                    mode="contained">
                    {bahasa === 'id' ? id.unduh_resi : en.unduh_resi}
                  </Button>
                  <Button
                    onPress={() => {
                      this.getNoRekening();
                      this.mirroringEndpoint(14);
                      this.setState({session: 11});
                    }}
                    style={[
                      styles.btn,
                      {
                        width: 'auto',
                        alignSelf: 'center',
                      },
                    ]}
                    labelStyle={styles.btnLabel}
                    buttonColor={COLORS.red}
                    mode="contained">
                    {bahasa === 'id' ? id.selesai : en.selesai}
                  </Button>
                </View>
              </View>
            </View>
          </ScrollView>
        </View>
      );
    } else if (this.state.session == 11) {
      return (
        <View
          style={{
            width: '100%',
            height: '65%',
            alignSelf: 'center',
            flexDirection: 'column',
          }}>
          <View
            style={{
              padding: 14,
              justifyContent: 'center',
              backgroundColor: 'white',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <View
              style={{
                justifyContent: 'center',
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <FastImage
                source={user}
                style={{width: 20, height: 20, marginEnd: 10}}
              />
              <Text
                style={{
                  alignSelf: 'center',
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  color: COLORS.textColorGray,
                  textAlign: 'center',
                  fontWeight: 'bold',
                  borderBottomWidth: 1,
                  borderBottomColor: 'black',
                }}>
                {bahasa === 'id' ? id.portofolio_saya : en.portofolio_saya}
              </Text>
            </View>
            <Text
              style={{
                alignSelf: 'center',
                fontFamily: 'Helvetica',
                fontSize: 18,
                color: COLORS.textColorGray,
                textAlign: 'center',
                fontWeight: 'bold',
                borderBottomWidth: 1,
                borderBottomColor: 'black',
              }}>
              20 April 2022
            </Text>
          </View>
          <ScrollView
            automaticallyAdjustKeyboardInsets="true"
            keyboardDismissMode="interactive"
            contentContainerStyle={{padding: 15, width: '100%'}}
            style={{height: '100%'}}>
            <Text style={{fontSize: 18, marginBottom: 20}}>
              {bahasa == 'id'
                ? id.informasi_portofolio
                : en.informasi_portofolio}
            </Text>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-around',
              }}>
              <PieChart
                data={dataPie}
                donut
                radius={90}
                innerRadius={45}
                showText={true}
                textColor="black"
                focusOnPress
                showValuesAsLabels
              />
              <View
                style={{
                  flexDirection: 'column',
                  justifyContent: 'space-evenly',
                }}>
                {this.renderLegend(this.state.dataPortofolio)}
              </View>
            </View>
            <Button
              onPress={() => {
                this.mirroringEndpoint(15);
                this.setState({session: 12});
              }}
              style={styles.btn}
              labelStyle={styles.btnLabel}
              buttonColor={COLORS.red}
              mode="contained">
              {bahasa == 'id' ? id.layanan : en.layanan}
            </Button>
            {this.renderListTabungan(this.state.dataPortofolio)}
          </ScrollView>
        </View>
      );
    } else if (this.state.session == 12) {
      return (
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            height: '65%',
            alignSelf: 'center',
            flexDirection: 'column',
          }}>
          <View
            style={{
              width: 420,
              backgroundColor: 'white',
              flexDirection: 'row',
            }}>
            <Pressable
              onPress={() => {
                if (this.state.isUnderDevelopment) {
                  this.setState({isUnderDevelopment: false});
                } else {
                  this.getNoRekening();
                  this.mirroringEndpoint(14);
                  this.setState({session: 11});
                }
              }}
              style={{marginEnd: 10, alignSelf: 'center'}}>
              <FastImage
                style={{
                  marginStart: 20,
                  width: 30,
                  height: 30,
                }}
                source={back}
              />
            </Pressable>
            <Text
              style={{
                marginTop: 15,
                marginBottom: 15,
                fontFamily: 'Helvetica',
                fontSize: 18,
                fontWeight: 'bold',
                borderBottomWidth: 1,
                borderBottomColor: 'black',
              }}>
              {bahasa === 'id' ? id.layanan : en.layanan}
            </Text>
          </View>
          <FlatList
            style={{
              display: this.state.isUnderDevelopment ? 'none' : 'flex',
            }}
            data={this.formatData(
              this.state.itemLayanan,
              this.state.numColumns,
            )}
            renderItem={({item, index}) => (
              <TouchableOpacity
                key={index}
                onPress={() => {
                  if (item.id == 6) {
                    //Form Komplain
                    this.mirroringEndpoint(359);
                    this.mirroringKeyKomplain();
                    this.setState({session: 13});
                    this.getNoRekening();
                    this.inqueryCIF();
                  } else if (item.id == 7) {
                    //Pembukaan akun
                    this.mirroringEndpoint(201);
                    this.setState({isOpeningAccount: true, session: 3});
                  } else if (item.id == 12) {
                    //Menu Transaksi
                    this.mirroringEndpoint(199);
                    this.setState({session: 22});
                  } else if (item.id == 21) {
                    //Menu deposito
                    this.setState({session: 18});
                  } else if (item.id == 9) {
                    //Menu Perbarui Data
                    this.setState({formCode: '59', session: 24});
                    this.formBuilder('59');
                  } else if (item.id == 19) {
                    //Menu Wealth Management
                    this.setState({session: 27});
                  } else {
                    this.setState({isUnderDevelopment: true});
                  }
                }}
                style={{
                  alignSelf: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                  alignContent: 'center',
                  width: 121,
                  marginTop: 10,
                  justifyContent: 'flex-start',
                  flexDirection: 'column',
                  height: 165,
                }}>
                <FastImage
                  style={{
                    width: 100,
                    height: 100,
                    alignContent: 'center',
                    alignSelf: 'center',
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginBottom: 10,
                    borderRadius: 13,
                  }}
                  source={item.img}
                />
                <Text
                  ellipsizeMode="tail"
                  textAlign="center"
                  numberOfLines={2}
                  style={{
                    width: 121,
                    color: 'black',
                    fontSize: 15,
                    fontWeight: 'bold',
                    textAlign: 'center',
                    fontFamily: 'Helvetica',
                  }}>
                  {item.title}
                </Text>
              </TouchableOpacity>
            )}
            numColumns={this.state.numColumns}
            keyExtractor={item => item.id}
          />
          <View
            style={{
              display: this.state.isUnderDevelopment ? 'flex' : 'none',
              height: '90%',
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'column',
              width: '100%',
            }}>
            <Text
              style={{
                marginTop: 15,
                fontFamily: 'Helvetica',
                fontSize: 22,
                textAlign: 'center',
                fontWeight: 'bold',
              }}>
              {bahasa === 'id' ? id.dalam_pengerjaan : id.dalam_pengerjaan}
            </Text>
            <Text
              style={{
                marginBottom: 15,
                fontFamily: 'Helvetica',
                fontSize: 17,
                textAlign: 'center',
              }}>
              {bahasa === 'id'
                ? id.halaman_ini_sedang_dalam_pengerjaan
                : en.halaman_ini_sedang_dalam_pengerjaan}
            </Text>
            <FastImage
              style={{
                width: 290,
                height: 200,
              }}
              source={dalampengerjaan}
            />
          </View>
        </View>
      );
    } else if (this.state.session == 13) {
      if (this.state.isResiPengaduan) {
        return (
          <View
            style={{
              height: '65%',
              alignSelf: 'center',
              flexDirection: 'column',
            }}>
            <View
              style={{
                padding: 14,
                justifyContent: 'center',
                backgroundColor: 'white',
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  alignSelf: 'center',
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  color: COLORS.textColorGray,
                  textAlign: 'center',
                  fontWeight: 'bold',
                  borderBottomWidth: 1,
                  borderBottomColor: 'black',
                }}>
                {bahasa === 'id' ? id.terima_kasih : en.terima_kasih}
              </Text>
            </View>
            <ScrollView
              automaticallyAdjustKeyboardInsets="true"
              keyboardDismissMode="interactive"
              style={{height: '100%'}}>
              <View
                style={{
                  height: '100%',
                  padding: 30,
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginBottom: 20,
                    borderRadius: 10,
                    backgroundColor: 'white',
                    height: 250,
                    width: '100%',
                  }}>
                  <FastImage
                    source={{
                      uri: encodeURI(base64Resi),
                      priority: FastImage.priority.high,
                    }}
                    resizeMode="contain"
                    style={{width: '100%', height: '100%'}}
                  />
                </View>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'column',
                  }}>
                  <Text
                    textAlign="center"
                    numberOfLines={5}
                    style={{
                      marginBottom: 20,
                      fontFamily: 'Helvetica',
                      fontSize: 17,
                      textAlign: 'center',
                      alignItems: 'center',
                      fontWeight: 'bold',
                      textAlign: 'center',
                      justifyContent: 'center',
                    }}>
                    {bahasa === 'id'
                      ? id.pengaduan_anda_berhasil
                      : en.pengaduan_anda_berhasil}
                  </Text>
                  <Text
                    textAlign="center"
                    numberOfLines={5}
                    style={{
                      marginBottom: 20,
                      fontFamily: 'Helvetica',
                      fontSize: 19,
                      textAlign: 'center',
                      alignItems: 'center',
                      fontWeight: 'bold',
                      textAlign: 'center',
                      justifyContent: 'center',
                    }}>
                    {noPengaduan}
                  </Text>
                  <Text
                    textAlign="center"
                    numberOfLines={5}
                    style={{
                      marginBottom: 20,
                      fontFamily: 'Helvetica',
                      fontSize: 17,
                      textAlign: 'center',
                      alignItems: 'center',
                      fontWeight: 'bold',
                      textAlign: 'center',
                      justifyContent: 'center',
                    }}>
                    {bahasa === 'id' ? id.headline_resi : en.headline_resi}
                  </Text>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-around',
                    }}>
                    <Button
                      onPress={() => {
                        let link = linkPDFResi;
                        this.downloadResi(link, noPengaduan + 'Komplain');
                      }}
                      icon={() => (
                        <Image
                          transition={false}
                          source={ic_download}
                          style={{
                            tintColor: 'white',
                            justifyContent: 'center',
                            alignItems: 'center',
                            alignContent: 'center',
                            alignSelf: 'center',
                            width: 20,
                            height: 20,
                            resizeMode: 'contain',
                          }}
                        />
                      )}
                      style={[
                        styles.btn,
                        {
                          width: '100%',
                          alignSelf: 'center',
                        },
                      ]}
                      labelStyle={styles.btnLabel}
                      buttonColor={COLORS.red}
                      mode="contained">
                      {bahasa === 'id' ? id.unduh_resi : en.unduh_resi}
                    </Button>
                    <Button
                      onPress={() => {
                        detailPengaduan = '';
                        fileNamePendukung = '';
                        fileUriPendukung = '';
                        fileNamePendukung2 = '';
                        fileUriPendukung2 = '';
                        fileNamePendukung3 = '';
                        fileUriPendukung3 = '';
                        noRekening = '';
                        this.setState({pernyataanPengaduan: false});
                        this.mirroringEndpoint(14);
                        this.setState({session: 11, isResiPengaduan: false});
                      }}
                      style={[
                        styles.btn,
                        {
                          width: '100%',
                          alignSelf: 'center',
                        },
                      ]}
                      labelStyle={styles.btnLabel}
                      buttonColor={COLORS.red}
                      mode="contained">
                      {bahasa === 'id' ? id.selesai : en.selesai}
                    </Button>
                  </View>
                </View>
              </View>
            </ScrollView>
          </View>
        );
      } else {
        return (
          <View
            style={{
              alignItems: 'center',
              height: '65%',
              alignSelf: 'center',
              flexDirection: 'column',
            }}>
            <View
              style={{
                width: 420,
                backgroundColor: 'white',
                flexDirection: 'row',
              }}>
              <Pressable
                onPress={() => {
                  this.mirroringEndpoint(15);
                  this.setState({session: 12});
                }}
                style={{marginEnd: 10, alignSelf: 'center'}}>
                <FastImage
                  style={{
                    marginStart: 20,
                    width: 30,
                    height: 30,
                  }}
                  source={back}
                />
              </Pressable>
              <Text
                style={{
                  marginTop: 15,
                  marginBottom: 15,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  fontWeight: 'bold',
                  borderBottomWidth: 1,
                  borderBottomColor: 'black',
                }}>
                {bahasa === 'id' ? id.form_pengaduan : en.form_pengaduan}
              </Text>
            </View>
            <ScrollView
              keyboardDismissMode="none"
              automaticallyAdjustKeyboardInsets="true"
              automaticallyAdjustContentInsets="true">
              <View
                style={{
                  marginTop: 15,
                  marginStart: 15,
                  marginEnd: 15,
                  borderRadius: 15,
                  flexDirection: 'column',
                  backgroundColor: 'white',
                }}>
                <Text
                  style={{
                    margin: 20,
                    textAlign: 'left',
                    fontSize: 16,
                    marginBottom: 10,
                    fontWeight: 'bold',
                    color: COLORS.textColorGray,
                  }}>
                  {bahasa === 'id'
                    ? id.pengaduan_nasabah
                    : en.pengaduan_nasabah}
                </Text>
                <Text
                  style={{
                    marginStart: 20,
                    marginEnd: 20,
                    textAlign: 'left',
                    fontSize: 16,
                    marginBottom: 20,
                    fontWeight: 'bold',
                    color: COLORS.textColorGray,
                  }}>
                  {bahasa === 'id'
                    ? id.headline_pengaduan_nasabah
                    : en.headline_pengaduan_nasabah}
                </Text>
                <Text
                  style={{
                    marginStart: 20,
                    marginEnd: 20,
                    textAlign: 'left',
                    fontSize: 15,
                    marginBottom: 10,
                    color: COLORS.textColorGray,
                  }}>
                  {bahasa === 'id'
                    ? id.tanggal_pengaduan
                    : en.tanggal_pengaduan}
                </Text>
                <TextInput
                  style={{
                    marginBottom: 10,
                    height: 50,
                    marginStart: 20,
                    marginEnd: 20,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 1.5,
                    borderRadius: 10,
                    backgroundColor: 'white',
                    paddingLeft: 10,
                    fontSize: 16,
                    color: COLORS.border_textInput,
                  }}
                  defaultValue={this.state.dateNow}
                  maxLength={16}
                  editable={false}
                  underlineColorAndroid="transparent"
                  keyboardType="numeric"
                />
                <Text
                  style={{
                    marginStart: 20,
                    marginEnd: 20,
                    textAlign: 'left',
                    fontSize: 15,
                    marginBottom: 10,
                    color: COLORS.textColorGray,
                  }}>
                  {bahasa === 'id' ? id.nomor_rekening : en.nomor_rekening}
                </Text>
                <DropDownPicker
                  zIndex={1000000}
                  setItems={this.state.ListNoRek}
                  items={this.state.ListNoRek}
                  onSelectItem={value => {
                    noRekening = value.value;
                    this.mirroringKeyKomplain();
                  }}
                  style={{
                    width: 330,
                    marginStart: 20,
                    borderColor: '#DADADA',
                    borderWidth: 1.5,
                    marginBottom: 10,
                  }}
                  labelStyle={{
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                  }}
                  placeholderStyle={{
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                  }}
                  dropDownContainerStyle={{
                    marginStart: 20,
                    width: 330,
                  }}
                  listItemContainerStyle={{
                    borderColor: '#000',
                    borderBottomWidth: 1,
                  }}
                  listItemLabelStyle={{
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                  }}
                  dropDownMaxHeight={250}
                  placeholder={
                    noRekening.length > 0
                      ? noRekening
                      : this.state.ListNoRek.length > 0
                      ? bahasa === 'id'
                        ? '== Silahkan Pilih =='
                        : '== Please Choose =='
                      : ''
                  }
                  dropDownDirection="BOTTOM"
                  open={this.state.open}
                  setOpen={set => {
                    this.setState({open: set});
                  }}
                />
                <Text
                  style={{
                    marginStart: 20,
                    marginEnd: 20,
                    textAlign: 'left',
                    fontSize: 15,
                    marginBottom: 10,
                    color: COLORS.textColorGray,
                  }}>
                  {bahasa === 'id' ? id.detail_pengaduan : en.detail_pengaduan}
                </Text>
                <TextInput
                  style={{
                    paddingTop: 10,
                    marginBottom: 10,
                    height: 100,
                    marginStart: 20,
                    marginEnd: 20,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 1.5,
                    borderRadius: 10,
                    backgroundColor: 'white',
                    paddingLeft: 10,
                    fontSize: 16,
                  }}
                  multiline={true}
                  onChangeText={value => {
                    detailPengaduan = value;
                    this.mirroringKeyKomplain();
                  }}
                  defaultValue={detailPengaduan}
                  underlineColorAndroid="transparent"
                  keyboardType="default"
                />
                <Text
                  style={{
                    marginStart: 20,
                    marginEnd: 20,
                    textAlign: 'left',
                    fontSize: 15,
                    marginBottom: 10,
                    color: COLORS.textColorGray,
                  }}>
                  {bahasa === 'id' ? id.file_pendukung : en.file_pendukung}
                </Text>
                <Pressable
                  onPress={() => {
                    if (this.state.maxFilePendukung < 3) {
                      this.setState({
                        pendukung: true,
                        maxFilePendukung: this.state.maxFilePendukung + 1,
                      });
                      this.launchImageLibraryfunc();
                    } else {
                      Alert.alert(
                        'Masksimal 3 kali melampirkan file pendukung',
                      );
                    }
                  }}
                  style={{
                    marginBottom: 10,
                    height: 50,
                    marginStart: 20,
                    marginEnd: 20,
                    borderRadius: 10,
                    justifyContent: 'center',
                    backgroundColor: 'gray',
                    paddingLeft: 10,
                    fontSize: 16,
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                      width: '100%',
                    }}>
                    <FastImage
                      source={pickGallery}
                      style={{
                        marginEnd: 8,
                        width: 26,
                        height: 26,
                      }}
                    />
                    <Text
                      style={{
                        fontFamily: 'Helvetica',
                        color: 'white',
                        fontSize: 15,
                      }}>
                      {bahasa === 'id' ? id.file_pendukung : en.file_pendukung}
                    </Text>
                  </View>
                </Pressable>

                <Pressable
                  style={{
                    display: fileUriPendukung === '' ? 'none' : 'flex',
                    marginBottom: 10,
                    marginStart: 20,
                    marginEnd: 20,
                  }}
                  onPress={() => {
                    fileNamePendukung = '';
                    fileUriPendukung = '';
                    this.mirroringKeyKomplain();
                    this.setState({
                      session: 13,
                      maxFilePendukung: this.state.maxFilePendukung - 1,
                    });
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                    }}>
                    <Image
                      source={delete_icon}
                      style={{
                        justifyContent: 'center',
                        alignSelf: 'center',
                        alignItems: 'center',
                        tintColor: 'red',
                        marginEnd: 10,
                        width: 20,
                        height: 20,
                      }}
                    />
                    <Text
                      ellipsizeMode="tail"
                      numberOfLines={1}
                      style={{
                        fontFamily: 'Helvetica',
                        color: 'gray',
                        fontSize: 16,
                      }}>
                      {fileNamePendukung.length > 30
                        ? `${fileNamePendukung.substring(0, 30)}...`
                        : fileNamePendukung}
                    </Text>
                  </View>
                </Pressable>

                <Pressable
                  style={{
                    display: fileUriPendukung2 === '' ? 'none' : 'flex',
                    marginBottom: 10,
                    marginStart: 20,
                    marginEnd: 20,
                  }}
                  onPress={() => {
                    fileNamePendukung2 = '';
                    fileUriPendukung2 = '';
                    this.mirroringKeyKomplain();
                    this.setState({
                      session: 13,
                      maxFilePendukung: this.state.maxFilePendukung - 1,
                    });
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                    }}>
                    <Image
                      source={delete_icon}
                      style={{
                        justifyContent: 'center',
                        alignSelf: 'center',
                        alignItems: 'center',
                        tintColor: 'red',
                        marginEnd: 10,
                        width: 20,
                        height: 20,
                      }}
                    />
                    <Text
                      ellipsizeMode="tail"
                      numberOfLines={1}
                      style={{
                        fontFamily: 'Helvetica',
                        color: 'gray',
                        fontSize: 16,
                      }}>
                      {fileNamePendukung2.length > 30
                        ? `${fileNamePendukung2.substring(0, 30)}...`
                        : fileNamePendukung2}
                    </Text>
                  </View>
                </Pressable>

                <Pressable
                  style={{
                    display: fileUriPendukung3 === '' ? 'none' : 'flex',
                    marginBottom: 10,
                    marginStart: 20,
                    marginEnd: 20,
                  }}
                  onPress={() => {
                    fileNamePendukung3 = '';
                    fileUriPendukung3 = '';
                    this.mirroringKeyKomplain();
                    this.setState({
                      session: 13,
                      maxFilePendukung: this.state.maxFilePendukung - 1,
                    });
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                    }}>
                    <Image
                      source={delete_icon}
                      style={{
                        justifyContent: 'center',
                        alignSelf: 'center',
                        alignItems: 'center',
                        tintColor: 'red',
                        marginEnd: 10,
                        width: 20,
                        height: 20,
                      }}
                    />
                    <Text
                      ellipsizeMode="tail"
                      numberOfLines={1}
                      style={{
                        fontFamily: 'Helvetica',
                        color: 'gray',
                        fontSize: 16,
                      }}>
                      {fileNamePendukung3.length > 30
                        ? `${fileNamePendukung3.substring(0, 30)}...`
                        : fileNamePendukung3}
                    </Text>
                  </View>
                </Pressable>

                <Text
                  style={{
                    marginStart: 20,
                    marginEnd: 20,
                    textAlign: 'left',
                    fontSize: 15,
                    marginBottom: 10,
                    color: COLORS.textColorGray,
                  }}>
                  {bahasa === 'id' ? id.pernyataan : en.pernyataan}
                </Text>
                <CheckBox
                  label={
                    bahasa === 'id'
                      ? id.title_pernyaataan
                      : en.title_pernyaataan
                  }
                  status={
                    this.state.pernyataanPengaduan ? 'checked' : 'unchecked'
                  }
                  onPress={() => {
                    if (this.state.pernyataanPengaduan) {
                      boolPernyataan = false;
                      this.setState({pernyataanPengaduan: false});
                      this.mirroringKeyKomplain();
                    } else {
                      boolPernyataan = true;
                      this.setState({pernyataanPengaduan: true});
                      this.mirroringKeyKomplain();
                    }
                  }}
                  containerStyle={{
                    width: 290,
                    marginStart: 20,
                    marginEnd: 20,
                    marginBottom: 20,
                  }}
                  labelStyle={{fontSize: 14}}
                  boxColor={COLORS.red}
                />
                <Button
                  style={{
                    marginStart: 20,
                    marginEnd: 20,
                    marginBottom: 20,
                    alignItems: 'center',
                    height: 45,
                    borderRadius: 10,
                    borderColor: COLORS.red,
                    borderWidth: 2,
                  }}
                  labelStyle={styles.btnLabel}
                  buttonColor={COLORS.red}
                  mode="contained"
                  onPress={this.onSubmitPengaduan}>
                  {bahasa === 'id' ? id.lanjut : en.lanjut}
                </Button>
              </View>
            </ScrollView>
          </View>
        );
      }
    } else if (
      this.state.session == 14 ||
      this.state.session == 16 ||
      this.state.session == 17
    ) {
      var myloop = [];
      for (let i = this.state.nextPage; i <= arrayBarcode.length; i++) {
        if (i % 5 === 0) {
          myloop.push(
            <TouchableOpacity
              key={i}
              onPress={() => {
                console.log('Item No : ' + i);
                this.setState({currentPage: i});
              }}>
              <View
                style={{
                  width: 30,
                  height: 30,
                  backgroundColor:
                    this.state.currentPage === i ? '#E34243' : '#DADADA',
                  borderRadius: 3,
                  marginEnd: 10,
                }}>
                <Text
                  style={{
                    textAlign: 'center',
                    marginTop: 5,
                    color: this.state.currentPage === i ? '#fff' : '#000',
                  }}>
                  {i}
                </Text>
              </View>
            </TouchableOpacity>,
            <TouchableOpacity
              key={'...'}
              onPress={() => {
                console.log('Item No : ' + i);
                this.setState({nextPage: i + 1});
                this.setState({currentPage: i + 1});
              }}>
              <View
                style={{
                  width: 30,
                  height: 30,
                  backgroundColor: '#DADADA',
                  borderRadius: 3,
                  marginEnd: 10,
                }}>
                <Text style={{textAlign: 'center', marginTop: 5}}>...</Text>
              </View>
            </TouchableOpacity>,
          );
          break;
        } else if (i === this.state.nextPage) {
          myloop.push(
            <TouchableOpacity
              key={'prev'}
              style={{
                display: i <= 5 ? 'none' : 'flex',
              }}
              onPress={() => {
                console.log('Item No : ' + i);
                this.setState({nextPage: this.state.nextPage - 5});
                this.setState({currentPage: this.state.nextPage - 5});
              }}>
              <View
                style={{
                  width: 30,
                  height: 30,
                  backgroundColor: '#DADADA',
                  borderRadius: 3,
                  marginEnd: 10,
                }}>
                <Text style={{textAlign: 'center', marginTop: 5}}>...</Text>
              </View>
            </TouchableOpacity>,
            <TouchableOpacity
              key={i}
              onPress={() => {
                console.log('Item No : ' + i);
                this.setState({currentPage: i});
              }}>
              <View
                style={{
                  width: 30,
                  height: 30,
                  backgroundColor:
                    this.state.currentPage === i ? '#E34243' : '#DADADA',
                  borderRadius: 3,
                  marginEnd: 10,
                }}>
                <Text
                  style={{
                    textAlign: 'center',
                    marginTop: 5,
                    color: this.state.currentPage === i ? '#fff' : '#000',
                  }}>
                  {i}
                </Text>
              </View>
            </TouchableOpacity>,
          );
        } else {
          myloop.push(
            <TouchableOpacity
              key={i}
              onPress={() => {
                console.log('Item No : ' + i);
                this.setState({currentPage: i});
              }}>
              <View
                style={{
                  width: 30,
                  height: 30,
                  backgroundColor:
                    this.state.currentPage === i ? '#E34243' : '#DADADA',
                  borderRadius: 3,
                  marginEnd: 10,
                }}>
                <Text
                  style={{
                    textAlign: 'center',
                    marginTop: 5,
                    color: this.state.currentPage === i ? '#fff' : '#000',
                  }}>
                  {i}
                </Text>
              </View>
            </TouchableOpacity>,
          );
        }
      }
      var uploadQrLoop = [];
      for (let i = 0; i < arrayUploadBarcode.length; i++) {
        uploadQrLoop.push(
          <View
            key={i}
            style={{
              flexDirection: 'row',
              height: 70,
              marginBottom: 10,
            }}>
            <TouchableOpacity
              onPress={() => {
                this.setState({transaksiKe: i});
                this.launchImageLibraryfunc();
              }}>
              <View
                style={{
                  width: 70,
                  height: 70,
                  borderRadius: 10,
                  borderWidth: 1,
                  padding: 20,
                  backgroundColor: '#fff',
                  justifyContent: 'center',
                  borderColor: '#DADADA',
                }}>
                <FastImage
                  style={{
                    alignSelf: 'center',
                    width: '100%',
                    height: '75%',
                    resizeMode: 'contain',
                  }}
                  source={
                    arrayUploadBarcode[i].nameBarcode === undefined
                      ? unggahabu
                      : unggahbiru
                  }
                />
              </View>
            </TouchableOpacity>
            <Text
              style={{
                alignSelf: 'center',
                fontFamily: 'Helvetica',
                color: '#B5B5B5',
                fontStyle: 'italic',
                fontSize: 17,
                marginStart: 20,
                marginEnd: 20,
              }}>
              {arrayUploadBarcode[i].nameBarcode === undefined
                ? bahasa === 'id'
                  ? id.UploadQR + (i + 1)
                  : en.UploadQR + (i + 1)
                : arrayUploadBarcode[i].nameBarcode}
            </Text>
          </View>,
        );
      }
      console.log('myloop', myloop.length);
      if (this.state.isConfirmTransaksi) {
        return (
          <View
            style={{
              width: '100%',
              alignItems: 'center',
              height: '60%',
              alignSelf: 'center',
              flexDirection: 'column',
            }}>
            <View
              style={{
                width: 420,
                backgroundColor: 'white',
                flexDirection: 'row',
              }}>
              <Pressable
                onPress={() => {
                  this.setState({
                    isConfirmTransaksi: false,
                    currentPage: 1,
                    nextPage: 1,
                  });
                }}
                style={{marginEnd: 10, alignSelf: 'center'}}>
                <FastImage
                  style={{
                    marginStart: 20,
                    width: 30,
                    height: 30,
                  }}
                  source={back}
                />
              </Pressable>
              <Text
                style={{
                  marginTop: 15,
                  marginBottom: 15,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  fontWeight: 'bold',
                  borderBottomWidth: 1,
                  borderBottomColor: 'black',
                }}>
                {bahasa === 'id' ? id.konfirmasi : en.konfirmasi}
              </Text>
            </View>
            {this.popUpInvalidInquiry()}
            <ScrollView
              keyboardDismissMode="none"
              style={{width: '100%'}}
              automaticallyAdjustKeyboardInsets="true"
              automaticallyAdjustContentInsets="true">
              <View
                style={{
                  marginTop: 15,
                  marginStart: 25,
                  marginEnd: 25,
                  flexDirection: 'column',
                }}>
                <View
                  style={{
                    display:
                      this.state.showFormBuilderQr && myloop.length > 2
                        ? 'flex'
                        : 'none',
                    flexDirection: 'row',
                    marginBottom: 10,
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                    }}>
                    {bahasa === 'id'
                      ? id.transaksi + ' : '
                      : en.transaksi + ' : '}
                  </Text>
                  {myloop}
                </View>

                <View
                  style={{
                    display: this.state.showFormBuilderQr ? 'flex' : 'none',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginBottom: 10,
                    marginTop: 10,
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                    }}>
                    {Object.keys(arrayBarcode).length > 0 &&
                    arrayBarcode[this.state.currentPage - 1].jenisform ===
                      'non_qr'
                      ? bahasa === 'id'
                        ? id.transaksi_non_qr + this.state.currentPage
                        : en.transaksi_non_qr + this.state.currentPage
                      : bahasa === 'id'
                      ? id.transaksi_qr + this.state.currentPage
                      : en.transaksi_qr + this.state.currentPage}
                  </Text>
                  <TouchableOpacity
                    onPress={() => {
                      arrayBarcode.splice(this.state.currentPage - 1, 1);
                      arraynoForm.splice(this.state.currentPage - 1, 1);
                      this.setState({
                        currentPage:
                          this.state.currentPage > 1
                            ? this.state.currentPage - 1
                            : 1,
                      });
                      if (arrayBarcode.length === 0) {
                        this.setState({
                          showFormBuilderQr: false,
                          currentPage: 1,
                          isConfirmTransaksi: false,
                        });
                      }
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        alignContent: 'center',
                        alignSelf: 'center',
                      }}>
                      <Image
                        tintColor={COLORS.red}
                        transition={false}
                        source={delete_icon}
                        style={{
                          width: 25,
                          height: 25,
                          resizeMode: 'contain',
                        }}
                      />
                      <Text style={{fontSize: 15, marginStart: 5}}>
                        {bahasa === 'id' ? id.delete : en.delete}
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>

                <View
                  style={{
                    display: this.state.showFormBuilderQr ? 'flex' : 'none',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginBottom: 10,
                    marginBottom: 10,
                    marginTop: 10,
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                    }}>
                    {bahasa === 'id' ? id.no_formulir : en.no_formulir}
                  </Text>
                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                    }}>
                    {arraynoForm[this.state.currentPage - 1]?.noForm}
                  </Text>
                </View>

                <View
                  style={{
                    flexDirection: 'column',
                  }}>
                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                    }}>
                    {bahasa == 'id'
                      ? id.rekening_sumber_dana
                      : en.rekening_sumber_dana}
                  </Text>
                  <Text
                    style={{
                      fontSize: 15,
                      marginEnd: 10,
                    }}>
                    {arrayBarcode[this.state.currentPage - 1]
                      ?.rekeningsumberdana !== undefined
                      ? arrayBarcode[this.state.currentPage - 1]
                          .rekeningsumberdana.prodName
                      : '-'}
                  </Text>
                  <Text
                    style={{
                      fontSize: 15,
                      marginEnd: 10,
                    }}>
                    {arrayBarcode[this.state.currentPage - 1]
                      ?.rekeningsumberdana !== undefined
                      ? arrayBarcode[this.state.currentPage - 1]
                          .rekeningsumberdana.accountNo +
                        ' - ' +
                        arrayBarcode[this.state.currentPage - 1]
                          .rekeningsumberdana.accountNo
                      : '-'}
                  </Text>
                  <Text
                    style={{
                      fontSize: 15,
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {arrayBarcode[this.state.currentPage - 1]
                      ?.rekeningsumberdana !== undefined
                      ? 'Rp ' +
                        (
                          parseInt(
                            arrayBarcode[this.state.currentPage - 1]
                              .rekeningsumberdana.availBalance,
                          ) / 100
                        )
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, '.')
                      : '-'}
                  </Text>

                  <Text
                    style={{
                      display: this.state.session === 14 ? 'flex' : 'none',
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                    }}>
                    {bahasa == 'id' ? id.jenis_layanan : en.jenis_layanan}
                  </Text>
                  <Text
                    style={{
                      display: this.state.session === 14 ? 'flex' : 'none',
                      fontSize: 15,
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {arrayBarcode[this.state.currentPage - 1]?.jenislayanan !==
                    undefined
                      ? arrayBarcode[this.state.currentPage - 1].jenislayanan
                          .label !== undefined
                        ? arrayBarcode[this.state.currentPage - 1].jenislayanan
                            .label
                        : arrayBarcode[this.state.currentPage - 1].jenislayanan
                      : '-'}
                  </Text>

                  <Text
                    style={{
                      display: this.state.session === 14 ? 'flex' : 'none',
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                    }}>
                    {bahasa == 'id' ? id.bank_penerima : en.bank_penerima}
                  </Text>
                  <Text
                    style={{
                      display: this.state.session === 14 ? 'flex' : 'none',
                      fontSize: 15,
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {arrayBarcode[this.state.currentPage - 1]?.bankpenerima !==
                    undefined
                      ? arrayBarcode[this.state.currentPage - 1].bankpenerima
                          .label !== undefined
                        ? arrayBarcode[this.state.currentPage - 1].bankpenerima
                            .label
                        : arrayBarcode[this.state.currentPage - 1].bankpenerima
                      : '-'}
                  </Text>

                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                    }}>
                    {bahasa == 'id'
                      ? id.rekening_penerima
                      : en.rekening_penerima}
                  </Text>
                  <Text
                    style={{
                      fontSize: 15,
                      marginEnd: 10,
                      marginBottom: 10,
                      color:
                        arrayInquiry[this.state.currentPage - 1]
                          ?.destCustomerName === undefined
                          ? '#E34243'
                          : 'black',
                    }}>
                    {arrayInquiry[this.state.currentPage - 1]
                      ?.destCustomerName !== undefined
                      ? arrayBarcode[this.state.currentPage - 1]
                          ?.rekeningpenerima
                      : bahasa === 'id'
                      ? id.penerima_tidak_ditemukan
                      : en.penerima_tidak_ditemukan}
                  </Text>

                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                    }}>
                    {bahasa == 'id' ? id.nama_penerima : en.nama_penerima}
                  </Text>
                  <Text
                    style={{
                      fontSize: 15,
                      marginEnd: 10,
                      marginBottom: 10,
                      color: 'black',
                    }}>
                    {arrayInquiry[this.state.currentPage - 1]
                      ?.destCustomerName !== undefined
                      ? arrayInquiry[this.state.currentPage - 1]
                          ?.destCustomerName
                      : '-'}
                  </Text>

                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                    }}>
                    {bahasa == 'id'
                      ? id.nominal_transaksi
                      : en.nominal_transaksi}
                  </Text>
                  <Text
                    style={{
                      fontSize: 15,
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {arrayBarcode[this.state.currentPage - 1]
                      ?.nominaltransaksi !== undefined
                      ? arrayBarcode[this.state.currentPage - 1]
                          .nominaltransaksi
                      : '-'}
                  </Text>

                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                      display: this.state.session === 17 ? 'none' : 'flex',
                    }}>
                    {bahasa == 'id' ? id.tujuan_transaksi : en.tujuan_transaksi}
                  </Text>
                  <Text
                    style={{
                      fontSize: 15,
                      marginEnd: 10,
                      marginBottom: 10,
                      display: this.state.session === 17 ? 'none' : 'flex',
                    }}>
                    {arrayBarcode[this.state.currentPage - 1]
                      ?.tujuantransaksi !== undefined
                      ? arrayBarcode[this.state.currentPage - 1].tujuantransaksi
                          .label !== undefined
                        ? arrayBarcode[this.state.currentPage - 1]
                            .tujuantransaksi.label
                        : arrayBarcode[this.state.currentPage - 1]
                            .tujuantransaksi
                      : '-'}
                  </Text>

                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {bahasa == 'id'
                      ? id.tanggal_transaksi
                      : en.tanggal_transaksi}
                  </Text>
                  <Text
                    style={{
                      fontSize: 15,
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {moment().format('DD-MM-YYYY HH:mm')}
                  </Text>

                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                    }}>
                    {bahasa == 'id' ? id.berita : en.berita}
                  </Text>
                  <Text
                    style={{
                      fontSize: 15,
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {arrayBarcode[this.state.currentPage - 1]?.berita !==
                    undefined
                      ? arrayBarcode[this.state.currentPage - 1].berita
                      : '-'}
                  </Text>
                </View>

                <View
                  style={{
                    marginBottom: 10,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <Button
                    onPress={() => {
                      this.setState({
                        isConfirmTransaksi: false,
                        currentPage: 1,
                        nextPage: 1,
                      });
                    }}
                    style={[
                      styles.btn,
                      {
                        width: 'auto',
                        alignSelf: 'center',
                      },
                    ]}
                    labelStyle={styles.btnLabel}
                    buttonColor={COLORS.white}
                    textColor={COLORS.red}
                    mode="contained">
                    {bahasa === 'id' ? id.cek_ulang : en.cek_ulang}
                  </Button>

                  <Button
                    onPress={() => {
                      if (!wrongInquiry) {
                        this.loopsaveForm();
                      }
                    }}
                    style={[
                      {
                        width: 'auto',
                        borderRadius: 12,
                        borderColor: wrongInquiry ? '#B5B5B5' : COLORS.red,
                        borderWidth: 2,
                        alignSelf: 'center',
                      },
                    ]}
                    labelStyle={styles.btnLabel}
                    buttonColor={wrongInquiry ? '#B5B5B5' : COLORS.red}
                    mode="contained">
                    {bahasa == 'id' ? id.lanjut : en.lanjut}
                  </Button>
                </View>
              </View>
            </ScrollView>
          </View>
        );
      } else {
        return (
          <View
            style={{
              width: '100%',
              alignItems: 'center',
              height: '60%',
              alignSelf: 'center',
              flexDirection: 'column',
            }}>
            <View
              style={{
                width: 420,
                backgroundColor: 'white',
                flexDirection: 'row',
              }}>
              <Pressable
                onPress={() => {
                  this.setState({
                    showFormBuilderQr: false,
                    session: 22,
                  });
                  arrayUploadBarcode = [];
                  arrayBarcode = [];
                  arraynoForm = [];
                }}
                style={{marginEnd: 10, alignSelf: 'center'}}>
                <FastImage
                  style={{
                    marginStart: 20,
                    width: 30,
                    height: 30,
                  }}
                  source={back}
                />
              </Pressable>
              <Text
                style={{
                  marginTop: 15,
                  marginBottom: 15,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  fontWeight: 'bold',
                  borderBottomWidth: 1,
                  borderBottomColor: 'black',
                }}>
                {this.state.session == 14
                  ? bahasa === 'id'
                    ? id.transaksi_antar_bank
                    : en.transaksi_antar_bank
                  : this.state.session == 16
                  ? bahasa === 'id'
                    ? id.transaksi_antar_rekening
                    : en.transaksi_antar_rekening
                  : bahasa === 'id'
                  ? id.transaksi_rekening_pribadi
                  : en.transaksi_rekening_pribadi}
              </Text>
            </View>
            <ScrollView
              keyboardDismissMode="none"
              style={{width: '100%'}}
              automaticallyAdjustKeyboardInsets="true"
              automaticallyAdjustContentInsets="true">
              <View
                style={{
                  marginTop: 15,
                  marginStart: 25,
                  marginEnd: 25,
                  flexDirection: 'column',
                }}>
                {uploadQrLoop}

                <Text
                  onPress={() => {
                    arrayUploadBarcode.push({});
                    console.log('arrayUploadBarcode', arrayUploadBarcode);
                  }}
                  style={{
                    marginTop: 10,
                    marginBottom: 20,
                    fontFamily: 'Helvetica',
                    color: '#000',
                    fontSize: 17,
                  }}>
                  {bahasa === 'id' ? id.tambah_qr_button : e.tambah_qr_button}
                </Text>

                <View
                  style={{
                    display: this.state.showFormBuilderQr ? 'flex' : 'none',
                    flexDirection: 'row',
                    marginBottom: 10,
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                    }}>
                    {bahasa === 'id'
                      ? id.transaksi + ' : '
                      : en.transaksi + ' : '}
                  </Text>
                  {myloop}
                </View>

                <View
                  style={{
                    display: this.state.showFormBuilderQr ? 'flex' : 'none',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginBottom: 10,
                    marginTop: 10,
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                    }}>
                    {Object.keys(arrayBarcode).length > 0 &&
                    arrayBarcode[this.state.currentPage - 1].jenisform ===
                      'non_qr'
                      ? bahasa === 'id'
                        ? id.transaksi_non_qr + this.state.currentPage
                        : en.transaksi_non_qr + this.state.currentPage
                      : bahasa === 'id'
                      ? id.transaksi_qr + this.state.currentPage
                      : en.transaksi_qr + this.state.currentPage}
                  </Text>
                  <TouchableOpacity
                    onPress={() => {
                      arrayBarcode.splice(this.state.currentPage - 1, 1);
                      arraynoForm.splice(this.state.currentPage - 1, 1);
                      this.setState({
                        currentPage:
                          this.state.currentPage > 1
                            ? this.state.currentPage - 1
                            : 1,
                      });
                      if (arrayBarcode.length === 0) {
                        this.setState({
                          showFormBuilderQr: false,
                          currentPage: 1,
                        });
                      }
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        alignContent: 'center',
                        alignSelf: 'center',
                      }}>
                      <Image
                        tintColor={COLORS.red}
                        transition={false}
                        source={delete_icon}
                        style={{
                          width: 25,
                          height: 25,
                          resizeMode: 'contain',
                        }}
                      />
                      <Text style={{fontSize: 15, marginStart: 5}}>
                        {bahasa === 'id' ? id.delete : en.delete}
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>

                <View
                  style={{
                    display: this.state.showFormBuilderQr ? 'flex' : 'none',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginBottom: 10,
                    marginBottom: 10,
                    marginTop: 10,
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                    }}>
                    {bahasa === 'id' ? id.no_formulir : en.no_formulir}
                  </Text>
                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                    }}>
                    {arraynoForm[this.state.currentPage - 1]?.noForm}
                  </Text>
                </View>

                <View
                  style={{
                    display: this.state.showFormBuilderQr ? 'flex' : 'none',
                  }}>
                  <DynamicForm
                    bahasa={bahasa}
                    accessToken={accessToken}
                    exchangeToken={exchangeToken}
                    formTemplate={{data: dataresp}}
                    onSubmit={() => {}}
                    defaultValue={
                      arrayBarcode.length > 0
                        ? arrayBarcode[this.state.currentPage - 1]
                        : {}
                    }
                    redaksiTextStyle={[
                      {
                        color: 'red',
                        marginStart: 10,
                        marginEnd: 10,
                        textAlign: 'left',
                      },
                      {
                        color: 'red',
                        marginStart: 10,
                        marginEnd: 10,
                        textAlign: 'left',
                      },
                    ]}
                    arrayRedaksi={[
                      {
                        pointerRedaksi:
                          arrayBarcode[
                            this.state.currentPage - 1
                          ]?.hasOwnProperty('rekeningpenerima') &&
                          arrayBarcode[
                            this.state.currentPage - 1
                          ]?.hasOwnProperty('rekeningsumberdana')
                            ? arrayBarcode[this.state.currentPage - 1]
                                .rekeningpenerima ===
                              arrayBarcode[this.state.currentPage - 1]
                                .rekeningsumberdana.accountNo
                              ? 'Rekening Penerima'
                              : ''
                            : '',
                        redaksiText:
                          bahasa === 'id'
                            ? id.redaksi_same_norek
                            : en.redaksi_same_norek,
                      },
                      {
                        pointerRedaksi:
                          arrayBarcode[
                            this.state.currentPage - 1
                          ]?.hasOwnProperty('nominaltransaksi') &&
                          arrayBarcode[
                            this.state.currentPage - 1
                          ]?.hasOwnProperty('rekeningsumberdana')
                            ? parseInt(
                                arrayBarcode[
                                  this.state.currentPage - 1
                                ].nominaltransaksi.replaceAll(/\./g, ''),
                                10,
                              ) >
                              parseInt(
                                arrayBarcode[this.state.currentPage - 1]
                                  .rekeningsumberdana.availBalance,
                              ) /
                                100
                              ? 'Nominal Transaksi'
                              : ''
                            : '',
                        redaksiText:
                          bahasa === 'id'
                            ? id.nominal_reach_balance
                            : en.nominal_reach_balance,
                      },
                    ]}
                    onPressed={this.onSubmitTransaction}
                    btnStyle={{
                      display: 'none',
                    }}
                  />
                </View>

                <View
                  style={{
                    marginBottom: 10,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <Button
                    onPress={() => {
                      this.getNoForm();
                      arrayBarcode.push({
                        jenisform: 'non_qr',
                      });
                      this.formBuilder2(
                        this.state.session === 14
                          ? 48
                          : this.state.session === 16
                          ? 49
                          : this.state.session === 17
                          ? 56
                          : 0,
                      );
                      console.log('Array Barcode', arrayBarcode);
                      this.setState({showFormBuilderQr: true});
                    }}
                    style={[
                      styles.btn,
                      {
                        alignSelf: 'center',
                      },
                    ]}
                    labelStyle={styles.btnLabel}
                    buttonColor={COLORS.white}
                    textColor={COLORS.red}
                    mode="contained">
                    {bahasa === 'id' ? id.tambah_form : en.tambah_form}
                  </Button>

                  <Button
                    onPress={() => {
                      if (this.state.showFormBuilderQr) {
                        if (this.state.session === 14) {
                          if (
                            arrayBarcode[this.state.currentPage - 1]
                              ?.rekeningpenerima ===
                              arrayBarcode[this.state.currentPage - 1]
                                ?.rekeningsumberdana?.accountNo ||
                            parseInt(
                              arrayBarcode[
                                this.state.currentPage - 1
                              ]?.nominaltransaksi?.replaceAll(/\./g, ''),
                              10,
                            ) >
                              parseInt(
                                arrayBarcode[this.state.currentPage - 1]
                                  ?.rekeningsumberdana?.availBalance,
                              ) /
                                100
                          ) {
                          } else {
                            this.mirroringEndpoint(195);
                            this.inquiryAntarBank();
                          }
                        } else if (this.state.session === 16) {
                          if (
                            arrayBarcode[this.state.currentPage - 1]
                              ?.rekeningpenerima ===
                              arrayBarcode[this.state.currentPage - 1]
                                ?.rekeningsumberdana?.accountNo ||
                            parseInt(
                              arrayBarcode[
                                this.state.currentPage - 1
                              ]?.nominaltransaksi?.replaceAll(/\./g, ''),
                              10,
                            ) >
                              parseInt(
                                arrayBarcode[this.state.currentPage - 1]
                                  ?.rekeningsumberdana?.availBalance,
                              ) /
                                100
                          ) {
                          } else {
                            this.mirroringEndpoint(195);
                            this.inquiryAntarRekening();
                          }
                        } else if (this.state.session === 17) {
                          //Transaksi Rekening Sendiri
                          if (correctTransaction) {
                            if (
                              arrayBarcode[
                                this.state.currentPage - 1
                              ].hasOwnProperty('rekeningsumberdana') &&
                              arrayBarcode[
                                this.state.currentPage - 1
                              ].hasOwnProperty('rekeningpenerima') &&
                              arrayBarcode[
                                this.state.currentPage - 1
                              ].hasOwnProperty('nominaltransaksi') &&
                              arrayBarcode[this.state.currentPage - 1]
                                ?.rekeningpenerima !== '' &&
                              arrayBarcode[this.state.currentPage - 1]
                                ?.nominaltransaksi !== '' &&
                              arrayBarcode[this.state.currentPage - 1]
                                ?.nominaltransaksi !== '0' &&
                              mandatoryIsOke
                            ) {
                              this.mirroringEndpoint(195);
                              this.inquiryRekeningSendiri();
                            } else {
                              Alert.alert(
                                bahasa === 'id'
                                  ? id.silahkan_isi_kolom
                                  : en.silahkan_isi_kolom,
                              );
                            }
                          }
                        }
                      }
                    }}
                    style={[
                      {
                        borderRadius: 12,
                        borderColor: correctTransaction
                          ? COLORS.red
                          : '#B5B5B5',
                        borderWidth: 2,
                        alignSelf: 'center',
                      },
                    ]}
                    labelStyle={styles.btnLabel}
                    buttonColor={correctTransaction ? COLORS.red : '#B5B5B5'}
                    mode="contained">
                    {bahasa == 'id' ? id.lanjut : en.lanjut}
                  </Button>
                </View>
              </View>
            </ScrollView>
          </View>
        );
      }
    } else if (this.state.session == 15) {
      return (
        <View
          style={{
            width: '100%',
            height: '65%',
            flexDirection: 'column',
          }}>
          <View
            style={{
              width: 420,
              backgroundColor: 'white',
              flexDirection: 'row',
            }}>
            <Pressable
              onPress={() => {
                this.setState({session: 3});
                arrayBarcode = [];
              }}
              style={{marginEnd: 10, alignSelf: 'center'}}>
              <FastImage
                style={{
                  marginStart: 20,
                  width: 30,
                  height: 30,
                }}
                source={back}
              />
            </Pressable>
            <Text
              style={{
                marginTop: 15,
                marginBottom: 15,
                fontFamily: 'Helvetica',
                fontSize: 18,
                fontWeight: 'bold',
                borderBottomWidth: 1,
                borderBottomColor: 'black',
              }}>
              {bahasa === 'id' ? id.pembukaan_akun : en.pembukaan_akun}
            </Text>
          </View>
          <ScrollView
            keyboardDismissMode="none"
            contentContainerStyle={{
              paddingBottom: '40%',
            }}
            automaticallyAdjustKeyboardInsets="true"
            automaticallyAdjustContentInsets="true"
            refreshControl={
              this.state.session >= 8 ? (
                <RefreshControl
                  refreshing={this.state.setRefreshing}
                  onRefresh={this.onRefresh}
                />
              ) : undefined
            }>
            <View
              style={{
                display: dataresp.length === 0 ? 'none' : 'flex',
                marginTop: 20,
                marginStart: 20,
                marginEnd: 20,
                padding: 20,
                height: '100%',
                flexDirection: 'column',
              }}>
              <Text
                style={{
                  marginStart: 10,
                  textAlign: 'left',
                  fontSize: 20,
                  marginBottom: 10,
                  fontWeight: 'bold',
                  color: COLORS.textColorGray,
                }}>
                {bahasa === 'id'
                  ? id.informasi_data_utama
                  : en.informasi_data_utama}
              </Text>
              <DynamicForm
                bahasa={bahasa}
                defaultValue={
                  arrayBarcode.length > 0
                    ? arrayBarcode[0]
                    : {
                        nomorcif: noCIF,
                        kodecabang: custBranchCode,
                        namanasabah: custName,
                        noidentitas: nik_,
                      }
                }
                accessToken={accessToken}
                exchangeToken={exchangeToken}
                formTemplate={{data: dataresp}}
                onSubmit={this.onSubmit}
                onPressed={this.onSubmitTransaction}
                btnStyle={{
                  borderRadius: 12,
                  backgroundColor: COLORS.red,
                  top: 20,
                  marginEnd: 10,
                  width: 'auto',
                  alignSelf: 'flex-end',
                }}
                btnLabel={bahasa === 'id' ? id.selanjutnya : en.selanjutnya}
              />
            </View>
          </ScrollView>
        </View>
      );
    } else if (
      this.state.session == 18 ||
      this.state.session == 22 ||
      this.state.session == 27
    ) {
      return (
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            height: '65%',
            alignSelf: 'center',
            flexDirection: 'column',
          }}>
          <View
            style={{
              width: 420,
              backgroundColor: 'white',
              flexDirection: 'row',
            }}>
            <Pressable
              onPress={() => {
                if (this.state.isUnderDevelopment) {
                  this.setState({isUnderDevelopment: false});
                } else {
                  this.setState({session: 12});
                }
              }}
              style={{marginEnd: 10, alignSelf: 'center'}}>
              <FastImage
                style={{
                  marginStart: 20,
                  width: 30,
                  height: 30,
                }}
                source={back}
              />
            </Pressable>
            <Text
              style={{
                marginTop: 15,
                marginBottom: 15,
                fontFamily: 'Helvetica',
                fontSize: 18,
                fontWeight: 'bold',
                borderBottomWidth: 1,
                borderBottomColor: 'black',
              }}>
              {this.state.session == 18
                ? bahasa === 'id'
                  ? id.deposito_online.toUpperCase()
                  : en.deposito_online.toUpperCase()
                : this.state.session == 22
                ? bahasa === 'id'
                  ? id.transaksi
                  : en.transaksi
                : this.state.session == 27
                ? bahasa === 'id'
                  ? id.transaksi_wealth_manage
                  : en.transaksi_wealth_manage
                : ''}
            </Text>
          </View>
          <FlatList
            key={
              this.state.session == 18
                ? 'menu_deposito_online'
                : this.state.session == 22
                ? 'menu_transaksi_online'
                : this.state.session == 27
                ? 'menu_wealth_management'
                : ''
            }
            style={{
              display: this.state.isUnderDevelopment ? 'none' : 'flex',
            }}
            data={this.formatData(
              this.state.session == 18
                ? this.state.itemDepositoOnline
                : this.state.session == 22
                ? this.state.itemTransaksi
                : this.state.session == 27
                ? this.state.itemWealthManagement
                : [],
              this.state.numColumns,
            )}
            renderItem={({item, index}) => (
              <TouchableOpacity
                key={index}
                onPress={() => {
                  if (this.state.session == 18) {
                    if (item.id == 1) {
                      //Transaksi Penempatan Deposito
                      console.log('Penempatan Deposito');
                      this.getNoRekening();
                      this.setState({session: 19});
                      this.formBuilder2(52);
                      this.getNoForm();
                    } else if (item.id == 2) {
                      //Transaksi Pencairan Deposito
                      this.setState({session: 20});
                      this.getPortoDeposito();
                    } else if (item.id == 3) {
                      //Transaksi Perubah ARO & Non ARO
                      this.setState({
                        session: 21,
                      });
                      this.getDropdownInstruksi();
                      this.getPortoDeposito();
                    } else {
                      this.setState({isUnderDevelopment: true});
                    }
                  } else if (this.state.session === 22) {
                    if (item.id == 1) {
                      //Transaksi Rekening Sendiri
                      this.mirroringEndpoint(200);
                      this.getNoRekening();
                      this.setState({session: 17});
                      arrayUploadBarcode.push({});
                    } else if (item.id == 2) {
                      //Transaksi Antar Rekening
                      this.mirroringEndpoint(190);
                      this.getNoRekening();
                      this.setState({session: 16});
                      arrayUploadBarcode.push({});
                    } else if (item.id == 3) {
                      //Transaksi Antar Bank
                      this.mirroringEndpoint(191);
                      this.getNoRekening();
                      this.setState({session: 14});
                      arrayUploadBarcode.push({});
                    }
                  } else if (this.state.session == 27) {
                    if (item.id == 1) {
                      //Transaksi Reksa Dana
                      this.checkStatusSID({
                        nik: nik_,
                        noCif: noCIF,
                      });
                    }
                  }
                }}
                style={{
                  alignSelf: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                  alignContent: 'center',
                  width: 121,
                  marginTop: 10,
                  justifyContent: 'flex-start',
                  flexDirection: 'column',
                  height: 165,
                }}>
                <FastImage
                  style={{
                    width: 100,
                    height: 100,
                    alignContent: 'center',
                    alignSelf: 'center',
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginBottom: 10,
                    borderRadius: 13,
                  }}
                  source={item.img}
                />
                <Text
                  ellipsizeMode="tail"
                  textAlign="center"
                  numberOfLines={2}
                  style={{
                    width: 121,
                    color: 'black',
                    fontSize: 15,
                    fontWeight: 'bold',
                    textAlign: 'center',
                    fontFamily: 'Helvetica',
                  }}>
                  {item.title}
                </Text>
              </TouchableOpacity>
            )}
            numColumns={this.state.numColumns}
            keyExtractor={item => item.id}
          />
          <View
            style={{
              display: this.state.isUnderDevelopment ? 'flex' : 'none',
              height: '90%',
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'column',
              width: '100%',
            }}>
            <Text
              style={{
                marginTop: 15,
                fontFamily: 'Helvetica',
                fontSize: 22,
                textAlign: 'center',
                fontWeight: 'bold',
              }}>
              {bahasa === 'id' ? id.dalam_pengerjaan : id.dalam_pengerjaan}
            </Text>
            <Text
              style={{
                marginBottom: 15,
                fontFamily: 'Helvetica',
                fontSize: 17,
                textAlign: 'center',
              }}>
              {bahasa === 'id'
                ? id.halaman_ini_sedang_dalam_pengerjaan
                : en.halaman_ini_sedang_dalam_pengerjaan}
            </Text>
            <FastImage
              style={{
                width: 290,
                height: 200,
              }}
              source={dalampengerjaan}
            />
          </View>
        </View>
      );
    } else if (this.state.session == 19) {
      if (this.state.isConfirmTransaksi) {
        return (
          <View
            style={{
              width: '100%',
              alignItems: 'center',
              height: '60%',
              alignSelf: 'center',
              flexDirection: 'column',
            }}>
            <View
              style={{
                width: 420,
                backgroundColor: 'white',
                flexDirection: 'row',
              }}>
              <Pressable
                onPress={() => {
                  this.setState({
                    isConfirmTransaksi: false,
                  });
                }}
                style={{marginEnd: 10, alignSelf: 'center'}}>
                <FastImage
                  style={{
                    marginStart: 20,
                    width: 30,
                    height: 30,
                  }}
                  source={back}
                />
              </Pressable>
              <Text
                style={{
                  marginTop: 15,
                  marginBottom: 15,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  fontWeight: 'bold',
                  borderBottomWidth: 1,
                  borderBottomColor: 'black',
                }}>
                {bahasa === 'id' ? id.konfirmasi : en.konfirmasi}
              </Text>
            </View>
            <ScrollView
              keyboardDismissMode="none"
              style={{width: '100%'}}
              automaticallyAdjustKeyboardInsets="true"
              automaticallyAdjustContentInsets="true">
              <View
                style={{
                  marginTop: 15,
                  marginStart: 25,
                  marginEnd: 25,
                  flexDirection: 'column',
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginBottom: 10,
                    marginBottom: 10,
                    marginTop: 10,
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                    }}>
                    {bahasa === 'id' ? id.no_formulir : en.no_formulir}
                  </Text>
                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                    }}>
                    {arraynoForm[0]?.noForm}
                  </Text>
                </View>

                <View
                  style={{
                    flexDirection: 'column',
                  }}>
                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                    }}>
                    {bahasa == 'id'
                      ? id.rekening_sumber_dana
                      : en.rekening_sumber_dana}
                  </Text>
                  <Text
                    style={{
                      fontSize: 15,
                      marginEnd: 10,
                    }}>
                    {arrayBarcode[0]?.rekeningsumberdana !== undefined
                      ? arrayBarcode[0].rekeningsumberdana.prodName
                      : '-'}
                  </Text>
                  <Text
                    style={{
                      fontSize: 15,
                      marginEnd: 10,
                    }}>
                    {arrayBarcode[0]?.rekeningsumberdana !== undefined
                      ? arrayBarcode[0].rekeningsumberdana.accountNo +
                        ' - ' +
                        arrayBarcode[0].rekeningsumberdana.accountNo
                      : '-'}
                  </Text>
                  <Text
                    style={{
                      fontSize: 15,
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {arrayBarcode[0]?.rekeningsumberdana !== undefined
                      ? 'IDR ' +
                        parseInt(
                          arrayBarcode[this.state.currentPage - 1]
                            .rekeningsumberdana.availBalance,
                        )
                      : '-'}
                  </Text>

                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {bahasa == 'id' ? id.jenis_deposito : en.jenis_deposito}
                  </Text>
                  <Text
                    style={{
                      fontSize: 15,
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {arrayBarcode[0]?.matauang !== undefined
                      ? arrayBarcode[0].matauang.label
                      : '-'}
                  </Text>

                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {bahasa == 'id' ? id.nominal_deposito : en.nominal_deposito}
                  </Text>
                  <Text
                    style={{
                      fontSize: 15,
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {arrayBarcode[0]?.nominaldeposito !== undefined
                      ? arrayBarcode[0].nominaldeposito
                      : '-'}
                  </Text>

                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {bahasa == 'id' ? id.jangka_waktu : en.jangka_waktu}
                  </Text>
                  <Text
                    style={{
                      fontSize: 15,
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {arrayBarcode[0]?.jangkawaktu !== undefined
                      ? arrayBarcode[0].jangkawaktu.value
                      : '-'}
                  </Text>

                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {bahasa == 'id' ? id.intruksi : en.intruksi}
                  </Text>
                  <Text
                    style={{
                      fontSize: 15,
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {arrayBarcode[0]?.instruksi !== undefined
                      ? arrayBarcode[0]?.instruksi.value
                      : '-'}
                  </Text>

                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {bahasa == 'id' ? id.bunga_didapat : en.bunga_didapat}
                  </Text>
                  <Text
                    style={{
                      fontSize: 15,
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {this.state.interesetDeposito + ' %'}
                  </Text>

                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {bahasa == 'id'
                      ? id.tanggal_transaksi
                      : en.tanggal_transaksi}
                  </Text>
                  <Text
                    style={{
                      fontSize: 15,
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {moment().format('DD-MM-YYYY HH:mm')}
                  </Text>
                </View>

                <View
                  style={{
                    marginBottom: 10,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <Button
                    onPress={() => {
                      this.setState({
                        isConfirmTransaksi: false,
                      });
                    }}
                    style={[
                      styles.btn,
                      {
                        width: 'auto',
                        alignSelf: 'center',
                      },
                    ]}
                    labelStyle={styles.btnLabel}
                    buttonColor={COLORS.white}
                    textColor={COLORS.red}
                    mode="contained">
                    {bahasa === 'id' ? id.cek_ulang : en.cek_ulang}
                  </Button>

                  <Button
                    onPress={() => {
                      this.setState({
                        showFormBuilderQr: false,
                        isConfirmTransaksi: false,
                        currentPage: 1,
                        nextPage: 1,
                        session: 9,
                        sessionBeforeOTP: 19,
                      });
                      //this.sendOtp();
                      this.penempatanDeposito({
                        noCif: noCIF,
                        noRek: arrayBarcode[0]?.rekeningsumberdana.accountNo,
                        nasabah:
                          arrayBarcode[0]?.rekeningsumberdana.accountName,
                        nominal: arrayBarcode[0]?.nominaldeposito,
                        ratePercent: this.state.interesetDeposito,
                        flagAro: arrayBarcode[0]?.instruksi.valueCode,
                        timePeriod: arrayBarcode[0]?.jangkawaktu.value,
                        depoCode: this.state.depoCode_,
                      });
                      arraynoForm = [];
                    }}
                    style={[
                      {
                        width: 'auto',
                        borderRadius: 12,
                        borderColor: COLORS.red,
                        borderWidth: 2,
                        alignSelf: 'center',
                      },
                    ]}
                    labelStyle={styles.btnLabel}
                    buttonColor={COLORS.red}
                    mode="contained">
                    {bahasa == 'id' ? id.lanjut : en.lanjut}
                  </Button>
                </View>
              </View>
            </ScrollView>
          </View>
        );
      } else {
        return (
          <View
            style={{
              width: '100%',
              alignItems: 'center',
              height: '60%',
              alignSelf: 'center',
              flexDirection: 'column',
            }}>
            <View
              style={{
                width: 420,
                backgroundColor: 'white',
                flexDirection: 'row',
              }}>
              <Pressable
                onPress={() => {
                  arrayBarcode = [];
                  arraynoForm = [];
                  this.setState({
                    session: 18,
                  });
                }}
                style={{marginEnd: 10, alignSelf: 'center'}}>
                <FastImage
                  style={{
                    marginStart: 20,
                    width: 30,
                    height: 30,
                  }}
                  source={back}
                />
              </Pressable>
              <Text
                style={{
                  marginTop: 15,
                  marginBottom: 15,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  fontWeight: 'bold',
                  borderBottomWidth: 1,
                  borderBottomColor: 'black',
                }}>
                {bahasa === 'id'
                  ? id.penempatan_deposito
                  : en.penempatan_deposito}
              </Text>
            </View>
            <ScrollView
              keyboardDismissMode="none"
              style={{width: '100%'}}
              automaticallyAdjustKeyboardInsets="true"
              automaticallyAdjustContentInsets="true">
              <View
                style={{
                  marginTop: 15,
                  marginStart: 25,
                  marginEnd: 25,
                  flexDirection: 'column',
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginBottom: 10,
                    marginBottom: 10,
                    marginTop: 10,
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      display: dataresp.length === 0 ? 'none' : 'flex',
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginStart: 10,
                    }}>
                    {bahasa === 'id' ? id.no_formulir : en.no_formulir}
                  </Text>
                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                    }}>
                    {arraynoForm[0]?.noForm}
                  </Text>
                </View>

                <View>
                  <DynamicForm
                    bahasa={bahasa}
                    accessToken={accessToken}
                    exchangeToken={exchangeToken}
                    formTemplate={{data: dataresp}}
                    onSubmit={this.onSubmit}
                    defaultValue={
                      arrayBarcode.length > 0 ? arrayBarcode[0] : {}
                    }
                    onPressed={this.onSubmitTransaction}
                    btnStyle={{
                      borderRadius: 12,
                      backgroundColor: COLORS.red,
                      width: 310,
                      display: 'none',
                      alignSelf: 'center',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                    btnLabel={bahasa === 'id' ? id.lanjut : en.lanjut}
                  />
                  <Text
                    style={{
                      display: dataresp.length === 0 ? 'none' : 'flex',
                      fontFamily: 'Helvetica',
                      fontSize: 16,
                      marginStart: 10,
                      zIndex: -10,
                      color: '#54575C',
                    }}>
                    {bahasa === 'id' ? id.bunga_didapat : en.bunga_didapat}
                  </Text>
                  <Text
                    style={{
                      display: dataresp.length === 0 ? 'none' : 'flex',
                      fontFamily: 'Helvetica',
                      fontSize: 17,
                      marginTop: 5,
                      marginStart: 10,
                      zIndex: -10,
                    }}>
                    {this.state.interesetDeposito + ' %'}
                  </Text>
                  <Text
                    style={{
                      display: dataresp.length === 0 ? 'none' : 'flex',
                      fontFamily: 'Helvetica',
                      fontSize: 16,
                      marginTop: 10,
                      marginStart: 10,
                      zIndex: -10,
                      color: '#54575C',
                    }}>
                    {bahasa === 'id' ? id.info_suku_bunga : en.info_suku_bunga}
                  </Text>
                  <View
                    style={{
                      zIndex: -10,
                      display: dataresp.length === 0 ? 'none' : 'flex',
                      margin: 10,
                      borderWidth: 2,
                      borderRadius: 12,
                      borderColor: '#D9D9D9',
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        width: '100%',
                        borderTopStartRadius: 12,
                        borderTopEndRadius: 12,
                        borderBottomWidth: 1,
                        backgroundColor: COLORS.red,
                        alignItems: 'center',
                        borderBottomColor: '#D9D9D9',
                      }}>
                      <Text
                        style={{
                          fontFamily: 'Helvetica',
                          fontSize: 12,
                          width: '30%',
                          color: 'white',
                          fontWeight: 'bold',
                          margin: 10,
                          textAlign: 'center',
                        }}>
                        Nominal Deposito
                      </Text>
                      <View
                        style={{
                          width: '70%',
                          flexDirection: 'column',
                        }}>
                        <Text
                          style={{
                            fontFamily: 'Helvetica',
                            fontSize: 12,
                            color: 'white',
                            fontWeight: 'bold',
                            margin: 10,
                            textAlign: 'center',
                          }}>
                          Jangka Waktu
                        </Text>
                        <View
                          style={{
                            flexDirection: 'row',
                          }}>
                          <Text
                            style={{
                              fontFamily: 'Helvetica',
                              fontSize: 12,
                              color: 'white',
                              fontWeight: 'bold',
                              margin: 10,
                              textAlign: 'center',
                            }}>
                            1 Bln
                          </Text>
                          <Text
                            style={{
                              fontFamily: 'Helvetica',
                              fontSize: 12,
                              color: 'white',
                              fontWeight: 'bold',
                              margin: 10,
                              textAlign: 'center',
                            }}>
                            3 Bln
                          </Text>
                          <Text
                            style={{
                              fontFamily: 'Helvetica',
                              fontSize: 12,
                              color: 'white',
                              fontWeight: 'bold',
                              margin: 10,
                              textAlign: 'center',
                            }}>
                            6 Bln
                          </Text>
                          <Text
                            style={{
                              fontFamily: 'Helvetica',
                              fontSize: 12,
                              color: 'white',
                              fontWeight: 'bold',
                              margin: 10,
                              textAlign: 'center',
                            }}>
                            12 Bln
                          </Text>
                        </View>
                      </View>
                    </View>

                    <View
                      style={{
                        width: '100%',
                        backgroundColor: '#FDE5D9',
                        flexDirection: 'row',
                        alignItems: 'center',
                      }}>
                      <Text
                        style={{
                          fontFamily: 'Helvetica',
                          fontSize: 12,
                          width: '30%',
                          margin: 10,
                          color: 'black',
                          textAlign: 'center',
                        }}>
                        {'8 Jt <= 200 Jt'}
                      </Text>
                      <View
                        style={{
                          width: '70%',
                          flexDirection: 'row',
                        }}>
                        <Text
                          style={{
                            fontFamily: 'Helvetica',
                            fontSize: 12,
                            color: 'black',
                            margin: 10,
                            textAlign: 'center',
                          }}>
                          3,00%
                        </Text>
                        <Text
                          style={{
                            fontFamily: 'Helvetica',
                            fontSize: 12,
                            color: 'black',
                            margin: 10,
                            textAlign: 'center',
                          }}>
                          3,00%
                        </Text>
                        <Text
                          style={{
                            fontFamily: 'Helvetica',
                            fontSize: 12,
                            color: 'black',
                            margin: 10,
                            textAlign: 'center',
                          }}>
                          3,00%
                        </Text>
                        <Text
                          style={{
                            fontFamily: 'Helvetica',
                            fontSize: 12,
                            color: 'black',
                            margin: 10,
                            textAlign: 'center',
                          }}>
                          3,25%
                        </Text>
                      </View>
                    </View>

                    <View
                      style={{
                        width: '100%',
                        backgroundColor: 'white',
                        flexDirection: 'row',
                        alignItems: 'center',
                      }}>
                      <Text
                        style={{
                          fontFamily: 'Helvetica',
                          fontSize: 12,
                          width: '30%',
                          margin: 10,
                          color: 'black',
                          textAlign: 'center',
                        }}>
                        {'200 Jt <= 500 Jt'}
                      </Text>
                      <View
                        style={{
                          width: '70%',
                          flexDirection: 'row',
                        }}>
                        <Text
                          style={{
                            fontFamily: 'Helvetica',
                            fontSize: 12,
                            color: 'black',
                            margin: 10,
                            textAlign: 'center',
                          }}>
                          3,00%
                        </Text>
                        <Text
                          style={{
                            fontFamily: 'Helvetica',
                            fontSize: 12,
                            color: 'black',
                            margin: 10,
                            textAlign: 'center',
                          }}>
                          3,00%
                        </Text>
                        <Text
                          style={{
                            fontFamily: 'Helvetica',
                            fontSize: 12,
                            color: 'black',
                            margin: 10,
                            textAlign: 'center',
                          }}>
                          3,00%
                        </Text>
                        <Text
                          style={{
                            fontFamily: 'Helvetica',
                            fontSize: 12,
                            color: 'black',
                            margin: 10,
                            textAlign: 'center',
                          }}>
                          3,25%
                        </Text>
                      </View>
                    </View>

                    <View
                      style={{
                        width: '100%',
                        backgroundColor: '#FDE5D9',
                        flexDirection: 'row',
                        alignItems: 'center',
                      }}>
                      <Text
                        style={{
                          fontFamily: 'Helvetica',
                          fontSize: 12,
                          width: '30%',
                          margin: 10,
                          color: 'black',
                          textAlign: 'center',
                        }}>
                        {'500 Jt <= 2 M'}
                      </Text>
                      <View
                        style={{
                          width: '70%',
                          flexDirection: 'row',
                        }}>
                        <Text
                          style={{
                            fontFamily: 'Helvetica',
                            fontSize: 12,
                            color: 'black',
                            margin: 10,
                            textAlign: 'center',
                          }}>
                          3,00%
                        </Text>
                        <Text
                          style={{
                            fontFamily: 'Helvetica',
                            fontSize: 12,
                            color: 'black',
                            margin: 10,
                            textAlign: 'center',
                          }}>
                          3,00%
                        </Text>
                        <Text
                          style={{
                            fontFamily: 'Helvetica',
                            fontSize: 12,
                            color: 'black',
                            margin: 10,
                            textAlign: 'center',
                          }}>
                          3,00%
                        </Text>
                        <Text
                          style={{
                            fontFamily: 'Helvetica',
                            fontSize: 12,
                            color: 'black',
                            margin: 10,
                            textAlign: 'center',
                          }}>
                          3,25%
                        </Text>
                      </View>
                    </View>

                    <View
                      style={{
                        width: '100%',
                        borderBottomStartRadius: 12,
                        borderBottomEndRadius: 12,
                        backgroundColor: 'white',
                        flexDirection: 'row',
                        alignItems: 'center',
                      }}>
                      <Text
                        style={{
                          fontFamily: 'Helvetica',
                          fontSize: 12,
                          width: '30%',
                          margin: 10,
                          color: 'black',
                          textAlign: 'center',
                        }}>
                        {'2 M <= 5 M'}
                      </Text>
                      <View
                        style={{
                          width: '70%',
                          flexDirection: 'row',
                        }}>
                        <Text
                          style={{
                            fontFamily: 'Helvetica',
                            fontSize: 12,
                            color: 'black',
                            margin: 10,
                            textAlign: 'center',
                          }}>
                          3,00%
                        </Text>
                        <Text
                          style={{
                            fontFamily: 'Helvetica',
                            fontSize: 12,
                            color: 'black',
                            margin: 10,
                            textAlign: 'center',
                          }}>
                          3,00%
                        </Text>
                        <Text
                          style={{
                            fontFamily: 'Helvetica',
                            fontSize: 12,
                            color: 'black',
                            margin: 10,
                            textAlign: 'center',
                          }}>
                          3,00%
                        </Text>
                        <Text
                          style={{
                            fontFamily: 'Helvetica',
                            fontSize: 12,
                            color: 'black',
                            margin: 10,
                            textAlign: 'center',
                          }}>
                          3,25%
                        </Text>
                      </View>
                    </View>
                  </View>

                  <Button
                    onPress={() => {
                      if (
                        parseInt(
                          arrayBarcode[0]?.nominaldeposito.replace(/\./g, ''),
                          10,
                        ) >=
                        parseInt(
                          arrayBarcode[0]?.rekeningsumberdana.availBalance,
                        ) /
                          100
                      ) {
                        Alert.alert(
                          bahasa === 'id'
                            ? id.saldo_tidak_mecukupi
                            : en.saldo_tidak_mecukupi,
                        );
                      } else {
                        this.setState({isConfirmTransaksi: true});
                        console.log('arrayBarcode', arrayBarcode);
                      }
                    }}
                    style={[
                      {
                        display: dataresp.length === 0 ? 'none' : 'flex',
                        width: 'auto',
                        borderRadius: 12,
                        marginEnd: 10,
                        marginBottom: 30,
                        borderColor: COLORS.red,
                        borderWidth: 2,
                        alignSelf: 'flex-end',
                      },
                    ]}
                    labelStyle={styles.btnLabel}
                    buttonColor={COLORS.red}
                    mode="contained">
                    {bahasa == 'id' ? id.lanjut : en.lanjut}
                  </Button>
                </View>
              </View>
            </ScrollView>
          </View>
        );
      }
    } else if (this.state.session == 20 || this.state.session == 21) {
      if (this.state.isConfirmTransaksi) {
        return (
          <View
            style={{
              width: '100%',
              alignItems: 'center',
              height: '60%',
              alignSelf: 'center',
              flexDirection: 'column',
            }}>
            <View
              style={{
                width: 420,
                backgroundColor: 'white',
                flexDirection: 'row',
              }}>
              <Pressable
                onPress={() => {
                  this.setState({
                    isConfirmTransaksi: false,
                  });
                }}
                style={{marginEnd: 10, alignSelf: 'center'}}>
                <FastImage
                  style={{
                    marginStart: 20,
                    width: 30,
                    height: 30,
                  }}
                  source={back}
                />
              </Pressable>
              <Text
                style={{
                  marginTop: 15,
                  marginBottom: 15,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  fontWeight: 'bold',
                  borderBottomWidth: 1,
                  borderBottomColor: 'black',
                }}>
                {this.state.session == 20
                  ? bahasa === 'id'
                    ? id.pencairan_deposito
                    : en.pencairan_deposito
                  : this.state.session == 21
                  ? bahasa === 'id'
                    ? id.perubahan_ARO_NON_ARO
                    : en.perubahan_ARO_NON_ARO
                  : '-'}
              </Text>
            </View>
            <ScrollView
              keyboardDismissMode="none"
              style={{width: '100%'}}
              automaticallyAdjustKeyboardInsets="true"
              automaticallyAdjustContentInsets="true">
              <View
                style={{
                  marginTop: 15,
                  marginStart: 25,
                  marginEnd: 25,
                  flexDirection: 'column',
                }}>
                <View
                  style={{
                    borderRadius: 15,
                    alignSelf: 'center',
                    alignItems: 'center',
                    justifyContent: 'center',
                    alignContent: 'center',
                    width: '100%',
                    marginTop: 15,
                    marginBottom: 25,
                    backgroundColor: 'white',
                    justifyContent: 'flex-start',
                    flexDirection: 'column',
                    elevation: 3, // Nilai ini mengontrol efek elevation (bayangan) pada card
                    shadowColor: '#000',
                    shadowOffset: {width: 0, height: 2},
                    shadowOpacity: 0.3,
                    shadowRadius: 2,
                  }}>
                  <View
                    style={{
                      width: '100%',
                      borderTopRightRadius: 15,
                      borderTopLeftRadius: 15,
                      padding: 14,
                      backgroundColor:
                        this.state.choosenDeposito % 2 === 0
                          ? '#099F6B'
                          : '#50A0FF',
                    }}>
                    <Text
                      ellipsizeMode="tail"
                      textAlign="center"
                      style={{
                        color: 'black',
                        fontSize: 17,
                        fontWeight: 'bold',
                        textAlign: 'left',
                        fontFamily: 'Helvetica',
                      }}>
                      {this.state.portofolioDeposito[this.state.choosenDeposito]
                        ?.noDeposit +
                        ' ' +
                        bahasa ===
                      'id'
                        ? this.state.portofolioDeposito[
                            this.state.choosenDeposito
                          ]?.intruksi.labelIdn
                        : this.state.portofolioDeposito[
                            this.state.choosenDeposito
                          ]?.intruksi.labelEng}
                    </Text>
                  </View>
                  <View
                    style={{
                      width: '100%',
                      flexDirection: 'row',
                      padding: 14,
                    }}>
                    <View
                      style={{
                        flexDirection: 'column',
                        marginEnd: 20,
                      }}>
                      <Text
                        style={{
                          fontSize: 17,
                          fontFamily: 'Helvetica',
                          color: 'black',
                        }}>
                        {bahasa === 'id' ? id.jangka_waktu : en.jangka_waktu}
                      </Text>
                      <Text
                        style={{
                          fontSize: 17,
                          fontFamily: 'Helvetica',
                          color: 'black',
                          fontWeight: 'bold',
                        }}>
                        {
                          this.state.portofolioDeposito[
                            this.state.choosenDeposito
                          ]?.depositType.months
                        }
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: 'column',
                      }}>
                      <Text
                        style={{
                          fontSize: 17,
                          fontFamily: 'Helvetica',
                          color: 'black',
                        }}>
                        {bahasa === 'id' ? id.jatuh_tempo : en.jatuh_tempo}
                      </Text>
                      <Text
                        style={{
                          fontSize: 17,
                          fontFamily: 'Helvetica',
                          color: 'black',
                          fontWeight: 'bold',
                        }}>
                        {moment(
                          this.state.portofolioDeposito[
                            this.state.choosenDeposito
                          ]?.dueDate,
                        )
                          .locale('id')
                          .format('D MMMM YYYY')}
                      </Text>
                    </View>
                  </View>
                  <Text
                    style={{
                      width: '100%',
                      padding: 14,
                      fontSize: 20,
                      fontFamily: 'Helvetica',
                      color: 'black',
                      textAlign: 'left',
                      fontWeight: 'bold',
                    }}>
                    {'Rp ' +
                      this.state.portofolioDeposito[this.state.choosenDeposito]
                        ?.nominal}
                  </Text>
                  <View
                    style={{
                      width: '93%',
                      borderWidth: 1,
                      borderStyle: 'dashed',
                      borderColor: '#DADADA',
                      marginBottom: 15,
                    }}
                  />
                </View>

                <View
                  style={{
                    flexDirection: 'column',
                  }}>
                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {bahasa == 'id' ? id.nomor_referensi : en.nomor_referensi}
                  </Text>
                  <Text
                    style={{
                      fontSize: 15,
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {
                      this.state.portofolioDeposito[this.state.choosenDeposito]
                        ?.referenceNumber
                    }
                  </Text>

                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {bahasa == 'id' ? id.nama_lengkap : en.nama_lengkap}
                  </Text>
                  <Text
                    style={{
                      fontSize: 15,
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {
                      this.state.portofolioDeposito[this.state.choosenDeposito]
                        ?.nasabah
                    }
                  </Text>

                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {bahasa == 'id' ? id.nomor_rekening : en.nomor_rekening}
                  </Text>
                  <Text
                    style={{
                      fontSize: 15,
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {
                      this.state.portofolioDeposito[this.state.choosenDeposito]
                        ?.noRek
                    }
                  </Text>

                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {bahasa == 'id' ? id.tipe_rekening : en.tipe_rekening}
                  </Text>
                  <Text
                    style={{
                      fontSize: 15,
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    Tabungan Victori
                  </Text>

                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {bahasa == 'id' ? id.jenis_deposito : en.jenis_deposito}
                  </Text>
                  <Text
                    style={{
                      fontSize: 15,
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {
                      this.state.portofolioDeposito[this.state.choosenDeposito]
                        ?.depositType.currency
                    }
                  </Text>

                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {bahasa == 'id' ? id.nominal_deposito : en.nominal_deposito}
                  </Text>
                  <Text
                    style={{
                      fontSize: 15,
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {'Rp ' +
                      this.state.portofolioDeposito[this.state.choosenDeposito]
                        ?.nominal}
                  </Text>

                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {bahasa == 'id' ? id.jangka_waktu : en.jangka_waktu}
                  </Text>
                  <Text
                    style={{
                      fontSize: 15,
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {
                      this.state.portofolioDeposito[this.state.choosenDeposito]
                        ?.depositType.months
                    }
                  </Text>

                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {bahasa == 'id' ? id.intruksi : en.intruksi}
                  </Text>
                  <Text
                    style={{
                      display: this.state.session === 20 ? 'flex' : 'none',
                      fontSize: 15,
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {bahasa === 'id'
                      ? this.state.portofolioDeposito[
                          this.state.choosenDeposito
                        ]?.intruksi.labelIdn
                      : this.state.portofolioDeposito[
                          this.state.choosenDeposito
                        ]?.intruksi.labelEng}
                  </Text>
                  <DropDownPicker
                    zIndex={1000000}
                    setItems={this.state.arrayInstruksi}
                    items={this.state.arrayInstruksi}
                    onSelectItem={value => {
                      this.setState({intruksi: value.value});
                    }}
                    style={{
                      width: '100%',
                      display: this.state.session === 20 ? 'none' : 'flex',
                      borderColor: '#DADADA',
                      borderWidth: 1.5,
                      marginBottom: 10,
                    }}
                    labelStyle={{
                      fontFamily: 'Helvetica',
                      fontSize: 16,
                    }}
                    placeholderStyle={{
                      fontFamily: 'Helvetica',
                      fontSize: 16,
                    }}
                    dropDownContainerStyle={{
                      width: '100%',
                    }}
                    listItemContainerStyle={{
                      borderColor: '#000',
                      borderBottomWidth: 1,
                    }}
                    listItemLabelStyle={{
                      fontFamily: 'Helvetica',
                      fontSize: 16,
                    }}
                    placeholder={
                      this.state.intruksi !== ''
                        ? this.state.intruksi
                        : bahasa === 'id'
                        ? this.state.portofolioDeposito[
                            this.state.choosenDeposito
                          ]?.intruksi.labelIdn
                        : this.state.portofolioDeposito[
                            this.state.choosenDeposito
                          ]?.intruksi.labelEng
                    }
                    dropDownDirection="BOTTOM"
                    open={this.state.open}
                    setOpen={set => {
                      this.setState({open: set});
                    }}
                  />

                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {bahasa == 'id' ? id.bunga_didapat : en.bunga_didapat}
                  </Text>
                  <Text
                    style={{
                      fontSize: 15,
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {this.state.portofolioDeposito[this.state.choosenDeposito]
                      ?.ratePercent + '%'}
                  </Text>

                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {bahasa == 'id'
                      ? id.tanggal_transaksi
                      : en.tanggal_transaksi}
                  </Text>
                  <Text
                    style={{
                      fontSize: 15,
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {moment().format('DD-MM-YYYY HH:mm')}
                  </Text>

                  <Text
                    style={{
                      fontSize: 15,
                      fontWeight: 'bold',
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {bahasa == 'id'
                      ? id.tanggal_pencairan
                      : en.tanggal_pencairan}
                  </Text>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({isDatePickerVisible: true});
                    }}
                    style={{
                      display: this.state.session === 20 ? 'flex' : 'none',
                      justifyContent: 'center',
                      borderRadius: 10,
                      backgroundColor: 'white',
                      borderColor: COLORS.border_textInput,
                      borderWidth: 1.5,
                      marginBottom: 10,
                      width: '100%',
                      height: 50,
                      padding: 10,
                    }}>
                    <View style={{flexDirection: 'row'}}>
                      <Text
                        style={[
                          ,
                          {
                            alignSelf: 'flex-start',
                            textAlign: 'left',
                            fontFamily: 'Helvetica',
                            fontSize: 17,
                          },
                        ]}>
                        {this.state.dateShcedule === ''
                          ? moment().format('DD-MM-YYYY')
                          : this.state.dateShcedule}
                      </Text>
                    </View>
                  </TouchableOpacity>
                  <DateTimePickerModal
                    display="inline"
                    date={new Date()}
                    locale="id_ID"
                    minimumDate={new Date()}
                    accentColor={COLORS.red}
                    buttonTextColorIOS={COLORS.red}
                    cancelTextIOS={bahasa === 'id' ? id.batal : en.batal}
                    confirmTextIOS={bahasa === 'id' ? id.pilih : en.pilih}
                    isVisible={this.state.isDatePickerVisible}
                    mode="date"
                    onConfirm={date => {
                      const NewDate = moment(date).format('DD-MM-YYYY');
                      this.setState({dateShcedule: NewDate});
                      this.setState({isDatePickerVisible: false});
                    }}
                    onCancel={() => {
                      this.setState({isDatePickerVisible: false});
                    }}
                  />
                  <Text
                    style={{
                      display: this.state.session === 20 ? 'none' : 'flex',
                      fontSize: 15,
                      marginEnd: 10,
                      marginBottom: 10,
                    }}>
                    {moment().format('DD-MM-YYYY')}
                  </Text>
                </View>

                <Button
                  onPress={() => {
                    this.setState({
                      isConfirmTransaksi: false,
                      choosenDeposito: 0,
                      session: 9,
                      sessionBeforeOTP: this.state.session,
                    });
                    //this.sendOtp();
                  }}
                  style={[
                    {
                      marginBottom: 10,
                      width: '100%',
                      borderRadius: 12,
                      borderColor: COLORS.red,
                      borderWidth: 2,
                      alignSelf: 'center',
                    },
                  ]}
                  labelStyle={styles.btnLabel}
                  buttonColor={COLORS.red}
                  mode="contained">
                  {this.state.session === 20
                    ? bahasa == 'id'
                      ? id.cairkan
                      : en.cairkan
                    : bahasa == 'id'
                    ? id.simpan
                    : en.simpan}
                </Button>
              </View>
            </ScrollView>
          </View>
        );
      } else {
        return (
          <View
            style={{
              width: '100%',
              alignItems: 'center',
              height: '60%',
              alignSelf: 'center',
              flexDirection: 'column',
            }}>
            <View
              style={{
                width: 420,
                backgroundColor: 'white',
                flexDirection: 'row',
              }}>
              <Pressable
                onPress={() => {
                  this.setState({
                    session: 18,
                    numColumns: 3,
                  });
                }}
                style={{marginEnd: 10, alignSelf: 'center'}}>
                <FastImage
                  style={{
                    marginStart: 20,
                    width: 30,
                    height: 30,
                  }}
                  source={back}
                />
              </Pressable>
              <Text
                style={{
                  marginTop: 15,
                  marginBottom: 15,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  fontWeight: 'bold',
                  borderBottomWidth: 1,
                  borderBottomColor: 'black',
                }}>
                {this.state.session == 20
                  ? bahasa === 'id'
                    ? id.pencairan_deposito
                    : en.pencairan_deposito
                  : this.state.session == 21
                  ? bahasa === 'id'
                    ? id.perubahan_ARO_NON_ARO
                    : en.perubahan_ARO_NON_ARO
                  : '-'}
              </Text>
            </View>
            <FlatList
              key={'list_porto_deposito'}
              style={{
                display:
                  this.state.portofolioDeposito.length === 0 ? 'none' : 'flex',
                width: '100%',
              }}
              data={this.state.portofolioDeposito}
              renderItem={({item, index}) => (
                <TouchableOpacity
                  key={index}
                  onPress={() => {
                    this.setState({
                      isConfirmTransaksi: true,
                      choosenDeposito: index,
                    });
                  }}
                  style={{
                    borderRadius: 15,
                    alignSelf: 'center',
                    alignItems: 'center',
                    justifyContent: 'center',
                    alignContent: 'center',
                    width: '90%',
                    marginTop: 10,
                    marginBottom: 5,
                    backgroundColor: 'white',
                    justifyContent: 'flex-start',
                    flexDirection: 'column',
                    elevation: 3, // Nilai ini mengontrol efek elevation (bayangan) pada card
                    shadowColor: '#000',
                    shadowOffset: {width: 0, height: 2},
                    shadowOpacity: 0.3,
                    shadowRadius: 2,
                  }}>
                  <View
                    style={{
                      width: '100%',
                      borderTopRightRadius: 15,
                      borderTopLeftRadius: 15,
                      padding: 14,
                      backgroundColor: index % 2 === 0 ? '#099F6B' : '#50A0FF',
                    }}>
                    <Text
                      ellipsizeMode="tail"
                      textAlign="center"
                      style={{
                        color: 'black',
                        fontSize: 17,
                        fontWeight: 'bold',
                        textAlign: 'left',
                        fontFamily: 'Helvetica',
                      }}>
                      {item.noDeposit + ' ' + bahasa === 'id'
                        ? item.intruksi.labelIdn
                        : item.intruksi.labelEng}
                    </Text>
                  </View>
                  <View
                    style={{
                      width: '100%',
                      flexDirection: 'row',
                      padding: 14,
                    }}>
                    <View
                      style={{
                        flexDirection: 'column',
                        marginEnd: 20,
                      }}>
                      <Text
                        style={{
                          fontSize: 17,
                          fontFamily: 'Helvetica',
                          color: 'black',
                        }}>
                        {bahasa === 'id' ? id.jangka_waktu : en.jangka_waktu}
                      </Text>
                      <Text
                        style={{
                          fontSize: 17,
                          fontFamily: 'Helvetica',
                          color: 'black',
                          fontWeight: 'bold',
                        }}>
                        {item.depositType?.months}
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: 'column',
                      }}>
                      <Text
                        style={{
                          fontSize: 17,
                          fontFamily: 'Helvetica',
                          color: 'black',
                        }}>
                        {bahasa === 'id' ? id.jatuh_tempo : en.jatuh_tempo}
                      </Text>
                      <Text
                        style={{
                          fontSize: 17,
                          fontFamily: 'Helvetica',
                          color: 'black',
                          fontWeight: 'bold',
                        }}>
                        {moment(item.dueDate)
                          .locale('id')
                          .format('D MMMM YYYY')}
                      </Text>
                    </View>
                  </View>
                  <Text
                    style={{
                      width: '100%',
                      padding: 14,
                      fontSize: 20,
                      fontFamily: 'Helvetica',
                      color: 'black',
                      textAlign: 'left',
                      fontWeight: 'bold',
                    }}>
                    {'Rp ' + item.nominal}
                  </Text>
                  <View
                    style={{
                      width: '93%',
                      borderWidth: 1,
                      borderStyle: 'dashed',
                      borderColor: '#DADADA',
                      marginBottom: 15,
                    }}
                  />
                </TouchableOpacity>
              )}
              horizontal={false}
              numColumns={1}
              keyExtractor={item => item.id}
            />
            <View
              style={{
                display:
                  this.state.portofolioDeposito.length === 0 ? 'flex' : 'none',
                width: '100%',
                height: '90%',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontSize: 16,
                  textAlign: 'center',
                  fontWeight: 'bold',
                }}>
                {bahasa === 'id' ? id.deposito_kosong : en.deposito_kosong}
              </Text>
            </View>
          </View>
        );
      }
    } else if (this.state.session == 23) {
      var resiForm = [];
      for (let i = 0; i < arrayTransaksiMultiForm.length; i++) {
        resiForm.push(
          <View
            key={'form ke' + i}
            style={{
              marginBottom: 20,
              borderRadius: 10,
              flexDirection: 'row',
              width: '100%',
              height: 100,
            }}>
            <FastImage
              source={{
                uri: encodeURI(
                  `data:image/png;base64,${arrayTransaksiMultiForm[i].data.image}`,
                ),
                priority: FastImage.priority.high,
              }}
              resizeMode="contain"
              style={{width: '50%', height: 'auto'}}
            />
            <View
              style={{
                width: '50%',
                flexDirection: 'column',
                height: 'auto',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  marginBottom: 5,
                  marginStart: 15,
                  fontFamily: 'Helvetica',
                  fontSize: 15,
                  textAlign: 'center',
                  alignItems: 'center',
                  textAlign: 'justify',
                  justifyContent: 'center',
                }}>
                {bahasa === 'id'
                  ? id.transaksi + ' ' + (i + 1)
                  : en.transaksi + ' ' + (i + 1)}
              </Text>
              <Button
                onPress={() => {
                  this.downloadResi(
                    arrayTransaksiMultiForm[i].data.pdf,
                    arrayTransaksiMultiForm[i].data.idForm +
                      ' ' +
                      arrayTransaksiMultiForm[i].data.transactionType,
                  );
                }}
                icon={() => (
                  <Image
                    transition={false}
                    source={ic_download}
                    style={{
                      tintColor: COLORS.red,
                      justifyContent: 'center',
                      alignItems: 'center',
                      alignContent: 'center',
                      alignSelf: 'center',
                      width: 20,
                      height: 20,
                      resizeMode: 'contain',
                    }}
                  />
                )}
                style={[
                  styles.btn,
                  {
                    width: 'auto',
                    alignSelf: 'center',
                  },
                ]}
                textColor={COLORS.red}
                labelStyle={styles.btnLabel}
                buttonColor={COLORS.white}
                mode="contained">
                {bahasa === 'id' ? id.unduh_resi : en.unduh_resi}
              </Button>
            </View>
          </View>,
        );
      }
      return (
        <View
          style={{
            width: '100%',
            height: '65%',
            alignSelf: 'center',
            flexDirection: 'column',
          }}>
          <View
            style={{
              padding: 14,
              justifyContent: 'center',
              backgroundColor: 'white',
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                alignSelf: 'center',
                fontFamily: 'Helvetica',
                fontSize: 18,
                color: COLORS.textColorGray,
                textAlign: 'center',
                fontWeight: 'bold',
                borderBottomWidth: 1,
                borderBottomColor: 'black',
              }}>
              {bahasa === 'id'
                ? id.title_resi_transaksi
                : en.title_resi_transaksi}
            </Text>
          </View>
          <ScrollView
            automaticallyAdjustKeyboardInsets="true"
            keyboardDismissMode="interactive"
            style={{height: '100%'}}>
            <View
              style={{
                height: '100%',
                padding: 30,
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              {resiForm}
              <View
                style={{
                  width: '100%',
                  flexDirection: 'column',
                }}>
                <Text
                  numberOfLines={5}
                  style={{
                    marginBottom: 20,
                    fontFamily: 'Helvetica',
                    fontSize: 15,
                    textAlign: 'center',
                    alignItems: 'center',
                    textAlign: 'justify',
                    justifyContent: 'center',
                  }}>
                  {bahasa === 'id' ? id.headline_resi : en.headline_resi}
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                  }}>
                  <Button
                    style={[
                      styles.btn,
                      {
                        width: 'auto',
                        alignSelf: 'center',
                      },
                    ]}
                    textColor={COLORS.red}
                    labelStyle={styles.btnLabel}
                    buttonColor={COLORS.white}
                    mode="contained">
                    {arrayTransaksiMultiForm.length == 1
                      ? bahasa === 'id'
                        ? id.title_download_redaksi
                        : en.title_download_redaksi
                      : bahasa === 'id'
                      ? id.title_download_redaksi2
                      : en.title_download_redaksi2}
                  </Button>
                  <Button
                    onPress={() => {
                      arrayUploadBarcode = [];
                      arrayBarcode = [];
                      arraynoForm = [];
                      arrayTransaksiMultiForm = [];
                      arrayInquiry = [];
                      this.getNoRekening();
                      this.setState({session: 11});
                    }}
                    style={[
                      styles.btn,
                      {
                        width: 'auto',
                        alignSelf: 'center',
                      },
                    ]}
                    labelStyle={styles.btnLabel}
                    buttonColor={COLORS.red}
                    mode="contained">
                    {bahasa === 'id' ? id.selesai : en.selesai}
                  </Button>
                </View>
              </View>
            </View>
          </ScrollView>
        </View>
      );
    } else if (
      this.state.session == 24 ||
      this.state.session == 25 ||
      this.state.session == 26
    ) {
      return (
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            width: '100%',
            height: '65%',
            alignSelf: 'center',
            flexDirection: 'column',
          }}>
          <View
            style={{
              width: 420,
              backgroundColor: 'white',
              flexDirection: 'row',
              borderBottomWidth: this.state.isUpdateSwafoto ? 1 : 0,
              borderBottomColor: this.state.isUpdateSwafoto
                ? COLORS.border_textInput
                : 'white',
            }}>
            <Pressable
              onPress={() => {
                if (this.state.isUpdateSwafoto) {
                  this.setState({isUpdateSwafoto: false});
                } else {
                  this.setState({session: 12});
                }
              }}
              style={{marginEnd: 10, alignSelf: 'center'}}>
              <FastImage
                style={{
                  marginStart: 20,
                  width: 30,
                  height: 30,
                }}
                source={back}
              />
            </Pressable>
            <Text
              style={{
                marginTop: 15,
                marginBottom: 15,
                fontFamily: 'Helvetica',
                fontSize: 18,
                fontWeight: 'bold',
                borderBottomWidth: 1,
                borderBottomColor: 'black',
              }}>
              {this.state.isUpdateSwafoto
                ? bahasa === 'id'
                  ? id.title_foto_diri_dengan_ktp
                  : en.title_foto_diri_dengan_ktp
                : bahasa === 'id'
                ? id.perbarui_data
                : en.perbarui_data}
            </Text>
          </View>
          <View
            style={{
              padding: 10,
              flexDirection: 'row',
              backgroundColor: COLORS.bgHeadUpload,
              height: '12%',
              width: '100%',
              display:
                dataresp.length === 0 || this.state.isUpdateSwafoto
                  ? 'none'
                  : 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              alignSelf: 'center',
            }}>
            <View
              style={{
                width: '33%',
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: 12,
                  height: 12,
                  marginEnd: 5,
                  borderRadius: 12 / 2,
                  backgroundColor:
                    this.state.session == 24
                      ? COLORS.mainDot
                      : COLORS.sucessDot,
                }}
              />
              <Text style={{fontSize: 12, width: '70%'}} numberOfLines={3}>
                {bahasa == 'id'
                  ? id.informasi_data_utama
                  : en.informasi_data_utama}
              </Text>
            </View>

            <View
              style={{
                width: '33%',
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: 12,
                  height: 12,
                  marginEnd: 5,
                  borderRadius: 12 / 2,
                  backgroundColor:
                    this.state.session > 24
                      ? this.state.session == 25
                        ? COLORS.mainDot
                        : COLORS.sucessDot
                      : COLORS.offDot,
                }}
              />
              <Text
                style={{fontSize: 12}}
                ellipsizeMode="tail"
                numberOfLines={3}>
                {bahasa == 'id' ? id.data_pekerjaan : en.data_pekerjaan}
              </Text>
            </View>

            <View
              style={{
                width: '33%',
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <View
                style={{
                  marginStart: 5,
                  width: 12,
                  height: 12,
                  marginEnd: 5,
                  borderRadius: 12 / 2,
                  backgroundColor:
                    this.state.session > 25
                      ? this.state.session == 26
                        ? COLORS.mainDot
                        : COLORS.sucessDot
                      : COLORS.offDot,
                }}
              />
              <Text style={{fontSize: 12}} numberOfLines={3}>
                {bahasa == 'id' ? id.data_keuangan : en.data_keuangan}
              </Text>
            </View>
          </View>
          <ScrollView
            keyboardDismissMode="none"
            style={{
              width: '100%',
            }}
            automaticallyAdjustKeyboardInsets="true"
            automaticallyAdjustContentInsets="true"
            refreshControl={
              <RefreshControl
                refreshing={this.state.setRefreshing}
                onRefresh={this.onRefresh}
              />
            }>
            <View
              style={{
                display: this.state.isUpdateSwafoto ? 'none' : 'flex',
                marginTop: 20,
                marginStart: 20,
                marginEnd: 20,
                borderRadius: 15,
                flexDirection: 'column',
              }}>
              <View
                style={{
                  display: dataresp.length === 0 ? 'none' : 'flex',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <Button
                  onPress={() => {
                    if (this.state.session == 25) {
                      this.setState({formCode: '59', session: 24});
                      this.formBuilder('59');
                    } else if (this.state.session == 26) {
                      this.setState({formCode: '5', session: 25});
                      this.formBuilder('5');
                    }
                  }}
                  style={[
                    styles.btn,
                    {
                      width: 'auto',
                      margin: 10,
                      display:
                        dataresp.length === 0 || this.state.session == 24
                          ? 'none'
                          : 'flex',
                    },
                  ]}
                  labelStyle={styles.btnLabel}
                  buttonColor={'white'}
                  textColor={COLORS.red}
                  mode="contained">
                  {this.state.session !== 24
                    ? bahasa === 'id'
                      ? '< ' + id.sebelumnya
                      : '< ' + en.sebelumnya
                    : ''}
                </Button>
                <Button
                  onPress={() => {
                    if (this.state.session == 24) {
                      this.setState({formCode: '5', session: 25});
                      this.formBuilder('5');
                    } else if (this.state.session == 25) {
                      this.setState({formCode: '14', session: 26});
                      this.formBuilder('14');
                    }
                  }}
                  style={[
                    styles.btn,
                    {
                      width: 'auto',
                      margin: 10,
                      display:
                        dataresp.length === 0 || this.state.session == 26
                          ? 'none'
                          : 'flex',
                    },
                  ]}
                  labelStyle={styles.btnLabel}
                  buttonColor={'white'}
                  textColor={COLORS.red}
                  mode="contained">
                  {bahasa === 'id' ? id.lewati + ' >' : en.lewati + ' >'}
                </Button>
              </View>
              <Button
                onPress={() => {
                  this.setState({openCamera: true, isUpdateSwafoto: true});
                }}
                style={[
                  styles.btn,
                  {
                    margin: 10,
                    display:
                      dataresp.length === 0 || this.state.session !== 24
                        ? 'none'
                        : 'flex',
                  },
                ]}
                labelStyle={styles.btnLabel}
                buttonColor={COLORS.red}
                mode="contained">
                {bahasa == 'id'
                  ? id.title_foto_diri_dengan_ktp
                  : id.title_foto_diri_dengan_ktp}
              </Button>
              <Text
                style={{
                  display:
                    dataresp.length === 0 || this.state.session !== 24
                      ? 'none'
                      : 'flex',
                  marginStart: 10,
                  marginEnd: 10,
                  marginBottom: 15,
                }}>
                {bahasa === 'id'
                  ? '*' +
                    id.title_foto_diri_dengan_ktp +
                    ' untuk memperbaharui data'
                  : '*' + en.title_foto_diri_dengan_ktp + ' for update data'}
              </Text>
              <DynamicForm
                bahasa={bahasa}
                defaultValue={{}}
                accessToken={accessToken}
                exchangeToken={exchangeToken}
                formTemplate={{data: dataresp}}
                onSubmit={() => {}}
                onPressed={() => {}}
                btnStyle={{
                  display: 'none',
                }}
              />
              <Button
                onPress={() => {
                  if (this.state.session == 24) {
                    this.setState({formCode: '5', session: 25});
                    this.formBuilder('5');
                  } else if (this.state.session == 25) {
                    this.setState({formCode: '14', session: 26});
                    this.formBuilder('14');
                  }
                }}
                style={[
                  {
                    display: dataresp.length === 0 ? 'none' : 'flex',
                    width: 'auto',
                    borderRadius: 12,
                    marginEnd: 10,
                    marginBottom: 30,
                    borderColor: COLORS.red,
                    borderWidth: 2,
                    alignSelf: 'flex-end',
                  },
                ]}
                labelStyle={styles.btnLabel}
                buttonColor={COLORS.red}
                mode="contained">
                {bahasa == 'id' ? id.lanjut : en.lanjut}
              </Button>
            </View>
            <View
              style={{
                display: this.state.isUpdateSwafoto ? 'flex' : 'none',
                alignItems: 'center',
                width: '100%',
                height: '100%',
              }}>
              <Text
                style={{
                  fontSize: 16,
                  margin: 20,
                }}>
                {bahasa == 'id'
                  ? id.headline_foto_diri_dengan_ktp
                  : en.headline_foto_diri_dengan_ktp}
              </Text>
              <FastImage
                transition={false}
                resizeMode="stretch"
                style={{
                  justifyContent: 'center',
                  width: 360,
                  height: 300,
                }}
                source={bgPickImage}>
                <View
                  style={{
                    padding: 20,
                    width: '100%',
                    height: '100%',
                  }}>
                  <FastImage
                    resizeMode="contain"
                    style={{width: '100%', height: '100%'}}
                    source={{
                      uri: fileUriSwafoto,
                      priority: FastImage.priority.normal,
                    }}
                  />
                </View>
              </FastImage>
              <View
                style={{
                  display: this.state.session == 3 ? 'none' : 'flex',
                  justifyContent: 'space-between',
                  marginTop: 10,
                  width: 360,
                  flexDirection: 'row',
                }}>
                <Pressable
                  onPress={() => {
                    fileUriSwafoto = '';
                    base64Swafoto = '';
                    this.setState({openCamera: true, isUpdateSwafoto: true});
                  }}>
                  <FastImage
                    transition={false}
                    style={{
                      width: 50,
                      height: 50,
                    }}
                    source={changeImage}
                  />
                </Pressable>
                <Button
                  style={{
                    justifyContent: 'center',
                    height: 50,
                    borderRadius: 7,
                    borderColor: COLORS.red,
                    borderWidth: 2,
                  }}
                  labelStyle={styles.btnLabel}
                  buttonColor={COLORS.red}
                  mode="contained"
                  onPress={() => {
                    this.processOCR2();
                  }}>
                  {bahasa === 'id' ? id.lanjut : en.lanjut}
                </Button>
              </View>
            </View>
          </ScrollView>
        </View>
      );
    } else if (
      this.state.session == 28 ||
      this.state.session == 29 ||
      this.state.session == 30 ||
      this.state.session == 31
    ) {
      return (
        <View
          style={{
            width: '100%',
            alignItems: 'center',
            height: '65%',
            alignSelf: 'center',
            flexDirection: 'column',
          }}>
          <View
            style={{
              width: 420,
              backgroundColor: 'white',
              flexDirection: 'row',
            }}>
            <Pressable
              onPress={() => {
                if (this.state.session == 28 || this.state.session == 31) {
                  this.setState({dataInquiryCIFbyNik: {}});
                  arrayBarcode = [];
                  arraynoForm = [];
                  this.setState({
                    session: 27,
                  });
                } else if (this.state.session == 29) {
                  this.setState({session: 28});
                } else if (this.state.session == 30) {
                  if (this.state.currentQuestion == 1) {
                    this.setState({
                      session: 29,
                      currentQuestion: 0,
                      radioChoosen: {},
                      arrayJawabanProfilResiko: [],
                      arrayPertanyaanProfilResiko: [],
                    });
                    arrayJawaban = [];
                    arrayJawaban2 = [];
                  } else {
                    this.setState({
                      currentQuestion: this.state.currentQuestion - 1,
                      radioChoosen:
                        arrayJawaban2[this.state.currentQuestion - 2],
                    });
                    this.getJawabanProfilResiko(
                      this.state.arrayPertanyaanProfilResiko[
                        this.state.currentQuestion - 2
                      ]?.id,
                    );
                  }
                }
              }}
              style={{marginEnd: 10, alignSelf: 'center'}}>
              <FastImage
                style={{
                  marginStart: 20,
                  width: 30,
                  height: 30,
                }}
                source={back}
              />
            </Pressable>
            <Text
              style={{
                marginTop: 15,
                marginBottom: 15,
                fontFamily: 'Helvetica',
                fontSize: 18,
                fontWeight: 'bold',
                borderBottomWidth: 1,
                borderBottomColor: 'black',
              }}>
              {this.state.session == 28
                ? bahasa === 'id'
                  ? id.reksa_dana
                  : en.reksa_dana
                : bahasa === 'id'
                ? id.profilResiko
                : en.profilResiko}
            </Text>
          </View>
          <ScrollView
            keyboardDismissMode="none"
            style={{
              width: '100%',
              display:
                dataresp.length === 0 ||
                this.state.session == 29 ||
                this.state.session == 30 ||
                this.state.session == 31
                  ? 'none'
                  : 'flex',
            }}
            contentContainerStyle={{
              paddingBottom: '20%',
            }}
            automaticallyAdjustKeyboardInsets="true"
            automaticallyAdjustContentInsets="true">
            <View
              style={{
                marginTop: 10,
                marginStart: 25,
                marginEnd: 25,
                flexDirection: 'column',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginBottom: 10,
                  marginBottom: 10,
                  marginTop: 10,
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    display: dataresp.length === 0 ? 'none' : 'flex',
                    fontSize: 15,
                    fontWeight: 'bold',
                    marginStart: 10,
                  }}>
                  {bahasa === 'id' ? id.no_formulir : en.no_formulir}
                </Text>
                <Text
                  style={{
                    fontSize: 15,
                    fontWeight: 'bold',
                    marginEnd: 10,
                  }}>
                  {arraynoForm[0]?.noForm}
                </Text>
              </View>

              <View>
                <DynamicForm
                  bahasa={bahasa}
                  accessToken={accessToken}
                  exchangeToken={exchangeToken}
                  formTemplate={{data: dataresp}}
                  onSubmit={this.onSubmit}
                  onProfilResikoPressed={() => {
                    this.setState({session: 29});
                    this.getTNC(23);
                  }}
                  statusProfilResiko={this.state.isDoneProfilResiko}
                  defaultValue={arrayBarcode.length > 0 ? arrayBarcode[0] : {}}
                  onPressed={this.onSubmitTransaction}
                  btnStyle={{
                    display: 'none',
                  }}
                  btnLabel={bahasa === 'id' ? id.lanjut : en.lanjut}
                />
                <View
                  style={{
                    marginStart: 10,
                    marginEnd: 10,
                    zIndex: -10,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <Text
                    style={{
                      fontSize: 16,
                      color: '#54575C',
                      fontFamily: 'Helvetica',
                    }}>
                    {bahasa == 'id' ? id.permintaanSID : en.permintaanSID}
                  </Text>
                  <Text
                    style={{
                      fontSize: 16,
                      color: '#54575C',
                      fontFamily: 'Helvetica',
                      fontWeight: 'bold',
                    }}>
                    {bahasa === 'id' ? id.reksa_dana : en.reksa_dana}
                  </Text>
                </View>
                <View
                  style={{
                    zIndex: -10,
                    marginTop: 30,
                    marginBottom: 30,
                  }}>
                  <Button
                    onPress={() => {
                      if (this.state.isDoneProfilResiko) {
                        this.getTNC(22);
                        this.setState({modalTNC: true});
                      }
                    }}
                    style={[
                      {
                        display: dataresp.length === 0 ? 'none' : 'flex',
                        width: '100%',
                        borderRadius: 12,
                        borderColor: this.state.isDoneProfilResiko
                          ? COLORS.red
                          : COLORS.border_textInput,
                        borderWidth: 2,
                      },
                    ]}
                    labelStyle={styles.btnLabel}
                    buttonColor={
                      this.state.isDoneProfilResiko
                        ? COLORS.red
                        : COLORS.border_textInput
                    }
                    mode="contained">
                    {bahasa == 'id' ? id.prosesSID : en.prosesSID}
                  </Button>
                </View>
              </View>
            </View>
          </ScrollView>
          <View
            style={{
              width: '100%',
              height: '90%',
              padding: 20,
              flexDirection: 'column',
              justifyContent: 'space-between',
              display:
                this.state.session == 29 || this.state.session == 30
                  ? 'flex'
                  : 'none',
            }}>
            <View
              style={{
                display: this.state.session == 29 ? 'flex' : 'none',
              }}>
              <RenderHtml
                contentWidth={300}
                source={{html: this.state.htmlTNC}}
              />
            </View>
            <View
              style={{
                display: this.state.session == 30 ? 'flex' : 'none',
                width: '100%',
                flexDirection: 'column',
              }}>
              <Text
                style={{
                  fontSize: 16,
                  marginBottom: 10,
                  fontFamily: 'Helvetica',
                }}>
                {'Pertanyaan ' +
                  this.state.currentQuestion +
                  ' dari ' +
                  this.state.arrayPertanyaanProfilResiko?.length}
              </Text>
              <View
                style={{
                  height: 10,
                  backgroundColor: COLORS.border_textInput,
                  marginBottom: 10,
                  borderRadius: 20,
                  flexDirection: 'row',
                }}>
                <View
                  style={{
                    width:
                      (
                        (this.state.currentQuestion /
                          this.state.arrayPertanyaanProfilResiko.length) *
                        100
                      ).toString() + '%',
                    borderRadius: 20,
                    backgroundColor: COLORS.red,
                  }}
                />
              </View>
              <Text
                style={{
                  fontSize: 16,
                  marginBottom: 20,
                  fontFamily: 'Helvetica',
                }}>
                {
                  this.state.arrayPertanyaanProfilResiko[
                    this.state.currentQuestion - 1
                  ]?.question
                }
              </Text>
              {this.state.arrayJawabanProfilResiko?.map((item, index) => (
                <View
                  key={index}
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <RadioButton.Android
                    color="#E34243"
                    status={
                      this.state.radioChoosen?.answer === item?.answer
                        ? 'checked'
                        : 'unchecked'
                    }
                    uncheckedColor="black"
                    onPress={() => {
                      this.setState({radioChoosen: item});
                      console.log('radio', item?.answer);
                    }}
                  />
                  <Text
                    numberOfLines={20}
                    ellipsizeMode="tail"
                    style={{
                      width: '90%',
                      fontSize: 16,
                      textAlign: 'justify',
                      fontFamily: 'Helvetica',
                    }}>
                    {item?.answer}
                  </Text>
                </View>
              ))}
            </View>
            <Button
              onPress={() => {
                if (this.state.session == 29) {
                  this.setState({session: 30, currentQuestion: 1});
                  this.getJawabanProfilResiko(
                    this.state.arrayPertanyaanProfilResiko[0]?.id,
                  );
                } else if (this.state.session == 30) {
                  if (
                    this.state.currentQuestion ===
                    this.state.arrayPertanyaanProfilResiko.length
                  ) {
                    this.saveQuestion(
                      this.state.currentQuestion,
                      this.state.radioChoosen,
                      this.state.currentQuestion - 1,
                    );
                    this.getTNC(22);
                    if (this.state.isDoneProfilResiko) {
                      this.update_risk_profile({
                        nik: nik_,
                        questionData: arrayJawaban,
                      });
                    } else {
                      this.create_risk_profile({
                        nik: nik_,
                        questionData: arrayJawaban,
                      });
                    }
                  } else {
                    if (Object.keys(this.state.radioChoosen).length === 0) {
                      Alert.alert(
                        bahasa === 'id'
                          ? id.silahkan_jawab_pertanyaan
                          : en.silahkan_jawab_pertanyaan,
                      );
                    } else {
                      this.saveQuestion(
                        this.state.currentQuestion,
                        this.state.radioChoosen,
                        this.state.currentQuestion - 1,
                      );
                      this.getJawabanProfilResiko(
                        this.state.arrayPertanyaanProfilResiko[
                          this.state.currentQuestion
                        ]?.id,
                      );
                      if (
                        arrayJawaban2[this.state.currentQuestion] !== undefined
                      ) {
                        this.setState({
                          currentQuestion: this.state.currentQuestion + 1,
                          radioChoosen:
                            arrayJawaban2[this.state.currentQuestion],
                        });
                      } else {
                        this.setState({
                          currentQuestion: this.state.currentQuestion + 1,
                          radioChoosen: {},
                        });
                      }
                    }
                  }
                }
              }}
              style={[
                styles.btn,
                {
                  marginBottom: 10,
                  width: '100%',
                },
              ]}
              labelStyle={styles.btnLabel}
              buttonColor={COLORS.red}
              mode="contained">
              {this.state.currentQuestion ===
              this.state.arrayPertanyaanProfilResiko.length
                ? bahasa == 'id'
                  ? id.proses
                  : en.proses
                : this.state.session == 30
                ? bahasa == 'id'
                  ? id.selanjutnya
                  : en.selanjutnya
                : bahasa == 'id'
                ? id.mulai
                : en.mulai}
            </Button>
          </View>
          <ScrollView
            style={{
              width: '100%',
              display:
                this.state.session == 29 ||
                this.state.session == 30 ||
                this.state.session == 28
                  ? 'none'
                  : 'flex',
            }}
            contentContainerStyle={{
              paddingStart: 20,
              paddingEnd: 20,
              paddingBottom: '35%',
            }}
            keyboardDismissMode="none"
            automaticallyAdjustKeyboardInsets="true">
            <View
              style={{
                marginStart: 10,
                marginEnd: 10,
                flexDirection: 'column',
              }}>
              <FastImage
                source={logoSuccess}
                resizeMode="contain"
                style={{
                  width: '20%',
                  height: '20%',
                  alignSelf: 'center',
                }}
              />
              <Text
                style={{
                  alignSelf: 'center',
                  textAlign: 'center',
                  fontSize: 15,
                  marginEnd: 10,
                  marginBottom: 10,
                }}>
                {bahasa == 'id'
                  ? id.redaksiKonfirmasiReksaDana
                  : en.redaksiKonfirmasiReksaDana}
              </Text>
              <View
                style={{
                  alignItems: 'center',
                  width: '100%',
                  height: 1,
                  marginBottom: 10,
                  borderRadius: 12,
                  borderColor: COLORS.border_textInput,
                  borderWidth: 1.5,
                  borderStyle: 'dashed',
                }}
              />
              <Text
                style={{
                  fontSize: 15,
                  fontWeight: 'bold',
                  marginEnd: 10,
                  marginBottom: 10,
                }}>
                {bahasa == 'id' ? id.nama_lengkap : en.nama_lengkap}
              </Text>
              <Text
                style={{
                  fontSize: 15,
                  marginEnd: 10,
                  marginBottom: 10,
                }}>
                {this.state.dataInquiryCIFbyNik?.namaCust}
              </Text>

              <Text
                style={{
                  fontSize: 15,
                  fontWeight: 'bold',
                  marginEnd: 10,
                  marginBottom: 10,
                }}>
                {bahasa == 'id' ? id.no_ektp : en.no_ektp}
              </Text>
              <Text
                style={{
                  fontSize: 15,
                  marginEnd: 10,
                  marginBottom: 10,
                }}>
                {nik_ === '' ? '-' : nik_}
              </Text>

              <Text
                style={{
                  fontSize: 15,
                  fontWeight: 'bold',
                  marginEnd: 10,
                  marginBottom: 10,
                }}>
                {bahasa == 'id' ? id.no_npwp : en.no_npwp}
              </Text>
              <Text
                style={{
                  fontSize: 15,
                  marginEnd: 10,
                  marginBottom: 10,
                }}>
                {npwp_ === '' ? '-' : npwp_}
              </Text>

              <Text
                style={{
                  fontSize: 15,
                  fontWeight: 'bold',
                  marginEnd: 10,
                  marginBottom: 10,
                }}>
                {bahasa == 'id' ? id.jenis_kelamin : en.jenis_kelamin}
              </Text>
              <Text
                style={{
                  fontSize: 15,
                  marginEnd: 10,
                  marginBottom: 10,
                }}>
                {arrayBarcode[0]?.pilihrekening !== undefined
                  ? arrayBarcode[0].pilihrekening.accountName
                  : '-'}
              </Text>

              <Text
                style={{
                  fontSize: 15,
                  fontWeight: 'bold',
                  marginEnd: 10,
                  marginBottom: 10,
                }}>
                {bahasa == 'id' ? id.tempat_lahir : en.tempat_lahir}
              </Text>
              <Text
                style={{
                  fontSize: 15,
                  marginEnd: 10,
                  marginBottom: 10,
                }}>
                {this.state.dataInquiryCIFbyNik?.tempatLahir}
              </Text>

              <Text
                style={{
                  fontSize: 15,
                  fontWeight: 'bold',
                  marginEnd: 10,
                  marginBottom: 10,
                }}>
                {bahasa == 'id' ? id.tanggal_lahir : en.tanggal_lahir}
              </Text>
              <Text
                style={{
                  fontSize: 15,
                  marginEnd: 10,
                  marginBottom: 20,
                }}>
                {this.state.dataInquiryCIFbyNik?.tanggalLahir?.slice(6, 8) +
                  '-' +
                  this.state.dataInquiryCIFbyNik?.tanggalLahir?.slice(4, 6) +
                  '-' +
                  this.state.dataInquiryCIFbyNik?.tanggalLahir?.slice(0, 4)}
              </Text>

              <Text
                style={{
                  fontSize: 15,
                  fontWeight: 'bold',
                  marginEnd: 10,
                  marginBottom: 10,
                }}>
                {bahasa == 'id' ? id.profilResiko : en.profilResiko}
              </Text>
              <Text
                style={{
                  fontSize: 15,
                  marginEnd: 10,
                  marginBottom: 20,
                }}>
                {this.state.tipeProfilResikoNasabah}
              </Text>
            </View>
            <Button
              onPress={() => {
                this.setState({dataInquiryCIFbyNik: {}});
                arrayBarcode = [];
                arraynoForm = [];
                this.setState({
                  session: 27,
                });
              }}
              style={[
                styles.btn,
                {
                  marginBottom: 10,
                  width: '100%',
                },
              ]}
              labelStyle={styles.btnLabel}
              buttonColor={COLORS.red}
              mode="contained">
              OK
            </Button>
          </ScrollView>
        </View>
      );
    } else if (this.state.session == 32 || this.state.session == 33) {
      return (
        <View
          style={{
            width: '100%',
            alignItems: 'center',
            height: '65%',
            alignSelf: 'center',
            flexDirection: 'column',
          }}>
          <View
            style={{
              width: 420,
              backgroundColor: 'white',
              flexDirection: 'row',
            }}>
            <Pressable
              onPress={() => {
                this.setState({session: this.state.session == 32 ? 27 : 32});
              }}
              style={{marginEnd: 10, alignSelf: 'center'}}>
              <FastImage
                style={{
                  marginStart: 20,
                  width: 30,
                  height: 30,
                }}
                source={back}
              />
            </Pressable>
            <Text
              style={{
                marginTop: 15,
                marginBottom: 15,
                fontFamily: 'Helvetica',
                fontSize: 18,
                fontWeight: 'bold',
                borderBottomWidth: 1,
                borderBottomColor: 'black',
              }}>
              {this.state.session == 32
                ? bahasa === 'id'
                  ? id.reksa_dana
                  : en.reksa_dana
                : bahasa === 'id'
                ? id.kategori_produk
                : en.kategori_produk}
            </Text>
          </View>
          <ScrollView
            keyboardDismissMode="none"
            style={{width: '100%'}}
            automaticallyAdjustKeyboardInsets="true"
            automaticallyAdjustContentInsets="true">
            <View
              style={{
                marginTop: 15,
                marginStart: 25,
                marginEnd: 25,
                flexDirection: 'column',
              }}>
              <View
                style={{
                  borderRadius: 15,
                  width: '100%',
                  display: this.state.session == 32 ? 'flex' : 'none',
                  marginTop: 15,
                  marginBottom: 15,
                  justifyContent: 'flex-start',
                  flexDirection: 'column',
                  backgroundColor: '#E9121A',
                  padding: 30,
                }}>
                <Text
                  style={{
                    color: 'white',
                    fontSize: 18,
                    fontFamily: 'Helvetica',
                    marginBottom: 5,
                  }}>
                  {bahasa == 'id' ? id.profilResiko : en.profilResiko}
                </Text>
                <Text
                  style={{
                    color: 'white',
                    fontSize: 20,
                    fontFamily: 'Helvetica',
                    fontWeight: 'bold',
                  }}>
                  {this.state.tipeProfilResikoNasabah?.toUpperCase()}
                </Text>
              </View>
              <Text
                style={{
                  display: this.state.session == 32 ? 'flex' : 'none',
                  color: 'black',
                  fontSize: 17,
                  fontFamily: 'Helvetica',
                  fontWeight: 'bold',
                }}>
                {bahasa == 'id' ? id.layanan_reksa_dana : en.layanan_reksa_dana}
              </Text>
              <FlatList
                key={
                  this.state.session == 32
                    ? 'list_layanan_reksa_dana'
                    : 'list_kategori_produk_pembelian_reksa_dana'
                }
                style={{
                  width: '100%',
                }}
                data={
                  this.state.session == 32
                    ? this.state.itemLayananReksaDana
                    : this.state.itemKategoriProduk
                }
                renderItem={({item, index}) => (
                  <TouchableOpacity
                    key={index}
                    onPress={() => {
                      if (this.state.session == 32) {
                        if (item.id == 1) {
                          this.setState({session: 33});
                          this.getKategoriProduk();
                        }
                      } else {
                      }
                    }}
                    style={{
                      borderRadius: 10,
                      alignSelf: 'center',
                      alignItems: 'center',
                      justifyContent: 'center',
                      alignContent: 'center',
                      width: '100%',
                      marginTop: 10,
                      marginBottom: 5,
                      backgroundColor: '#E9121A',
                      justifyContent: 'flex-start',
                      flexDirection: 'row',
                      padding: 15,
                    }}>
                    <FastImage
                      source={this.state.session == 32 ? item.img : null}
                      style={{width: 30, height: 30}}
                    />
                    <Text
                      style={{
                        color: 'white',
                        fontSize: 16,
                        fontFamily: 'Helvetica',
                        fontWeight: 'bold',
                      }}>
                      {this.state.session == 32 ? item.title : item.label}
                    </Text>
                  </TouchableOpacity>
                )}
                horizontal={false}
                numColumns={1}
                keyExtractor={item => item.id}
              />
            </View>
          </ScrollView>
        </View>
      );
    } else if (
      this.state.session == 34 ||
      this.state.session == 35 ||
      this.state.session == 36 ||
      this.state.session == 37
    ) {
      return (
        <View
          style={{
            width: '100%',
            alignItems: 'center',
            height: this.state.session === 37 ? '100%' : '65%',
            alignSelf: 'center',
            flexDirection: 'column',
          }}>
          <View
            style={{
              width: 420,
              backgroundColor: 'white',
              flexDirection: 'row',
            }}>
            <Pressable
              onPress={() => {
                if (this.state.session === 34) {
                  this.setState({
                    session: 8,
                    formCode: '14',
                  });

                  this.formBuilder('14');
                } else if (this.state.session === 35) {
                  this.getTNC(24);
                  this.setState({
                    session: 34,
                  });
                } else if (this.state.session == 36) {
                  this.setState({session: 35});
                  this.formBuilder2(69);
                } else if (this.state.session == 37) {
                  this.setState({session: 36});
                  this.formBuilder2(65);
                }
              }}
              style={{marginEnd: 10, alignSelf: 'center'}}>
              <FastImage
                style={{
                  marginStart: 20,
                  width: 30,
                  height: 30,
                }}
                source={back}
              />
            </Pressable>
            <Text
              style={{
                marginTop: 15,
                marginBottom: 15,
                fontFamily: 'Helvetica',
                fontSize: 18,
                fontWeight: 'bold',
                borderBottomWidth: 1,
                borderBottomColor: 'black',
              }}>
              {bahasa === 'id' ? id.aktivasi_ibmb : en.aktivasi_ibmb}
            </Text>
          </View>

          <View
            style={{
              display: this.state.session == 34 ? 'flex' : 'none',
              width: '100%',
              height: '90%',
              padding: 20,
              flexDirection: 'column',
              justifyContent: 'space-between',
            }}>
            <View
              style={{
                width: '100%',
              }}>
              <ScrollView
                style={{
                  width: '100%',
                }}
                contentContainerStyle={{
                  paddingStart: 20,
                  paddingEnd: 20,
                  paddingBottom: '10%',
                }}
                keyboardDismissMode="none"
                automaticallyAdjustKeyboardInsets="true">
                <RenderHtml
                  contentWidth={300}
                  source={{html: this.state.htmlTNC}}
                />
                <View>
                  <CheckBox
                    label={bahasa == 'id' ? id.saya_setuju : en.saya_setuju}
                    status={this.state.isChecked ? 'checked' : 'unchecked'}
                    onPress={() => {
                      if (this.state.isChecked) {
                        this.setState({isChecked: false});
                      } else {
                        this.setState({isChecked: true});
                      }
                    }}
                    containerStyle={{
                      marginBottom: 10,
                    }}
                    labelStyle={{fontSize: 16}}
                    boxColor={COLORS.red}
                  />
                  <Button
                    onPress={() => {
                      if (this.state.isChecked) {
                        ///Menuju Form Informasi Data Utama Aktivasi IBMB
                        this.setState({session: 35});
                        this.formBuilder2(69);
                      }
                    }}
                    style={{
                      alignSelf: 'center',
                      marginBottom: 10,
                      width: 100,
                      borderRadius: 10,
                      borderColor: this.state.isChecked
                        ? COLORS.red
                        : COLORS.border_textInput,
                      borderWidth: 2,
                    }}
                    labelStyle={styles.btnLabel}
                    buttonColor={
                      this.state.isChecked
                        ? COLORS.red
                        : COLORS.border_textInput
                    }
                    mode="contained">
                    {bahasa == 'id' ? id.lanjut : en.lanjut}
                  </Button>
                </View>
              </ScrollView>
            </View>
          </View>

          <ScrollView
            style={{
              width: '100%',
              display: this.state.session >= 35 ? 'flex' : 'none',
            }}
            contentContainerStyle={{
              paddingStart: 20,
              paddingEnd: 20,
              paddingBottom: '10%',
            }}
            keyboardDismissMode="none"
            automaticallyAdjustKeyboardInsets="true">
            <View
              style={{
                width: '100%',
                marginTop: 15,
              }}>
              <Text
                style={{
                  marginStart: 10,
                  fontSize: 20,
                  marginBottom: 10,
                  fontWeight: 'bold',
                  color: COLORS.textColorGray,
                }}>
                {this.state.session == 35
                  ? bahasa === 'id'
                    ? id.informasi_data_utama
                    : en.informasi_data_utama
                  : bahasa === 'id'
                  ? id.registrasi_aplikasi
                  : en.registrasi_aplikasi}
              </Text>
              <DynamicForm
                bahasa={bahasa}
                defaultValue={{}}
                accessToken={accessToken}
                exchangeToken={exchangeToken}
                formTemplate={{data: dataresp}}
                onSubmit={this.onSubmit}
                onPressed={() => {}}
                btnStyle={{
                  display: dataresp.length === 0 ? 'none' : 'flex',
                  borderRadius: 10,
                  backgroundColor: COLORS.red,
                  top: 20,
                  marginEnd: 10,
                  width: 'auto',
                  alignSelf: 'center',
                }}
                btnLabel={
                  this.state.session === 37
                    ? bahasa === 'id'
                      ? id.kirim_kode_otp
                      : en.kirim_kode_otp
                    : bahasa === 'id'
                    ? id.selanjutnya
                    : en.selanjutnya
                }
              />
            </View>
          </ScrollView>
        </View>
      );
    }
  }
  async getKategoriProduk() {
    this.setState({loadingvisible: true});
    fetch(Server.BASE_URL_API + ApiService.getKategoriProduk, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log('Response getKategoriProduk', responseJson);
        this.setState({loadingvisible: false});
        if (responseJson.code < 300) {
          this.setState({itemKategoriProduk: responseJson.data});
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        Alert.alert('Gagal Terhubung ke server', error);
      });
  }
  async requestSID(payload_) {
    console.log('payload requestSID', payload_);
    this.setState({loadingvisible: true});
    fetch(Server.BASE_URL_API + ApiService.requestSID, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: JSON.stringify(payload_),
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log('Response requestSID', responseJson);
        if (responseJson.code < 300) {
          this.setState({loadingvisible: false});
          this.getRiskProfile({
            nik: nik_,
            bahasa: bahasa,
          });
          this.inquiryCIFbyNIK({
            noKtp: nik_,
            idDips: idDips,
          });
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        Alert.alert('Gagal Terhubung ke server', error);
      });
  }
  async inquiryCIFbyNIK(payload_) {
    console.log('payload inquiryCIFbyNIK', payload_);
    fetch(Server.BASE_URL_API + ApiService.inquiry_cif_nik, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: JSON.stringify(payload_),
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log('Response inquiryCIFbyNIK', responseJson);
        if (responseJson.code < 300) {
          this.setState({dataInquiryCIFbyNik: responseJson.data});
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        Alert.alert('Gagal Terhubung ke server', error);
      });
  }
  async checkStatusSID(payload_) {
    console.log('payload save checkStatusSID', payload_);
    this.setState({loadingvisible: true});
    fetch(Server.BASE_URL_API + ApiService.cekStatusSID, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: JSON.stringify(payload_),
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({loadingvisible: false});
        console.log('Response checkStatusSID', responseJson);
        if (responseJson.code < 300) {
          if (responseJson.status === 'request') {
            this.getRiskProfile({
              nik: nik_,
              bahasa: bahasa,
            });
            this.inquiryCIFbyNIK({
              noKtp: nik_,
              idDips: idDips,
            });
            setTimeout(() => {
              this.setState({
                session: 31,
                modalrequestSID: true,
              });
            }, 1500);
          } else {
            this.getRiskProfile({
              nik: nik_,
              bahasa: bahasa,
            });
            this.setState({session: 32});
          }
        } else {
          this.getRiskProfile({
            nik: nik_,
            bahasa: bahasa,
          });
          this.setState({session: 28});
          this.getNoRekening();
          this.formBuilder2(66);
          this.getNoForm();

          this.getPertanyaanProfilResiko();
        }
      })
      .catch(error => {
        Alert.alert('Gagal Terhubung ke server', error);
      });
  }
  async getRiskProfile(payload_) {
    console.log('payload save getRiskProfile', payload_);
    fetch(Server.BASE_URL_API + ApiService.get_risk_profil, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: JSON.stringify(payload_),
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log('Response getRiskProfile', responseJson);
        if (responseJson.code < 300) {
          this.setState({
            isDoneProfilResiko: true,
            tipeProfilResikoNasabah: responseJson.data,
          });
        } else {
          this.setState({isDoneProfilResiko: false});
        }
      })
      .catch(error => {
        Alert.alert('Gagal Terhubung ke server', error);
      });
  }
  saveQuestion(questionNo_, object_answer, index) {
    arrayJawaban[index] = {
      questionNo: questionNo_,
      answer: object_answer?.sortAnswer,
      type: object_answer?.risk,
    };
    arrayJawaban2[index] = object_answer;
    console.log('Array Jawaban', arrayJawaban);
    console.log('Array Jawaban 2', arrayJawaban2);
  }
  async getPertanyaanProfilResiko() {
    fetch(Server.BASE_URL_API + ApiService.getPertanyaanProfilResiko + bahasa, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log('Response getPertanyaanProfilResiko', responseJson);
        if (responseJson.code < 300) {
          this.setState({arrayPertanyaanProfilResiko: responseJson.data});
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        Alert.alert('Gagal Terhubung ke server', error);
      });
  }
  async getJawabanProfilResiko(questionId) {
    console.log('questionId', questionId);
    this.setState({loadingvisible: true});
    fetch(
      Server.BASE_URL_API +
        ApiService.getJawabanProfilResiko +
        bahasa +
        '&questionId=' +
        questionId,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + accessToken,
          exchangeToken: exchangeToken,
        },
        timeout: 1500,
      },
    )
      .then(response => response.json())
      .then(responseJson => {
        console.log('Response getPertanyaanProfilResiko', responseJson);
        if (responseJson.code < 300) {
          this.setState({
            arrayJawabanProfilResiko: responseJson.data,
            loadingvisible: false,
          });
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        Alert.alert('Gagal Terhubung ke server', error);
      });
  }
  async penempatanDeposito(payload_) {
    console.log('payload save penempatanDeposito', payload_);
    fetch(Server.BASE_URL_API + ApiService.savePenempatanDeposito, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: JSON.stringify(payload_),
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log('Response penempatanDeposito', responseJson);
        if (responseJson.code < 300) {
          this.saveForm('Deposit', {
            rekeningsumberdana:
              arrayBarcode[0]?.rekeningsumberdana.prodName +
              ' / ' +
              arrayBarcode[0]?.rekeningsumberdana.accountNo +
              ' - ' +
              arrayBarcode[0]?.rekeningsumberdana.accountName +
              ' / IDR ' +
              parseInt(arrayBarcode[0]?.rekeningsumberdana.availBalance) / 100,
            jenisdeposito: arrayBarcode[0]?.matauang.value,
            nominaldeposito: arrayBarcode[0]?.nominaldeposito,
            jangkawaktu: arrayBarcode[0]?.jangkawaktu.value,
            intruksi: arrayBarcode[0]?.instruksi.value,
            bungadidapat: this.state.interesetDeposito + '%',
            tanggaltransaksi: moment().format('DD-MM-YYYY HH:mm'),
            referenceNumber: responseJson.data.referenceNumber,
          });
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        Alert.alert('Gagal Terhubung ke server', error);
      });
  }
  async create_risk_profile(payload_) {
    this.setState({loadingvisible: true});
    console.log('payload save create_risk_profile', payload_);
    fetch(Server.BASE_URL_API + ApiService.createRiskProfile, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: JSON.stringify(payload_),
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({loadingvisible: false});
        console.log('Response create_risk_profile', responseJson);
        if (responseJson.code < 300) {
          this.setState({
            isDoneProfilResiko: true,
            session: 28,
            radioChoosen: {},
            arrayJawabanProfilResiko: [],
            arrayPertanyaanProfilResiko: [],
          });
          arrayJawaban = [];
          arrayJawaban2 = [];
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        Alert.alert('Gagal Terhubung ke server', error);
      });
  }
  async update_risk_profile(payload_) {
    this.setState({loadingvisible: true});
    console.log('payload save update_risk_profile', payload_);
    fetch(Server.BASE_URL_API + ApiService.updateRiskProfile, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: JSON.stringify(payload_),
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({loadingvisible: false});
        console.log('Response update_risk_profile', responseJson);
        if (responseJson.code < 300) {
          this.setState({
            isDoneProfilResiko: true,
            session: 28,
            radioChoosen: {},
            arrayJawabanProfilResiko: [],
            arrayPertanyaanProfilResiko: [],
          });
          arrayJawaban = [];
          arrayJawaban2 = [];
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        Alert.alert('Gagal Terhubung ke server', error);
      });
  }

  //loopGetResi
  loopResi(sess) {
    console.log('session', sess);
    console.log('sessionBeforeOTP', this.state.sessionBeforeOTP);
    for (let i = 0; i < arrayIdForm.length; i++) {
      console.log('i', i);
      console.log('arrayIdForm', arrayIdForm[i]);
      console.log(arrayBarcode);
      this.getResiTransaction(
        this.state.sessionBeforeOTP === 14
          ? arrayBarcode[i]?.jenislayanan.label?.toLowerCase()
          : 'interbank',
        arrayIdForm[i],
      );
      if (i == arrayIdForm.length - 1) {
        setTimeout(() => {
          console.log('masuk end loop resi');
          if (arrayTransaksiMultiForm.length === arrayIdForm.length) {
            console.log('Completed Resi');
            arrayIdForm = [];
            this.setState({
              loadingvisible: false,
              transactionId: '',
              pin: '',
              session: sess,
            });
          }
        }, 5000);
      }
    }
  }
  async getResiTransaction(type, idForm) {
    console.log(
      'URL RESI',
      Server.BASE_URL_API +
        ApiService.resiTransaksi +
        type +
        '/' +
        idForm +
        '?bahasa=' +
        bahasa,
    );
    fetch(
      Server.BASE_URL_API +
        ApiService.resiTransaksi +
        type +
        '/' +
        idForm +
        '?bahasa=' +
        bahasa,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + accessToken,
          exchangeToken: exchangeToken,
        },
        timeout: 1500,
      },
    )
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.code < 300) {
          arrayTransaksiMultiForm.push(responseJson);
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        Alert.alert('Gagal Terhubung ke server', error);
      });
  }
  async resiPembukaanAkun(sess) {
    console.log(
      'URL RESI PEMBUKAAN AKUN',
      Server.BASE_URL_API + ApiService.resiPembukaanAkun + arrayIdForm[0],
    );
    fetch(Server.BASE_URL_API + ApiService.resiPembukaanAkun + arrayIdForm[0], {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log('response resi Pembukaan Akun', responseJson);
        this.setState({loadingvisible: false});
        if (responseJson.code < 300) {
          this.mirroringEndpoint(131);
          this.setState({session: sess});
          arrayIdForm = [];
          base64Resi = `data:image/png;base64,${responseJson.data.image}`;
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        Alert.alert('Gagal Terhubung ke server', error);
      });
  }

  ///saveform
  loopsaveForm() {
    //For transaksi antar bank
    if (this.state.session === 14) {
      for (let i = 0; i < arrayBarcode.length; i++) {
        console.log('i', i);
        console.log('arrayBarcode', arrayBarcode[i]);
        this.setState({loadingvisible: true});
        this.saveForm('ANTARBANK - ' + arrayBarcode[i]?.jenislayanan.label, {
          rekeningsumberdana:
            arrayBarcode[i]?.rekeningsumberdana.prodName +
            ' / ' +
            arrayBarcode[i]?.rekeningsumberdana.accountNo +
            ' - ' +
            arrayBarcode[i]?.rekeningsumberdana.accountName +
            ' / IDR ' +
            parseInt(arrayBarcode[i]?.rekeningsumberdana.availBalance) / 100,
          jenislayanan: arrayBarcode[i]?.jenislayanan.label,
          bankpenerima: arrayBarcode[i]?.bankpenerima.label,
          rekeningpenerima: arrayBarcode[i]?.rekeningpenerima,
          nominaltransaksi: parseInt(
            arrayBarcode[i]?.nominaltransaksi?.replaceAll('.', ''),
          ).toString(),
          noformulir: arraynoForm[i]?.noForm,
          tanggaltransaksi: moment().format('YYYY-MM-DD'),
          tujuantransaksi: arrayBarcode[i]?.tujuantransaksi.label,
          berita:
            arrayBarcode[i].berita == undefined ? '' : arrayBarcode[i].berita,
          message:
            arrayBarcode[i].berita == undefined ? '' : arrayBarcode[i].berita,
          currencyCode: '360',
          accountType: arrayBarcode[i].rekeningsumberdana.accountType,
          systemTraceAuditNumber: arraynoForm[i].systemTraceAuditNumber,
          retrievalReferenceNumber: arraynoForm[i].retrievalReferenceNumber,
          localDateTime: Math.floor(Date.now() / 1000).toString(),
          amount: (
            parseInt(arrayBarcode[i]?.nominaltransaksi?.replaceAll('.', '')) *
            100
          ).toString(),
          fromAccountNumber: arrayBarcode[i].rekeningsumberdana.accountNo,
          toAccountNumber: arrayBarcode[i].rekeningpenerima,
          issuerCustomerName: arrayBarcode[i].rekeningsumberdana.accountName,
          beneficiaryCode:
            arrayBarcode[i].bankpenerima?.beneficiaryCode.length === 1
              ? '00' + arrayBarcode[i].bankpenerima.beneficiaryCode
              : arrayBarcode[i].bankpenerima?.beneficiaryCode.length === 2
              ? '0' + arrayBarcode[i].bankpenerima.beneficiaryCode
              : arrayBarcode[i].bankpenerima?.beneficiaryCode,
          adminFee: '00650000',
          transactionType: 'ONLINE',
          namapenerima: arrayInquiry[i]?.destCustomerName,
          settlementDate: moment().format('DD-MM-YYYY'),
          benefitRecipients: '2',
          residentType: '2',
          destSwiftCode: arrayBarcode[i]?.bankpenerima?.swiftCode,
          destCityCode: arrayBarcode[i]?.bankpenerima?.cityCode,
        });
        if (i == arrayBarcode.length - 1) {
          this.setState({
            loadingvisible: false,
            showFormBuilderQr: false,
            isConfirmTransaksi: false,
            currentPage: 1,
            nextPage: 1,
            session: 9,
            sessionBeforeOTP: 14,
          });
          this.sendOtp();
        }
      }
    }
    //For transaksi antar rekening
    else if (this.state.session === 16) {
      for (let i = 0; i < arrayBarcode.length; i++) {
        console.log('i', i);
        console.log('arrayBarcode', arrayBarcode[i]);
        this.setState({loadingvisible: true});
        this.saveForm('INTERBANK', {
          rekeningsumberdana:
            arrayBarcode[i]?.rekeningsumberdana.prodName +
            ' / ' +
            arrayBarcode[i]?.rekeningsumberdana.accountNo +
            ' - ' +
            arrayBarcode[i]?.rekeningsumberdana.accountName +
            ' / IDR ' +
            parseInt(arrayBarcode[i]?.rekeningsumberdana.availBalance) / 100,
          rekeningpenerima: arrayBarcode[i]?.rekeningpenerima,
          nominaltransaksi: parseInt(
            arrayBarcode[i]?.nominaltransaksi?.replaceAll('.', ''),
          ).toString(),
          noformulir: arraynoForm[i]?.noForm,
          tanggaltransaksi: moment().format('YYYY-MM-DD'),
          tujuantransaksi: arrayBarcode[i]?.tujuantransaksi.label,
          berita:
            arrayBarcode[i].berita == undefined ? '' : arrayBarcode[i].berita,
          namapenerima: arrayInquiry[i]?.destCustomerName,
          currencyCode: '360',
          accountType: arrayBarcode[i].rekeningsumberdana.accountType,
          systemTraceAuditNumber: arraynoForm[i].systemTraceAuditNumber,
          retrievalReferenceNumber: arraynoForm[i].retrievalReferenceNumber,
          localDateTime: Math.floor(Date.now() / 1000).toString(),
          amount: (
            parseInt(arrayBarcode[i]?.nominaltransaksi?.replaceAll('.', '')) *
            100
          ).toString(),
          fromAccountNumber: arrayBarcode[i].rekeningsumberdana.accountNo,
          toAccountNumber: arrayBarcode[i].rekeningpenerima,
          issuerCustomerName: arrayBarcode[i].rekeningsumberdana.accountName,
          settlementDate: moment().format('DD-MM-YYYY'),
          message:
            arrayBarcode[i].berita == undefined ? '' : arrayBarcode[i].berita,
        });
        if (i == arrayBarcode.length - 1) {
          this.setState({
            loadingvisible: false,
            showFormBuilderQr: false,
            isConfirmTransaksi: false,
            currentPage: 1,
            nextPage: 1,
            session: 9,
            sessionBeforeOTP: 16,
          });
          this.sendOtp();
          arrayUploadBarcode = [];
          arrayBarcode = [];
          arraynoForm = [];
        }
      }
    }
    //For transaksi rekening sendiri
    else if (this.state.session === 17) {
      for (let i = 0; i < arrayBarcode.length; i++) {
        console.log('i', i);
        console.log('arrayBarcode', arrayBarcode[i]);
        this.setState({loadingvisible: true});
        this.saveForm('Private Transaction', {
          rekeningsumberdana:
            arrayBarcode[i]?.rekeningsumberdana.prodName +
            ' / ' +
            arrayBarcode[i]?.rekeningsumberdana.accountNo +
            ' - ' +
            arrayBarcode[i]?.rekeningsumberdana.accountName +
            ' / IDR ' +
            parseInt(arrayBarcode[i]?.rekeningsumberdana.availBalance) / 100,
          rekeningpenerima: arrayBarcode[i]?.rekeningpenerima,
          nominaltransaksi: parseInt(
            arrayBarcode[i]?.nominaltransaksi?.replaceAll('.', ''),
          ).toString(),
          noformulir: arraynoForm[i]?.noForm,
          tanggaltransaksi: moment().format('YYYY-MM-DD'),
          berita:
            arrayBarcode[i].berita == undefined ? '' : arrayBarcode[i].berita,
          namapenerima: arrayInquiry[i]?.destCustomerName,
          currencyCode: '360',
          accountType: arrayBarcode[i].rekeningsumberdana.accountType,
          systemTraceAuditNumber: arraynoForm[i].systemTraceAuditNumber,
          retrievalReferenceNumber: arraynoForm[i].retrievalReferenceNumber,
          localDateTime: Math.floor(Date.now() / 1000).toString(),
          amount: (
            parseInt(arrayBarcode[i]?.nominaltransaksi?.replaceAll('.', '')) *
            100
          ).toString(),
          fromAccountNumber: arrayBarcode[i].rekeningsumberdana.accountNo,
          toAccountNumber: arrayBarcode[i].rekeningpenerima,
          issuerCustomerName: arrayBarcode[i].rekeningsumberdana.accountName,
          settlementDate: moment().format('DD-MM-YYYY'),
          message:
            arrayBarcode[i].berita == undefined ? '' : arrayBarcode[i].berita,
        });
        if (i == arrayBarcode.length - 1) {
          this.setState({
            loadingvisible: false,
            showFormBuilderQr: false,
            isConfirmTransaksi: false,
            currentPage: 1,
            nextPage: 1,
            session: 9,
            sessionBeforeOTP: 17,
          });
          this.sendOtp();
          arrayUploadBarcode = [];
          arrayBarcode = [];
          arraynoForm = [];
        }
      }
    }
  }
  async saveForm(formCode, payload) {
    var payload = {
      formCode: formCode,
      idDips: idDips,
      payload: payload,
    };

    console.log('formCode Save Form', formCode);
    console.log('Payload Save Form', payload);
    fetch(Server.BASE_URL_API + ApiService.saveForm, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: JSON.stringify(payload),
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log('Response Save Form', responseJson);
        if (responseJson.code < 300) {
          arrayIdForm.push(responseJson.data.idForm);
          console.log('arrayIdForm', arrayIdForm);
          if (this.state.session === 15) {
            this.setState({session: 9});
            this.sendOtp();
            this.mirroringKey('noponsel', '08' + custNoHp.slice(1));
            this.mirroringEndpoint(11);
          }
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        Alert.alert('Gagal Terhubung ke server', error);
      });
  }

  //Pengajuan
  loopPengajuan(sess) {
    console.log('masuk log pengajuan');
    for (let i = 0; i < arrayIdForm.length; i++) {
      console.log('arrayIdForm', arrayIdForm[i]);
      this.setState({loadingvisible: true});
      this.getStatusPengajuan(Number(arrayIdForm[i]));
      if (i == arrayIdForm.length - 1) {
        setTimeout(() => {
          console.log('masuk end loop pengajuan');
          const hasNoValue = arrayBerhasilPengajuan.some(obj =>
            Object.values(obj).includes('NO'),
          );
          if (hasNoValue) {
            console.log('masuk NO');
            this.loopPengajuan(sess);
          } else {
            arrayBerhasilPengajuan = [];
            this.loopResi(sess);
          }
        }, 7000);
      }
    }
  }
  loopPengajuanPembukaanAkun(sess) {
    console.log('masuk loopPengajuanPembukaanAkun');
    for (let i = 0; i < arrayIdForm.length; i++) {
      console.log('arrayIdForm', arrayIdForm[i]);
      this.setState({loadingvisible: true});
      this.getStatusPengajuan(Number(arrayIdForm[i]));
      if (i == arrayIdForm.length - 1) {
        setTimeout(() => {
          console.log('masuk end loop pengajuan');
          const hasNoValue = arrayBerhasilPengajuan.some(obj =>
            Object.values(obj).includes('NO'),
          );
          if (hasNoValue) {
            console.log('masuk NO');
            this.loopPengajuanPembukaanAkun(sess);
          } else {
            arrayBerhasilPengajuan = [];
            this.resiPembukaanAkun(sess);
          }
        }, 7000);
      }
    }
  }

  async getStatusPengajuan(idForm) {
    fetch(Server.BASE_URL_API + ApiService.getStatusPengajuan + idForm, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log('Response Status Pengajuan', responseJson);
        if (responseJson.code < 300) {
          this.updateValue(idForm, 'YES');
        } else {
          this.updateValue(idForm, 'NO');
        }
        console.log('arrayBerhasilPengajuan', arrayBerhasilPengajuan);
      })
      .catch(error => {
        Alert.alert('Gagal Terhubung ke server', error);
      });
  }

  updateValue(key, newValue) {
    const existingObj = arrayBerhasilPengajuan.find(obj =>
      obj.hasOwnProperty(key),
    );
    if (existingObj) {
      existingObj[key] = newValue;
    } else {
      const newObj = {[key]: newValue};
      arrayBerhasilPengajuan.push(newObj);
    }
  }

  //loopPengecekan rekening penerima & nominal Transaction
  loopcekTransaksi() {
    correctTransaction = true;
    for (let i = 0; i < arrayBarcode.length; i++) {
      if (
        arrayBarcode[i]?.rekeningpenerima ===
        arrayBarcode[i]?.rekeningsumberdana?.accountNo
      ) {
        correctTransaction = false;
        console.log(
          'rekening penerima sama dengan sumber dana di transaksi - ' + i + 1,
        );
      } else if (
        parseInt(arrayBarcode[i]?.nominaltransaksi?.replaceAll(/\./g, ''), 10) >
        parseInt(arrayBarcode[i]?.rekeningsumberdana?.availBalance) / 100
      ) {
        correctTransaction = false;
        console.log(
          'nominal transfer lebih besar dari sumber dana di transaksi - ' +
            i +
            1,
        );
      }
      if (i == arrayBarcode.length - 1) {
        console.log('correctTransaction', correctTransaction);
      }
    }
  }
  loopcekMandatoryFieldRekeningSendiri() {
    mandatoryIsOke = true;
    for (let i = 0; i < arrayBarcode.length; i++) {
      if (
        arrayBarcode[i].hasOwnProperty('rekeningsumberdana') &&
        arrayBarcode[i].hasOwnProperty('rekeningpenerima') &&
        arrayBarcode[i].hasOwnProperty('nominaltransaksi') &&
        arrayBarcode[i]?.rekeningpenerima !== '' &&
        arrayBarcode[i]?.nominaltransaksi !== '' &&
        arrayBarcode[i]?.nominaltransaksi !== '0'
      ) {
      } else {
        mandatoryIsOke = false;
      }
      if (i == arrayBarcode.length - 1) {
        console.log('mandatoryIsOke', mandatoryIsOke);
      }
    }
  }
  ///Inquiry
  popUpInvalidInquiry() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.modalWrongInquiry}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({modalWrongInquiry: false});
        }}>
        <View
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View
              style={{
                margin: 100,
                backgroundColor: 'white',
                borderRadius: 20,
                justifyContent: 'space-between',
                padding: 35,
                width: '80%',
                alignItems: 'center',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
              }}>
              <FastImage
                source={logoInfo}
                style={{width: 55, height: 55, marginBottom: 15}}
              />
              <Text
                style={{
                  marginBottom: 20,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  textAlign: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {bahasa === 'id' ? id.warning_inquiry : en.warning_inquiry}
              </Text>
              <Pressable
                style={{
                  width: '40%',
                  height: 43,
                  borderRadius: 10,
                  padding: 10,
                  elevation: 2,
                  backgroundColor: COLORS.red,
                }}
                onPress={() => {
                  this.setState({modalWrongInquiry: false});
                }}>
                <Text style={styles.textbtn}>OK</Text>
              </Pressable>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
  inquiryAntarBank() {
    for (let i = 0; i < arrayBarcode.length; i++) {
      var payload = {
        currencyCode: '360',
        accountType: arrayBarcode[i]?.rekeningsumberdana?.accountType,
        systemTraceAuditNumber: arraynoForm[i]?.systemTraceAuditNumber,
        retrievalReferenceNumber: arraynoForm[i]?.retrievalReferenceNumber,
        localDateTime: Math.floor(Date.now() / 1000).toString(),
        amount: (
          parseInt(arrayBarcode[i].nominaltransaksi?.replaceAll('.', '')) * 100
        ).toString(),
        fromAccountNumber: arrayBarcode[i]?.rekeningsumberdana?.accountNo,
        toAccountNumber: arrayBarcode[i]?.rekeningpenerima,
        issuerCustomerName: arrayBarcode[i]?.rekeningsumberdana?.accountName,
        beneficiaryCode:
          arrayBarcode[i]?.bankpenerima?.beneficiaryCode.length === 1
            ? '00' + arrayBarcode[i]?.bankpenerima.beneficiaryCode
            : arrayBarcode[i]?.bankpenerima?.beneficiaryCode.length === 2
            ? '0' + arrayBarcode[i]?.bankpenerima?.beneficiaryCode
            : arrayBarcode[i]?.bankpenerima?.beneficiaryCode,
        adminFee: '00650000',
        transactionType: 'ONLINE',
        settlementDate: moment().format('DD-MM-YYYY'),
      };
      console.log('payload Inquiry Antar Bank', payload);
      //jika antar bank maka inquiry online
      this.processInquiry(payload, 'online');
      if (i == arrayBarcode.length - 1) {
        this.setState({
          isConfirmTransaksi: true,
          currentPage: 1,
          nextPage: 1,
        });
      }
    }
  }
  inquiryAntarRekening() {
    for (let i = 0; i < arrayBarcode.length; i++) {
      var payload = {
        currencyCode: '360',
        accountType: arrayBarcode[i]?.rekeningsumberdana.accountType,
        systemTraceAuditNumber: arraynoForm[i].systemTraceAuditNumber,
        retrievalReferenceNumber: arraynoForm[i].retrievalReferenceNumber,
        localDateTime: Math.floor(Date.now() / 1000).toString(),
        amount: (
          parseInt(arrayBarcode[i].nominaltransaksi.replaceAll('.', '')) * 100
        ).toString(),
        fromAccountNumber: arrayBarcode[i].rekeningsumberdana.accountNo,
        toAccountNumber: arrayBarcode[i].rekeningpenerima,
        issuerCustomerName: arrayBarcode[i].rekeningsumberdana.accountName,
        settlementDate: moment().format('DD-MM-YYYY'),
        message:
          arrayBarcode[i].berita === undefined ? '' : arrayBarcode[i].berita,
      };
      //jika antar rekening maka inquiry online
      this.processInquiry(payload, 'overbook');
      if (i == arrayBarcode.length - 1) {
        this.setState({
          isConfirmTransaksi: true,
          currentPage: 1,
          nextPage: 1,
        });
      }
    }
  }
  inquiryRekeningSendiri() {
    for (let i = 0; i < arrayBarcode.length; i++) {
      var payload = {
        currencyCode: '360',
        accountType: arrayBarcode[i].rekeningsumberdana.accountType,
        systemTraceAuditNumber: arraynoForm[i].systemTraceAuditNumber,
        retrievalReferenceNumber: arraynoForm[i].retrievalReferenceNumber,
        localDateTime: Math.floor(Date.now() / 1000).toString(),
        amount: (
          parseInt(arrayBarcode[i].nominaltransaksi.replaceAll('.', '')) * 100
        ).toString(),
        fromAccountNumber: arrayBarcode[i].rekeningsumberdana.accountNo,
        toAccountNumber: arrayBarcode[i].rekeningpenerima,
        issuerCustomerName: arrayBarcode[i].rekeningsumberdana.accountName,
        settlementDate: moment().format('DD-MM-YYYY'),
        message:
          arrayBarcode[i].berita === undefined ? '' : arrayBarcode[i].berita,
      };
      //jika rekening sendiri maka inquiry online
      this.processInquiry(payload, 'overbook');
      if (i == arrayBarcode.length - 1) {
        this.setState({
          isConfirmTransaksi: true,
          currentPage: 1,
          nextPage: 1,
        });
      }
    }
  }
  async processInquiry(payload_, jenis_inquiry) {
    wrongInquiry = false;
    console.log('payload Inquiry', payload_);
    console.log(Server.BASE_URL_API + ApiService.inquiry + jenis_inquiry);
    fetch(Server.BASE_URL_API + ApiService.inquiry + jenis_inquiry, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: JSON.stringify(payload_),
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log('Response Save Inquiry', responseJson);
        this.setState({loadingvisible: false});
        if (responseJson.code < 300) {
          arrayInquiry.push(responseJson.data.data);
        } else {
          wrongInquiry = true;
          this.setState({modalWrongInquiry: true});
        }
      })
      .catch(error => {
        Alert.alert('Gagal Terhubung ke server', error);
      });
  }

  renderLegend(data) {
    let render = '';
    if (data.listTypeProduk !== undefined) {
      render = data.listTypeProduk.map((element, index) => {
        return (
          <View key={index} style={{flexDirection: 'row'}}>
            <View
              style={{
                height: 18,
                width: 18,
                marginRight: 10,
                borderRadius: 4,
                backgroundColor:
                  element.type == 'Tabungan' || element.type == 'Saving'
                    ? '#FF9658'
                    : element.type == 'Giro'
                    ? '#50A0FF'
                    : element.type == 'Deposito'
                    ? '#A5A5A5'
                    : element.type == 'Bancassurance'
                    ? '#F9C014'
                    : element.type == 'Reksa dana'
                    ? '#84BCE7'
                    : '#72AC46',
              }}
            />
            <Text style={{color: 'black', fontSize: 16}}>
              {element.type || ''}
            </Text>
          </View>
        );
      });

      return render;
    }
  }
  renderListTabungan(data) {
    let render = '';
    if (data.portotabungan !== undefined) {
      return (
        <View style={{flexDirection: 'column'}}>
          <Pressable
            onPress={() => {
              if (this.state.isExpanded) {
                this.setState({isExpanded: false});
              } else {
                this.setState({isExpanded: true});
              }
            }}
            style={{
              marginTop: 20,
              height: 60,
              borderRadius: 10,
              backgroundColor: 'white',
              borderColor: 'black',
              borderWidth: 1,
            }}>
            <View
              style={{
                marginStart: 10,
                marginEnd: 10,
                justifyContent: 'space-between',
                alignItems: 'center',
                flexDirection: 'row',
                height: '100%',
              }}>
              <View
                style={{
                  alignItems: 'center',
                  flexDirection: 'row',
                  height: '100%',
                }}>
                <FastImage
                  resizeMode="contain"
                  style={{width: 30, height: 30}}
                  source={ic_tabungan}
                />
                <Text
                  style={{fontSize: 20, fontWeight: 'bold', marginStart: 10}}>
                  {data.listTypeProduk[0].type}
                </Text>
              </View>

              <Image
                tintColor="grey"
                resizeMode="contain"
                style={{width: 15, height: 15}}
                source={this.state.isExpanded ? upArrow : downArrow}
              />
            </View>
          </Pressable>
          <View
            style={{
              display: this.state.isExpanded ? 'flex' : 'none',
              padding: 10,
              flexDirection: 'column',
              borderRadius: 10,
              backgroundColor: 'white',
              borderColor: 'black',
              borderWidth: 1,
            }}>
            {
              (render = data.portotabungan.map((element, index) => {
                return (
                  <View
                    key={index}
                    style={{
                      marginEnd: 10,
                      borderBottomColor: 'black',
                      borderBottomWidth: 1,
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      flexDirection: 'row',
                      height: 60,
                    }}>
                    <View
                      style={{
                        justifyContent: 'center',
                        flexDirection: 'column',
                        height: '100%',
                      }}>
                      <Text
                        textAlign="start"
                        style={{fontSize: 20, fontWeight: 'bold'}}>
                        {element.prodName}
                      </Text>
                      <Text textAlign="start" style={{fontSize: 18}}>
                        IDR{' '}
                        {this.state.isSecretBalnce
                          ? (parseInt(element.availBalance) / 100)
                              .toString()
                              .replace(/\B(?=(\d{3})+(?!\d))/g, '.')
                          : 'XXX'}
                      </Text>
                    </View>
                    <Pressable
                      onPress={() => {
                        if (this.state.isSecretBalnce) {
                          this.setState({isSecretBalnce: false});
                        } else {
                          this.setState({isSecretBalnce: true});
                        }
                      }}>
                      <Image
                        tintColor="grey"
                        resizeMode="contain"
                        style={{width: 20, height: 20}}
                        source={this.state.isSecretBalnce ? eyeOpen : eyeClose}
                      />
                    </Pressable>
                  </View>
                );
              }))
            }
          </View>
        </View>
      );
      return render;
    }
  }
  popUpAktivasiVimo() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.popUpAktivasiVimo}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({popUpAktivasiVimo: false});
        }}>
        <View
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View
              style={{
                margin: 100,
                backgroundColor: 'white',
                borderRadius: 20,
                justifyContent: 'space-between',
                padding: 35,
                width: '80%',
                alignItems: 'center',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
              }}>
              <FastImage
                source={logoSuccess}
                style={{width: 55, height: 55, marginBottom: 15}}
              />
              <Text
                style={{
                  marginBottom: 20,
                  fontFamily: 'Helvetica',
                  fontSize: 16,
                  textAlign: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {bahasa === 'id'
                  ? id.redaksi_aktivasi_vimo
                  : en.redaksi_aktivasi_vimo}
              </Text>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <Pressable
                  style={{
                    width: '48%',
                    height: 43,
                    borderRadius: 10,
                    borderColor: COLORS.red,
                    borderWidth: 2,
                    padding: 10,
                    elevation: 2,
                    backgroundColor: 'white',
                  }}
                  onPress={() => {
                    this.sendOtp();
                    this.setState({
                      sessionBeforeOTP: 8,
                      popUpAktivasiVimo: false,
                      session: 9,
                    });
                  }}>
                  <Text
                    style={{
                      alignSelf: 'center',
                      textAlign: 'center',
                      fontFamily: 'Helvetica',
                      fontSize: 15,
                      color: COLORS.red,
                    }}>
                    {bahasa === 'id' ? id.tidak : en.tidak}
                  </Text>
                </Pressable>
                <Pressable
                  style={{
                    width: '48%',
                    height: 43,
                    borderRadius: 10,
                    padding: 10,
                    elevation: 2,
                    backgroundColor: COLORS.red,
                  }}
                  onPress={() => {
                    this.getTNC(24);
                    this.setState({
                      popUpAktivasiVimo: false,
                      session: 34,
                    });
                  }}>
                  <Text
                    style={{
                      alignSelf: 'center',
                      textAlign: 'center',
                      fontFamily: 'Helvetica',
                      fontSize: 15,
                      color: 'white',
                    }}>
                    {bahasa === 'id' ? id.ya : en.ya}
                  </Text>
                </Pressable>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
  onSubmit = formFields => {
    if (this.state.session === 8) {
      //Khusus CIF OPENING
      if (this.state.formCode === '10') {
        if (Object.keys(formFields).length === 0) {
          Alert.alert(
            bahasa === 'id' ? id.silahkan_isi_kolom : en.silahkan_isi_kolom,
          );
        } else {
          if (
            formFields.hasOwnProperty('alamatdomisili') &&
            formFields.alamatdomisili === true
          ) {
            console.log('alamatdomisili', true);
            this.setState({
              formCode: '9',
            });
            //Menuju Form Builder alamat berbeda
            this.formBuilder('9');
          } else {
            this.setState({
              formCode: '5',
            });
            //Menuju Form Builder data pekerjaan
            this.formBuilder('5');
            console.log('alamat_jika_berbeda_dengan_identitas', false);
          }
        }
        console.log('formfield', formFields);
      } else if (this.state.formCode === '9') {
        if (Object.keys(formFields).length === 0) {
          Alert.alert(
            bahasa === 'id' ? id.silahkan_isi_kolom : en.silahkan_isi_kolom,
          );
        } else {
          this.setState({
            formCode: '5',
          });
          //Menuju Form Builder data pekerjaan
          this.formBuilder('5');
        }
      } else if (this.state.formCode === '5') {
        if (Object.keys(formFields).length === 0) {
          Alert.alert(
            bahasa === 'id' ? id.silahkan_isi_kolom : en.silahkan_isi_kolom,
          );
        } else {
          console.log('formField', formFields);
          this.setState({
            formCode: '14',
          });
          //Menuju Form Builder data keuangan
          this.formBuilder('14');
        }
        console.log('formField', formFields);
      } else if (this.state.formCode === '14') {
        if (Object.keys(formFields).length === 0) {
          Alert.alert(
            bahasa === 'id' ? id.silahkan_isi_kolom : en.silahkan_isi_kolom,
          );
        } else {
          console.log('formFields', formFields);
          this.setState({popUpAktivasiVimo: true});
        }
      }
    } else if (this.state.session === 15) {
      console.log('formfields pembukaan akun', formFields);
      if (
        formFields.hasOwnProperty('tanggalpembukaanrekening') &&
        formFields.hasOwnProperty('nomorcif') &&
        formFields.hasOwnProperty('kodecabang') &&
        formFields.hasOwnProperty('namanasabah') &&
        formFields.hasOwnProperty('noidentitas') &&
        formFields.hasOwnProperty('tipetabungan')
      ) {
        var formatBaru = moment(
          moment(formFields.tanggalpembukaanrekening, 'DD-MM-YYYY'),
        ).format('YYYY-MM-DD');
        this.setState({sessionBeforeOTP: this.state.session});
        this.saveForm('OCreate Account', {
          kodecabang: formFields?.kodecabang,
          namanasabah: formFields?.namanasabah,
          noidentitas: formFields?.noidentitas,
          nomorcif: formFields?.nomorcif,
          tanggalpembukaanrekening: formatBaru,
          tipetabungan: formFields?.tipetabungan?.label,
        });
      } else {
        Alert.alert(
          bahasa === 'id' ? id.silahkan_isi_kolom : en.silahkan_isi_kolom,
        );
      }
    } else if (this.state.session === 35) {
      ///Menuju Registrasi Aplikasi 1 IBMB
      this.setState({session: 36});
      this.formBuilder2(65);
    } else if (this.state.session == 36) {
      ///Menuju Registrasi Aplikasi 2 IBMB
      this.setState({session: 37});
      this.formBuilder2(68);
    } else if (this.state.session == 37) {
      this.setState({session: 9, sessionBeforeOTP: this.state.session});
      this.sendOtp();
    }
  };
  async getDataOcr() {
    fetch(Server.BASE_URL_API + ApiService.getOcrKtp + idDips, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      timeout: 1500,
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.code < 300) {
          this.getKodePos(
            responseJson.data.propinsi,
            responseJson.data.kabupaten,
            responseJson.data.kecamatan,
            responseJson.data.kelurahan,
          ).then(kodePos_ => {
            this.setState({
              dataOcr: {
                pilihcabang: {
                  id: 0,
                  label: '',
                  value: '',
                },
                namasesuaiidentitas: responseJson.data.namaLengkap,
                gelar: '',
                alamatdomisili: false,
                notelp: '',
                noponsel: '',
                jumlahtanggungan: '',
                statusrumah: '',
                pendidikan: {
                  id: 0,
                  label: '',
                  value: '',
                },
                email: '',
                npwp: '',
                jeniskelamin: responseJson.data.jenisKelamin,
                alamatsesuaiidentitas: responseJson.data.alamat,
                rt: responseJson.data.rt,
                rw: responseJson.data.rw,
                provinsi: responseJson.data.propinsi,
                'kabupaten/kota': responseJson.data.kabupaten,
                kecamatan: responseJson.data.kecamatan,
                'kelurahan/desa': responseJson.data.kelurahan,
                tempatlahir: responseJson.data.tempatlahir,
                tanggallahir: responseJson.data.tglLahir,
                agama: responseJson.data.agama,
                kewarganegaraan: responseJson.data.kewarganegaraan,
                jenisidentitas: 'KTP',
                noidentitas: responseJson.data.nik,
                tanggalterbit: bahasa === 'id' ? 'Seumur Hidup' : 'Lifetime',
                statusmenikah: responseJson.data.statusKawin.replace(
                  'KAWIN',
                  'MENIKAH',
                ),
                kodepos: kodePos_,
                negara: 'INDONESIA',
                namaibukandung: responseJson.data.namaLengkapIbu,
              },
            });
          });
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        console.error(error);
        Alert.alert('Gagal Terhubung ke server');
      });
  }
  getKodePos(prov, kab, kec, kel) {
    return new Promise((result, reject) => {
      var payload = {
        kelurahan: kel,
        kecamatan: kec,
        kabupaten: kab,
        provinsi: prov,
      };
      fetch(Server.BASE_URL_API + ApiService.kodePos, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + accessToken,
          exchangeToken: exchangeToken,
        },
        timeout: 1500,
        body: JSON.stringify(payload),
      })
        .then(response => response.json())
        .then(responseJson => {
          if (responseJson.code < 300) {
            console.log('Kode Pos', responseJson.data.kodepos);
            result(responseJson.data.kodepos);
          } else {
            result('');
          }
        })
        .catch(error => {
          Alert.alert('Gagal Terhubung ke server', error);
          reject('Gagal terhubung ke server');
        });
    });
  }
  onSubmitTransaction = formFields => {
    if (this.state.showFormBuilderQr) {
      arrayBarcode[this.state.currentPage - 1] = formFields;
      console.log('arrayBarcode', arrayBarcode);
    }

    //Untuk cek transaksi sesuai apa tidak
    if (this.state.session === 17) {
      //Transaksi Rekening Sendiri
      this.loopcekTransaksi();
      this.loopcekMandatoryFieldRekeningSendiri();
    } else if (this.state.session === 16) {
      //Transaksi Antar Rekening
      this.loopcekTransaksi();
    } else if (this.state.session === 14) {
      //Transaksi Antar Bank
      this.loopcekTransaksi();
    }

    if (
      this.state.session === 19 ||
      this.state.session === 15 ||
      this.state.session === 28
    ) {
      console.log('formFields', formFields);
      arrayBarcode[0] = formFields;
      if (this.state.session === 19) {
        if (
          arrayBarcode[0].hasOwnProperty('jangkawaktu') &&
          arrayBarcode[0].hasOwnProperty('matauang') &&
          arrayBarcode[0].hasOwnProperty('jenispenduduk') &&
          arrayBarcode[0].hasOwnProperty('nominaldeposito')
        ) {
          this.depositType(
            arrayBarcode[0]?.jangkawaktu?.value,
            arrayBarcode[0]?.matauang?.value,
            arrayBarcode[0]?.jenispenduduk,
            parseInt(arrayBarcode[0]?.nominaldeposito.replace(/\./g, ''), 10),
          );
        }
      } else if (this.state.session === 15) {
        this.mirroringKey('pembukaanakun', {
          datautama: {
            gelarnasabah:
              formFields?.gelarnasabah === undefined
                ? ''
                : formFields?.gelarnasabah,
            kodecabang:
              formFields?.kodecabang === undefined
                ? ''
                : formFields?.kodecabang,
            namanasabah:
              formFields?.namanasabah === undefined
                ? ''
                : formFields?.namanasabah,
            namasingkatan:
              formFields?.namasingkatan === undefined
                ? ''
                : formFields?.namasingkatan,
            noidentitas:
              formFields?.noidentitas === undefined
                ? ''
                : formFields?.noidentitas,
            nomorcif:
              formFields?.nomorcif === undefined ? '' : formFields?.nomorcif,
            tanggalpembukaanrekening:
              formFields?.tanggalpembukaanrekening === undefined
                ? ''
                : formFields?.tanggalpembukaanrekening,
            tipetabungan:
              formFields?.tipetabungan?.label === undefined
                ? ''
                : formFields?.tipetabungan?.label,
          },
        });
      }
    }
  };
  async depositType(bulan, matauang, tipependuduk, amount) {
    console.log('masuk depositType');
    var payload = {
      months: bulan,
      currency: matauang,
      chanel: 'IBMB',
      resident: tipependuduk,
    };
    if (
      JSON.stringify(payload) !== JSON.stringify(this.state.previousPayload)
    ) {
      console.log('payload depositType', payload);
      fetch(Server.BASE_URL_API + ApiService.depositType, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + accessToken,
          exchangeToken: exchangeToken,
        },
        timeout: 1500,
        body: JSON.stringify(payload),
      })
        .then(response => response.json())
        .then(responseJson => {
          console.log('Response depositType', responseJson);
          if (responseJson.code < 300) {
            this.getInteresetDeposito(responseJson.data.depoCode, amount);
          } else {
            this.setState({interesetDeposito: 0});
          }
        })
        .catch(error => {
          Alert.alert('Gagal Terhubung ke server', error);
        });
      this.setState({previousPayload: payload});
    }
  }
  async getInteresetDeposito(depocode, amount_) {
    var payload = {
      depoCode: depocode,
      amount: amount_,
    };
    console.log('payload getInteresetDeposito', payload);
    fetch(Server.BASE_URL_API + ApiService.getBungaDidapat, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: JSON.stringify(payload),
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log('Response getInteresetDeposito', responseJson);
        if (responseJson.code < 300) {
          this.setState({
            interesetDeposito: responseJson.data,
            depoCode_: depocode,
          });
        } else {
          this.setState({interesetDeposito: 0});
        }
      })
      .catch(error => {
        Alert.alert('Gagal Terhubung ke server', error);
      });
  }
  view_cif_new() {
    if (
      this.state.session == 4 ||
      this.state.session == 6 ||
      this.state.session == 7
    ) {
      return (
        <View
          style={{
            marginTop: 5,
            marginStart: 30,
            marginEnd: 30,
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'column',
          }}>
          <Text
            style={{
              margin: 20,
              textAlign: 'center',
              fontSize: 16,
              marginBottom: 10,
              color: COLORS.textColorGray,
            }}>
            {this.state.session == 4
              ? bahasa === 'id'
                ? id.headline_foto_ktp
                : en.headline_foto_ktp
              : this.state.session == 6
              ? bahasa === 'id'
                ? id.headline_foto_npwp
                : en.headline_foto_npwp
              : bahasa === 'id'
              ? id.headline_foto_ttd
              : en.headline_foto_ttd}
          </Text>
          {this.state.session == 4 && fileUriKTP == '' ? (
            <View
              style={{
                flexDirection: 'column',
                alignItems: 'center',
                marginStart: 10,
                marginTop: 10,
                marginEnd: 10,
                width: '100%',
                justifyContent: 'center',
                height: '65%',
              }}>
              <FastImage
                transition={false}
                resizeMode="stretch"
                style={{
                  marginStart: 10,
                  marginEnd: 10,
                  height: this.state.session == 6 ? '85%' : '100%',
                  marginBottom: this.state.session == 6 ? 0 : 20,
                  justifyContent: 'center',
                }}
                source={bgPickImage}>
                <View
                  style={{
                    padding: 50,
                    justifyContent: 'center',
                    flexDirection: 'column',
                    alignItems: 'center',
                  }}>
                  <Pressable
                    onPress={() => {
                      this.setState({openCamera: true});
                    }}>
                    <FastImage
                      source={pickCamera}
                      style={{
                        marginBottom: 20,
                        width: 60,
                        height: 60,
                      }}
                    />
                  </Pressable>
                  <View
                    style={{
                      alignItems: 'center',
                      justifyContent: 'center',
                      flexDirection: 'row',
                      width: '33%',
                      height: '20%',
                    }}>
                    <View
                      style={{
                        marginStart: 10,
                        marginEnd: 10,
                        backgroundColor: COLORS.textColorGray,
                        width: '100%',
                        height: '2.5%',
                      }}></View>
                    <Text
                      style={{
                        fontFamily: 'Helvetica',
                        color: COLORS.textColorGray,
                        fontSize: 17,
                      }}>
                      {bahasa === 'id' ? id.atau : en.atau}
                    </Text>
                    <View
                      style={{
                        marginEnd: 10,
                        marginStart: 10,
                        backgroundColor: COLORS.textColorGray,
                        width: '100%',
                        height: '2.5%',
                      }}></View>
                  </View>
                  <Pressable
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                    onPress={() => {
                      this.launchImageLibraryfunc();
                    }}>
                    <View
                      style={{
                        marginTop: 20,
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                        width: '100%',
                      }}>
                      <FastImage
                        source={pickGallery}
                        style={{
                          marginEnd: 8,
                          width: 26,
                          height: 26,
                        }}
                      />
                      <Text
                        style={{
                          fontFamily: 'Helvetica',
                          color: COLORS.textColorGray,
                          fontSize: 15,
                        }}>
                        {bahasa === 'id'
                          ? id.pilih_dari_galeri
                          : en.pilih_dari_galeri}
                      </Text>
                    </View>
                  </Pressable>
                </View>
              </FastImage>
            </View>
          ) : this.state.session == 6 && fileUriNPWP == '' ? (
            <View
              style={{
                flexDirection: 'column',
                alignItems: 'center',
                marginStart: 10,
                marginTop: 20,
                marginEnd: 10,
                width: '100%',
                justifyContent: 'center',
                height: '65%',
              }}>
              <FastImage
                transition={false}
                resizeMode="stretch"
                style={{
                  marginStart: 10,
                  marginEnd: 10,
                  height: this.state.session == 6 ? '85%' : '100%',
                  marginBottom: this.state.session == 6 ? 10 : 20,
                  justifyContent: 'center',
                }}
                source={bgPickImage}>
                <View
                  style={{
                    padding: 50,
                    justifyContent: 'center',
                    flexDirection: 'column',
                    alignItems: 'center',
                  }}>
                  <Pressable
                    onPress={() => {
                      this.setState({openCamera: true});
                    }}>
                    <FastImage
                      source={pickCamera}
                      style={{
                        marginBottom: 20,
                        width: 60,
                        height: 60,
                      }}
                    />
                  </Pressable>
                  <View
                    style={{
                      alignItems: 'center',
                      justifyContent: 'center',
                      flexDirection: 'row',
                      width: '33%',
                      height: '20%',
                    }}>
                    <View
                      style={{
                        marginStart: 10,
                        marginEnd: 10,
                        backgroundColor: COLORS.textColorGray,
                        width: '100%',
                        height: '2.5%',
                      }}></View>
                    <Text
                      style={{
                        fontFamily: 'Helvetica',
                        color: COLORS.textColorGray,
                        fontSize: 17,
                      }}>
                      {bahasa === 'id' ? id.atau : en.atau}
                    </Text>
                    <View
                      style={{
                        marginEnd: 10,
                        marginStart: 10,
                        backgroundColor: COLORS.textColorGray,
                        width: '100%',
                        height: '2.5%',
                      }}></View>
                  </View>
                  <Pressable
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                    onPress={() => {
                      this.launchImageLibraryfunc();
                    }}>
                    <View
                      style={{
                        marginTop: 20,
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                        width: '100%',
                      }}>
                      <FastImage
                        source={pickGallery}
                        style={{
                          marginEnd: 8,
                          width: 26,
                          height: 26,
                        }}
                      />
                      <Text
                        style={{
                          fontFamily: 'Helvetica',
                          color: COLORS.textColorGray,
                          fontSize: 15,
                        }}>
                        {bahasa === 'id'
                          ? id.pilih_dari_galeri
                          : en.pilih_dari_galeri}
                      </Text>
                    </View>
                  </Pressable>
                </View>
              </FastImage>
              <View
                style={{
                  width: '80%',
                  marginBottom: this.state.session == 6 ? 10 : 0,
                  justifyContent: 'space-between',
                  flexDirection: 'row',
                }}>
                <Text
                  style={{
                    fontSize: 16,
                    fontFamily: 'Helvetica',
                    alignSelf: 'center',
                  }}>
                  {bahasa === 'id' ? id.lewat_npwp : en.lewat_npwp}
                </Text>
                <Button
                  style={{
                    alignItems: 'flex-end',
                    height: 45,
                    borderRadius: 7,
                    borderColor: COLORS.red,
                    borderWidth: 2,
                  }}
                  labelStyle={styles.btnLabel}
                  buttonColor={COLORS.red}
                  mode="contained"
                  onPress={() => {
                    this.setState({session: 7});
                  }}>
                  {bahasa === 'id' ? id.lanjut : en.lanjut}
                </Button>
              </View>
            </View>
          ) : this.state.session == 7 && fileUriTTD == '' ? (
            <View
              style={{
                flexDirection: 'column',
                alignItems: 'center',
                marginStart: 10,
                marginTop: 10,
                marginEnd: 10,
                width: '100%',
                justifyContent: 'center',
                height: '65%',
              }}>
              <FastImage
                transition={false}
                resizeMode="stretch"
                style={{
                  marginStart: 10,
                  marginEnd: 10,
                  height: this.state.session == 6 ? '85%' : '100%',
                  marginBottom: this.state.session == 6 ? 0 : 20,
                  justifyContent: 'center',
                }}
                source={bgPickImage}>
                <View
                  style={{
                    padding: 50,
                    justifyContent: 'center',
                    flexDirection: 'column',
                    alignItems: 'center',
                  }}>
                  <Pressable
                    onPress={() => {
                      this.setState({openCamera: true});
                    }}>
                    <FastImage
                      source={pickCamera}
                      style={{
                        marginBottom: 20,
                        width: 60,
                        height: 60,
                      }}
                    />
                  </Pressable>
                  <View
                    style={{
                      alignItems: 'center',
                      justifyContent: 'center',
                      flexDirection: 'row',
                      width: '33%',
                      height: '20%',
                    }}>
                    <View
                      style={{
                        marginStart: 10,
                        marginEnd: 10,
                        backgroundColor: COLORS.textColorGray,
                        width: '100%',
                        height: '2.5%',
                      }}></View>
                    <Text
                      style={{
                        fontFamily: 'Helvetica',
                        color: COLORS.textColorGray,
                        fontSize: 17,
                      }}>
                      {bahasa === 'id' ? id.atau : en.atau}
                    </Text>
                    <View
                      style={{
                        marginEnd: 10,
                        marginStart: 10,
                        backgroundColor: COLORS.textColorGray,
                        width: '100%',
                        height: '2.5%',
                      }}></View>
                  </View>
                  <Pressable
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                    onPress={() => {
                      this.launchImageLibraryfunc();
                    }}>
                    <View
                      style={{
                        marginTop: 20,
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                        width: '100%',
                      }}>
                      <FastImage
                        source={pickGallery}
                        style={{
                          marginEnd: 8,
                          width: 26,
                          height: 26,
                        }}
                      />
                      <Text
                        style={{
                          fontFamily: 'Helvetica',
                          color: COLORS.textColorGray,
                          fontSize: 15,
                        }}>
                        {bahasa === 'id'
                          ? id.pilih_dari_galeri
                          : en.pilih_dari_galeri}
                      </Text>
                    </View>
                  </Pressable>
                </View>
              </FastImage>
            </View>
          ) : (
            <View
              style={{
                flexDirection: 'column',
                alignItems: 'center',
                marginStart: 10,
                marginEnd: 10,
                marginTop: 25,
                width: '100%',
                justifyContent: 'center',
                height: '60%',
              }}>
              <FastImage
                transition={false}
                resizeMode="stretch"
                style={{
                  marginTop: 10,
                  marginStart: 10,
                  marginEnd: 10,
                  height: '100%',
                  marginBottom: this.state.session == 6 ? 10 : 20,
                  justifyContent: 'center',
                }}
                source={bgPickImage}>
                <View
                  style={{
                    alignSelf: 'center',
                    padding: 20,
                    width: '100%',
                    height: '100%',
                  }}>
                  <Image
                    resizeMode="contain"
                    style={{
                      width: 225,
                      height: 125,
                    }}
                    source={{
                      uri:
                        this.state.session == 4
                          ? fileUriKTP
                          : this.state.session == 6
                          ? fileUriNPWP
                          : fileUriTTD,
                      priority: FastImage.priority.normal,
                    }}
                  />
                </View>
              </FastImage>
              <View
                style={{
                  width: '80%',
                  justifyContent: 'space-between',
                  flexDirection: 'row',
                }}>
                <Button
                  icon={() => (
                    <Image
                      transition={false}
                      source={delete_icon}
                      style={{
                        tintColor: COLORS.red,
                        justifyContent: 'center',
                        alignItems: 'center',
                        alignContent: 'center',
                        alignSelf: 'center',
                        width: 20,
                        height: 20,
                        resizeMode: 'contain',
                      }}
                    />
                  )}
                  style={[
                    styles.btn,
                    {
                      width: 'auto',
                      alignSelf: 'center',
                    },
                  ]}
                  onPress={() => {
                    if (this.state.session == 4) {
                      fileUriKTP = '';
                      base64KTP = '';
                      this.setState({session: 4});
                    } else if (this.state.session == 6) {
                      fileUriNPWP = '';
                      base64NPWP = '';
                      this.setState({session: 6});
                    } else if (this.state.session == 7) {
                      fileUriTTD = '';
                      base64TTD = '';
                      this.setState({session: 7});
                    }
                  }}
                  textColor={COLORS.red}
                  labelStyle={styles.btnLabel}
                  buttonColor={COLORS.white}
                  mode="contained">
                  {bahasa === 'id' ? id.delete : en.delete}
                </Button>
                <Button
                  style={{
                    justifyContent: 'center',
                    height: 45,
                    borderRadius: 7,
                    borderColor: COLORS.red,
                    borderWidth: 2,
                  }}
                  labelStyle={styles.btnLabel}
                  buttonColor={COLORS.red}
                  mode="contained"
                  onPress={() => {
                    if (this.state.session == 4) {
                      this.processOCR();
                    } else if (this.state.session == 6) {
                      this.processOCRNPWP();
                      this.processAttachment('npwp', fileUriNPWP);
                    } else if (this.state.session == 7) {
                      this.processAttachment('ttd', fileUriTTD);
                    }
                  }}>
                  {bahasa === 'id' ? id.lanjut : en.lanjut}
                </Button>
              </View>
            </View>
          )}
        </View>
      );
    } else if (this.state.session == 5) {
      return (
        <View
          style={{
            marginTop: 15,
            marginStart: 40,
            marginEnd: 40,
            borderRadius: 15,
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'column',
          }}>
          <View
            style={{
              width: '100%',
            }}>
            <Button
              onPress={() => {
                this.setState({openCamera: true});
              }}
              style={[
                styles.btn,
                {
                  margin: 10,
                  display: dataresp.length === 0 ? 'none' : 'flex',
                },
              ]}
              labelStyle={styles.btnLabel}
              buttonColor={COLORS.red}
              mode="contained">
              {bahasa == 'id'
                ? id.title_foto_diri_dengan_ktp
                : id.title_foto_diri_dengan_ktp}
            </Button>
            <Text
              style={{
                display: dataresp.length === 0 ? 'none' : 'flex',
                marginStart: 10,
                marginEnd: 10,
                marginBottom: 15,
              }}>
              {bahasa === 'id'
                ? '*' +
                  id.title_foto_diri_dengan_ktp +
                  ' untuk memperbaharui data'
                : '*' + en.title_foto_diri_dengan_ktp + ' for update data'}
            </Text>
            <DynamicForm
              bahasa={bahasa}
              defaultValue={{
                namasesuaiidentitas: nama_lengkap_,
                alamatsesuaiidentitas: alamat_,
                rt: rt_rw_?.split('/')[0]?.trim(),
                rw: rt_rw_?.split('/')[1]?.trim(),
                kelurahan: kelurahan_desa_,
                kecamatan: kecamatan_,
                ['kabupaten/kota']: kabupaten_kota_,
                provinsi: provinsi_,
                kewarganegaraan: kewarganegaraan_,
                negara: 'INDONESIA',
                jenisbuktiidentitas: 'KTP',
                jeniskelamin: jenis_kelamin_,
                agama: agama_,
                statusperkawinan: status_perkawinan_,
                tanggalberakhiridentitas:
                  bahasa === 'id' ? 'Seumur Hidup' : 'Lifetime',
              }}
              accessToken={accessToken}
              exchangeToken={exchangeToken}
              formTemplate={{data: dataresp}}
              onSubmit={() => {}}
              onPressed={() => {}}
              btnStyle={{
                display: 'none',
              }}
            />
            <CheckBox
              label={bahasa == 'id' ? id.data_sudah_benar : en.data_sudah_benar}
              status={this.state.isChecked ? 'checked' : 'unchecked'}
              onPress={() => {
                if (this.state.isChecked) {
                  this.setState({
                    isChecked: false,
                  });
                } else {
                  this.setState({isChecked: true});
                }
              }}
              containerStyle={{
                marginBottom: 10,
                display: dataresp.length > 0 ? 'flex' : 'none',
              }}
              labelStyle={{fontSize: 16}}
              boxColor={COLORS.red}
            />
            <Button
              onPress={() => {
                if (this.state.isChecked) {
                  this.setState({session: 6, isChecked: false});
                }
              }}
              style={[
                {
                  display: dataresp.length === 0 ? 'none' : 'flex',
                  width: 'auto',
                  borderRadius: 12,
                  marginEnd: 10,
                  marginBottom: 30,
                  borderColor: this.state.isChecked
                    ? COLORS.red
                    : COLORS.border_textInput,
                  borderWidth: 2,
                  alignSelf: 'flex-end',
                },
              ]}
              labelStyle={styles.btnLabel}
              buttonColor={
                this.state.isChecked ? COLORS.red : COLORS.border_textInput
              }
              mode="contained">
              {bahasa == 'id' ? id.lanjut : en.lanjut}
            </Button>
          </View>
        </View>
      );
    } else if (this.state.session == 8) {
      return (
        <View
          style={{
            marginTop: 20,
            marginStart: 20,
            marginEnd: 20,
            padding: 20,
            height: '100%',
            flexDirection: 'column',
          }}>
          <Text
            style={{
              marginStart: 10,
              fontSize: 20,
              marginBottom: 10,
              fontWeight: 'bold',
              color: COLORS.textColorGray,
            }}>
            {this.state.formCode === '10'
              ? bahasa === 'id'
                ? id.data_diri
                : en.data_diri
              : this.state.formCode === '5'
              ? bahasa === 'id'
                ? id.data_pekerjaan
                : en.data_pekerjaan
              : this.state.formCode === '14'
              ? bahasa === 'id'
                ? id.data_keuangan
                : en.data_keuangan
              : this.state.formCode === '9'
              ? bahasa === 'id'
                ? id.data_diri
                : en.data_diri
              : ''}
          </Text>
          <DynamicForm
            bahasa={bahasa}
            defaultValue={
              this.state.formCode === '10' ? this.state.dataOcr : {}
            }
            accessToken={accessToken}
            exchangeToken={exchangeToken}
            formTemplate={{data: dataresp}}
            onSubmit={this.onSubmit}
            onPressed={() => {}}
            btnStyle={{
              display: dataresp.length === 0 ? 'none' : 'flex',
              borderRadius: 10,
              backgroundColor: COLORS.red,
              top: 20,
              marginEnd: 10,
              width: 'auto',
              alignSelf: 'flex-end',
            }}
            btnLabel={bahasa === 'id' ? id.lanjut : en.lanjut}
          />
        </View>
      );
    }
  }
  popUpTNC() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.modalTNC}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({modalTNC: false});
        }}>
        <View
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              marginTop: 0,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                margin: 100,
                backgroundColor: 'white',
                borderRadius: 20,
                justifyContent: 'center',
                padding: 20,
                width: '80%',
                height: '70%',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                alignItems: 'center',
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
              }}>
              <ScrollView
                style={{
                  width: '97%',
                  height: '60%',
                }}
                keyboardDismissMode="interactive"
                automaticallyAdjustKeyboardInsets={true}
                contentContainerStyle={{
                  alignItems: 'center',
                }}>
                <Text
                  style={{fontSize: 22, fontWeight: 'bold', marginBottom: 20}}>
                  {bahasa === 'id'
                    ? id.syarat_dan_ketentuan
                    : en.syarat_dan_ketentuan}
                </Text>
                <RenderHtml
                  contentWidth={300}
                  source={{html: this.state.htmlTNC}}
                  style={{
                    width: '100%',
                  }}
                />
                <CheckBox
                  label={bahasa == 'id' ? id.tnc_check : en.tnc_check}
                  status={this.state.isChecked ? 'checked' : 'unchecked'}
                  onPress={() => {
                    if (this.state.isChecked) {
                      this.setState({isChecked: false});
                      this.mirroringKey(
                        this.state.isOpeningAccount ? 'pembukaanakun' : 'tnc1',
                        this.state.isOpeningAccount
                          ? {
                              datautama: {},
                              tnc35: false,
                            }
                          : false,
                      );
                    } else {
                      this.setState({isChecked: true});
                      this.mirroringKey(
                        this.state.isOpeningAccount ? 'pembukaanakun' : 'tnc1',
                        this.state.isOpeningAccount
                          ? {
                              datautama: {},
                              tnc35: true,
                            }
                          : true,
                      );
                    }
                  }}
                  containerStyle={{
                    marginBottom: 10,
                  }}
                  labelStyle={{fontSize: 14}}
                  boxColor={COLORS.red}
                />
                <Pressable
                  style={{
                    width: '40%',
                    height: 43,
                    borderRadius: 10,
                    padding: 10,
                    elevation: 2,
                    backgroundColor: this.state.isChecked
                      ? COLORS.red
                      : COLORS.btnFalse,
                  }}
                  onPress={() => {
                    if (this.state.isChecked) {
                      if (this.state.isOpeningAccount) {
                        this.setState({
                          session: 15,
                          modalTNC: false,
                          formCode: '19',
                          isChecked: false,
                        });
                        this.mirroringEndpoint(150);
                        this.formBuilder('19');
                      } else {
                        if (this.state.session == 28) {
                          this.requestSID({
                            nik: nik_,
                            noCif: noCIF,
                            investmentGoal:
                              arrayBarcode[0]?.[
                                'objektif/tujuaninvestasi'
                              ].valueCode.toString(),
                            souceOfFund:
                              arrayBarcode[0]?.sumberpendapatan.valueCode.toString(),
                          });
                          this.setState({
                            modalTNC: false,
                            session: 31,
                            isChecked: false,
                          });
                          setTimeout(() => {
                            this.setState({modalSuccesReksaDana: true});
                          }, 1500);
                        } else {
                          this.setState({
                            modalTNC: false,
                            session: 5,
                            formCode: '72',
                            isChecked: false,
                          });
                          this.formBuilder('72');
                          this.getDataOcr();
                          this.mirroringEndpoint(4);
                        }
                      }
                    }
                  }}>
                  <Text style={styles.textbtn}>
                    {bahasa === 'id' ? id.lanjut : en.lanjut}
                  </Text>
                </Pressable>
              </ScrollView>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
  popUpKeluar() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.isDecline}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({isDecline: false});
        }}>
        <View
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View
              style={{
                margin: 100,
                backgroundColor: 'white',
                borderRadius: 20,
                justifyContent: 'space-between',
                padding: 35,
                width: '80%',
                alignItems: 'center',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
              }}>
              <FastImage
                source={logoInfo}
                style={{width: 55, height: 55, marginBottom: 15}}
              />
              <Text
                style={{
                  marginBottom: 20,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  textAlign: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {bahasa === 'id' ? id.konten_tolak : en.konten_tolak}
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  width: '100%',
                  justifyContent: 'center',
                }}>
                <Button
                  textColor={COLORS.red}
                  labelStyle={styles.btnLabel}
                  style={[styles.btn, {marginEnd: 10}]}
                  mode="outlined"
                  onPress={() => this.setState({isDecline: false})}>
                  {bahasa == 'id' ? id.tidak : en.tidak}
                </Button>
                <Button
                  style={styles.btn}
                  labelStyle={styles.btnLabel}
                  buttonColor={COLORS.red}
                  mode="contained"
                  onPress={() => {
                    this.mirroringEndpoint(99);
                    this.setState({isDecline: false});
                    this.leaveSession(false);

                    // RNExitApp.exitApp();
                  }}>
                  {bahasa == 'id' ? id.ya : en.ya}
                </Button>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
  popUpOCR() {
    const showDatePicker = () => {
      this.setState({isDatePickerVisible: true});
    };
    const hideDatePicker = () => {
      this.setState({isDatePickerVisible: false});
    };
    const handleConfirm = date => {
      const NewDate = moment(date).format('DD-MM-YYYY');
      tgl_lahir_ = NewDate;
      hideDatePicker();
    };
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.modalOCR}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({modalOCR: false});
        }}>
        <View
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              marginTop: 0,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                margin: 100,
                backgroundColor: 'white',
                borderRadius: 20,
                justifyContent: 'center',
                padding: 20,
                width: '80%',
                height: '70%',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                alignItems: 'center',
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
              }}>
              <ScrollView
                style={{
                  width: '95%',
                }}
                keyboardDismissMode="interactive"
                automaticallyAdjustKeyboardInsets={true}
                contentContainerStyle={{
                  alignItems: 'center',
                }}>
                <FastImage
                  source={logoInfo}
                  style={{width: 55, height: 55, marginBottom: 15}}
                />
                <Text
                  style={{
                    marginBottom: 15,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    textAlign: 'left',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  {bahasa === 'id'
                    ? id.apakah_data_anda_sesuai
                    : en.apakah_data_anda_sesuai}
                </Text>
                <Text
                  style={{
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    fontWeight: 'bold',
                  }}>
                  {bahasa === 'id' ? id.nik : en.nik}
                </Text>
                <TextInput
                  style={{
                    marginBottom: 10,
                    width: '100%',
                    height: 50,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 1.5,
                    borderRadius: 10,
                    backgroundColor: 'white',
                    paddingLeft: 10,
                    fontSize: 16,
                  }}
                  onChangeText={value => (nik_ = value)}
                  defaultValue={nik_}
                  maxLength={16}
                  underlineColorAndroid="transparent"
                  keyboardType="numeric"
                />
                <Text
                  style={{
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    fontWeight: 'bold',
                  }}>
                  {bahasa === 'id' ? id.nama_lengkap : en.nama_lengkap}
                </Text>
                <TextInput
                  style={{
                    marginBottom: 10,
                    width: '100%',
                    height: 50,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 1.5,
                    borderRadius: 10,
                    backgroundColor: 'white',
                    paddingLeft: 10,
                    fontSize: 16,
                  }}
                  onChangeText={value => (nama_lengkap_ = value)}
                  autoCapitalize="characters"
                  defaultValue={nama_lengkap_}
                  underlineColorAndroid="transparent"
                  keyboardType="default"
                />
                <Text
                  style={{
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    fontWeight: 'bold',
                  }}>
                  {bahasa === 'id' ? id.tempat_lahir : en.tempat_lahir}
                </Text>
                <TextInput
                  style={{
                    marginBottom: 10,
                    width: '100%',
                    height: 50,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 1.5,
                    borderRadius: 10,
                    backgroundColor: 'white',
                    paddingLeft: 10,
                    fontSize: 16,
                  }}
                  autoCapitalize="characters"
                  defaultValue={tempat_lahir_}
                  onChangeText={value => (tempat_lahir_ = value)}
                  underlineColorAndroid="transparent"
                  keyboardType="default"
                />
                <Text
                  style={{
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    fontWeight: 'bold',
                  }}>
                  {bahasa === 'id' ? id.tgl_lahir : en.tgl_lahir}
                </Text>
                <TouchableOpacity
                  onPress={showDatePicker}
                  style={{
                    justifyContent: 'center',
                    borderRadius: 10,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 1.5,
                    marginBottom: 10,
                    width: '100%',
                    height: 50,
                    padding: 10,
                  }}>
                  <View style={{flexDirection: 'row'}}>
                    <Text
                      style={[
                        ,
                        {
                          alignSelf: 'flex-start',
                          textAlign: 'left',
                          fontFamily: 'Helvetica',
                          fontSize: 17,
                        },
                      ]}>
                      {tgl_lahir_}
                    </Text>
                  </View>
                </TouchableOpacity>
                <DateTimePickerModal
                  display="inline"
                  date={new Date()}
                  locale="id_ID"
                  accentColor={COLORS.red}
                  buttonTextColorIOS={COLORS.red}
                  cancelTextIOS={bahasa === 'id' ? id.batal : en.batal}
                  confirmTextIOS={bahasa === 'id' ? id.pilih : en.pilih}
                  isVisible={this.state.isDatePickerVisible}
                  mode="date"
                  onConfirm={handleConfirm}
                  onCancel={hideDatePicker}
                />
                <Text
                  style={{
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    fontWeight: 'bold',
                  }}>
                  {bahasa === 'id' ? id.jenis_kelamin : en.jenis_kelamin}
                </Text>
                <TextInput
                  style={{
                    marginBottom: 10,
                    width: '100%',
                    height: 50,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 1.5,
                    borderRadius: 10,
                    backgroundColor: 'white',
                    paddingLeft: 10,
                    fontSize: 16,
                  }}
                  autoCapitalize="characters"
                  defaultValue={jenis_kelamin_}
                  onChangeText={value => (jenis_kelamin_ = value)}
                  underlineColorAndroid="transparent"
                  keyboardType="default"
                />
                <Text
                  style={{
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    fontWeight: 'bold',
                  }}>
                  {bahasa === 'id' ? id.golongan_darah : en.golongan_darah}
                </Text>
                <TextInput
                  style={{
                    marginBottom: 10,
                    width: '100%',
                    height: 50,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 1.5,
                    borderRadius: 10,
                    backgroundColor: 'white',
                    paddingLeft: 10,
                    fontSize: 16,
                  }}
                  autoCapitalize="characters"
                  defaultValue={golongan_darah_}
                  onChangeText={value => (golongan_darah_ = value)}
                  underlineColorAndroid="transparent"
                  keyboardType="default"
                />
                <Text
                  style={{
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    fontWeight: 'bold',
                  }}>
                  {bahasa === 'id' ? id.alamat : en.alamat}
                </Text>
                <TextInput
                  style={{
                    marginBottom: 10,
                    width: '100%',
                    height: 80,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 1.5,
                    borderRadius: 10,
                    backgroundColor: 'white',
                    paddingLeft: 10,
                    fontSize: 16,
                  }}
                  defaultValue={alamat_}
                  onChangeText={value => (alamat_ = value)}
                  multiline={true}
                  numberOfLines={3}
                  autoCapitalize="characters"
                  underlineColorAndroid="transparent"
                  keyboardType="default"
                />
                <Text
                  style={{
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    fontWeight: 'bold',
                  }}>
                  {bahasa === 'id' ? id.rt_rw : en.rt_rw}
                </Text>
                <TextInput
                  style={{
                    marginBottom: 10,
                    width: '100%',
                    height: 50,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 1.5,
                    borderRadius: 10,
                    backgroundColor: 'white',
                    paddingLeft: 10,
                    fontSize: 16,
                  }}
                  onChangeText={value => (rt_rw_ = value)}
                  defaultValue={rt_rw_}
                  underlineColorAndroid="transparent"
                  keyboardType="default"
                />
                <Text
                  style={{
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    fontWeight: 'bold',
                  }}>
                  {bahasa === 'id' ? id.provinsi : en.provinsi}
                </Text>
                <TextInput
                  style={{
                    marginBottom: 10,
                    width: '100%',
                    height: 50,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 1.5,
                    borderRadius: 10,
                    backgroundColor: 'white',
                    paddingLeft: 10,
                    fontSize: 16,
                  }}
                  onChangeText={value => (provinsi_ = value)}
                  autoCapitalize="characters"
                  defaultValue={provinsi_}
                  underlineColorAndroid="transparent"
                  keyboardType="default"
                />
                <Text
                  style={{
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    fontWeight: 'bold',
                  }}>
                  {bahasa === 'id' ? id.kabupaten_kota : en.kabupaten_kota}
                </Text>
                <TextInput
                  style={{
                    marginBottom: 10,
                    width: '100%',
                    height: 50,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 1.5,
                    borderRadius: 10,
                    backgroundColor: 'white',
                    paddingLeft: 10,
                    fontSize: 16,
                  }}
                  onChangeText={value => (kabupaten_kota_ = value)}
                  autoCapitalize="characters"
                  defaultValue={kabupaten_kota_}
                  underlineColorAndroid="transparent"
                  keyboardType="default"
                />
                <Text
                  style={{
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    fontWeight: 'bold',
                  }}>
                  {bahasa === 'id' ? id.kecamatan : en.kecamatan}
                </Text>
                <TextInput
                  style={{
                    marginBottom: 10,
                    width: '100%',
                    height: 50,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 1.5,
                    borderRadius: 10,
                    backgroundColor: 'white',
                    paddingLeft: 10,
                    fontSize: 16,
                  }}
                  onChangeText={value => (kecamatan_ = value)}
                  autoCapitalize="characters"
                  defaultValue={kecamatan_}
                  underlineColorAndroid="transparent"
                  keyboardType="default"
                />
                <Text
                  style={{
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    fontWeight: 'bold',
                  }}>
                  {bahasa === 'id' ? id.kelurahan_desa : en.kelurahan_desa}
                </Text>
                <TextInput
                  style={{
                    marginBottom: 10,
                    width: '100%',
                    height: 50,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 1.5,
                    borderRadius: 10,
                    backgroundColor: 'white',
                    paddingLeft: 10,
                    fontSize: 16,
                  }}
                  onChangeText={value => (kelurahan_desa_ = value)}
                  autoCapitalize="characters"
                  defaultValue={kelurahan_desa_}
                  underlineColorAndroid="transparent"
                  keyboardType="default"
                />
                <Text
                  style={{
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    fontWeight: 'bold',
                  }}>
                  {bahasa === 'id' ? id.agama : en.agama}
                </Text>
                <TextInput
                  style={{
                    marginBottom: 10,
                    width: '100%',
                    height: 50,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 1.5,
                    borderRadius: 10,
                    backgroundColor: 'white',
                    paddingLeft: 10,
                    fontSize: 16,
                  }}
                  onChangeText={value => (agama_ = value)}
                  autoCapitalize="characters"
                  defaultValue={agama_}
                  underlineColorAndroid="transparent"
                  keyboardType="default"
                />
                <Text
                  style={{
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    fontWeight: 'bold',
                  }}>
                  {bahasa === 'id'
                    ? id.status_perkawinan
                    : en.status_perkawinan}
                </Text>
                <TextInput
                  style={{
                    marginBottom: 10,
                    width: '100%',
                    height: 50,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 1.5,
                    borderRadius: 10,
                    backgroundColor: 'white',
                    paddingLeft: 10,
                    fontSize: 16,
                  }}
                  onChangeText={value => (status_perkawinan_ = value)}
                  autoCapitalize="characters"
                  defaultValue={status_perkawinan_}
                  underlineColorAndroid="transparent"
                  keyboardType="default"
                />
                <Text
                  style={{
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    fontWeight: 'bold',
                  }}>
                  {bahasa === 'id' ? id.kewarganegaraan : en.kewarganegaraan}
                </Text>
                <TextInput
                  style={{
                    marginBottom: 10,
                    width: '100%',
                    height: 50,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 1.5,
                    borderRadius: 10,
                    backgroundColor: 'white',
                    paddingLeft: 10,
                    fontSize: 16,
                  }}
                  onChangeText={value => (kewarganegaraan_ = value)}
                  autoCapitalize="characters"
                  defaultValue={kewarganegaraan_}
                  underlineColorAndroid="transparent"
                  keyboardType="default"
                />
                <Text
                  style={{
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    fontWeight: 'bold',
                  }}>
                  {bahasa === 'id' ? id.pekerjaan : en.pekerjaan}
                </Text>
                <TextInput
                  style={{
                    marginBottom: 10,
                    width: '100%',
                    height: 50,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 1.5,
                    borderRadius: 10,
                    backgroundColor: 'white',
                    paddingLeft: 10,
                    fontSize: 16,
                  }}
                  onChangeText={value => (pekerjaan_ = value)}
                  autoCapitalize="characters"
                  defaultValue={pekerjaan_}
                  underlineColorAndroid="transparent"
                  keyboardType="default"
                />
                <Text
                  style={{
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    fontWeight: 'bold',
                  }}>
                  {bahasa === 'id' ? id.nama_ibu_kandung : en.nama_ibu_kandung}
                </Text>
                <TextInput
                  style={{
                    marginBottom: 5,
                    width: '100%',
                    height: 50,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 1.5,
                    borderRadius: 10,
                    backgroundColor: 'white',
                    paddingLeft: 10,
                    fontSize: 16,
                  }}
                  onChangeText={value => (nama_ibu_kandung_ = value)}
                  autoCapitalize="characters"
                  underlineColorAndroid="transparent"
                  keyboardType="default"
                />
                <Text
                  style={{
                    alignSelf: 'flex-start',
                    marginBottom: 10,
                    fontFamily: 'Helvetica',
                    fontSize: 15,
                    alignItems: 'flex-start',
                    textAlign: 'left',
                    color: COLORS.red,
                  }}>
                  {bahasa === 'id' ? id.wajib_diisi : en.wajib_diisi}
                </Text>
                <View
                  style={{
                    marginTop: 15,
                    width: '100%',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <Button
                    textColor={COLORS.red}
                    labelStyle={styles.btnLabel}
                    style={{
                      width: 120,
                      borderRadius: 12,
                      borderColor: COLORS.red,
                      borderWidth: 2,
                    }}
                    mode="outlined"
                    onPress={() => {
                      fileUriKTP = '';
                      this.setState({modalOCR: false, session: 4});
                    }}>
                    {bahasa == 'id' ? id.batal : en.batal}
                  </Button>
                  <Button
                    style={{
                      width: 120,
                      borderRadius: 12,
                      borderColor: COLORS.red,
                      borderWidth: 2,
                    }}
                    labelStyle={styles.btnLabel}
                    buttonColor={COLORS.red}
                    mode="contained"
                    onPress={() => this.submitOCR()}>
                    {bahasa == 'id' ? id.lanjut : en.lanjut}
                  </Button>
                </View>
              </ScrollView>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
  popUpFailedDukcapil() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.modalFailedDukcapil}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({modalFailedDukcapil: false});
        }}>
        <View
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View
              style={{
                margin: 100,
                backgroundColor: 'white',
                borderRadius: 20,
                justifyContent: 'space-between',
                padding: 35,
                width: '80%',
                alignItems: 'center',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
              }}>
              <FastImage
                source={logoInfo}
                style={{width: 55, height: 55, marginBottom: 15}}
              />
              <Text
                style={{
                  marginBottom: 20,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  textAlign: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {bahasa === 'id'
                  ? id.konten_failed_dukcapil
                  : en.konten_failed_dukcapil}
              </Text>
              <Pressable
                style={{
                  width: '100%',
                  height: 43,
                  borderRadius: 10,
                  padding: 10,
                  elevation: 2,
                  backgroundColor: COLORS.red,
                }}
                onPress={() => {
                  this.setState({modalOCR: true});
                  this.setState({modalFailedDukcapil: false});
                }}>
                <Text style={styles.textbtn}>
                  {' '}
                  {bahasa === 'id'
                    ? id.masukkan_ulang_ktp
                    : en.masukkan_ulang_ktp}
                </Text>
              </Pressable>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
  // callbacks
  handlePresentModalPress = () => {
    this.BottomSheetModal.present();
  };
  bottomSheetChat() {
    return (
      <BottomSheetModal
        ref={ref => {
          this.BottomSheetModal = ref;
        }}
        style={{
          shadowColor: '#000',
          shadowOffset: {width: 0, height: 2},
          shadowOpacity: 3,
          shadowRadius: 5,
          elevation: 5,
        }}
        index={1}
        keyboardBehavior="fillParent"
        enableOverDrag={false}
        snapPoints={['25%', '70%']}>
        <BottomSheetScrollView
          keyboardDismissMode="interactive"
          automaticallyAdjustKeyboardInsets={true}
          contentContainerStyle={{
            alignItems: 'center',
            width: '100%',
            height: '100%',
          }}>
          <View
            style={{
              alignItems: 'center',
              width: '100%',
              height: '100%',
              alignSelf: 'center',
              flexDirection: 'column',
            }}>
            <FlatList
              style={{width: '100%', height: '87%'}}
              data={this.state.chatMessages}
              renderItem={({item, index}) => (
                <View
                  key={index}
                  style={{
                    alignSelf: item.isSelfSend ? 'flex-end' : 'flex-start',
                    marginEnd: item.isSelfSend ? 10 : 0,
                    marginStart: !item.isSelfSend ? 10 : 0,
                    padding: 15,
                    marginBottom: 10,
                    width: '55%',
                    backgroundColor: item.isSelfSend ? '#EEF6FF' : COLORS.red,
                    borderRadius: 12,
                    flexDirection: 'column',
                    height: 'auto',
                  }}>
                  <Text
                    style={{
                      color: item.isSelfSend ? 'black' : 'white',
                      fontSize: 14,
                      marginBottom: 5,
                    }}
                    ellipsizeMode="tail"
                    numberOfLines={1}>
                    {item.senderUser.userName}
                  </Text>
                  <Text
                    style={{
                      fontSize: 14,
                      color: item.isSelfSend ? 'black' : 'white',
                    }}
                    numberOfLines={4}
                    ellipsizeMode="tail">
                    {item.content}
                  </Text>
                </View>
              )}
              numColumns={1}
            />

            <View
              style={{
                width: '100%',
                height: '13%',
                backgroundColor: COLORS.red,
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'row',
              }}>
              <TextInput
                ref={input => {
                  this.InputChat = input;
                }}
                multiline={true}
                numberOfLines={1}
                style={{
                  width: '85%',
                  marginEnd: 10,
                  height: 'auto',
                  borderColor: COLORS.border_textInput,
                  borderWidth: 1.5,
                  borderRadius: 10,
                  backgroundColor: 'white',
                  padding: 10,
                  fontSize: 20,
                }}
                placeholder={
                  bahasa === 'id'
                    ? id.masukkan_pesan_anda
                    : en.masukkan_pesan_anda
                }
                onChangeText={text => {
                  this.scaleChatSend(text.length !== 0);
                  this.setState({chatMessage: text});
                }}
                underlineColorAndroid="transparent"
                keyboardType="default"
              />
              <Pressable
                onPress={() => {
                  this.InputChat.clear();
                  this.sendChatMessage();
                }}>
                <FastImage
                  style={{width: 30, height: 30}}
                  source={btnKirimChat}
                />
              </Pressable>
            </View>
          </View>
        </BottomSheetScrollView>
      </BottomSheetModal>
    );
  }
  popUpFailedSwafoto() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.modalFailedSwafoto}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({modalFailedSwafoto: false});
        }}>
        <View
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View
              style={{
                margin: 100,
                backgroundColor: 'white',
                borderRadius: 20,
                justifyContent: 'space-between',
                padding: 35,
                width: '80%',
                alignItems: 'center',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
              }}>
              <FastImage
                source={logoInfo}
                style={{width: 55, height: 55, marginBottom: 15}}
              />
              <Text
                style={{
                  marginBottom: 20,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  textAlign: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {bahasa === 'id'
                  ? id.title_failed_swafoto
                  : en.konten_failed_dukcapil}
              </Text>
              <Pressable
                style={{
                  width: '100%',
                  height: 43,
                  borderRadius: 10,
                  padding: 10,
                  elevation: 2,
                  backgroundColor: COLORS.red,
                }}
                onPress={() => {
                  fileUriSwafoto = '';
                  this.setState({modalFailedSwafoto: false, session: 5});
                }}>
                <Text style={styles.textbtn}>
                  {bahasa === 'id'
                    ? id.text_btn_failed_swafoto
                    : en.text_btn_failed_swafoto}
                </Text>
              </Pressable>
            </View>
          </View>
          <ScrollView
            keyboardDismissMode="none"
            contentContainerStyle={{width: '100%'}}
            automaticallyAdjustKeyboardInsets="true"
            automaticallyAdjustContentInsets="true">
            <View
              style={{
                marginTop: 15,
                marginStart: 15,
                marginEnd: 15,
                borderRadius: 15,
                flexDirection: 'column',
                backgroundColor: 'white',
              }}></View>
          </ScrollView>
        </View>
      </Modal>
    );
  }
  popUpFailedDTOTT() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.modalFailedDTOTT}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({modalFailedDTOTT: false});
        }}>
        <View
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View
              style={{
                margin: 100,
                backgroundColor: 'white',
                borderRadius: 20,
                justifyContent: 'space-between',
                padding: 35,
                width: '80%',
                alignItems: 'center',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
              }}>
              <FastImage
                source={logoWarning}
                style={{width: 55, height: 55, marginBottom: 15}}
              />
              <Text
                style={{
                  marginBottom: 20,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  textAlign: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {bahasa === 'id'
                  ? id.konten_failed_dtott
                  : en.konten_failed_dtott}
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  width: '100%',
                  justifyContent: 'center',
                }}>
                <Button
                  textColor={COLORS.red}
                  labelStyle={styles.btnLabel}
                  style={[styles.btn, {marginEnd: 10}]}
                  mode="outlined"
                  onPress={() => Linking.openURL('tel:1500977')}>
                  Call Center
                </Button>
                <Button
                  style={styles.btn}
                  labelStyle={styles.btnLabel}
                  buttonColor={COLORS.red}
                  mode="contained"
                  onPress={() => RNExitApp.exitApp()}>
                  OK
                </Button>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
  submitOCR() {
    if (
      nik_ === '' ||
      nama_lengkap_ === '' ||
      tempat_lahir_ === '' ||
      tgl_lahir_ === '' ||
      jenis_kelamin_ === '' ||
      golongan_darah_ === '' ||
      alamat_ === '' ||
      rt_rw_ === '' ||
      provinsi_ === '' ||
      kabupaten_kota_ === '' ||
      kecamatan_ === '' ||
      kelurahan_desa_ === '' ||
      agama_ === '' ||
      status_perkawinan_ === '' ||
      kewarganegaraan_ === '' ||
      pekerjaan_ === '' ||
      nama_ibu_kandung_ === ''
    ) {
      Alert.alert(
        bahasa === 'id' ? id.silahkan_isi_kolom : en.silahkan_isi_kolom,
      );
    } else {
      this.processDukcapil();
    }
  }
  facesDetectedselfie(faces) {
    if (faces.faces.length > 0) {
      const rightEye = faces.faces[0].rightEyeOpenProbability;
      const leftEye = faces.faces[0].leftEyeOpenProbability;
      const bothEyes = (rightEye + leftEye) / 2;

      if (bothEyes < 0.3) {
        this.setState({blinkDetected: true, canDetectFaces: false});

        setTimeout(() => {
          this.takePicture();
        }, 1000);
      }
      if (this.state.blinkDetected && bothEyes >= 0.9) {
        this.setState({blinkDetected: false});
      }
    }
  }
  async ratingAgent() {
    this.setState({loadingvisibleRating: true});
    var payload = {
      idDips: idDips,
      idAgent: Number(csId),
      rating: this.state.ratingStar.toString(),
      komentar: this.state.comment,
    };
    fetch(Server.BASE_URL_API + ApiService.ratingAgent, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: JSON.stringify(payload),
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({loadingvisibleRating: false});
        if (responseJson.code < 300) {
          setTimeout(() => {
            this.setState({popUpFinish: true});
          }, 2000);
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        console.error(error);
        Alert.alert('Gagal Terhubung ke server', JSON.stringify(error));
      });
  }
  popUpFinish() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.popUpFinishRating}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({popUpFinishRating: false});
        }}>
        <View
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View
              style={{
                margin: 100,
                backgroundColor: 'white',
                borderRadius: 20,
                justifyContent: 'space-between',
                padding: 35,
                width: '80%',
                alignItems: 'center',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
              }}>
              <FastImage
                source={logoSuccess}
                style={{width: 55, height: 55, marginBottom: 15}}
              />
              <Text
                style={{
                  marginBottom: 20,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  textAlign: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {bahasa === 'id' ? id.pop_up_rating : en.pop_up_rating}
              </Text>
              <Pressable
                style={{
                  width: '40%',
                  height: 43,
                  borderRadius: 10,
                  padding: 10,
                  elevation: 2,
                  backgroundColor: COLORS.red,
                }}
                onPress={() => {
                  this.setState({popUpFinishRating: false});
                  RNExitApp.exitApp();
                }}>
                <Text style={styles.textbtn}>
                  {bahasa === 'id' ? id.selesai : en.selesai}
                </Text>
              </Pressable>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
  loadingBarRating() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.loadingvisibleRating}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({loadingvisibleRating: false});
        }}>
        <View
          style={{
            backgroundColor: 'rgba(52, 52, 52, 0.8)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View style={styles.centeredView}>
              <View style={styles.containerLoading}>
                <FastImage
                  transition={false}
                  source={loadingvictoria}
                  style={styles.imgSize}
                />
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
  render() {
    console.log('Session = ', this.state.session);
    console.log('isRating = ', this.state.isRating);
    if (this.state.isRating) {
      return (
        <View
          style={{
            flex: 1,
            width: '100%',
            height: '100%',
            backgroundColor: COLORS.colorBG,
            flexDirection: 'column',
          }}>
          <StatusBar hidden="true" />
          {this.loadingBarRating()}
          {this.popUpFinish()}
          <FastImage
            transition={false}
            source={batikbvic}
            style={{
              width: '100%',
              height: 230,
              position: 'absolute',
              bottom: 0,
              resizeMode: 'contain',
            }}
          />
          <FastImage
            resizeMode="contain"
            style={{
              marginStart: 20,
              marginEnd: 20,
              marginBottom: 8,
              marginTop: 30,
              height: 100,
            }}
            source={logo_merah}
          />
          <Text
            style={{
              fontSize: 22,
              textAlign: 'center',
            }}>
            {bahasa === 'id' ? id.headline_rating : en.headline_rating}
          </Text>

          <View
            style={{
              marginTop: 20,
              marginStart: 20,
              marginEnd: 20,
              borderColor: '#b3b3b3',
              borderWidth: 1,
              borderRadius: 15,
              flexDirection: 'column',
              padding: 10,
            }}>
            <Text style={{fontSize: 20, textAlign: 'center'}}>
              {bahasa === 'id' ? id.rating_1 : en.rating_1}
            </Text>
            <Rating
              type="custom"
              minValue={0}
              startingValue={0}
              onFinishRating={rating => {
                this.setState({ratingStar: rating});
              }}
              tintColor={'white'}
              ratingBackgroundColor="gray"
              style={{
                marginTop: 10,
                marginBottom: 10,
                width: '100%',
              }}
            />
          </View>

          <TextInput
            keyboardType="default"
            multiline={true}
            numberOfLines={3}
            textAlign="left"
            onChangeText={text => this.setState({comment: text})}
            placeholder={
              bahasa === 'id' ? id.placeholder_rating : en.placeholder_rating
            }
            style={{
              margin: 20,
              fontSize: 20,
              borderRadius: 15,
              padding: 10,
              height: 120,
              borderColor: '#b3b3b3',
              borderWidth: 1,
            }}
          />
          <Pressable
            style={{
              marginRight: 20,
              marginLeft: 20,
              borderRadius: 10,
              padding: 12,
              elevation: 2,
              backgroundColor:
                this.state.ratingStar == 0 ? COLORS.btnFalse : COLORS.red,
            }}
            onPress={() => {
              if (this.state.ratingStar == 0) {
                Alert.alert(
                  bahasa === 'id' ? id.alert_rating_agen : en.alert_rating_agen,
                );
              } else {
                this.ratingAgent();
              }
            }}>
            <Text style={styles.textbtn}>
              {bahasa === 'id' ? id.kirim : en.kirim}
            </Text>
          </Pressable>
        </View>
      );
    } else {
      return (
        <SafeAreaView style={styles.safeArea} pointerEvents="box-none">
          {this.state.isInSession &&
            this.state.isKeyboardOpen &&
            !this.state.openCamera && (
              <View
                style={{
                  flex: 1,
                  width: '100%',
                  height: '100%',
                  backgroundColor: 'white',
                }}>
                <StatusBar hidden="true" />
                <View
                  style={{
                    width: '100%',
                    height: '100%',
                    backgroundColor: 'white',
                  }}>
                  <ImageBackground
                    transition={false}
                    style={{
                      width: '100%',
                      height: 210,
                      bottom: this.state.isLoading ? 0 : 20,
                      position: 'absolute',
                    }}
                    imageStyle={{
                      width: '100%',
                      height: '100%',
                      resizeMode: 'contain',
                      alignItems: 'flex-end',
                    }}
                    source={batikbvic}
                  />
                  {this.popUpTNC()}
                  {this.popUpOCR()}
                  {this.popUpKeluar()}
                  {this.popUpFailedDukcapil()}
                  {this.popUpFailedDTOTT()}
                  {this.popUpFailedSwafoto()}
                  {this.popUpFailedOCRKtp()}
                  {this.popUpFailedSwafoto2()}
                  {this.popUpSuccesReksaDana()}
                  {this.popUpRequestSID()}
                  {this.popUpAktivasiVimo()}
                  <View
                    style={{
                      alignItems: 'center',
                      flexDirection: 'column',
                      justifyContent: 'space-evenly',
                      width: '100%',
                      height: '100%',
                    }}>
                    <View
                      style={{
                        width: '100%',
                        height: '30%',
                        padding: 10,
                        display: this.state.session === 37 ? 'none' : 'flex',
                        backgroundColor: 'white',
                      }}>
                      <FlatList
                        style={styles.userList}
                        contentContainerStyle={styles.userListContentContainer}
                        data={this.state.users}
                        extraData={this.state.users}
                        renderItem={({item}) => (
                          <VideoView2
                            user={item}
                            // focused={item.userId === fullScreenUser?.userId}
                            // onPress={selectedUser => onSelectedUser(selectedUser)}
                            key={item.userId}
                          />
                        )}
                        keyExtractor={item => item.userId}
                        fadingEdgeLength={50}
                        decelerationRate={0}
                        snapToAlignment="center"
                        snapToInterval={100}
                        showsHorizontalScrollIndicator={false}
                        horizontal
                      />

                      <View
                        style={{
                          justifyContent: 'space-between',
                          alignContent: 'flex-end',
                          alignSelf: 'flex-end',
                          alignItems: 'flex-end',
                          flexDirection: 'column',
                          right: 35,
                          width: 'auto',
                          marginTop: 25,
                          marginBottom: 15,
                          height: '65%',
                          position: 'absolute',
                        }}>
                        <FastImage
                          style={{
                            width: 30,
                            height: 15,
                          }}
                          source={bvicIcon}
                        />
                        {/* <FastImage
                        style={{
                          width: 35,
                          height: 15,
                        }}
                        source={zoomIcon}
                      /> */}
                      </View>
                      <View
                        style={[
                          {
                            alignContent: 'center',
                            position: 'absolute',
                            left: 35,
                            top: 25,
                            backgroundColor: '#22D882',
                            padding: 5,
                            borderRadius: 5,
                          },
                          {flexDirection: 'row'},
                        ]}>
                        <Text
                          style={{color: 'white', fontSize: 10, paddingEnd: 3}}>
                          {this.getFormattedTime()}
                        </Text>
                        <FastImage
                          style={{
                            alignSelf: 'center',
                            height: 10,
                            width: 10,
                            overflow: 'hidden',
                          }}
                          source={iconTimer}
                        />
                      </View>
                    </View>

                    {this.view()}
                    <BottomSheetModalProvider>
                      <View
                        style={{
                          justifyContent: 'center',
                          alignItems: 'center',
                          flexDirection: 'row',
                          backgroundColor: 'white',
                          width: '100%',
                          height: '10%',
                        }}>
                        <Button
                          onPress={() => {
                            this.onPressAudio();
                          }}
                          icon={() => (
                            <Image
                              transition={false}
                              source={this.state.isMuted ? unmute : mic}
                              style={{
                                marginStart: 15,
                                justifyContent: 'center',
                                alignItems: 'center',
                                alignContent: 'center',
                                alignSelf: 'center',
                                width: 27,
                                height: 27,
                                resizeMode: 'contain',
                              }}
                            />
                          )}
                          textColor={COLORS.red}
                          labelStyle={styles.btnLabel}
                          style={{
                            height: 44,
                            width: 70,
                            marginEnd: 10,
                            borderRadius: 12,
                            borderColor: COLORS.red,
                            borderWidth: 2,
                          }}
                          mode="outlined"></Button>

                        <Button
                          onPress={() => {
                            this.handlePresentModalPress();
                          }}
                          icon={() => (
                            <Image
                              transition={false}
                              source={chat}
                              style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                alignContent: 'center',
                                alignSelf: 'center',
                                width: 27,
                                height: 27,
                                resizeMode: 'contain',
                              }}
                            />
                          )}
                          textColor={COLORS.red}
                          labelStyle={styles.btnLabel}
                          style={[styles.btn, {marginEnd: 10}]}
                          mode="outlined">
                          {bahasa == 'id' ? id.pesan : en.pesan}
                        </Button>
                        {this.bottomSheetChat()}
                        <Button
                          style={styles.btn}
                          labelStyle={styles.btnLabel}
                          buttonColor={COLORS.red}
                          mode="contained"
                          onPress={() => {
                            this.setState({isDecline: true});
                          }}>
                          {bahasa == 'id'
                            ? id.akhiri_panggilan
                            : en.akhiri_panggilan}
                        </Button>
                      </View>
                    </BottomSheetModalProvider>

                    {this.state.loadingvisible ? (
                      <View
                        style={{
                          bottom: 0,
                          position: 'absolute',
                          backgroundColor: 'rgba(255, 255, 255, 0.8)',
                          height: '65%',
                          width: '100%',
                        }}>
                        <View
                          style={{
                            flex: 1,
                            alignItems: 'center',
                            justifyContent: 'center',
                            marginTop: 0,
                          }}>
                          <View style={styles.centeredView}>
                            <View style={styles.containerLoading}>
                              <FastImage
                                transition={false}
                                source={loadingvictoria}
                                style={styles.imgSize}
                              />
                            </View>
                          </View>
                        </View>
                      </View>
                    ) : (
                      <></>
                    )}
                  </View>
                </View>
              </View>
            )}
          {this.state.isInSession &&
            this.state.isKeyboardOpen &&
            this.state.openCamera &&
            (this.state.session == 4 ||
              this.state.session == 6 ||
              this.state.session == 7 ||
              this.state.session == 13) && (
              <View
                style={[
                  styles.container,
                  {
                    backgroundColor: 'white',
                    alignItems: 'center',
                    position: 'relative',
                  },
                ]}>
                <StatusBar hidden="true" />
                <View
                  style={{
                    width: '100%',
                    height: '100%',
                  }}>
                  <RNCamera
                    captureAudio={false}
                    ref={ref => {
                      this.camera = ref;
                    }}
                    style={{
                      justifyContent: 'space-between',
                      alignSelf: 'center',
                      alignItems: 'center',
                      height: '100%',
                      flexDirection: 'column',
                      width: '100%',
                    }}
                    type={this.state.session == 5 ? 'front' : 'back'}>
                    <View
                      style={{
                        padding: 20,
                        justifyContent: 'flex-end',
                        width: '100%',
                        height: '10%',
                        backgroundColor: COLORS.red,
                      }}>
                      <Pressable
                        onPress={() => {
                          this.setState({openCamera: false});
                        }}>
                        <Text
                          style={{
                            textAlign: 'left',
                            color: 'white',
                            fontFamily: 'Helvetica',
                            fontSize: 20,
                          }}>
                          {bahasa == 'id' ? id.kembali : en.kembali}
                        </Text>
                      </Pressable>
                    </View>
                    <View
                      style={{
                        marginTop: this.state.session == 5 ? 350 : 0,
                        alignItems: 'center',
                        width: '85%',
                        height: '25%',
                        display: this.state.session == 7 ? 'none' : 'flex',
                        borderRadius: 12,
                        borderColor: COLORS.red,
                        borderWidth: 4,
                        borderStyle: 'dashed',
                      }}
                    />
                    <View
                      style={{
                        borderTopStartRadius: 15,
                        borderTopEndRadius: 15,
                        alignItems: 'center',
                        justifyContent: 'center',
                        width: '100%',
                        height: '14%',
                        backgroundColor: 'rgba(0, 0, 0, 0.3)',
                      }}>
                      <Pressable onPress={this.takePicture}>
                        <View
                          style={{
                            width: 70,
                            height: 70,
                            borderRadius: 70 / 2,
                            borderWidth: 10,
                            borderColor: COLORS.red,
                            backgroundColor: 'white',
                          }}
                        />
                      </Pressable>
                    </View>
                  </RNCamera>
                </View>
              </View>
            )}
          {this.state.isInSession &&
            this.state.isKeyboardOpen &&
            this.state.openCamera &&
            (this.state.session == 5 || this.state.session == 24) && (
              <View
                style={[
                  styles.container,
                  {
                    backgroundColor: 'white',
                    alignItems: 'center',
                    position: 'relative',
                  },
                ]}>
                <StatusBar hidden="true" />
                <View
                  style={{
                    width: '100%',
                    height: '100%',
                  }}>
                  <RNCamera
                    captureAudio={false}
                    ref={ref => {
                      this.camera = ref;
                    }}
                    style={{
                      justifyContent: 'space-between',
                      alignSelf: 'center',
                      alignItems: 'center',
                      height: '100%',
                      flexDirection: 'column',
                      width: '100%',
                    }}
                    onCameraReady={() => {
                      this.setState({canDetectFaces: true});
                    }}
                    faceDetectionMode={
                      RNCamera.Constants.FaceDetection.Mode.accurate
                    }
                    faceDetectionLandmarks={
                      RNCamera.Constants.FaceDetection.Landmarks.all
                    }
                    faceDetectionClassifications={
                      RNCamera.Constants.FaceDetection.Classifications.all
                    }
                    onFacesDetected={faces => {
                      this.state.canDetectFaces
                        ? this.facesDetectedselfie(faces)
                        : null;
                    }}
                    onFaceDetectionError={error =>
                      console.log('FDError', error)
                    } // This is never triggered
                    type={'front'}>
                    <View
                      style={{
                        flexDirection: 'column',
                        width: '100%',
                        height: '40%',
                      }}>
                      <View
                        style={{
                          height: 180,
                          padding: 20,
                          marginBottom: 10,
                          justifyContent: 'space-evenly',
                          flexDirection: 'column',
                          borderBottomEndRadius: 30,
                          borderBottomStartRadius: 30,
                          backgroundColor: 'white',
                        }}>
                        <Text
                          style={{
                            alignSelf: 'center',
                            textAlign: 'center',
                            color: 'black',
                            fontWeight: 'bold',
                            fontFamily: 'Helvetica',
                            fontSize: 17,
                          }}>
                          {bahasa === 'id'
                            ? id.title_blink_swafoto
                            : en.title_blink_swafoto}
                        </Text>
                        <Text
                          style={{
                            alignSelf: 'center',
                            textAlign: 'center',
                            color: 'black',
                            fontFamily: 'Helvetica',
                            fontSize: 17,
                          }}>
                          {bahasa === 'id'
                            ? id.content_blink_swafoto
                            : en.content_blink_swafoto}
                        </Text>
                      </View>

                      <View
                        style={{
                          padding: 20,
                          alignSelf: 'center',
                          justifyContent: 'space-evenly',
                          width: 320,
                          height: 'auto',
                          flexDirection: 'column',
                          borderRadius: 20,
                          backgroundColor: 'rgba(255, 255, 255, 0.5)',
                        }}>
                        <Text
                          style={{
                            alignSelf: 'center',
                            textAlign: 'center',
                            color: 'black',
                            fontFamily: 'Helvetica',
                            fontSize: 17,
                          }}>
                          {bahasa === 'id'
                            ? id.kedipkan_mata
                            : en.kedipkan_mata}
                        </Text>
                      </View>
                    </View>

                    <View
                      style={{
                        marginBottom: 80,
                        alignItems: 'center',
                        width: '85%',
                        height: '25%',
                        borderRadius: 12,
                        borderColor: COLORS.red,
                        borderWidth: 4,
                        borderStyle: 'dashed',
                      }}
                    />
                  </RNCamera>
                </View>
              </View>
            )}
          {!this.state.isInSession && (
            <View style={styles.connectingWrapper}>
              <View
                style={[
                  {
                    position: 'absolute',
                    margin: 15,
                    borderRadius: 30,
                    borderColor: COLORS.border_textInput,
                    borderWidth: 2,
                    top: 0,
                    backgroundColor: 'white',
                  },
                  {flexDirection: 'row'},
                ]}>
                <View
                  style={{
                    padding: 20,
                    width: '49.75%',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <FastImage
                    style={{
                      alignSelf: 'center',
                      height: 145,
                      width: 145,
                      overflow: 'hidden',
                    }}
                    source={noAvatar}
                  />
                </View>
                <View
                  style={{
                    alignSelf: 'center',
                    backgroundColor: COLORS.border_textInput,
                    width: '0.5%',
                    height: '55%',
                    borderRadius: 20,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}></View>
                <View
                  style={{
                    padding: 20,
                    width: '49.75%',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <FastImage
                    style={{
                      alignSelf: 'center',
                      height: 145,
                      width: 145,
                      overflow: 'hidden',
                    }}
                    source={noAvatar}
                  />
                </View>
              </View>
              {this.state.loadingvisible ? (
                <View
                  style={{
                    bottom: 0,
                    position: 'absolute',
                    backgroundColor: 'rgba(255, 255, 255, 0.8)',
                    height: '100%',
                    width: '100%',
                  }}>
                  <View
                    style={{
                      flex: 1,
                      alignItems: 'center',
                      justifyContent: 'center',
                      marginTop: 0,
                    }}>
                    <View style={styles.centeredView}>
                      <View style={styles.containerLoading}>
                        <FastImage
                          transition={false}
                          source={loadingvictoria}
                          style={styles.imgSize}
                        />
                      </View>
                    </View>
                  </View>
                </View>
              ) : (
                <></>
              )}
            </View>
          )}
        </SafeAreaView>
      );
    }
  }
}
class DipsWaitingRoom extends Component {
  state = {
    idSchedule: null,
    loadingvisible: false,
    modalScheduleDone: false,
    modalscheduler: false,
    modalscheduler2: false,
    modalVisible: true,
    modalApproveCalling: false,
    setRefreshing: false,
    dateShcedule: '',
    isDatePickerVisible: false,
    setValueDropdown: '',
    setItemDropdown: '',
    open: false,
    value: null,
    isOutbound: false,
    itemsProduct: [],
    itemUrlMedia: [],
    itemsSpanduk: [],
    itemUrlMediaSpanduk: [],
    items: [],
    data_currentTicket: [],
    data_ticket: [],
    panggilanmasuk_outbond: false,
    cameraType: 'front',
    zoomcamera: 0,
    agentImage: '',
    ratio: '16:9',
    popUpWaiting: false,
    titlePopUpProduct: '',
    bodyProduct: '',
    popUpProduct: false,
    popUpFailedSiganture: false,
    rotateValueHolder: new AnimatedReact.Value(0),
    popUpRejectCall: false,
    isFormTransaksi: false,
    base64QR: '',
    //Session 0 : List Form Transaksi
    //Session 1 : Form Builder
    //Session 2: QrCode
    sessionTransaksi: 0,
    // 1 : Transaksi Antar Bank
    // 2 : Transaksi Antar Rekening
    jenisTransaksiId: 1,
    itemLayanan: [
      {
        title:
          bahasa === 'id' ? id.transaksi_antar_bank : en.transaksi_antar_bank,
        id: 1,
        img: img_transaksi_antar_bank,
      },
      {
        title:
          bahasa === 'id'
            ? id.transaksi_antar_rekening
            : en.transaksi_antar_rekening,
        id: 2,
        img: img_transaksi_inter_bank,
      },
    ],
    numColumns: 3,
  };
  formatData = (data, numColumns) => {
    if (data !== undefined) {
      const numberOfFullRows = Math.floor(data.length / numColumns);
      let numberOfElementsLastRow = data.length - numberOfFullRows * numColumns;
      while (
        numberOfElementsLastRow !== numColumns &&
        numberOfElementsLastRow !== 0
      ) {
        data.push({key: `blank-${numberOfElementsLastRow}`, empty: true});
        numberOfElementsLastRow++;
      }
      return data;
    }
  };
  componentDidMount() {
    //this.setState({loadingvisible: true});
    //idDips = 'Gzk1U3G8spg3NiJd';
    setInterval(() => {
      if (
        !this.state.popUpWaiting &&
        count_popUpWaiting < parseInt(valueWaitingTime.count) &&
        !this.state.isFormTransaksi
      ) {
        count_popUpWaiting = count_popUpWaiting + 1;
        this.setState({popUpWaiting: true});
        console.log(
          'muncul selama : ' +
            valueWaitingTime.time +
            ' detik | sudah muncul sebanyak : ' +
            count_popUpWaiting +
            'x | Maksimal muncul : ' +
            valueWaitingTime.count +
            'x',
        );
      }
    }, parseInt(valueWaitingTime.time) * 1000);

    fileUriKTP = '';
    if (idDips == '') {
      Alert.alert('Belum dapat mengakses');
      this.setState({modalVisible: true});
    } else {
      // this.setState({
      //   ws_session_name: "adasa",///id dips
      //   ws_password: "faffa", //////// dari respon si password
      //   isMuted: true,
      //   csId: "sasda",
      //   signatureZoom: 'kkaf87a8hafa',
      //   zoomView: true
      // });

      this.initBackgroundFetch();
      this.rabbitGetTicketInfo();
      this.getSpandukWaiting();
      this.getProductWaiting();
      this.scheduleDropdown();
    }
  }
  popUpWaitingAgent() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.popUpWaiting}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({popUpWaiting: false});
        }}>
        <View
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View
              style={{
                margin: 100,
                backgroundColor: 'white',
                borderRadius: 20,
                justifyContent: 'space-between',
                padding: 35,
                width: '80%',
                alignItems: 'center',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
              }}>
              <FastImage
                source={logoInfo}
                style={{width: 55, height: 55, marginBottom: 15}}
              />
              <Text
                style={{
                  marginBottom: 20,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  textAlign: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {bahasa === 'id'
                  ? id.headline_waiting_agent
                  : en.headline_waiting_agent}
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  width: '100%',
                  justifyContent: 'center',
                }}>
                <Pressable
                  style={[
                    styles.btn,
                    {
                      height: 'auto',
                      marginEnd: 10,
                      padding: 10,
                      width: '50%',
                      alignContent: 'center',
                      alignSelf: 'center',
                    },
                  ]}
                  textColor={COLORS.red}
                  labelStyle={styles.btnLabel}
                  mode="outlined"
                  onPress={() => {
                    popUpWaiting = false;
                    this.setState({popUpWaiting: false});
                    setTimeout(() => {
                      this.setState({modalscheduler: true});
                    }, 1500);
                  }}>
                  <Text
                    numberOfLines={2}
                    style={{
                      fontSize: 15,
                      color: COLORS.red,
                      textAlign: 'center',
                    }}>
                    {count_popUpWaiting === parseInt(valueWaitingTime.count)
                      ? bahasa === 'id'
                        ? id.jadwalkan_panggilan
                        : en.jadwalkan_panggilan
                      : bahasa === 'id'
                      ? id.btn_waiting_agent_jadwalkan
                      : en.btn_waiting_agent_jadwalkan}
                  </Text>
                </Pressable>
                <Pressable
                  style={[
                    styles.btn,
                    {
                      alignContent: 'center',
                      alignSelf: 'center',
                      height: 'auto',
                      backgroundColor: COLORS.red,
                      padding: 10,
                      width: '50%',
                    },
                  ]}
                  labelStyle={styles.btnLabel}
                  numberOfLines={2}
                  mode="contained"
                  onPress={() => {
                    count_popUpWaiting === parseInt(valueWaitingTime.count)
                      ? RNExitApp.exitApp()
                      : this.setState({popUpWaiting: false});
                  }}>
                  <Text
                    numberOfLines={2}
                    style={{
                      fontSize: 15,
                      color: 'white',
                      textAlign: 'center',
                    }}>
                    {count_popUpWaiting === parseInt(valueWaitingTime.count)
                      ? bahasa === 'id'
                        ? id.akhiri_panggilan
                        : en.akhiri_panggilan
                      : bahasa === 'id'
                      ? id.btn_waiting_agent_ready_wait
                      : en.btn_waiting_agent_ready_wait}
                  </Text>
                </Pressable>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
  componentWillUnmount() {}
  popUpProduct() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.popUpProduct}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({popUpProduct: false});
        }}>
        <View
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              marginTop: 0,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                margin: 100,
                backgroundColor: 'white',
                borderRadius: 20,
                flexDirection: 'column',
                padding: 20,
                width: '80%',
                height: '70%',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                alignItems: 'center',
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
              }}>
              <ScrollView
                style={{
                  width: '97%',
                  height: '60%',
                }}
                keyboardDismissMode="interactive"
                automaticallyAdjustKeyboardInsets={true}
                contentContainerStyle={{
                  alignItems: 'center',
                }}>
                <Text style={{fontSize: 22, marginBottom: 20}}>
                  {this.state.titlePopUpProduct}
                </Text>
                <RenderHtml
                  contentWidth={300}
                  source={{html: this.state.bodyProduct}}
                  style={{
                    width: '100%',
                  }}
                />
              </ScrollView>
              <Pressable
                style={{
                  width: '40%',
                  height: 43,
                  borderRadius: 30,
                  padding: 10,
                  elevation: 2,
                  backgroundColor: COLORS.red,
                }}
                onPress={() => {
                  this.setState({
                    popUpProduct: false,
                    titlePopUpProduct: '',
                    bodyProduct: '',
                  });
                }}>
                <Text style={styles.textbtn}>
                  {bahasa === 'id' ? id.kembali : en.kembali}
                </Text>
              </Pressable>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
  async getProductWaiting() {
    var dataListProduct = [];
    var dataListUrlMedia = [];
    fetch(`${Server.BASE_URL_API}${ApiService.productWaiting}`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.code < 300) {
          responseJson.data.map(element => {
            dataListProduct.push(element.produkId);
          });
          this.setState({itemsProduct: dataListProduct});

          dataListProduct.map(item => {
            var idProd = item.id;
            var encodedValue = encodeURIComponent(idProd);
            const urlImage = `${Server.BASE_URL_API}${ApiService.mediaProduct}${encodedValue}`;

            dataListUrlMedia.push({
              namaProduk: item.namaProduk,
              body: item.body,
              uri: urlImage,
            });
          });
          accessToken = responseJson.token;
          exchangeToken = responseJson.exchange;
          console.log('itemUrlMedia', dataListUrlMedia);
          this.setState({itemUrlMedia: dataListUrlMedia});
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        Alert.alert('Gagal Terhubung ke server', error);
      });
  }
  renderCardItem = ({item, index}) => (
    <Pressable
      style={{
        flex: 1,
        margin: 3,
        justifyContent: 'space-evenly',
      }}
      onPress={() => {
        this.setState({
          popUpProduct: true,
          titlePopUpProduct: item.namaProduk,
          bodyProduct: item.body,
        });
      }}
      key={index}>
      <Card>
        <Card.Cover
          style={{height: 120}}
          source={{
            uri: item.uri,
            headers: {
              Authorization: 'Bearer ' + accessToken,
              exchangeToken: exchangeToken,
            },
          }}
        />
      </Card>
    </Pressable>
  );
  async getSpandukWaiting() {
    var dataListSpanduk = [];
    var dataListUrlMedia = [];
    fetch(`${Server.BASE_URL_API}${ApiService.spandukWaiting}`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.code < 300) {
          responseJson.data.map(element => {
            dataListSpanduk.push(element);
          });

          this.setState({itemsSpanduk: dataListSpanduk});

          dataListSpanduk.map(item => {
            var idSpanduk = item.id;
            var encodedValue = encodeURIComponent(idSpanduk);
            var type = JSON.parse(item.media);
            const urlImage = `${Server.BASE_URL_API}${ApiService.mediaSpanduk}${encodedValue}`;
            dataListUrlMedia.push({
              type: type.mimetype.toLowerCase(),
              uri: urlImage,
            });
          });
          this.setState({itemUrlMediaSpanduk: dataListUrlMedia});
          this.setState({loadingvisible: false});
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        Alert.alert('Gagal Terhubung ke server', error);
      });
  }
  renderBannerItem = ({item, index}) => (
    <Pressable
      onPress={() => {}}
      style={{
        flex: 1,
        width: 220,
        overflow: 'hidden',
        elevation: 5,
        margin: 4,
        justifyContent: 'center',
      }}>
      {item.type === 'video/mp4' ? (
        <Video
          ref={ref => {
            this.player = ref;
          }}
          style={{
            flex: 1,
            width: 220,
            overflow: 'hidden',
            elevation: 5,
            margin: 4,
            justifyContent: 'center',
          }}
          volume={1.0}
          muted={true}
          paused={false}
          repeat={true}
          playInBackground={true}
          playWhenInactive={true}
          onLoadStart={() => {}}
          onError={param => {}}
          onLoad={() => {}}
          source={{
            uri: item.uri,
            headers: {
              Authorization: 'Bearer ' + accessToken,
              exchangeToken: exchangeToken,
            },
          }}
        />
      ) : (
        <Card
          style={{
            flex: 1,
            width: 220,
            overflow: 'hidden',
            elevation: 5,
            margin: 4,
            justifyContent: 'center',
          }}>
          <Card.Cover
            source={{
              uri: item.uri,
              headers: {
                Authorization: 'Bearer ' + accessToken,
                exchangeToken: exchangeToken,
              },
            }}
            resizeMode="stretch"
          />
        </Card>
      )}
    </Pressable>
  );
  async scheduleDropdown() {
    var dataSchedule = [];
    fetch(`${Server.BASE_URL_API}${ApiService.timePeriodSchedule}`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.code < 300) {
          responseJson.data.map(element => {
            dataSchedule.push({
              label: element.periode,
              value: element.id,
            });
          });
          this.setState({loadingvisible: false});
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        Alert.alert('Gagal Terhubung ke server', error);
      });
    this.setState({items: dataSchedule});
  }
  async saveSchedule(tgl) {
    var payload = {
      idDips: idDips,
      tanggal: tgl,
      periodeId: this.state.idSchedule,
    };

    fetch(Server.BASE_URL_API + ApiService.saveSchedule, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: JSON.stringify(payload),
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({dateShcedule: ''});
        this.setState({idSchedule: null});
        if (responseJson.code < 300) {
          setTimeout(() => {
            this.setState({modalScheduleDone: true});
            this.rabbitAcceptCall('cancel');
          }, 1000);
          this.start_proses_socket();
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        console.error(error);
        Alert.alert('Gagal Terhubung ke server', JSON.stringify(error));
      });
  }
  async initBackgroundFetch() {
    const onEvent = async taskId => {
      console.log('[BackgroundFetch] task: ', taskId);
      // Do your background work...
      //await this.rabbitListenCall();
      switch (taskId) {
        case 'co.id.dips361.ios3':
          print('Received custom task');
          break;
        default:
          print('Default fetch task');
      }
    };

    const onTimeout = async taskId => {
      console.warn('[BackgroundFetch] TIMEOUT task: ', taskId);
      BackgroundFetch.finish(taskId);
    };

    let status = await BackgroundFetch.configure(
      {minimumFetchInterval: 15},
      onEvent,
      onTimeout,
    );

    console.log('[BackgroundFetch] configure status: ', status);

    // Step 2:  Schedule a custom "oneshot" task "com.foo.customtask" to execute 5000ms from now.
    BackgroundFetch.scheduleTask({
      taskId: 'co.id.dips361.ios',
      forceAlarmManager: true,
      delay: 5000, // <-- milliseconds
    });
  }
  async start_proses_socket() {
    const sleep = time =>
      new Promise(resolve => setTimeout(() => resolve(), time));
    const veryIntensiveTask = async taskDataArguments => {
      const {delay} = taskDataArguments;

      await new Promise(async resolve => {
        for (let i = 0; backgroundServer.isRunning(); i++) {
          console.log(
            '==================start bs====================',
            i + '| ' + AppState.currentState,
          );
          // console.log(i);
          // this.proses_socket()
          await this.rabbitListenCall();
          await sleep(delay);
        }
      });
    };
    const options = {
      taskName: 'Example',
      taskTitle: 'ExampleTask title',
      taskDesc: 'ExampleTask description',
      taskIcon: {
        name: 'ic_launcher',
        type: 'mipmap',
      },
      color: '#ff00ff',
      linkingURI: 'yourSchemeHere://chat/jane', // See Deep Linking for more info
      parameters: {
        delay: 1000,
      },
    };
    await backgroundServer.start(veryIntensiveTask, options);
    await backgroundServer.updateNotification({
      taskDesc: 'New ExampleTask description',
    }); // Only Android, iOS will ignore this call
  }
  rabbitGetTicketInfo() {
    var payload = {
      custId: idDips,
    };

    fetch(`${Server.BASE_URL_API_RABBIT}${ApiService.rabbitGetTicketInfo}`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(payload),
    })
      .then(response => {
        const statusCode = response.status;
        const data = response.json();
        return Promise.all([statusCode, data]);
      })
      .then(([statusCode, responseJson]) => {
        if (responseJson.ticket != '') {
          this.setState({
            data_currentTicket: responseJson,
          });
          this.rabbitGetTicket();
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
      });
  }
  rabbitGetTicket() {
    var payload = {
      custId: idDips,
    };

    fetch(`${Server.BASE_URL_API_RABBIT}${ApiService.rabbitGetTicket}`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(payload),
    })
      .then(response => {
        const statusCode = response.status;
        const data = response.json();
        return Promise.all([statusCode, data]);
      })
      .then(([statusCode, responseJson]) => {
        if (statusCode <= 300) {
          this.setState({
            data_ticket: responseJson,
          });
          this.setState({loadingvisible: false});
          this.rabbitListenCall();
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        this.rabbitGetTicket();
        //Alert.alert('Gagal Terhubung ke server', error);
      });
  }
  popUpFailedSignature() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.popUpFailedSiganture}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({popUpFailedSiganture: false});
        }}>
        <View
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View
              style={{
                margin: 100,
                backgroundColor: 'white',
                borderRadius: 20,
                justifyContent: 'space-between',
                padding: 35,
                width: '80%',
                alignItems: 'center',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
              }}>
              <FastImage
                source={logoInfo}
                style={{width: 55, height: 55, marginBottom: 15}}
              />
              <Text
                style={{
                  marginBottom: 20,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  textAlign: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {bahasa === 'id'
                  ? id.konten_fail_signature
                  : en.konten_fail_signature}
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  width: '100%',
                  justifyContent: 'center',
                }}>
                <Button
                  textColor={COLORS.red}
                  labelStyle={styles.btnLabel}
                  style={[styles.btn, {marginEnd: 10}]}
                  mode="outlined"
                  onPress={() => {
                    this.setState({popUpFailedSiganture: false});
                    RNExitApp.exitApp();
                  }}>
                  {bahasa == 'id' ? id.akhiri_panggilan : en.akhiri_panggilan}
                </Button>
                <Button
                  style={styles.btn}
                  labelStyle={styles.btnLabel}
                  buttonColor={COLORS.red}
                  mode="contained"
                  onPress={() => {
                    this.setState({popUpFailedSiganture: false});
                    this.processSignature();
                  }}>
                  {bahasa == 'id' ? id.coba_lagi : en.coba_lagi}
                </Button>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
  async rabbitListenCall() {
    var payload = {
      custId: idDips,
    };

    await fetch(`${Server.BASE_URL_API_RABBIT}${ApiService.rabbitListenCall}`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(payload),
    })
      .then(response => {
        const statusCode = response.status;
        const data = response.json();
        return Promise.all([statusCode, data]);
      })
      .then(([statusCode, responseJson]) => {
        if (statusCode < 300) {
          this.setState({loadingvisible: false});
          var actionCall = '';
          if (responseJson.action != undefined) {
            actionCall = responseJson.action;
          }
          if (actionCall == 'info') {
            this.setState({csId: responseJson.csId});
            this.rabbitListenCall();
          } else {
            var sessionId = '';
            if (responseJson.agentImage === undefined) {
              if (this.state.data_ticket.ticket == responseJson.ticket) {
                if (responseJson.sessionId != undefined) {
                  sessionId = responseJson.sessionId;
                }
                AsyncStorage.setItem('ws_session_name', sessionId);
                AsyncStorage.setItem('ws_password', responseJson.password);
                AsyncStorage.setItem('csId', responseJson.csId);
                AsyncStorage.setItem('custName', custName);
                csId = responseJson.csId;

                //Jika pop up waiting muncul, tapi agent tiba2 pickup  maka masuk fungsi ini lalu nutup popup
                if (this.state.popUpWaiting || this.state.popUpProduct) {
                  this.setState({popUpWaiting: false, popUpProduct: false});
                  count_popUpWaiting = 100; //Supaya tidak muncul lagi
                  setTimeout(() => {
                    this.setState({
                      modalApproveCalling: true,
                      ws_session_name: sessionId, ///id dips
                      ws_password: responseJson.password, //////// dari respon si password
                      isMuted: true,
                      csId: responseJson.csId,
                    });
                  }, 1500);
                } else {
                  this.setState({
                    modalApproveCalling: true,
                    ws_session_name: sessionId, ///id dips
                    ws_password: responseJson.password, //////// dari respon si password
                    isMuted: true,
                    csId: responseJson.csId,
                  });
                }
              } else {
                this.rabbitListenCall();
              }
            } else {
              backgroundServer.stop();
              if (responseJson.sessionId != undefined) {
                sessionId = responseJson.sessionId;
              }
              AsyncStorage.setItem('ws_session_name', sessionId);
              AsyncStorage.setItem('ws_password', responseJson.password);
              AsyncStorage.setItem('csId', responseJson.csId);
              AsyncStorage.setItem('custName', custName);
              csId = responseJson.csId;
              if (AppState.currentState == 'active') {
                this.setState({popUpWaiting: false});
                count_popUpWaiting = 100; //Supaya tidak muncul lagi
                setTimeout(() => {
                  this.setState({
                    panggilanmasuk_outbond: true,
                    ws_session_name: sessionId, ///id dips
                    ws_password: responseJson.password, //////// dari respon si password
                    sessionKey: responseJson.password, ////////// dari respon si password
                    isMuted: false,
                    csId: responseJson.csId,
                    agentImage: responseJson.agentImage,
                  });
                }, 1000);
              } else if (AppState.currentState == 'background') {
                this.setState({popUpWaiting: false});
                count_popUpWaiting = 100; //Supaya tidak muncul lagi
                setTimeout(() => {
                  this.setState({
                    panggilanmasuk_outbond: true,
                    ws_session_name: sessionId, ///id dips
                    ws_password: responseJson.password, //////// dari respon si password
                    sessionKey: responseJson.password, ////////// dari respon si password
                    isMuted: false,
                    csId: responseJson.csId,
                    agentImage: responseJson.agentImage,
                  });
                }, 1000);
              }
            }
          }
        } else {
          if (statusCode == 408) {
            this.rabbitListenCall();
          }
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        this.rabbitListenCall();
      });
  }
  rabbitAcceptCall(actionCall) {
    var payload = {
      csId: this.state.csId,
      custId: idDips,
      ticket: this.state.data_ticket.ticket,
      action: actionCall,
    };

    fetch(`${Server.BASE_URL_API_RABBIT}${ApiService.rabbitAcceptCall}`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(payload),
    })
      .then(response => {
        const statusCode = response.status;
        const data = response.json();
        return Promise.all([statusCode, data]);
      })
      .then(([statusCode, responseJson]) => {})
      .catch(error => {
        this.setState({loadingvisible: false});
      });
  }
  popUpDoneSchedule() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.modalScheduleDone}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({modalScheduleDone: false});
        }}>
        <View
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View
              style={{
                margin: 100,
                backgroundColor: 'white',
                borderRadius: 20,
                justifyContent: 'space-between',
                padding: 35,
                width: '80%',
                alignItems: 'center',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
              }}>
              <Image
                source={logoSuccess}
                style={{width: 55, height: 55, marginBottom: 15}}
              />
              <Text
                style={{
                  marginBottom: 20,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  textAlign: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {bahasa === 'id'
                  ? id.konten_sukses_jadwalkan
                  : en.konten_sukses_jadwalkan}
              </Text>
              <Pressable
                style={{
                  width: '40%',
                  height: 43,
                  borderRadius: 10,
                  padding: 10,
                  elevation: 2,
                  backgroundColor: COLORS.red,
                }}
                onPress={() => {
                  this.setState({
                    modalScheduleDone: false,
                    popUpWaiting: false,
                  });
                  count_popUpWaiting === 100; //Supaya tidak muncul lagi
                  //RNExitApp.exitApp();
                }}>
                <Text style={styles.textbtn}>
                  {bahasa === 'id' ? id.selesai : en.selesai}
                </Text>
              </Pressable>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
  onSubmitScedule() {
    if (this.state.dateShcedule === '') {
      Alert.alert(
        bahasa === 'id'
          ? id.pilih_tanggal_jadwalkan
          : en.pilih_tanggal_jadwalkan,
      );
    } else if (this.state.idSchedule === null) {
      Alert.alert(
        bahasa === 'id' ? id.pilih_jam_jadwalkan : en.pilih_jam_jadwalkan,
      );
    } else {
      var formatLama = moment(this.state.dateShcedule, 'DD-MM-YYYY');
      var formatBaru = moment(formatLama).format('YYYY-MM-DD');
      count_popUpWaiting = 100; //Supaya tidak muncul lagi
      this.setState({
        modalscheduler: false,
        popUpWaiting: false,
        modalscheduler2: false,
      });
      this.saveSchedule(formatBaru);
    }
  }
  mirroringEndpoint(endpoint) {
    var payload = {
      csId: this.state.csId,
      endpoint: Number(endpoint),
    };
    fetch(Server.BASE_URL_API_RABBIT + ApiService.rabbitMirrorEnd, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      timeout: 1500,
      body: JSON.stringify(payload),
    })
      .then(response => response.json())
      .then(responseJson => {})
      .catch(error => {
        this.setState({loadingvisible: false});
        console.error(error);
        Alert.alert('Gagal Terhubung ke server');
      });
  }
  popUpScheduler() {
    const showDatePicker = () => {
      this.setState({isDatePickerVisible: true});
    };
    const hideDatePicker = () => {
      this.setState({isDatePickerVisible: false});
    };
    const handleConfirm = date => {
      const NewDate = moment(date).format('DD/MM/YYYY');
      this.setState({dateShcedule: NewDate});
      hideDatePicker();
    };
    const getValueDropdown = valu => {
      this.setState({idSchedule: valu.value, value: valu.label});
    };
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.modalscheduler}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({modalscheduler: false});
        }}>
        <TouchableOpacity
          onPress={() => {
            this.setState({modalscheduler: false});
          }}
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View
              style={{
                margin: 100,
                backgroundColor: 'white',
                borderRadius: 20,
                justifyContent: 'space-between',
                padding: 35,
                width: '80%',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
              }}>
              <Text
                style={{
                  marginBottom: 35,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  textAlign: 'left',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {bahasa === 'id'
                  ? id.konten_popup_jadwalkan
                  : en.konten_popup_jadwalkan}
              </Text>
              <Text
                style={{
                  marginBottom: 20,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  alignItems: 'flex-start',
                  textAlign: 'left',
                  fontWeight: 'bold',
                }}>
                {bahasa === 'id' ? id.hari_dan_tanggal : en.hari_dan_tanggal}
              </Text>
              <TouchableOpacity
                onPress={showDatePicker}
                style={{
                  justifyContent: 'center',
                  borderRadius: 5,
                  borderColor: COLORS.border_textInput,
                  borderWidth: 1.5,
                  marginBottom: 10,
                  width: '100%',
                  height: 50,
                  padding: 10,
                }}>
                <View style={{flexDirection: 'row'}}>
                  <Text
                    style={[
                      ,
                      {
                        alignSelf: 'flex-start',
                        textAlign: 'left',
                        fontFamily: 'Helvetica',
                        fontSize: 17,
                      },
                    ]}>
                    {this.state.dateShcedule}
                  </Text>
                </View>
              </TouchableOpacity>
              <DateTimePickerModal
                display="inline"
                date={new Date()}
                locale="id_ID"
                minimumDate={new Date()}
                accentColor={COLORS.red}
                buttonTextColorIOS={COLORS.red}
                cancelTextIOS={bahasa === 'id' ? id.batal : en.batal}
                confirmTextIOS={bahasa === 'id' ? id.pilih : en.pilih}
                isVisible={this.state.isDatePickerVisible}
                mode="date"
                onConfirm={handleConfirm}
                onCancel={hideDatePicker}
              />
              <Text
                style={{
                  marginBottom: 10,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  textAlign: 'left',
                  fontWeight: 'bold',
                }}>
                {bahasa === 'id' ? id.waktu : en.waktu}
              </Text>
              <DropDownPicker
                dropDownMaxHeight={250}
                dropDownDirection="BOTTOM"
                placeholder={
                  this.state.items.length == 0 ? '' : this.state.items[0].label
                }
                onSelectItem={getValueDropdown}
                style={{
                  borderColor: COLORS.border_textInput,
                  borderWidth: 1.5,
                  marginBottom: 20,
                }}
                labelStyle={{
                  fontFamily: 'Helvetica',
                  fontSize: 16,
                }}
                placeholderStyle={{
                  fontFamily: 'Helvetica',
                  fontSize: 16,
                }}
                listItemContainerStyle={{
                  borderColor: '#000',
                  borderBottomWidth: 1,
                }}
                listItemLabelStyle={{
                  fontFamily: 'Helvetica',
                  fontSize: 16,
                }}
                open={this.state.open}
                value={this.state.value}
                items={this.state.items}
                setOpen={set => {
                  this.setState({open: set});
                }}
              />
              <Pressable
                style={{
                  width: '100%',
                  height: 43,
                  borderRadius: 10,
                  padding: 10,
                  justifyContent: 'center',
                  alignItems: 'center',
                  elevation: 2,
                  backgroundColor: COLORS.red,
                }}
                onPress={() => {
                  this.onSubmitScedule();
                }}>
                <Text style={styles.textbtn}>
                  {bahasa === 'id'
                    ? id.jadwalkan_panggilan
                    : en.jadwalkan_panggilan}
                </Text>
              </Pressable>
            </View>
          </View>
        </TouchableOpacity>
      </Modal>
    );
  }
  popUpScheduler2() {
    const showDatePicker = () => {
      this.setState({isDatePickerVisible: true});
    };
    const hideDatePicker = () => {
      this.setState({isDatePickerVisible: false});
    };
    const handleConfirm = date => {
      const NewDate = moment(date).format('DD/MM/YYYY');
      this.setState({dateShcedule: NewDate});
      hideDatePicker();
    };
    const getValueDropdown = valu => {
      this.setState({idSchedule: valu.value, value: valu.label});
    };
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.modalscheduler2}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({modalscheduler2: false});
        }}>
        <View
          style={{
            backgroundColor: 'rgba(255, 255, 255, 100)',
            height: '100%',
            width: '100%',
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            marginTop: 0,
          }}>
          <View
            style={{
              margin: 100,
              backgroundColor: 'white',
              borderRadius: 20,
              justifyContent: 'space-between',
              padding: 35,
              width: '80%',
              shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: 0.5,
              shadowRadius: 7,
              elevation: 10,
            }}>
            <Text
              style={{
                marginBottom: 35,
                fontFamily: 'Helvetica',
                fontSize: 18,
                textAlign: 'left',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              {bahasa === 'id'
                ? id.redaksi_jadwalkan_outbound
                : en.redaksi_jadwalkan_outbound}
            </Text>
            <Text
              style={{
                marginBottom: 20,
                fontFamily: 'Helvetica',
                fontSize: 18,
                alignItems: 'flex-start',
                textAlign: 'left',
                fontWeight: 'bold',
              }}>
              {bahasa === 'id' ? id.hari_dan_tanggal : en.hari_dan_tanggal}
            </Text>
            <TouchableOpacity
              onPress={showDatePicker}
              style={{
                justifyContent: 'center',
                borderRadius: 5,
                borderColor: COLORS.border_textInput,
                borderWidth: 1.5,
                marginBottom: 10,
                width: '100%',
                height: 50,
                padding: 10,
              }}>
              <View style={{flexDirection: 'row'}}>
                <Text
                  style={[
                    ,
                    {
                      alignSelf: 'flex-start',
                      textAlign: 'left',
                      fontFamily: 'Helvetica',
                      fontSize: 17,
                    },
                  ]}>
                  {this.state.dateShcedule}
                </Text>
              </View>
            </TouchableOpacity>
            <DateTimePickerModal
              display="inline"
              date={new Date()}
              locale="id_ID"
              minimumDate={new Date()}
              accentColor={COLORS.red}
              buttonTextColorIOS={COLORS.red}
              cancelTextIOS={bahasa === 'id' ? id.batal : en.batal}
              confirmTextIOS={bahasa === 'id' ? id.pilih : en.pilih}
              isVisible={this.state.isDatePickerVisible}
              mode="date"
              onConfirm={handleConfirm}
              onCancel={hideDatePicker}
            />
            <Text
              style={{
                marginBottom: 10,
                fontFamily: 'Helvetica',
                fontSize: 18,
                textAlign: 'left',
                fontWeight: 'bold',
              }}>
              {bahasa === 'id' ? id.waktu : en.waktu}
            </Text>
            <DropDownPicker
              dropDownMaxHeight={250}
              dropDownDirection="BOTTOM"
              placeholder={
                this.state.items.length == 0 ? '' : this.state.items[0].label
              }
              onSelectItem={getValueDropdown}
              style={{
                borderColor: COLORS.border_textInput,
                borderWidth: 1.5,
                marginBottom: 20,
              }}
              labelStyle={{
                fontFamily: 'Helvetica',
                fontSize: 16,
              }}
              placeholderStyle={{
                fontFamily: 'Helvetica',
                fontSize: 16,
              }}
              listItemContainerStyle={{
                borderColor: '#000',
                borderBottomWidth: 1,
              }}
              listItemLabelStyle={{
                fontFamily: 'Helvetica',
                fontSize: 16,
              }}
              open={this.state.open}
              value={this.state.value}
              items={this.state.items}
              setOpen={set => {
                this.setState({open: set});
              }}
            />
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                justifyContent: 'center',
              }}>
              <Pressable
                style={[
                  styles.btn,
                  {
                    height: 'auto',
                    marginEnd: 10,
                    padding: 15,
                    width: 'auto',
                    backgroundColor: COLORS.red,
                    alignContent: 'center',
                    alignSelf: 'center',
                  },
                ]}
                labelStyle={styles.btnLabel}
                mode="outlined"
                onPress={() => {
                  this.setState({modalscheduler2: false});
                  RNExitApp.exitApp();
                }}>
                <Text
                  ellipsizeMode="tail"
                  numberOfLines={2}
                  style={{
                    textAlign: 'center',
                    fontSize: 15,
                    color: 'white',
                    textAlign: 'center',
                  }}>
                  {bahasa === 'id' ? id.akhiri : en.akhiri}
                </Text>
              </Pressable>
              <Pressable
                style={[
                  styles.btn,
                  {
                    alignContent: 'center',
                    alignSelf: 'center',
                    height: 'auto',
                    padding: 15,
                    width: 'auto',
                  },
                ]}
                labelStyle={styles.btnLabel}
                mode="contained"
                onPress={() => {
                  this.onSubmitScedule();
                }}>
                <Text
                  numberOfLines={1}
                  style={{
                    textAlign: 'center',
                    fontSize: 15,
                    color: COLORS.red,
                    textAlign: 'center',
                  }}>
                  {bahasa === 'id'
                    ? id.jadwalkan_panggilan
                    : en.jadwalkan_panggilan}
                </Text>
              </Pressable>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
  popupOutbound() {
    return (
      <View
        style={{
          backgroundColor: 'white',
          height: '100%',
          weight: '100%',
          display: this.state.panggilanmasuk_outbond ? 'flex' : 'none',
        }}>
        <Modal
          transparent
          animationType="fade"
          visible={this.state.panggilanmasuk_outbond}
          onRequestClose={() => this.setState({panggilanmasuk_outbond: false})}>
          <RNCamera
            ref={ref => {
              this.camera = ref;
            }}
            style={{
              justifyContent: 'space-between',
              height: '100%',
            }}
            type={this.state.cameraType}
            captureAudio={false}
            zoom={this.state.zoomcamera}
            ratio={this.state.ratio}>
            <View
              style={{
                backgroundColor: 'white',
                height: '35%',
                weight: '100%',
                justifyContent: 'center',
                borderBottomLeftRadius: 25,
                borderBottomRightRadius: 25,
                overflow: 'hidden',
              }}>
              <View>
                <AnimatedReact.View
                  style={{
                    transform: [
                      {
                        rotate: this.state.rotateValueHolder.interpolate({
                          inputRange: [0, 0.5, 1, 1.5],
                          outputRange: ['0deg', '8deg', '0deg', '8deg'],
                        }),
                      },
                    ],
                  }}>
                  <View
                    style={{
                      justifyContent: 'center',
                      width: 150,
                      height: 150,
                      backgroundColor: '#FAE5D6',
                      alignSelf: 'center',
                      paddingBottom: 5,
                      paddingTop: 5,
                      borderRadius: 80,
                      marginTop: '5%',
                      overflow: 'hidden',
                    }}>
                    <Image
                      style={{
                        width: 128,
                        height: 138,
                        alignSelf: 'center',
                        marginTop: 10,
                      }}
                      source={{
                        uri: this.state.agentImage,
                        headers: {
                          Authorization: 'Bearer ' + accessToken,
                          exchangeToken: exchangeToken,
                        },
                      }}
                    />
                  </View>
                </AnimatedReact.View>
                <View>
                  <Text
                    style={[
                      stylesglobal.textsedang_hitam_tebal,
                      {alignSelf: 'center', marginTop: 9},
                    ]}>
                    {bahasa === 'id' ? id.panggilan_masuk : en.panggilan_masuk}
                  </Text>
                  <Text
                    style={[
                      stylesglobal.textsedang_hitam_tebal,
                      {alignSelf: 'center', marginTop: 2},
                    ]}>
                    {bahasa === 'id' ? 'dari DIPS Agent' : 'from Agent DIPS'}
                  </Text>
                </View>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignSelf: 'center',
                width: '100%',
                justifyContent: 'space-between',
                bottom: 70,
              }}>
              <View style={{width: '42%'}}>
                <TouchableHighlight
                  style={{
                    justifyContent: 'center',
                    width: 80,
                    height: 80,
                    backgroundColor: '#FF002E',
                    alignSelf: 'flex-end',
                    marginTop: '25%',
                    paddingBottom: 5,
                    paddingTop: 5,
                    borderRadius: 50,
                  }}
                  onPress={() => {
                    this.mirroringEndpoint('99');
                    this.setState({panggilanmasuk_outbond: false});
                    setTimeout(() => {
                      this.setState({modalscheduler2: true});
                    }, 1000);
                  }}>
                  <View
                    style={{
                      width: 28,
                      height: 28,
                      justifyContent: 'center',
                      alignSelf: 'center',
                    }}>
                    <Image
                      style={{
                        alignSelf: 'center',
                      }}
                      source={rejectpanggilan}
                    />
                  </View>
                </TouchableHighlight>
              </View>

              <View style={{width: '42%'}}>
                <TouchableHighlight
                  style={{
                    justifyContent: 'center',
                    width: 80,
                    height: 80,
                    backgroundColor: '#22D882',
                    alignSelf: 'flex-start',
                    marginTop: '25%',
                    paddingBottom: 5,
                    paddingTop: 5,
                    borderRadius: 50,
                  }}
                  onPress={() => {
                    setTimeout(() => {
                      this.processSignature();
                      backgroundServer.stop();
                    }, 1000);
                  }}>
                  <View
                    style={{
                      width: 28,
                      height: 28,
                      justifyContent: 'center',
                      alignSelf: 'center',
                    }}>
                    <Image
                      style={{
                        alignSelf: 'center',
                      }}
                      source={terimapanggilan}
                    />
                  </View>
                </TouchableHighlight>
              </View>
            </View>
          </RNCamera>
        </Modal>
      </View>
    );
  }
  popUpApproveCalling() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.modalApproveCalling}
        statusBarTranslucent
        RequestClose={() => {
          this.setState({modalApproveCalling: false});
        }}>
        <TouchableOpacity
          // onPress={() => {
          //   this.setState({modalApproveCalling: false});
          // }}
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View
              style={{
                margin: 100,
                backgroundColor: 'white',
                borderRadius: 20,
                justifyContent: 'space-between',
                padding: 35,
                width: '80%',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
                alignItems: 'center',
              }}>
              <FastImage
                transition={false}
                source={logoSuccess}
                style={{width: 55, height: 55, marginBottom: 15}}
              />
              <Text
                style={{
                  marginBottom: 20,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  textAlign: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {bahasa === 'id' ? id.headline_success : en.headline_success}
              </Text>

              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Button
                  onPress={() => {
                    this.setState({modalApproveCalling: false});
                    this.mirroringEndpoint('99');
                    setTimeout(() => {
                      this.setState({popUpRejectCall: true});
                    }, 1000);
                  }}
                  textColor={COLORS.red}
                  labelStyle={styles.btnLabel}
                  style={[styles.btn, {marginEnd: 10}]}
                  mode="outlined">
                  {bahasa == 'id' ? id.reject : en.reject}
                </Button>
                <Button
                  style={styles.btn}
                  labelStyle={styles.btnLabel}
                  buttonColor={COLORS.red}
                  mode="contained"
                  onPress={() => {
                    this.processSignature();
                    this.rabbitAcceptCall('accept');
                  }}>
                  {bahasa == 'id' ? id.setuju : en.setuju}
                </Button>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      </Modal>
    );
  }
  processSignature() {
    this.setState({modalApproveCalling: false});
    //this.setState({loadingvisible: true});
    var body_signature = {
      sessionName: this.state.ws_session_name,
      role: 0,
      sessionKey: this.state.ws_password,
      userIdentity: custName,
    };

    fetch(`${Server.BASE_URL_API}${ApiService.signatureZoomRN}`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      body: JSON.stringify(body_signature),
    })
      .then(response => {
        this.setState({loadingvisible: false});
        const statusCode = response.status;
        const data = response.json();
        return Promise.all([statusCode, data]);
      })
      .then(([statusCode, responseJson]) => {
        var getSignature = responseJson.data.signature;
        this.setState({
          loadingvisible: false,
          zoomView: true,
        });
        AsyncStorage.setItem('signatureZoom', getSignature);
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        this.timeoutHandle = setTimeout(() => {
          this.setState({popUpFailedSiganture: true});
        }, 1000);
      });
  }
  popUpRejectCall() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.popUpRejectCall}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({popUpRejectCall: false});
        }}>
        <View
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View
              style={{
                margin: 100,
                backgroundColor: 'white',
                borderRadius: 20,
                justifyContent: 'space-between',
                padding: 35,
                width: '80%',
                alignItems: 'center',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
              }}>
              <FastImage
                source={logoInfo}
                style={{width: 55, height: 55, marginBottom: 15}}
              />
              <Text
                style={{
                  marginBottom: 20,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  textAlign: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {bahasa === 'id'
                  ? id.redaksi_reject_call
                  : en.redaksi_reject_call}
              </Text>
              <Button
                style={styles.btn}
                labelStyle={styles.btnLabel}
                buttonColor={COLORS.red}
                mode="contained"
                onPress={() => {
                  this.setState({popUpRejectCall: false});
                }}>
                OK
              </Button>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
  onSubmited = formFields => {
    if (this.state.jenisTransaksiId === 1) {
      if (
        formFields.bankpenerima === undefined ||
        formFields.bankpenerima === ''
      ) {
        Alert.alert(
          bahasa === 'id'
            ? id.silahkan_masukkan_bank_penerima
            : en.silahkan_masukkan_bank_penerima,
        );
      } else if (
        formFields.rekeningpenerima === undefined ||
        formFields.rekeningpenerima === ''
      ) {
        Alert.alert(
          bahasa === 'id'
            ? id.silahkan_masukkan_rekening_penerima
            : en.silahkan_masukkan_rekening_penerima,
        );
      } else if (
        formFields.namapenerima === undefined ||
        formFields.namapenerima === ''
      ) {
        Alert.alert(
          bahasa === 'id'
            ? id.silahkan_masukkan_nama_penerima
            : en.silahkan_masukkan_nama_penerima,
        );
      } else if (
        formFields.nominaltransaksi === undefined ||
        formFields.nominaltransaksi === ''
      ) {
        Alert.alert(
          bahasa === 'id'
            ? id.silahkan_masukkan_nominal_transaksi
            : en.silahkan_masukkan_nominal_transaksi,
        );
      } else if (
        formFields.jenislayanan === undefined ||
        formFields.jenislayanan === ''
      ) {
        Alert.alert(
          bahasa === 'id'
            ? id.silahkan_pilih_jenis_layanan
            : en.silahkan_pilih_jenis_layanan,
        );
      } else if (
        formFields.jenispenduduk === undefined ||
        formFields.jenispenduduk === ''
      ) {
        Alert.alert(
          bahasa === 'id'
            ? id.silahkan_pilih_jenis_penduduk
            : en.silahkan_pilih_jenis_penduduk,
        );
      } else if (formFields.berita === undefined || formFields.berita === '') {
        Alert.alert(bahasa === 'id' ? id.berita : en.berita);
      } else {
        console.log('Hasil Form', formFields);
        this.createQRCode(formFields);
      }
    }
  };
  async createQRCode(formFields) {
    this.setState({loadingvisible: true});
    var payload = {
      jenisTransaksi: formFields.jenislayanan,
      data: {
        bankpenerima: formFields.bankpenerima,
        jenislayanan: formFields.jenislayanan,
        jenispenduduk: formFields.jenispenduduk,
        namapenerima: formFields.namapenerima,
        nominaltransaksi: formFields.nominaltransaksi,
        rekeningpenerima: formFields.rekeningpenerima,
        berita: formFields.berita,
      },
    };
    fetch(Server.BASE_URL_API + ApiService.createQRForm, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: JSON.stringify(payload),
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({loadingvisible: false});
        if (responseJson.code < 300) {
          this.downloadQR(responseJson.data);
          this.setState({base64QR: responseJson.data, sessionTransaksi: 2});
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        console.error(error);
        Alert.alert('Gagal Terhubung ke server');
      });
  }
  getDropdown(url) {
    var dataItemList = [];
    var placeholder = '';
    var result = {};
    fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
    })
      .then(response => response.json())
      .then(responseJson => {
        responseJson.data.map(element => {
          if (responseJson.code < 300) {
            placeholder = responseJson.data[0];
            dataItemList.push({
              id: element.id !== undefined ? element.id : '',
              label: bahasa === 'id' ? element.labelIdn : element.labelEng,
              value: bahasa === 'id' ? element.labelIdn : element.labelEng,
            });
          }
        });
      })
      .catch(error => {
        Alert.alert('Gagal Terhubung ke server', error);
      });
    result = {
      placeholder:
        bahasa === 'id' ? '== Silakan Pilih ==' : '== Please Choose ==',
      list: dataItemList,
    };
    return result;
  }
  async formBuilder(formCode) {
    this.setState({loadingvisible: true});
    dataresp = [];
    fetch(`${Server.BASE_URL_API}${ApiService.formBuilder}${formCode}`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({loadingvisible: false});
        if (responseJson.code < 300) {
          const response = JSON.parse(responseJson.data.data);
          response.map((element, index) => {
            element.components.map(componentx => {
              dataresp.push({
                component: componentx.name,
                field_name: componentx.placeholder.placeholderIdn,
                is_mandatory: 'false',
                type:
                  componentx.label.labelIdn === 'NIK'
                    ? 'number'
                    : componentx.type,
                meta: {
                  multiline: componentx.name === 'TextArea' ? true : false,
                  numberOfLines: componentx.name === 'TextArea' ? 2 : 1,
                  listdd:
                    componentx.name === 'Dropdown' ||
                    componentx.name === 'Datalist'
                      ? this.getDropdown(componentx.url)
                      : '',
                  url: componentx.url,
                  label:
                    bahasa === 'id'
                      ? componentx.label.labelIdn
                      : componentx.label.labelEng,
                  pointer:
                    componentx.label.labelIdn === ''
                      ? element.components[index - 1].label.labelIdn
                      : componentx.label.labelIdn,
                  placeholder:
                    bahasa === 'id'
                      ? componentx.placeholder.placeholderIdn
                      : componentx.placeholder.placeholderEng,
                },
              });
            });
          });
          this.setState({sessionTransaksi: 1, setRefreshing: false});
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        Alert.alert('Gagal Terhubung ke server', error);
      });
  }
  downloadQR = base64_QR => {
    CameraRoll.save(base64_QR, {type: 'photo'})
      .then(result => {
        Alert.alert(
          bahasa == 'id' ? id.save_barcode_redaksi : en.save_barcode_redaksi,
        );
      })
      .catch(err => {});
  };
  onRefresh = () => {
    this.setState({setRefreshing: true});
    setTimeout(() => {
      this.formBuilder(this.state.jenisTransaksiId == 1 ? 44 : 0);
    }, 100);
  };
  loadingBar() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.loadingvisible}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({loadingvisible: false});
        }}>
        <View
          style={{
            backgroundColor: 'rgba(255,255,255, 0.8)',
            height: this.state.isFormTransaksi ? '100%' : '72%',
            width: '100%',
            position: 'absolute',
            bottom: 0,
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View style={styles.centeredView}>
              <View style={styles.containerLoading}>
                <FastImage
                  transition={false}
                  source={loadingvictoria}
                  style={styles.imgSize}
                />
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
  form_transaksi() {
    if (this.state.isFormTransaksi) {
      if (this.state.sessionTransaksi == 0) {
        return (
          <SafeAreaView
            style={{
              height: '90%',
              alignSelf: 'center',
              flexDirection: 'column',
            }}>
            <View
              style={{
                backgroundColor: 'white',
                flexDirection: 'row',
              }}>
              <Pressable
                onPress={() => {
                  this.setState({isFormTransaksi: false});
                }}
                style={{marginEnd: 10, alignSelf: 'center'}}>
                <FastImage
                  style={{
                    width: 30,
                    height: 30,
                  }}
                  source={back}
                />
              </Pressable>
              <Text
                style={{
                  marginTop: 15,
                  marginBottom: 15,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  fontWeight: 'bold',
                  borderBottomWidth: 1,
                  borderBottomColor: 'black',
                }}>
                {bahasa === 'id' ? id.transaksi : en.transaksi}
              </Text>
            </View>
            <FlatList
              data={this.formatData(
                this.state.itemLayanan,
                this.state.numColumns,
              )}
              renderItem={({item, index}) => (
                <TouchableOpacity
                  key={index}
                  onPress={() => {
                    if (item.id == 1) {
                      this.setState({sessionTransaksi: 1, jenisTransaksiId: 1});
                      this.formBuilder(44);
                    } else if (item.id == 2) {
                      this.setState({sessionTransaksi: 1, jenisTransaksiId: 2});
                    }
                  }}
                  style={{
                    alignSelf: 'center',
                    alignItems: 'center',
                    justifyContent: 'center',
                    alignContent: 'center',
                    width: 121,
                    marginTop: 10,
                    justifyContent: 'flex-start',
                    flexDirection: 'column',
                    height: 165,
                  }}>
                  <FastImage
                    style={{
                      width: 100,
                      height: 100,
                      alignContent: 'center',
                      alignSelf: 'center',
                      alignItems: 'center',
                      justifyContent: 'center',
                      marginBottom: 10,
                      borderRadius: 13,
                    }}
                    source={item.img}
                  />
                  <Text
                    ellipsizeMode="tail"
                    textAlign="center"
                    numberOfLines={2}
                    style={{
                      width: 121,
                      color: 'black',
                      fontSize: 15,
                      fontWeight: 'bold',
                      textAlign: 'center',
                      fontFamily: 'Helvetica',
                    }}>
                    {item.title}
                  </Text>
                </TouchableOpacity>
              )}
              numColumns={this.state.numColumns}
              keyExtractor={item => item.id}
            />
          </SafeAreaView>
        );
      } else if (this.state.sessionTransaksi == 1) {
        return (
          <SafeAreaView
            style={{
              height: '90%',
              alignSelf: 'center',
              flexDirection: 'column',
            }}>
            <View
              style={{
                marginStart: 20,
                width: 370,
                backgroundColor: 'white',
                flexDirection: 'row',
              }}>
              <Pressable
                onPress={() => {
                  this.setState({sessionTransaksi: 0});
                }}
                style={{marginEnd: 10, alignSelf: 'center'}}>
                <FastImage
                  style={{
                    width: 30,
                    height: 30,
                  }}
                  source={back}
                />
              </Pressable>
              <Text
                style={{
                  marginTop: 15,
                  marginBottom: 15,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  fontWeight: 'bold',
                  borderBottomWidth: 1,
                  borderBottomColor: 'black',
                }}>
                {this.state.jenisTransaksiId == 1
                  ? bahasa === 'id'
                    ? id.transaksi_antar_bank
                    : en.transaksi_antar_bank
                  : this.state.jenisTransaksiId == 2
                  ? bahasa === 'id'
                    ? id.transaksi_antar_rekening
                    : en.transaksi_antar_rekening
                  : ''}
              </Text>
            </View>
            <ScrollView
              style={{
                display: this.state.loadingvisible ? 'none' : 'flex',
              }}
              contentContainerStyle={{paddingStart: 20, paddingEnd: 20}}
              keyboardDismissMode="none"
              automaticallyAdjustKeyboardInsets="true"
              automaticallyAdjustContentInsets="true"
              refreshControl={
                <RefreshControl
                  refreshing={this.state.setRefreshing}
                  onRefresh={this.onRefresh}
                />
              }>
              <DynamicForm
                bahasa={bahasa}
                accessToken={accessToken}
                exchangeToken={exchangeToken}
                formCode={this.state.formCode}
                formTemplate={{data: dataresp}}
                onSubmit={this.onSubmited}
                defaultValue={{}}
                onPressed={() => {}}
                btnStyle={{
                  borderRadius: 12,
                  backgroundColor: COLORS.red,
                  top: 20,
                  width: 310,
                  alignSelf: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
                btnLabel={bahasa === 'id' ? id.lanjut : en.lanjut}
              />
            </ScrollView>
          </SafeAreaView>
        );
      } else if (this.state.sessionTransaksi == 2) {
        return (
          <SafeAreaView
            style={{
              height: '90%',
              alignSelf: 'center',
              flexDirection: 'column',
            }}>
            <View
              style={{
                marginStart: 20,
                width: 370,
                backgroundColor: 'white',
                flexDirection: 'row',
              }}>
              <Pressable
                onPress={() => {
                  this.setState({sessionTransaksi: 0});
                }}
                style={{marginEnd: 10, alignSelf: 'center'}}>
                <FastImage
                  style={{
                    width: 30,
                    height: 30,
                  }}
                  source={back}
                />
              </Pressable>
              <Text
                style={{
                  marginTop: 15,
                  marginBottom: 15,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  fontWeight: 'bold',
                  borderBottomWidth: 1,
                  borderBottomColor: 'black',
                }}>
                {this.state.jenisTransaksiId == 1
                  ? bahasa === 'id'
                    ? id.transaksi_antar_bank
                    : en.transaksi_antar_bank
                  : this.state.jenisTransaksiId == 2
                  ? bahasa === 'id'
                    ? id.transaksi_antar_rekening
                    : en.transaksi_antar_rekening
                  : ''}
              </Text>
            </View>
            <ScrollView
              contentContainerStyle={{paddingStart: 20, paddingEnd: 20}}
              keyboardDismissMode="none"
              automaticallyAdjustKeyboardInsets="true"
              automaticallyAdjustContentInsets="true"
              refreshControl={
                <RefreshControl
                  refreshing={this.state.setRefreshing}
                  onRefresh={this.onRefresh}
                />
              }>
              <View
                style={{
                  height: '100%',
                  padding: 30,
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 10,
                    marginBottom: 20,
                    backgroundColor: 'white',
                    height: 60,
                    width: '100%',
                  }}>
                  <FastImage
                    source={logoSuccess}
                    resizeMode="contain"
                    style={{width: '100%', height: '100%'}}
                  />
                </View>
                <Text
                  textAlign="center"
                  numberOfLines={5}
                  style={{
                    marginBottom: 20,
                    fontFamily: 'Helvetica',
                    fontSize: 16,
                    textAlign: 'center',
                    alignItems: 'center',
                    textAlign: 'center',
                    justifyContent: 'center',
                  }}>
                  {bahasa === 'id' ? id.headline_qrcode : en.headline_qrcode}
                </Text>
                <View
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginBottom: 20,
                    borderRadius: 10,
                    backgroundColor: 'white',
                    height: 250,
                    width: '100%',
                  }}>
                  <FastImage
                    source={{
                      uri: encodeURI(this.state.base64QR),

                      priority: FastImage.priority.high,
                    }}
                    // source={qrcode}
                    resizeMode="contain"
                    style={{width: '100%', height: '100%'}}
                  />
                </View>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <Button
                    onPress={() => {
                      this.setState({sessionTransaksi: 1});
                      this.formBuilder(44);
                    }}
                    style={[
                      styles.btn,
                      {
                        width: 'auto',
                        alignSelf: 'center',
                      },
                    ]}
                    textColor={COLORS.red}
                    labelStyle={styles.btnLabel}
                    buttonColor={COLORS.white}
                    mode="contained">
                    {bahasa === 'id' ? id.tambah_formulir : en.tambah_formulir}
                  </Button>
                  <Button
                    onPress={() => {
                      this.setState({
                        isFormTransaksi: false,
                        sessionTransaksi: 0,
                      });
                    }}
                    style={[
                      styles.btn,
                      {
                        width: 'auto',
                        alignSelf: 'center',
                      },
                    ]}
                    labelStyle={styles.btnLabel}
                    buttonColor={COLORS.red}
                    mode="contained">
                    {bahasa === 'id' ? id.selesai : en.selesai}
                  </Button>
                </View>
              </View>
            </ScrollView>
          </SafeAreaView>
        );
      }
    }
  }
  render() {
    if (this.state.zoomView == true) {
      return (
        <ZoomVideoSdkProvider
          config={{
            appGroupId: '',
            domain: 'zoom.us',
            enableLog: true,
          }}>
          <ZoomHook>
            {(zoom, isMounted) => (
              <BaseMeetingNew
                zoom={zoom}
                isMounted={isMounted}
                navigation={this.props.navigation}
              />
            )}
          </ZoomHook>
        </ZoomVideoSdkProvider>
      );
    } else {
      return (
        <View
          style={{
            flex: 1,
            width: '100%',
            height: '100%',
            backgroundColor: 'white',
          }}>
          <StatusBar hidden="true" />
          {this.popUpScheduler()}
          {this.popUpScheduler2()}
          {this.popUpDoneSchedule()}
          {this.popUpApproveCalling()}
          {this.popUpWaitingAgent()}
          {this.popupOutbound()}
          {this.popUpFailedSignature()}
          {this.popUpRejectCall()}
          {this.popUpProduct()}
          {this.loadingBar()}
          <View style={[styles.containerBatik, {flexDirection: 'column'}]}>
            <ImageBackground
              transition={false}
              style={{
                width: '100%',
                height: 210,
                bottom: this.state.isLoading ? 0 : 20,
                position: 'absolute',
              }}
              imageStyle={{
                width: '100%',
                height: '100%',
                resizeMode: 'contain',
                alignItems: 'flex-end',
              }}
              source={batikbvic}
            />
            <View
              style={{
                alignItems: 'center',
                flexDirection: 'column',
                height: '100%',
              }}>
              {this.form_transaksi()}
              <View
                style={[
                  styles.upperViewWaiting,
                  {
                    flexDirection: 'row',
                    display: this.state.isFormTransaksi ? 'none' : 'flex',
                  },
                ]}>
                <View
                  style={{
                    padding: 20,
                    width: '50%',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <View
                    style={{
                      alignSelf: 'center',
                      height: 145,
                      width: 145,
                      backgroundColor: '#E4E4E4',
                      borderRadius: 80,
                      overflow: 'hidden',
                    }}>
                    <RNCamera
                      captureAudio={false}
                      ref={ref => {
                        this.camera = ref;
                      }}
                      style={{
                        alignSelf: 'center',
                        height: '100%',
                        width: '100%',
                      }}
                      type={'front'}
                    />
                  </View>
                </View>
                <View
                  style={{
                    padding: 20,
                    width: '50%',
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'space-evenly',
                  }}>
                  <View
                    style={{
                      justifyContent: 'center',
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                    <FastImage
                      transition={false}
                      source={dot}
                      style={{
                        right: 5,
                        width: 14,
                        height: 14,
                      }}
                    />
                    <View
                      style={{
                        justifyContent: 'center',
                        width: '100%',
                        borderRadius: 9,
                        backgroundColor: 'white',
                        height: 32,
                      }}>
                      <Text
                        ellipsizeMode="tail"
                        numberOfLines={1}
                        style={{
                          marginLeft: 10,
                          marginRight: 10,
                          textAlign: 'left',
                          fontSize: 15,
                          fontFamily: 'Helvetica',
                          fontWeight: 'bold',
                        }}>
                        {this.state.data_currentTicket.ticket
                          ? this.state.data_currentTicket.ticket.padStart(
                              3,
                              '0',
                            )
                          : '000'}
                        &nbsp;
                        {bahasa == 'id'
                          ? id.antrian_sekarang
                          : en.antrian_sekarang}
                      </Text>
                    </View>
                  </View>

                  <View
                    style={{
                      justifyContent: 'center',
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                    <FastImage
                      transition={false}
                      source={people_three}
                      style={{
                        right: 5,
                        width: 16,
                        height: 14,
                      }}
                    />
                    <View
                      style={{
                        justifyContent: 'center',
                        width: '100%',
                        borderRadius: 9,
                        backgroundColor: COLORS.red,
                        height: 32,
                      }}>
                      <Text
                        ellipsizeMode="tail"
                        numberOfLines={1}
                        style={{
                          color: 'white',
                          marginLeft: 10,
                          marginEnd: 10,
                          textAlign: 'left',
                          fontSize: 15,
                          fontFamily: 'Helvetica',
                          fontWeight: 'bold',
                        }}>
                        {this.state.data_ticket.ticket
                          ? this.state.data_ticket.ticket.padStart(3, '0')
                          : '000'}
                        &nbsp;
                        {bahasa == 'id' ? id.antrian_saya : en.antrian_saya}
                      </Text>
                    </View>
                  </View>

                  <Pressable
                    onPress={() => {
                      this.setState({
                        isFormTransaksi: true,
                        sessionTransaksi: 0,
                      });
                    }}
                    style={{
                      display: 'none',
                      justifyContent: 'center',
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                    <FastImage
                      transition={false}
                      source={form_transact}
                      style={{
                        right: 5,
                        width: 14,
                        height: 14,
                      }}
                    />
                    <View
                      style={{
                        paddingStart: 10,
                        paddingEnd: 10,
                        alignItems: 'center',
                        width: '100%',
                        borderRadius: 9,
                        backgroundColor: 'white',
                        height: 32,
                        justifyContent: 'space-between',
                        flexDirection: 'row',
                      }}>
                      <Text
                        ellipsizeMode="tail"
                        numberOfLines={1}
                        style={{
                          textAlign: 'left',
                          fontSize: 15,
                          marginRight: 10,
                          fontFamily: 'Helvetica',
                          fontWeight: 'bold',
                        }}>
                        {bahasa === 'id'
                          ? id.form_transaksi
                          : en.form_transaksi}
                      </Text>
                      <FastImage
                        transition={false}
                        source={icon_next_play}
                        style={{
                          width: 10,
                          height: 10,
                        }}
                      />
                    </View>
                  </Pressable>
                </View>
              </View>

              <View
                style={{
                  width: '90%',
                  height: '65%',
                  marginTop: 40,
                  flexDirection: 'row',
                }}>
                <FlatList
                  data={this.state.itemUrlMedia}
                  renderItem={this.renderCardItem}
                  numColumns={2}
                />
              </View>

              <View style={styles.bottomView}>
                <View style={{flexDirection: 'row', bottom: 0}}>
                  <Button
                    textColor={COLORS.red}
                    labelStyle={styles.btnLabel}
                    style={[styles.btn, {marginEnd: 10}]}
                    mode="outlined"
                    onPress={() => {
                      this.setState({modalscheduler: true});
                      backgroundServer.stop();
                    }}>
                    {bahasa == 'id'
                      ? id.jadwalkan_panggilan
                      : en.jadwalkan_panggilan}
                  </Button>
                  <Button
                    style={styles.btn}
                    labelStyle={styles.btnLabel}
                    buttonColor={COLORS.red}
                    mode="contained"
                    onPress={() => {
                      RNExitApp.exitApp();
                    }}>
                    {bahasa == 'id' ? id.akhiri_panggilan : en.akhiri_panggilan}
                  </Button>
                </View>
              </View>
            </View>
          </View>
        </View>
      );
    }
  }
}
class ratingActivity extends Component {
  state = {
    loadingvisible: false,
    ratingStar: 0,
    comment: '',
    popUpFinish: false,
  };
  loadingBar() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.loadingvisible}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({loadingvisible: false});
        }}>
        <View
          style={{
            backgroundColor: 'rgba(52, 52, 52, 0.8)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View style={styles.centeredView}>
              <View style={styles.containerLoading}>
                <FastImage
                  transition={false}
                  source={loadingvictoria}
                  style={styles.imgSize}
                />
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
  async ratingAgent() {
    var payload = {
      idDips: idDips,
      idAgent: Number(csId),
      rating: this.state.ratingStar.toString(),
      komentar: this.state.comment,
    };
    fetch(Server.BASE_URL_API + ApiService.ratingAgent, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + accessToken,
        exchangeToken: exchangeToken,
      },
      timeout: 1500,
      body: JSON.stringify(payload),
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({loadingvisible: false});
        if (responseJson.code < 300) {
          setTimeout(() => {
            this.setState({popUpFinish: true});
          }, 2000);
        } else {
          Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        this.setState({loadingvisible: false});
        console.error(error);
        Alert.alert('Gagal Terhubung ke server', JSON.stringify(error));
      });
  }
  popUpFinish() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.popUpFinish}
        statusBarTranslucent
        onRequestClose={() => {
          this.setState({popUpFinish: false});
        }}>
        <View
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            height: '100%',
            width: '100%',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 0,
            }}>
            <View
              style={{
                margin: 100,
                backgroundColor: 'white',
                borderRadius: 20,
                justifyContent: 'space-between',
                padding: 35,
                width: '80%',
                alignItems: 'center',
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
              }}>
              <FastImage
                source={logoSuccess}
                style={{width: 55, height: 55, marginBottom: 15}}
              />
              <Text
                style={{
                  marginBottom: 20,
                  fontFamily: 'Helvetica',
                  fontSize: 18,
                  textAlign: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {bahasa === 'id' ? id.pop_up_rating : en.pop_up_rating}
              </Text>
              <Pressable
                style={{
                  width: '40%',
                  height: 43,
                  borderRadius: 10,
                  padding: 10,
                  elevation: 2,
                  backgroundColor: COLORS.red,
                }}
                onPress={() => {
                  this.setState({popUpFinish: false});
                  RNExitApp.exitApp();
                }}>
                <Text style={styles.textbtn}>
                  {bahasa === 'id' ? id.selesai : en.selesai}
                </Text>
              </Pressable>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
  render() {
    return (
      <View
        style={{
          flex: 1,
          width: '100%',
          height: '100%',
          backgroundColor: COLORS.colorBG,
          flexDirection: 'column',
        }}>
        <StatusBar hidden="true" />
        {this.loadingBar()}
        {this.popUpFinish()}
        <FastImage
          transition={false}
          source={batikbvic}
          style={{
            width: '100%',
            height: 230,
            position: 'absolute',
            bottom: 0,
            resizeMode: 'contain',
          }}
        />
        <FastImage
          resizeMode="contain"
          style={{
            marginStart: 20,
            marginEnd: 20,
            marginBottom: 8,
            marginTop: 30,
            height: 100,
          }}
          source={logo_merah}
        />
        <Text
          style={{
            fontSize: 22,
            textAlign: 'center',
          }}>
          {bahasa === 'id' ? id.headline_rating : en.headline_rating}
        </Text>

        <View
          style={{
            marginTop: 20,
            marginStart: 20,
            marginEnd: 20,
            borderColor: '#b3b3b3',
            borderWidth: 1,
            borderRadius: 15,
            flexDirection: 'column',
            padding: 10,
          }}>
          <Text style={{fontSize: 20, textAlign: 'center'}}>
            {bahasa === 'id' ? id.rating_1 : en.rating_1}
          </Text>
          <Rating
            type="custom"
            minValue={0}
            startingValue={0}
            onFinishRating={rating => {
              this.setState({ratingStar: rating});
            }}
            tintColor={'white'}
            ratingBackgroundColor="gray"
            style={{
              marginTop: 10,
              marginBottom: 10,
              width: '100%',
            }}
          />
        </View>

        <TextInput
          keyboardType="default"
          multiline={true}
          numberOfLines={3}
          textAlign="left"
          onChangeText={text => this.setState({comment: text})}
          placeholder={
            bahasa === 'id' ? id.placeholder_rating : en.placeholder_rating
          }
          style={{
            margin: 20,
            fontSize: 20,
            borderRadius: 15,
            padding: 10,
            height: 120,
            borderColor: '#b3b3b3',
            borderWidth: 1,
          }}
        />
        <Pressable
          style={{
            marginRight: 20,
            marginLeft: 20,
            borderRadius: 10,
            padding: 12,
            elevation: 2,
            backgroundColor:
              this.state.ratingStar == 0 ? COLORS.btnFalse : COLORS.red,
          }}
          onPress={() => {
            if (this.state.ratingStar == 0) {
              Alert.alert(
                bahasa === 'id' ? id.alert_rating_agen : en.alert_rating_agen,
              );
            } else {
              this.setState({loadingvisible: true});
              this.ratingAgent();
            }
          }}>
          <Text style={styles.textbtn}>
            {bahasa === 'id' ? id.kirim : en.kirim}
          </Text>
        </Pressable>
      </View>
    );
  }
}

//RootStacks
const RootStack = createStackNavigator(
  {
    ConnectionForm: {
      screen: ConnectionForm,
      navigationOptions: {
        title: 'ConnectionForm',
        headerShown: false,
      },
    },
    SplashScreen: {
      screen: SplashScreen,
      navigationOptions: {
        title: 'SplashScreen',
        headerShown: false,
      },
    },
    H5Liveness: {
      screen: H5Liveness,
      navigationOptions: {
        title: 'H5Liveness',
        headerShown: false,
      },
    },
    DipsSwafoto: {
      screen: DipsSwafoto,
      navigationOptions: {
        title: 'DipsSwafoto',
        headerShown: false,
      },
    },
    DipsWaitingRoom: {
      screen: DipsWaitingRoom,
      navigationOptions: {
        title: 'DipsWaitingRoom',
        headerShown: false,
      },
    },
    ratingActivity: {
      screen: ratingActivity,
      navigationOptions: {
        title: 'ratingActivity',
        headerShown: false,
      },
    },
    baseMeetingNew: {
      screen: BaseMeetingNew,
      navigationOptions: {
        title: 'baseMeetingNew',
        headerShown: false,
      },
    },
  },
  {
    initialRouteName: 'SplashScreen',
  },
);
const AppContainer = createAppContainer(RootStack);

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {zoomView: false};
  }
  render() {
    return <AppContainer />;
  }
}

export default App;
///NO REK MAS ADE 1028297024

import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  Alert,
  Image,
} from 'react-native';
import {Icon} from '../icon';
import {useIsMounted} from '../../utils/hooks';
import {
  EventType,
  VideoAspect,
  useZoom,
  ZoomView,
  ZoomVideoSdkUser,
  ZoomVideoSdkUserType,
} from '@zoom/react-native-videosdk';
import FastImage from 'react-native-fast-image';
const SHOW_TALKING_ICON_DURATION = 2000;
const zoomIcon = require('../../../asset/zoomtopicon.png');

export interface VideoViewInterface {
  user?: ZoomVideoSdkUser;
  sharing?: boolean;
  fullScreen?: boolean;
  focused?: boolean;
  videoAspect?: VideoAspect;
  onPress: (user: ZoomVideoSdkUser) => void;
}

var dataitem = [];
var dataitemfilter = [];

export function VideoView2(props: VideoViewInterface) {
  const {user, sharing, fullScreen, focused, videoAspect, onPress} = props;
  const [isVideoOn, setIsVideoOn] = useState(false);
  const [isTalking, setIsTalking] = useState(false);
  const [isMuted, setIsMuted] = useState(false);
  const isMounted = useIsMounted();
  const zoom = useZoom();

  dataitemfilter = dataitem.filter(data => data.namename == user?.userName);

  if (JSON.stringify(dataitemfilter) == '[]') {
    var datajson = {
      namename: user?.userName,
    };
    dataitem.push(datajson);
  }

  useEffect(() => {
    const updateVideoStatus = () => {
      if (!user) return;
      (async () => {
        // isMounted() && setIsVideoOn(await user.videoStatus.isOn());
        isMounted() && setIsVideoOn(true);
        isMounted() && setIsTalking(true);
        isMounted() && setIsMuted(true);

        // Alert.alert("usr", JSON.stringify(user.videoStatus));
      })();
    };

    const resetAudioStatus = () => {
      setIsTalking(false);
      setIsMuted(false);
    };

    const updateAudioStatus = async () => {
      if (!isMounted()) return;
      const muted = await user?.audioStatus.isMuted();
      const talking = await user?.audioStatus.isTalking();
      setIsMuted(muted);
      setIsTalking(talking);
      if (talking) {
        setTimeout(() => {
          isMounted() && setIsTalking(false);
        }, SHOW_TALKING_ICON_DURATION);
      }
    };

    updateVideoStatus();

    const userVideoStatusChangedListener = zoom.addListener(
      EventType.onUserVideoStatusChanged,
      async ({changedUsers}: {changedUsers: ZoomVideoSdkUserType[]}) => {
        changedUsers.map((u: ZoomVideoSdkUserType) => {
          if (user && u.userId === user.userId) {
            updateVideoStatus();
            return;
          }
        });
      },
    );

    const userAudioStatusChangedListener = zoom.addListener(
      EventType.onUserAudioStatusChanged,
      async ({changedUsers}: {changedUsers: ZoomVideoSdkUserType[]}) => {
        changedUsers.map((u: ZoomVideoSdkUserType) => {
          if (user && u.userId === user.userId) {
            (async () => {
              if (!isMounted()) return;
              resetAudioStatus();
              await updateAudioStatus();
            })();
            return;
          }
        });
      },
    );

    const userActiveAudioChangedListener = zoom.addListener(
      EventType.onUserActiveAudioChanged,
      async ({changedUsers}: {changedUsers: ZoomVideoSdkUserType[]}) => {
        changedUsers.map((u: ZoomVideoSdkUserType) => {
          if (user && u.userId === user.userId) {
            (async () => {
              if (!isMounted()) return;
              await updateAudioStatus();
            })();
            return;
          }
        });
      },
    );

    return () => {
      userVideoStatusChangedListener.remove();
      userAudioStatusChangedListener.remove();
      userActiveAudioChangedListener.remove();
    };
  }, [zoom, user, isMounted]);

  if (!user) return null;

  const audioStatusIcon = (isMuted && 'muted') || (isTalking && 'talking');
  const smallView = [styles.smallView, focused && styles.focusedBorder];
  const containerStyle = fullScreen ? styles.fullScreen : smallView;
  const avatarStyle = fullScreen ? styles.avatarLarge : styles.avatarSmall;
  const aspect = videoAspect || VideoAspect.PanAndScan;

  if (dataitem.length.toString() == '1') {
    if (user.isHost == false) {
      return (
        <TouchableWithoutFeedback
          onPress={() => onPress(user)}
          style={{alignSelf: 'center', justifyContent: 'center'}}>
          <View
            style={{
              alignSelf: 'center',
              justifyContent: 'center',
              flexDirection: 'row',
            }}>
            <View
              style={{
                width: 198,
                height: 201,
                borderWidth: 0,
                borderTopLeftRadius: 18,
                borderBottomLeftRadius: 18,
                overflow: 'hidden',
                borderColor: 'transparent',
                backgroundColor: '#232323',
                alignItems: 'center',
                justifyContent: 'center',
                marginTop: '5%',
                marginBottom: '48%',
              }}>
              {isVideoOn || sharing ? (
                <ZoomView
                  style={styles.zoomView}
                  userId={user.userId}
                  sharing={sharing}
                  fullScreen={fullScreen}
                  videoAspect={aspect}
                />
              ) : (
                <Icon style={avatarStyle} name="defaultAvatar" />
              )}
              {!fullScreen && (
                <View style={styles.userInfo}>
                  <Text
                    style={styles.userName}
                    ellipsizeMode="tail"
                    numberOfLines={1}>
                    {user.userName}
                  </Text>
                  {audioStatusIcon && (
                    <Icon
                      style={styles.audioStatusIcon}
                      name={audioStatusIcon}
                    />
                  )}
                </View>
              )}
            </View>

            <View
              style={{
                width: 198,
                height: 201,
                borderWidth: 0,
                borderTopRightRadius: 18,
                borderBottomRightRadius: 18,
                overflow: 'hidden',
                borderColor: 'transparent',
                backgroundColor: 'black',
                marginTop: '5%',
                marginBottom: '48%',
              }}></View>
          </View>
        </TouchableWithoutFeedback>
      );
    }
  }
  if (dataitem.length.toString() == '2') {
    if (user.isHost == false) {
      return (
        <TouchableWithoutFeedback>
          <View
            style={{
              width: 180,
              height: 180,
              borderWidth: 0,
              borderTopLeftRadius: 18,
              borderBottomLeftRadius: 18,
              overflow: 'hidden',
              borderColor: 'transparent',
              backgroundColor: '#232323',
              alignItems: 'center',
              justifyContent: 'center',
              // marginTop: '5%',
              // marginBottom: '48%',
            }}>
            {isVideoOn || sharing ? (
              <ZoomView
                style={styles.zoomView}
                userId={user.userId}
                sharing={sharing}
                fullScreen={fullScreen}
                videoAspect={aspect}
              />
            ) : (
              <Icon style={avatarStyle} name="defaultAvatar" />
            )}
            {!fullScreen && (
              <View style={styles.userInfo}>
                <Text
                  style={styles.userName}
                  ellipsizeMode="tail"
                  numberOfLines={1}>
                  {user.userName}
                </Text>
                {audioStatusIcon && (
                  <Icon style={styles.audioStatusIcon} name={audioStatusIcon} />
                )}
              </View>
            )}
          </View>
        </TouchableWithoutFeedback>
      );
    }
    if (user.isHost) {
      return (
        <TouchableWithoutFeedback>
          <View
            style={{
              width: 180,
              height: 180,
              borderWidth: 0,
              borderTopRightRadius: 18,
              borderBottomRightRadius: 18,
              overflow: 'hidden',
              borderColor: 'transparent',
              backgroundColor: '#232323',
              alignItems: 'center',
              justifyContent: 'center',
              // marginTop: '5%',
              // marginBottom: '100%',
            }}>
            {isVideoOn || sharing ? (
              <ZoomView
                style={styles.zoomView}
                userId={user.userId}
                sharing={sharing}
                fullScreen={fullScreen}
                videoAspect={aspect}
              />
            ) : (
              <Icon style={avatarStyle} name="defaultAvatar" />
            )}
            {!fullScreen && (
              <View style={styles.userInfo}>
                <View
                  style={{
                    alignItems: 'center',
                    flexDirection: 'row',
                  }}>
                  <Text
                    style={styles.userName}
                    ellipsizeMode="tail"
                    numberOfLines={1}>
                    {user.userName}
                  </Text>
                  {audioStatusIcon && (
                    <Icon
                      style={styles.audioStatusIcon}
                      name={audioStatusIcon}
                    />
                  )}
                </View>
                <Image
                  resizeMode="contain"
                  style={{width: 35, height: 15, marginEnd: 10}}
                  source={zoomIcon}
                />
              </View>
            )}
          </View>
        </TouchableWithoutFeedback>
      );
    }
  }
}

const styles = StyleSheet.create({
  fullScreen: {
    flex: 0,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    alignContent: 'center',
  },
  smallView: {
    width: 200,
    height: 201,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    alignContent: 'center',
    borderWidth: 0,
    // borderTopRightRadius: 28,
    // borderBottomRightRadius: 28,
    borderRadius: 28,
    borderColor: 'transparent',
    backgroundColor: '#232323',

    marginBottom: '100%',
  },
  zoomView: {
    width: '100%',
    height: '100%',
    alignSelf: 'center',
    alignContent: 'center',
  },
  focusedBorder: {
    borderColor: '#0FFF13',
  },
  avatarLarge: {
    width: 200,
    height: 200,
  },
  avatarSmall: {
    width: 60,
    height: 60,
  },
  userInfo: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    position: 'absolute',
    left: 0,
    bottom: 0,
    width: '100%',
    // justifyContent: 'space-between',
    alignItems: 'center',
    alignSelf: 'center',
    paddingVertical: 2,
    paddingHorizontal: 8,
    // borderBottomLeftRadius: 8,
    // borderBottomRightRadius: 8,
    backgroundColor: 'rgba(0,0,0,0.6)',
  },
  userName: {
    fontSize: 12,
    color: '#FFF',
  },
  audioStatusIcon: {
    width: 12,
    height: 12,
  },
});

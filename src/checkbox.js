import * as React from 'react';
import {View, TouchableOpacity, Text} from 'react-native';
import {Checkbox} from 'react-native-paper';

function CheckBox({
  label,
  status,
  onPress,
  boxStyle,
  labelStyle,
  boxColor,
  containerStyle,
}) {
  return (
    <TouchableOpacity onPress={onPress}>
      <View
        style={[containerStyle, {flexDirection: 'row', alignItems: 'center'}]}>
        <Checkbox.Android status={status} style={boxStyle} color={boxColor} />
        <Text style={labelStyle}>{label}</Text>
      </View>
    </TouchableOpacity>
  );
}

export default CheckBox;

const React = require("react-native");
const { Platform } = React;
const { Dimensions } = React;
const width = Dimensions.get("window").width;

export default {

    textbesar_hitam: {
        fontFamily: "Helvetica", 
        fontSize: 22, 
        color: "black"
    },
    textsedang_hitam: {
        fontFamily: "Helvetica", 
        fontSize: 18, 
        color: "black"
    },
    textkecil_hitam: {
        fontFamily: "Helvetica", 
        fontSize: 12, 
        color: "black"
    },
    textsangatkecil_hitam: {
        fontFamily: "Helvetica", 
        fontSize: 10, 
        color: "black"
    },
    ////////////////////////////////////////
    textbesar_hitam_tebal: {
        fontFamily: "Helvetica", 
        fontSize: 22, 
        color: "black",
        fontWeight: "bold"
    },
    textsedang_hitam_tebal: {
        fontFamily: "Helvetica", 
        fontSize: 18, 
        color: "black",
        fontWeight: "bold"
    },
    textkecil_hitam_tebal: {
        fontFamily: "Helvetica", 
        fontSize: 12, 
        color: "black",
        fontWeight: "bold"
    },
    textsangatkecil_hitam_tebal: {
        fontFamily: "Helvetica", 
        fontSize: 10, 
        color: "black",
        fontWeight: "bold"
    },
    











    textbesar_putih: {
        fontFamily: "Helvetica", 
        fontSize: 22, 
        color: "white"
    },
    textsedang_putih: {
        fontFamily: "Helvetica", 
        fontSize: 18, 
        color: "white"
    },
    textkecil_putih: {
        fontFamily: "Helvetica", 
        fontSize: 12, 
        color: "white"
    },
    textsangatkecil_putih: {
        fontFamily: "Helvetica", 
        fontSize: 10, 
        color: "white"
    },
    ////////////////////////////////////////
    textbesar_putih_tebal: {
        fontFamily: "Helvetica", 
        fontSize: 22, 
        color: "white",
        fontWeight: "bold"
    },
    textsedang_putih_tebal: {
        fontFamily: "Helvetica", 
        fontSize: 18, 
        color: "white",
        fontWeight: "bold"
    },
    textkecil_putih_tebal: {
        fontFamily: "Helvetica", 
        fontSize: 12, 
        color: "white",
        fontWeight: "bold"
    },
    textsangatkecil_putih_tebal: {
        fontFamily: "Helvetica", 
        fontSize: 10, 
        color: "white",
        fontWeight: "bold"
    },














    textbesar_birumuda: {
        fontFamily: "Helvetica", 
        fontSize: 22, 
        color: "#1E90FF"
    },
    textsedang_birumuda: {
        fontFamily: "Helvetica", 
        fontSize: 18, 
        color: "#1E90FF"
    },
    textkecil_birumuda: {
        fontFamily: "Helvetica", 
        fontSize: 12, 
        color: "#1E90FF"
    },
    textsangatkecil_birumuda: {
        fontFamily: "Helvetica",
        fontSize: 10,
        color: "#1E90FF"
    },
    ////////////////////////////////////////
    textbesar_birumuda_tebal: {
        fontFamily: "Helvetica",
        fontSize: 22,
        color: "#1E90FF",
        fontWeight: "bold"
    },
    textsedang_birumuda_tebal: {
        fontFamily: "Helvetica",
        fontSize: 18,
        color: "#1E90FF",
        fontWeight: "bold"
    },
    textkecil_birumuda_tebal: {
        fontFamily: "Helvetica",
        fontSize: 12,
        color: "#1E90FF",
        fontWeight: "bold"
    },
    textsangatkecil_birumuda_tebal: {
        fontFamily: "Helvetica",
        fontSize: 10,
        color: "#1E90FF",
        fontWeight: "bold"
    },











    textbesar_merah: {
        fontFamily: "Helvetica", 
        fontSize: 22, 
        color: "#E34243"
    },
    textsedang_merah: {
        fontFamily: "Helvetica", 
        fontSize: 18, 
        color: "#E34243"
    },
    textkecil_merah: {
        fontFamily: "Helvetica", 
        fontSize: 12, 
        color: "#E34243"
    },
    textsangatkecil_merah: {
        fontFamily: "Helvetica", 
        fontSize: 10, 
        color: "#E34243"
    },
    ////////////////////////////////////////
    textbesar_merah_tebal: {
        fontFamily: "Helvetica",
        fontSize: 22,
        color: "#E34243",
        fontWeight: "bold"
    },
    textsedang_merah_tebal: {
        fontFamily: "Helvetica",
        fontSize: 18,
        color: "#E34243",
        fontWeight: "bold"
    },
    textkecil_merah_tebal: {
        fontFamily: "Helvetica",
        fontSize: 12,
        color: "#E34243",
        fontWeight: "bold"
    },
    textsangatkecil_merah_tebal: {
        fontFamily: "Helvetica", 
        fontSize: 10, 
        color: "#E34243",
        fontWeight: "bold"
    },
















    textbesar_abuabu: {
        fontFamily: "Helvetica", 
        fontSize: 22, 
        color: "#989898"
    },
    textsedang_abuabu: {
        fontFamily: "Helvetica", 
        fontSize: 18, 
        color: "#989898"
    },
    textkecil_abuabu: {
        fontFamily: "Helvetica", 
        fontSize: 12, 
        color: "#989898"
    },
    textsangatkecil_abuabu: {
        fontFamily: "Helvetica", 
        fontSize: 10, 
        color: "#989898"
    },
    ////////////////////////////////////////
    textbesar_abuabu_tebal: {
        fontFamily: "Helvetica",
        fontSize: 22,
        color: "#989898",
        fontWeight: "bold"
    },
    textsedang_abuabu_tebal: {
        fontFamily: "Helvetica",
        fontSize: 18,
        color: "#989898",
        fontWeight: "bold"
    },
    textkecil_abuabu_tebal: {
        fontFamily: "Helvetica",
        fontSize: 12,
        color: "#989898",
        fontWeight: "bold"
    },
    textsangatkecil_abuabu_tebal: {
        fontFamily: "Helvetica", 
        fontSize: 10, 
        color: "#989898",
        fontWeight: "bold"
    },















    button_biru_kecil: {
        height: 40, 
        backgroundColor: "#1E90FF",
        borderRadius: 15
    },
    button_biru_sedang: {
        height: 50, 
        backgroundColor: "#1E90FF",
        borderRadius: 15
    },
    button_biru_besar: {
        height: 60, 
        backgroundColor: "#1E90FF",
        borderRadius: 15
    },
    ////////////////////////////////////////
    button_merah_kecil: {
        height: 40, 
        backgroundColor: "#E34243",
        borderRadius: 15
    },
    button_merah_sedang: {
        height: 50, 
        backgroundColor: "#E34243",
        borderRadius: 15
    },
    button_merah_besar: {
        height: 60, 
        backgroundColor: "#E34243",
        borderRadius: 15
    },
    //////////////////////////////////////
    button_hijau_kecil: {
        height: 40, 
        backgroundColor: "#008000",
        borderRadius: 15
    },
    button_hijau_sedang: {
        height: 50, 
        backgroundColor: "#008000",
        borderRadius: 15
    },
    button_hijau_besar: {
        height: 60, 
        backgroundColor: "#008000",
        borderRadius: 15
    },
    //////////////////////////////////////
    button_disabled_kecil: {
        height: 40, 
        backgroundColor: "#DCDCDC",
        borderRadius: 15
    },
    button_disabled_sedang: {
        height: 50, 
        backgroundColor: "#DCDCDC",
        borderRadius: 15
    },
    button_disabled_besar: {
        height: 60, 
        backgroundColor: "#DCDCDC",
        borderRadius: 15
    },
    //////////////////////////////////////
    button_disabledredwhite_kecil: {
        height: 40, 
        backgroundColor: "white",
        borderWidth: 2,
        borderColor: "#E34243",
        borderRadius: 15
    },
    button_disabledredwhite_sedang: {
        height: 50, 
        backgroundColor: "white",
        borderWidth: 2,
        borderColor: "#E34243",
        borderRadius: 15
    },
    button_disabledredwhite_besar: {
        height: 60, 
        backgroundColor: "white",
        borderWidth: 2,
        borderColor: "#E34243",
        borderRadius: 15
    },


};
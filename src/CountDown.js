import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {id, en} from './string/text';
import {styles} from 'react-native-gifted-charts/src/BarChart/styles';

class Countdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count: props.durationInSeconds, // durasi countdown dalam detik
      bahasa: props.bahasa,
      onlyMinutes: props.onlyMinutes,
      isPaused: false,
      style: props.styles,
    };
  }
  componentDidMount() {
    this.startCountdown();
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  startCountdown = () => {
    this.interval = setInterval(() => {
      const {count, isPaused} = this.state;
      if (!isPaused) {
        if (count === 0) {
          clearInterval(this.interval);
          if (this.props.onFinish) {
            this.props.onFinish();
          }
        } else {
          this.setState(prevState => ({count: prevState.count - 1}));
        }
      }
    }, 1000);
  };

  pauseCountdown = () => {
    this.setState(prevState => ({isPaused: !prevState.isPaused}));
  };
  resetCountdown = () => {
    clearInterval(this.interval);
    this.setState(
      {count: this.props.durationInSeconds, isPaused: false},
      () => {
        this.startCountdown();
      },
    );
  };

  render() {
    const {count, bahasa, isPaused, style, onlyMinutes} = this.state;

    const seconds = count % 60;
    const minutes = Math.floor(count / 60);
    const strDetik = bahasa === 'id' ? id.detik : en.detik;
    const formattedMinutes = `${minutes.toString().padStart(2, '0')}:${seconds
      .toString()
      .padStart(2, '0')}`;
    const formattedSeconds = String(seconds).padStart(2, '0') + ' ' + strDetik;
    console.log(formattedSeconds);
    return (
      <View>
        <Text style={style}>
          {onlyMinutes ? formattedSeconds : formattedMinutes}
        </Text>
      </View>
    );
  }
}

export default Countdown;
